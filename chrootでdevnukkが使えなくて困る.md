Kobo の chroot 環境で debian を動かしたりしているのでたまにアップデートを行うのですが，実機だと遅いのでPC のchroot 環境で実行したりします
＃アーキテクチャの違いはqemu で吸収． > [ユーザーモードエミュレーションqemu を使って Kono on Debian のイメージを Host PC で apt-get とかする | matoken's meme](http://matoken.org/blog/blog/2014/01/22/usermode-qemu-kono-on-debian/ "ユーザーモードエミュレーションqemu を使って Kono on Debian のイメージを Host PC で apt-get とかする | matoken&apos;s meme")

そこで今回 `apt-get update` 中に以下のようなエラーが発生．

> 
/bin/sh: 1: cannot create /dev/null: Permission denied

`/dev/null` を確認するとぱっと見デバイスは問題なさそうだけど使えない．
> 
\# ls -la /dev/null
crw-rw-rw- 1 root root 1, 3 Sep  2 02:57 /dev/null
\# echo hoge > /dev/null
bash: /dev/null: Permission denied

エラーメッセージで検索するとkinnekoさんのページがヒット．

- [cannot create /dev/null: Permission denied - kinneko@転職先募集中の日記](http://d.hatena.ne.jp/kinneko/20130726/p4 "cannot create /dev/null: Permission denied - kinneko@転職先募集中の日記")
> 
調べてみると、chrootしたツリーのあるボリュームのマウントオプションにnodevが入っているのがダメのようだ。

あーなるほど!

chroot 元で確認してみると確かに `nodev` が．
> 
% mount |grep rootfs
/dev/sdc1 on /media/mk/rootfs type ext4 (rw,nosuid,nodev,relatime,data=ordered,uhelper=udisks2)

`dev` を付けて `remount` してみると 

> 
% sudo mount -o remount,dev /dev/sdc1
% mount |grep rootfs                 
/dev/sdc1 on /media/mk/rootfs type ext4 (rw,nosuid,relatime,data=ordered,uhelper=udisks2)

`chroot` しなおさなくても使えるようになった．毎回チェックしてるんですかね．
> 
\# echo hoge > /dev/null

てことで update 出来た :)
> 
\# apt-get install -f


<iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00H8X84E6" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe><iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00FOSXTVA" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe><iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00A71AKXE" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>
