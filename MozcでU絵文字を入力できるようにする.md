Twitter ではよく寿司が流れてきたりします．


- - -
<blockquote class="twitter-tweet" lang="ja"><p>🍣さん🍣奢ってくれ</p>&mdash; あひる (@ahiru3net) <a href="https://twitter.com/ahiru3net/status/507399760769085440">2014, 9月 4</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
<blockquote class="twitter-tweet" lang="ja"><p>今週末は恐らく🍣やで</p>&mdash; Shimada Hirofumi (@shimadah) <a href="https://twitter.com/shimadah/status/507399880252223488">2014, 9月 4</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

- - -

これ入力方法が解らなくてコピペしてたりしたんですが，偶然mozc で入力する方法を見つけたのでメモしておきます．

mozc-tool で Mozc プロパティ画面を開きます．ステータスバーから呼び出したり．
<a href="https://www.flickr.com/photos/119142834@N05/15209061051" title="Screenshot from 2014-09-12 01:55:11 by Kenichiro MATOHARA, on Flickr"><img src="https://farm6.staticflickr.com/5591/15209061051_d8d2767558_o.png" width="204" height="402" alt="Screenshot from 2014-09-12 01:55:11"></a>

コマンドラインから呼び出したりします．
> 
% /usr/lib/mozc/mozc_tool --mode=config_dialog

＃ちなみに mozc-tool の option については以下を．
- <a href="http://matoken.org/blog/blog/2014/03/03/mozc-tool-option/">mozc-tool option | matoken&apos;s meme</a>

Mozc プロパティ画面が起動したら「辞書」タブを選択し，「Unicode 6 絵文字変換」にチェックを付けて「適用」ボタンを押します．
<a href="https://www.flickr.com/photos/119142834@N05/15206086972" title="Screenshot from 2014-09-11 19:32:05 by Kenichiro MATOHARA, on Flickr"><img src="https://farm4.staticflickr.com/3861/15206086972_db1317a00b.jpg" width="500" height="452" alt="Screenshot from 2014-09-11 19:32:05"></a>

これで入力できるようになりました．
試しに「すし」で変換してみましょう．
<a href="https://www.flickr.com/pho．tos/119142834@N05/15212185885" title="susi by Kenichiro MATOHARA, on Flickr"><img src="https://farm6.staticflickr.com/5582/15212185885_e2845fd925_o.png" width="360" height="261" alt="susi"></a>
大抵フォントがないので豆腐になりますがそのままTwitter に流すと表示されます．

- - -
<blockquote class="twitter-tweet" lang="ja"><p>🍣</p>&mdash; (「ΦωΦ)「 (@matoken) <a href="https://twitter.com/matoken/status/509733710699040769">2014, 9月 10</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
- - -

どういうことだ?と思ったら `https://abs.twimg.com/emoji/v1/72x72/1f363.png` 
どうやらTweitter は絵文字を画像として用意してWeb で閲覧する分には困らないようにしてあるようです．ただし，ブラウザ上での検索が出来ないですね><
<img src="https://abs.twimg.com/emoji/v1/72x72/1f363.png">

ちなみにAndroid なんかにはフォントがあるらしく表示されます．

<a href="https://www.flickr.com/photos/119142834@N05/15025855917" title="12 - 1 by Kenichiro MATOHARA, on Flickr"><img src="https://farm4.staticflickr.com/3846/15025855917_5e82c133c4_o.png" width="720" height="382" alt="12 - 1"></a>

でも絵柄が違いますね．マグロと巻き寿司，マグロと卵．

