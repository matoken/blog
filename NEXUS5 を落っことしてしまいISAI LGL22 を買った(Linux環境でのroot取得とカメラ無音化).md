Nexus5 をどこかに落っことしてしまい(体調が悪い時で記憶があやふや．桜島港から家の間のどこかに多分落ちている)不便なので代替機を探して中古のISAI LGL22 を入手した．兄弟機で面白みはあまりないが，LTE でmineo が使えるのとUnLock が安価&安易でDocomo の電波も掴みそこそこ安いということで…．

とりあえず初期化してroot 取ってカメラ無音化までした．root 取得の手順は確立されているけれどWindows向けでバッチファイルを叩くものしか見当たらなかったのでLinux 向けにメモ．（WinでもMacでも同じだけど）

- 初期化
- OS Update
TAO で 4.4.2 に．
- 画面のロックをパスワードに
- 所有者情報設定
- mineo 設定
- 暗号化
※充電が80%以上になってから出ないと実行できない．
- root取得
isai_rootkit_kk_only.rar を以下から入手して利用した．https://mega.co.nz/#!h5EAmY7K!rrM2jZlCnRGc-EzhPhrxQ8gfPtGAcuv_mPPH8lYo7_M
```
sha1sum	0a048c7be86ca64dc755a31a3c3a8d32455a3361
sha256sum	67b3481f15e5f597e60397b9294074354877cd283ecede5bd8ba171e14392cd2
```
前準備としてUSBデバッグを有効にしてPCと接続しUSBデバッグを有効にしておく．
isai_rootkit_kk_only.rar を展開して install.bat を見ながら以下のような感じでコマンドを叩く．
※要開発環境．
```sh
cd isai_rootkit_kk_only
adb wait-for-device
adb push "files" /data/local/tmp
adb shell chmod 755 /data/local/tmp/getroot
adb shell chmod 755 /data/local/tmp/busybox_file
adb shell chmod 755 /data/local/tmp/install.sh
adb shell chmod 755 /data/local/tmp/remove_apps_and_bins.sh
adb pull /system/app/LGDMSClient.odex ./backup/system/app/LGDMSClient.odex
adb pull /system/app/LGDMSClient.apk ./backup/system/app/LGDMSClient.apk
adb pull /system/bin/subsystem_ramdump ./backup/system/bin/subsystem_ramdump
adb pull /system/bin/dumpstate ./backup/system/bin/dumpstate
adb pull /system/bin/fssamond ./backup/system/bin/fssamond
adb shell "/data/local/tmp/getroot /data/local/tmp/remove_apps_and_bins.sh"
adb wait-for-device
adb shell "/data/local/tmp/getroot /data/local/tmp/install.sh"
adb wait-for-device
adb shell "rm /data/local/tmp/getroot"
adb shell "rm /data/local/tmp/busybox_file"
adb shell "rm /data/local/tmp/eu.chainfire.supersu-193.apk"
adb shell "rm /data/local/tmp/remove_apps_and_bins.sh"
adb shell "rm /data/local/tmp/install.sh"
adb shell "rm -rf /data/local/tmp/system"
```

- カメラ無音化
/system/media/audio/ui/camera_click.ogg と /system/media/audio/ui/VideoRecord.ogg を退避する．
```sh
adb shell
su
mount -o remount,rw /system
cd /system/media/audio/ui/
mv camera_click.ogg camera_click.ogg-
mv VideoRecord.ogg VideoRecord.ogg-
cd
sync
mount -o remount,ro /system
exit
exit
```
＃起動音と起動後のパスフレーズ入力時のキークリック音も消したいが見当たらず．

- アプリ入れたり
 - [Androidデバイスマネージャー - Google Play の Android アプリ](https://play.google.com/store/apps/details?id=com.google.android.apps.adm)
 落としたりした時に探したりデータ消したり出来るけど，今回のNexus5はバッテリ切れで探せなかった…．てことでやっぱ暗号化fs大事
 - [OsmAnd+ Maps & Navigation - Google Play の Android アプリ](https://play.google.com/store/apps/details?id=net.osmand.plus)
 - [Ingress - Google Play の Android アプリ](https://play.google.com/store/apps/details?id=com.nianticproject.ingress)
 - [Googleカメラ - Google Play の Android アプリ](https://play.google.com/store/apps/details?id=com.google.android.GoogleCamera)
 - [Google+ - Google Play の Android アプリ](https://play.google.com/store/apps/details?id=com.google.android.apps.plus)
 - [Picasa (Google+ photo) の完全なツール - Google Play の Android アプリ](https://play.google.com/store/apps/details?id=larry.zou.colorfullife)
 ：
主に使うアプリはだいたい入ったかな…．

