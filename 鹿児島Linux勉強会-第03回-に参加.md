7/25(金)に鹿児島らぐの鹿児島Linux勉強会に行ってきました．
- [鹿児島Linux勉強会 : ATND](https://atnd.org/events/53546 "鹿児島Linux勉強会 : ATND")

今回は鹿児島中央駅前のよかプラザでの開催でした．
<a href="https://www.flickr.com/photos/119142834@N05/14592578100" title="IMG_20140725_173144 by Kenichiro MATOHARA, on Flickr"><img src="https://farm4.staticflickr.com/3836/14592578100_ceffbc7ff4_n.jpg" width="320" height="240" alt="IMG_20140725_173144"></a>
＃カタカナでリナックスというのを見るとこないだのLinuxConのスタッフTシャツを思い出します :)

今回はATND での集まりが悪くもしかしたら2人での開催になりそうと思っていたのですが，始まってみると6人でした．
今回は前半はLinux読書会でLinux標準教科書の輪読，後半は発表で私は以下のよな発表を行いました．
- <iframe src="//www.slideshare.net/slideshow/embed_code/37374028" width="427" height="356" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px 1px 0; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="https://www.slideshare.net/matoken/pipelight-silverlight-raspberrypi-3" title="Pipelight でSilverlightを / RaspberryPi でリアルタイム動画配信をその3" target="_blank">Pipelight でSilverlightを / RaspberryPi でリアルタイム動画配信をその3</a> </strong> from <strong><a href="http://www.slideshare.net/matoken" target="_blank">Kenichiro Matohara</a></strong> </div>
href="http://www.slideshare.net/YoshimotoYukiyoshi" target="_blank">Yoshimoto Yukiyoshi</a></strong> </div>
このRaspberryPi 配信を実際にやりたかったのですが，携帯電話でのテザリングがうまく行かなくて挫折．テスト時は7時間ほど配信して問題無さそうでした．マイクがちゃんと音拾うかとかを試したかったのですが…．

その他Vimネタ，Seti@homeネタが披露されました．
- <iframe src="//www.slideshare.net/slideshow/embed_code/37345781" width="427" height="356" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px 1px 0; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="https://www.slideshare.net/YoshimotoYukiyoshi/setihome-on-debian-20140725" title="Seti@home on debian 20140725" target="_blank">Seti@home on debian 20140725</a> </strong> from <strong><a href="http://www.slideshare.net/YoshimotoYukiyoshi" target="_blank">Yoshimoto Yukiyoshi</a></strong> </div>

自分の発表は最後にしてもらって(スライド完成してなかったorz)
終了時間見ながら発表したのですが，共同購入をしていたRaspberryPi B+ の受渡し時間を考慮してなくて終了後適当なベンチで受け渡しとなってしまいましたorz


<a href="https://www.flickr.com/photos/119142834@N05/14760451704" title="IMG_20140723_233409 by Kenichiro MATOHARA, on Flickr"><img src="https://farm4.staticflickr.com/3861/14760451704_c608618bb1_n.jpg" width="320" height="240" alt="IMG_20140723_233409"></a>

終了後は有志(4人)で魚とか食べて

<a href="https://www.flickr.com/photos/119142834@N05/14592626809" title="IMG_20140725_211837 by Kenichiro MATOHARA, on Flickr"><img src="https://farm4.staticflickr.com/3906/14592626809_81f7f1b48a_m.jpg" width="240" height="180" alt="IMG_20140725_211837"></a><a href="https://www.flickr.com/photos/119142834@N05/14776932944" title="IMG_20140725_213943 by Kenichiro MATOHARA, on Flickr"><img src="https://farm3.staticflickr.com/2907/14776932944_62448ae31d_m.jpg" width="240" height="180" alt="IMG_20140725_213943"></a><a href="https://www.flickr.com/photos/119142834@N05/14592621519" title="IMG_20140725_215719 by Kenichiro MATOHARA, on Flickr"><img src="https://farm4.staticflickr.com/3857/14592621519_3eb5514c27_m.jpg" width="240" height="180" alt="IMG_20140725_215719"></a><a href="https://www.flickr.com/photos/119142834@N05/14756279986" title="IMG_20140725_215823 by Kenichiro MATOHARA, on Flickr"><img src="https://farm6.staticflickr.com/5556/14756279986_04be86c725_m.jpg" width="240" height="180" alt="IMG_20140725_215823"></a><a href="https://www.flickr.com/photos/119142834@N05/14778948612" title="IMG_20140726_002543 by Kenichiro MATOHARA, on Flickr"><img src="https://farm3.staticflickr.com/2920/14778948612_ed4526436b_m.jpg" width="240" height="180" alt="IMG_20140726_002543"></a>

その後漫画喫茶に泊まりました．公共交通機関欲しいです
＃この日は公共交通機関があっても乗れなかった気はしますが><

<a href="https://www.flickr.com/photos/119142834@N05/14592778997" title="IMG_20140726_031440 by Kenichiro MATOHARA, on Flickr"><img src="https://farm3.staticflickr.com/2919/14592778997_f8ef1fb56e_m.jpg" width="240" height="180" alt="IMG_20140726_031440"></a><a href="https://www.flickr.com/photos/119142834@N05/14756275246" title="5vD2ZXwC---1 by Kenichiro MATOHARA, on Flickr"><img src="https://farm3.staticflickr.com/2928/14756275246_1494e13aa8_m.jpg" width="240" height="180" alt="5vD2ZXwC---1"></a>