# Wardriving?
　Wardrivingはドライブしながら位置情報とWi-Fiの電波を拾うことでどこにどんなWi-Fi APがあるかを記録することを言います(多分)．これまで初期の頃はThinkPad 240Z でNetworkStumbler やKismet を動かしながら自転車で走り回ったり，iPad の脱獄アプリで記録したり，最近ではRaspberryPi でKismet を動かして保存していました．

- [war cycling | Matoken's meme](http://maty.jugem.jp/?eid=86)
- [Raspberry_Pi/kismet を動かしてコミケのESSID を記録してみる(WAR Driving)](http://hpv.cc/~maty/pukiwiki1/index.php?Raspberry_Pi#t960593d)
- [今日はRaspberry Pi でkismet を動かしてAP を拾ってみていた． AP の数12000件以上，SSID…](https://plus.google.com/+KenichiroMATOHARA/posts/a7mUXDzHVa6)
- [AP Radar - matoken's wiki.](http://hpv.cc/~maty/pukiwiki1/index.php?AP%20Radar)

#何が楽しいの?
　コミックマーケットではアクセスポイント名にネタを仕込んだネタAP がよく飛んでいるのでこれを見るのが楽しみです．今回RaspberryPi で設定を行い動かしていましたが，動作確認もしていなかったのでサブにAndroid のアプリがないか探したらWardrive というアプリケーションを見つけました．
　WiGlE( https://wigle.net/ )向けのアプリケーションで，WiGlE のアカウントを取得してアップロードすることも出来ます．アカウントを設定しなければアップロードされませんしデータの利用も可能です．
　実用的な使い方としてはログから契約している公共無線LANサービスのエリアを探すと言ったことが可能です．例えば https://wigle.net/ のmapページに移動して調べたい緯度経度に移動した後右側のSSIDに調べたいSSIDを入力して[Filter]ボタンを押すとWigleにアップロードされたデータの中から検索した結果が表示されます．

# Wardrive の使い方
　利用方法はとても簡単で Wardrive を導入して起動した後 *[START] SCANNING* ボタンを押すと記録が始まり，GoogleMap を利用した地図上にアクセスポイント名がポイントされていきます．メニューの *Export* からto KML (Google Earth) を選ぶと端末のルートディレクトリに `wardrive.kml` として保存されます．
Wigle のアカウントを持っている場合は設定からアカウントとパスワードを保存したうえで，*Export* からto KML for WiGlE(and Uploa it)* を押すとKML ファイルの作成とWiGlE へのアップロードが行われます．

<a href="https://www.flickr.com/photos/119142834@N05/16277581610" title="Screenshot_2015-02-08-00-51-00 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7314/16277581610_09386dcbac.jpg" width="281" height="500" alt="Screenshot_2015-02-08-00-51-00"></a>

　保存は KML 形式で行われてGoogleEarth などのアプリケーションで閲覧できます．アクセスポイント名を眺めたいだけであれば以下のようにしてAP名だけを抜き出すといいでしょう．

```sh
% grep name wardrive.kml | cut -c 18- | cut -d']' -f1| sort -n | uniq | lv
```

WiGLE を見に行ったらWardrive は公式アプリではないようです．
- [WiGLE Tools](https://wigle.net/tools)
- [Wigle Wifi Wardriving - Google Play の Android アプリ](https://play.google.com/store/apps/details?id=net.wigle.wigleandroid)

Wigle Wifi Wardriving が公式のようです．こっちだと設定でOSMのタイルマップが使えそうなのですがそれらしい設定の `Use WiGLE OSM tiles` にチェックを付けてアプリを再起動しても反映されません…．

<a href="https://www.flickr.com/photos/119142834@N05/16463699631" title="Screenshot_2015-02-08-02-16-06 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7365/16463699631_038a0e9404.jpg" width="281" height="500" alt="Screenshot_2015-02-08-02-16-06"></a><a href="https://www.flickr.com/photos/119142834@N05/16463272791" title="Screenshot_2015-02-08-00-53-17 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7335/16463272791_3dd84aab2e.jpg" width="281" height="500" alt="Screenshot_2015-02-08-00-53-17"></a><a href="https://www.flickr.com/photos/119142834@N05/16439441136" title="Screenshot_2015-02-08-02-16-14 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7311/16439441136_f8d72ee4dd.jpg" width="281" height="500" alt="Screenshot_2015-02-08-02-16-14"></a><a href="https://www.flickr.com/photos/119142834@N05/16465436155" title="Screenshot_2015-02-08-02-16-24 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7355/16465436155_37fc274fa0.jpg" width="281" height="500" alt="Screenshot_2015-02-08-02-16-24"></a><a href="https://www.flickr.com/photos/119142834@N05/16279552267" title="Screenshot_2015-02-08-02-16-42 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7386/16279552267_09308e4990.jpg" width="281" height="500" alt="Screenshot_2015-02-08-02-16-42"></a>

　さて，肝心のネタAPですが，感度が悪いのかしきい値が厳しいのか去年よりも大分少なかったです．でも利用方法はすごくお手軽なのでおすすめです．次はRaspberry Pi を携帯電話のリチウムイオンバッテリーで動かしてコンパクトにパッケージングして運用できないか試してみようと思っています．

<div>
<iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=4864940029" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe> <iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00RQGHDSS" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe> <iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00K68QQV8" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe> <iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00EP8MEU2" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>

</div>