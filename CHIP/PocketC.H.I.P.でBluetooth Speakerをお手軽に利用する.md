# PocketC.H.I.P.でBluetooth Speakerをお手軽に利用する

公式フォーラムの以下の記事を真似てみました．

* [Connecting Bluetooth Speaker to PocketChip - PocketC.H.I.P. - Next Thing Co. - Bulletin Board System](https://bbs.nextthing.co/t/connecting-bluetooth-speaker-to-pocketchip/7180 "Connecting Bluetooth Speaker to PocketChip - PocketC.H.I.P. - Next Thing Co. - Bulletin Board System")

## パッケージの更新と必要パッケージの導入

```
$ sudo apt update && sudo apt upgrade -y && sudo apt install -y bluez-tools pulseaudio-module-bluetooth 
```

## ボリュームコントロールに手の入ったpocket-homeの入手

```
$ wget https://bogdziewicz.xyz/files/pchip/pocket-home
```

## pocket-homeの入れ替え

```
$ killall pocket-home
$ sudo cp -p /usr/bin/pocket-home /usr/bin/pocket-home.org
$ sudo cp pocket-home /usr/bin/pocket-home
$ pocket-home
```

## Bluetooth接続用scriptの入手

```
$ wget https://bogdziewicz.xyz/files/pchip/bt.py
```

## Bluetooth SpeakerのMACアドレスを調べる

Bluetooth Speakerが起動した状態で行う．
以下の例では`00:00:00:00:01:70`がMACアドレス．
表示されたらCtrl+Cで終了する

```
$ bt-adapter -d
Searching...
[00:00:00:00:01:70]
  Name: SPTR01
  Alias: SPTR01
  Address: 00:00:00:00:01:70
  Icon: audio-card
  Class: 0x240404
  LegacyPairing: 0
  Paired: 0
  RSSI: -48

^C
```

## Bluetooth接続用scriptの修正

3行目の`mac = 'PASTE_HERE_MAC'`の`PASTE_HERE_MAC`を上で調べたMACアドレスに書き換えて保存する．

```
$ vi bt.py
```

* old

```
mac = 'PASTE_HERE_MAC'
```

* new

```
mac = '00:00:00:00:01:70'
```

## Bluetooth接続用scriptを実行して接続

```
$ python ./bt.py 

(bluetoothctl:2181): GLib-CRITICAL **: Source ID 26 was not found when attempting to remove it
Success
100%

```

これで接続されて利用できる．
次回からは`$ python ./bt.py `だけでOK．

ただ，手元の機器では遅延が気になるのでゲームには向かない．映像も同様．音楽やPodcastなどに良さそう．
(PCやスマホとの組み合わせでも遅延は起こるのでスピーカー側の問題だと思う)
