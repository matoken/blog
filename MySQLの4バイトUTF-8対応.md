

- MySQL 5.5.3 より前は3バイトまでの対応
- MySQL 5.5.3 以降で4バイトに対応Unicode6 の絵文字や「𩸽(ほっけ)」とかもOKに
- 但し，dbの文字コードはUTF-8のままではダメでdbの文字コードをutf8mb4 にする

> 
Debian Squeeze > 5.1.73-1
Debian Wheezy > 5.5.38-0+wheezy1

> 
RHEL-6.5 > 5.1.71
RHEL-7.0 > 5.6.20

- ++"DistroWatch.com: Red Hat Enterprise Linux" http://distrowatch.com/table.php?distribution=redhat++


```
Incompatible Change: The Unicode implementation has been extended to provide support for supplementary characters that lie outside the Basic Multilingual Plane (BMP). Noteworthy features:

utf16 and utf32 character sets have been added. These correspond to the UTF-16 and UTF-32 encodings of the Unicode character set, and they both support supplementary characters.

The utf8mb4 character set has been added. This is similar to utf8, but its encoding allows up to four bytes per character to enable support for supplementary characters.

The ucs2 character set is essentially unchanged except for the inclusion of some newer BMP characters.

In most respects, upgrading to MySQL 5.5 should present few problems with regard to Unicode usage, although there are some potential areas of incompatibility. These are the primary areas of concern:

For the variable-length character data types (VARCHAR and the TEXT types), the maximum length in characters is less for utf8mb4 columns than for utf8 columns.

For all character data types (CHAR, VARCHAR, and the TEXT types), the maximum number of characters that can be indexed is less for utf8mb4 columns than for utf8 columns.

Consequently, if you want to upgrade tables from utf8 to utf8mb4 to take advantage of supplementary-character support, it may be necessary to change some column or index definitions.
```
- ++"MySQL :: MySQL 5.5 Release Notes :: Changes in MySQL 5.5.3 (2010-03-24, Milestone 3)" http://dev.mysql.com/doc/relnotes/mysql/5.5/en/news-5-5-3.html++



db 作成時．
> 
$ tail -3 ~/wp.sql
alter database wp character set utf8mb4;
FLUSH PRIVILEGES;

wp設定
> 
$ tail -3 /etc/wordpress/config-org.php
define('DB_CHARSET', 'utf8mb4');
define('DB_COLLATE', 'utf8mb4_general_ci');
?>
