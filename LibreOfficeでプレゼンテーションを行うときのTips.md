この記事は[LibreOffice Advent Calendar 2014 - Adventar](http://www.adventar.org/calendars/507)の12/22の記事です．
昨日の記事は小笠原さんの[関東LibreOfficeオフラインミーティングとはなにか - なるひこの Linux Printing お勉強日記](http://d.hatena.ne.jp/naruoga/20141221/1419175134)でした．

LibreOfficeいつも便利に利用させてもらっています．私は最近Impressでスライドを作って勉強会などで発表するのによく使っています．最近気付いたImpress のTips などを書いてみます．

## スライドショー中断した後続きからスライドを開始する

F5 key を押すことでスライドショーが出来ますが，別の資料を見てもらったり実演などで一旦スライドを中断することがあります．その後スライドを再開しようとして再度F5 Key を押すと1枚目のスライドに戻ってしまいます．結構最近まで → Key を連打して元のページに戻っていたのですが，これじゃいけないなと調べてみたところ **shift + F5** で中断した場所から再開できることがわかりました．これによりスムーズに発表が出来るようになりました :)

## スライドに挿入した画像の解像度変更

先日画像を多用したスライドを作ったのですが，50pちょっとのスライドなのに270MBほどの容量になってしまいました．
<a href="https://www.flickr.com/photos/119142834@N05/15894072868" title="Screenshot from 2014-12-23 03:43:00 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7502/15894072868_cd4d7cddd6_n.jpg" width="320" height="173" alt="Screenshot from 2014-12-23 03:43:00"></a><a href="https://www.flickr.com/photos/119142834@N05/16079576651" title="Screenshot from 2014-12-23 03:41:39 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7578/16079576651_348675026d_o.png" width="82" height="115" alt="Screenshot from 2014-12-23 03:41:39"></a>
これはデジカメの画像を縮小せずにそのまま貼り付けたせいでこうなりました．前もって縮小しておいたらいいのですが貼り付けた後に縮小したものに差し替えるのは面倒です．こういう時に画像の上で右クリックを押し，「画像の圧縮(S)」から解像度の変更が可能です．以下の例では 10MB超から163KB になっています．

<a href="https://www.flickr.com/photos/119142834@N05/15895394099" title="Screenshot from 2014-12-23 03:30:01 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7556/15895394099_b603197d0a_n.jpg" width="155" height="320" alt="Screenshot from 2014-12-23 03:30:01"></a><a href="https://www.flickr.com/photos/119142834@N05/15459177564" title="Screenshot from 2014-12-23 03:31:07 by Kenichiro MATOHARA, on Flickr"><img src="https://farm9.staticflickr.com/8633/15459177564_5ff2aa8287_n.jpg" width="214" height="320" alt="Screenshot from 2014-12-23 03:31:07"></a>


1,2枚ならこの方法でいいのですが，沢山の画像がある場合は面倒です．一括で変更したい場合は「ツール(T)」の「プレゼンテーションの軽量化(P)」から一括で解像度変更が可能です．一気に9.5MBになりました．

<a href="https://www.flickr.com/photos/119142834@N05/15894200330" title="Screenshot from 2014-12-23 03:36:32 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7572/15894200330_7272048e3e_o.jpg" width="236" height="303" alt="Screenshot from 2014-12-23 03:36:32"></a><a href="https://www.flickr.com/photos/119142834@N05/16079562781" title="Screenshot from 2014-12-23 03:37:41 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7557/16079562781_234df9fce1_n.jpg" width="320" height="197" alt="Screenshot from 2014-12-23 03:37:41"></a><a href="https://www.flickr.com/photos/119142834@N05/16055701586" title="Screenshot from 2014-12-23 03:41:03 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7568/16055701586_d26af2a534_n.jpg" width="320" height="116" alt="Screenshot from 2014-12-23 03:41:03"></a><a href="https://www.flickr.com/photos/119142834@N05/16079576671" title="Screenshot from 2014-12-23 03:41:48 by Kenichiro MATOHARA, on Flickr"><img src="https://farm9.staticflickr.com/8679/16079576671_8f8c71d3ea_o.png" width="111" height="122" alt="Screenshot from 2014-12-23 03:41:48"></a>

# pdfやhtml形式でプレゼンテーションをする

Libreofficeが導入されてない環境や，リソースが少ない端末でプレゼンテーションをする場合でもpdfやhtml形式への書き出しを行ってプレゼンテーションを行うことが可能です．例えばRaspberryPiはメモリなどのリソースが少ないためImpressは重いのですが，pdfやhtml形式だと比較的軽いので快適にプレゼンが可能でした．

pdf への書き出しは「ファイル(F)」「PDF としてエクスポート」から行えます．この時「OpenDocumentファイルを埋め込む(B)」にチェックを付けておくと後でImpressで編集を行うことが出来るpdfファイルになります．SlideShareなどで共有する場合などにも便利です．
<a href="https://www.flickr.com/photos/119142834@N05/15895915777" title="Screenshot from 2014-12-23 04:00:26 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7505/15895915777_fe8a9d6898_n.jpg" width="320" height="180" alt="Screenshot from 2014-12-23 04:00:26"></a>

html への書き出しは「ファイル(F)」「エクスポート(T)」から「ファイルの種類（T)」から「HTML ドキュメント (Impress)	html,htm」を選択することで可能です．エクスポートしたフォルダの中に画像形式のスライドとhtmlファイルが生成されます．「スライド名.html」というファイルをブラウザで開くとプレゼンテーションが可能です．また，画像ファイルだけを使って画像ビュワーでプレゼンテーションをすることも可能です．

<a href="https://www.flickr.com/photos/119142834@N05/16055937666" title="Screenshot from 2014-12-23 04:11:53 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7527/16055937666_63dc033b7b_n.jpg" width="320" height="95" alt="Screenshot from 2014-12-23 04:11:53"></a><a href="https://www.flickr.com/photos/119142834@N05/15462069553" title="Screenshot from 2014-12-23 04:14:18 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7495/15462069553_4624fb3ac2_n.jpg" width="320" height="207" alt="Screenshot from 2014-12-23 04:14:18"></a>


----

明日の[LibreOffice Advent Calendar 2014 - Adventar](http://www.adventar.org/calendars/507) は [souichirho.ishikawa](http://www.adventar.org/users/4446)さんです．よろしくお願いします．


----

