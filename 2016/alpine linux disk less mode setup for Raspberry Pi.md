fat形式でmicroSD Cardを初期化します．

```
$ sudo fdisk /dev/sdb
Command (m for help): n
Partition number (1-12, default 1):
First sector (34-31422430, default 2048):
Last sector, +sectors or +size{K,M,G,T,P} (2048-31422430, default 31422430):

Created a new partition 1 of type 'Linux filesystem' and of size 15 GiB.

Command (m for help): t
Selected partition 1

Device     Start      End  Sectors Size Type
/dev/sdb1   2048 31422430 31420383  15G Linux filesystem

Hex code (type L to list all codes): 12
Changed type of partition 'Linux filesystem' to 'Microsoft LDM metadata'.

Command (m for help): p
Disk /dev/sdb: 15 GiB, 16088301568 bytes, 31422464 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: 98101B32-BBE2-4BF2-A06E-2BB33D000C20

Device     Start      End  Sectors Size Type
/dev/sdb1   2048 31422430 31420383  15G Microsoft LDM metadata

Command (m for help): w
The partition table has been altered.
Calling ioctl() to re-read partition table.
Syncing disks.
$ sudo mkfs.vfat -n alpine /dev/sdb1
```

適当な場所にマウントしてmicroSD Cardに展開します．

```
$ sudo mount -t vfat -o uid=`id -u` /dev/sdb1 /mnt
$ tar xvf ./alpine-rpi-3.3.1-armhf.rpi.tar.gz -C /mnt
$ sudo umount /mnt
$ sync
```

microSD を取り出してRaspberry Piに挿入して起動します．
serialは無効なのでUSBキーボードとモニタも必要です．

起動したら
root:nopasswordでloginできます．

alpine-setupで初期設定をします．

```
# alpine-setup
```

対話式に答えていくと設定が出来ます．

キーボードレイアウトの設定．

```
Select keyboad layout [none]: us
Available variants: us-acentos us
Select variant []: us
```

ホスト名の設定．

```
Enter system hostname (shirt from, e.g 'foo') [localhost]: alpine-pi
```

ネットワークの設定

```
Available interface are: eth0.
Enter '?' for help on bridges, bonding and vlans.
Which one do you want to initialize? (or '?' or 'done') [eth0]
Ip address for eth0? (or 'dhcp'. 'none', '?') [dhcp]
Do you want to do any manuak network configuration? [no]
```

rootパスワード設定

```
Changing password for root
New password:
Retype password:
Passrord for root changed by root
```

タイムゾーンの設定

```
Which timezone are you in? ('?' for list) [UTC] Tokyo
```

プロクシの設定

```
HTTP/FTP proxy URL? (e.g. 'http://proxy:8080', or'none') [none]
```

パッケージのミラーサイトの設定

```
Avallable mirrors:
Enter mirrir number (1-17) or URL to add (or r/f/e/done) [f]: r
```

sshdの設定

```
Which SSH server? ('openssh', 'dropbear' or 'none') [openssh]
```

ntpdの設定

```
Which NTP client to run? ('buisybox', 'openntpd', 'chrony' or 'none') [chrony]
```

データの保存先の設定

```
Enter where to store configs ('floppy', 'mmcblk0', 'usb' or 'none') mmcblk0]:
Enter apk cache directory (or  '?', 'none') [/mendia/mmcblk0/cache]:
```

設定の保存

```
# lbu commit
# reboot
```

パッケージを最新にして保存

```
# apk update
# apk upgrade
# lbu commit
# reboot
```

パッケージの検索と導入例

```
# apk search sudo
sudo-doc-1.8.15-r1
sudo-1.8.15-r1
sudo-dev-1.8.15-r1
# apk add sudo
```

Xの導入

```
# setup-xorg-base
# apk add xf86-video-fbdev xf86-input-mouse xf86-input-keyboard dbus setxkbmap
# rc-update add dbus
# cat <<__EOF__>>/etc/X11/xorg.conf.d/20-modules.conf
> Section "Module"
>     Load "fbdevhw"
>     Load "fb"
>     Load "shadow"
>     Load "shadowfb"
>     Load "dbe"
>     Disable "glx"
>     Disable "dri"
> EndSection
> __EOF__
```

リポジトリを追加
