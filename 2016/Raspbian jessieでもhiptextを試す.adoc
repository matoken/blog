= Raspbian jessieでもhiptextを試す

さっき

* http://matoken.org/blog/blog/2016/06/01/try-hiptext/[画像や動画をtextに変換するhiptextを試す | matoken's meme]

というのを書きましたが，なんとなくRaspberry Pi B+ の Raspbian jessie lite でも`hiptext`を試してみました．

----
$ sudo apt install build-essential libpng12-dev libjpeg-dev     libfreetype6-dev libgif-dev ragel libavformat-dev libavcodec-dev     libswscale-dev libgflags-dev libgoogle-glog-dev git
$ git clone https://github.com/jart/hiptext.git
$ cd hiptext
$ time make
   :
real    22m7.443s
user    21m53.190s
sys     0m8.910s
----

て感じでほぼREADMEのままmake一発でした．違ったのは導入パッケージに`git`が必要だったくらいです．
思いの外makeに時間が掛かりましたがRaspberry Pi B+で試しているので2Bや3Bだともっと速いでしょう．
Balls.pngやObama.jpgはうまく行き，さっきうまく行かなかったTux.pngは同じようにコケたので同じ動きのようです．

恐らく Debian jessie でも同様だと思います．

https://www.flickr.com/photos/119142834@N05/27293516472/in/dateposted/[image:https://c1.staticflickr.com/8/7596/27293516472_deedbe97a4.jpg[]]
