# コマンドラインで動作する絵文字検索ソフトウェアのemoj

PCで絵文字を入力するとき日本語入力システムがあればそれで入力できますが，英語圏では恐らく無理．てことでいくつかツールがあるのですが，コマンドラインで動作する`emoj`を見つけたので試してみました．

- [GitHub \- sindresorhus/emoj: Find relevant emoji from text on the command\-line](https://github.com/sindresorhus/emoj)

導入は`npm`か，

```
npm install emoj
```

`snap`で．

```
$ sudo snap install emoj
```

後は`emoj`コマンドに引数を与えるか，`emoj`コマンドを引数無しで実行してライブモードで利用します．
引数やライブモードで入力する文字列は表示したい絵文字に関連する単語を入力します．ライブモードはEnterでクリアされ，`Ctrl + c`でexitできます．表示された絵文字をコピー&ペーストして利用するようです．1つ目に出てくる絵文字をクリップボードにクリップする`--copy/-c`というオプションもあるようですが手元ではうまく動いていません．

```
$ emoj neko
🐱  👥  ↪  😻  ↔  🐈  🐯
$ emoj cat
🐱  🐈  😺  😻  😸  😼  😽
$ emoj

› sushi
🍣  🍱  🍙  🍤  😋  🍚  😍

```

[![asciicast](https://asciinema.org/a/95114.png)](https://asciinema.org/a/95114)


絵文字のフォントがない場合はNoto Fontsを利用するのが手っ取り早いと思います．

- [Google Noto Fonts](https://www.google.com/get/noto/#emoji-zsye-color)




日本語入力システムを使う人やGUIを利用する人は日本語入力システムやemojione-pickerを使ったほうが便利かもしれません．

- [絵文字を入力する\-Mozc&emojione\-picker\- \| matoken's meme](http://matoken.org/blog/blog/2016/02/17/to-enter-the-emoticons-mozce-and-mojione-picker/)

