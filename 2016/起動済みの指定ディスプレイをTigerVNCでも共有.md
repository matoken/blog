<!--
起動済みの指定ディスプレイをTigerVNCでも共有
-->

通常はVNCは新しいXを起動して使うことが多いと思いますが，ヘッドレス環境や入力デバイスの貧弱な環境デモの際など既に起動済みのXの画面を利用したいことがあります．（例えばKobo）

<!-- Place this tag in your head or just before your close body tag. -->
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<!-- Place this tag where you want the widget to render. -->
<div class="g-post" data-href="https://plus.google.com/+KenichiroMATOHARA/posts/NpudEqxo9PU"></div>

<!--
こんな感じで展示してみてる．やはりkobo/RaspberryPiの話がメインに… #osc14fk   #kagolug  
-->

昔はvinoを使っていましたが，設定画面がGnomeに統合されてGnome環境以外で利用するのが面倒になってしまいました．

<!-- Place this tag in your head or just before your close body tag. -->
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<!-- Place this tag where you want the widget to render. -->
<div class="g-post" data-href="https://plus.google.com/+KenichiroMATOHARA/posts/ZdVRei6iXsZ"></div>

<!--
vino の設定方法が変わった?以前はvino-preference というコマンドがあったと思うのだけどdebian jessie/stretch には見当たらない

% dpkg -L vino|grep -v /locale/
/.
/usr
/usr/share
/usr/share/doc
/usr/share/doc/vino
/usr/share/doc/vino/changelog.Debian.gz
/usr/share/doc/vino/changelog.gz
/usr/share/doc/vino/NEWS.gz
/usr/share/doc/vino/AUTHORS
/usr/share/doc/vino/README
/usr/share/doc/vino/copyright
/usr/share/glib-2.0
/usr/share/glib-2.0/schemas
/usr/share/glib-2.0/schemas/org.gnome.Vino.enums.xml
/usr/share/glib-2.0/schemas/org.gnome.Vino.gschema.xml
/usr/share/locale
/usr/share/dbus-1
/usr/share/dbus-1/services
/usr/share/dbus-1/services/org.freedesktop.Telepathy.Client.Vino.service
/usr/share/applications
/usr/share/applications/vino-server.desktop
/usr/share/telepathy
/usr/share/telepathy/clients
/usr/share/telepathy/clients/Vino.client
/usr/lib
/usr/lib/vino
/usr/lib/vino/vino-server

別パッケージになったわけでもなさそう
% apt-cache search vino-preference

/usr/share/doc/vino/NEWS.gz から 3.9.2 で消えた?
David King (4):
      Update NEWS for 3.9.2 release
      Post-release version bump to 3.9.1
      Back out "authentication-method" setting change
      Remove preferences dialog, bug 700070

GSettings でほげることは可能みたい
% gsettings list-recursively org.gnome.Vino
org.gnome.Vino notify-on-connect true
org.gnome.Vino alternative-port uint16 5900
org.gnome.Vino disable-background false
org.gnome.Vino use-alternative-port true
org.gnome.Vino icon-visibility 'client'
org.gnome.Vino use-upnp true
org.gnome.Vino view-only false
org.gnome.Vino prompt-enabled false
org.gnome.Vino disable-xdamage false
org.gnome.Vino authentication-methods ['vnc']
org.gnome.Vino network-interface ''
org.gnome.Vino require-encryption false
org.gnome.Vino mailto ''
org.gnome.Vino lock-screen-on-disconnect false
org.gnome.Vino vnc-password 'xxxxxxxxxxx'﻿
-->

<!-- Place this tag in your head or just before your close body tag. -->
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<!-- Place this tag where you want the widget to render. -->
<div class="g-post" data-href="https://plus.google.com/+KenichiroMATOHARA/posts/f76yobBsvrN"></div>

<!--
vino-preference が廃止されたのはこっちの画面あるからいいよねということぽい
ところでパスワードが8文字以上入力できません><

"Bug 700070 – don't need standalone prefs anymore" https://bugzilla.gnome.org/show_bug.cgi?id=700070
with gnome 3.8, the privacy and sharing panels cover this, so we don't need or want a standalone preferences dialog for vino anymore.

https://plus.google.com/u/0/+KenichiroMATOHARA/posts/ZdVRei6iXsZ﻿
-->


- [Bug 700070 – don't need standalone prefs anymore](https://bugzilla.gnome.org/show_bug.cgi?id=700070 "Bug 700070 – don&apos;t need standalone prefs anymore")

一応dconfを書き換えれば設定できます．

```
% gsettings list-recursively org.gnome.Vino
org.gnome.Vino notify-on-connect true
org.gnome.Vino alternative-port uint16 5900
org.gnome.Vino disable-background false
org.gnome.Vino use-alternative-port true
org.gnome.Vino icon-visibility 'client'
org.gnome.Vino use-upnp true
org.gnome.Vino view-only false
org.gnome.Vino prompt-enabled false
org.gnome.Vino disable-xdamage false
org.gnome.Vino authentication-methods ['vnc']
org.gnome.Vino network-interface ''
org.gnome.Vino require-encryption false
org.gnome.Vino mailto ''
org.gnome.Vino lock-screen-on-disconnect false
org.gnome.Vino vnc-password 'xxxxxxxxxxx'﻿
```

GUIが良ければ`dconf-editor`とかを．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/25105314249/in/dateposted-public/" title="20160304_00:03:38-25955"><img src="https://farm2.staticflickr.com/1499/25105314249_9ab5413160.jpg" width="500" height="499" alt="20160304_00:03:38-25955"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>


＃MATE環境でvinoが起動してこない場合は次のページを -> [Raspberry_Pi/Ubuntu_MATE - matoken's wiki.](http://hpv.cc/~maty/pukiwiki1/index.php?Raspberry_Pi%2FUbuntu_MATE#jba848d3 "Raspberry_Pi/Ubuntu_MATE - matoken&apos;s wiki.")


Gnome重いしdconf書き換えるんもめんどいなということで最近はx11vncを使っていました．

<!-- Place this tag in your head or just before your close body tag. -->
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<!-- Place this tag where you want the widget to render. -->
<div class="g-post" data-href="https://plus.google.com/+KenichiroMATOHARA/posts/R6PVzNQfRwv"></div>

<!--
vino がvino-preference 廃止でGNOME 以外で使い辛くなったのでx11vnc でDISPLAY=:0 使うメモ

https://plus.google.com/u/0/+KenichiroMATOHARA/posts/ZdVRei6iXsZ
https://plus.google.com/u/0/+KenichiroMATOHARA/posts/f76yobBsvrN

% sudo apt install x11vnc
% ps -ef|grep -i auth
root      1410  1371  2  7月05 tty7   01:44:53 /usr/bin/X :0 -seat seat0 -auth /var/run/lightdm/root/:0 -nolisten tcp vt7 -novtswitch
% sudo x11vnc -display :0 -auth /var/run/lightdm/root/:0
これだと要root

% x11vnc -display :0
と単にdisplay 指定でも行けた．
パスワードの設定は
% x11vnc -storepasswd
規定値では`$HOME/.vnc/passwd` に保存される
% x11vnc -storepasswd ~/.mypass
のようにパスワードファイル指定も可能．
パスワードファイルはこんな感じで指定して起動
% x11vnc -rfbauth $HOME/.vnc/passwd -display :0
sshポートフォワーディングやxrdp経由で呼ぶ場合はlocalhost からのアクセスだけでいいのでこんな感じで
% x11vnc -localhost -rfbauth $HOME/.vnc/passwd -display :0﻿
-->

```
$ sudo apt install x11vnc
$ x11vnc -localhost -rfbauth $HOME/.vnc/passwd -display :0﻿
```

今回以下の記事を読んでTigerVNCでも出来ることを知りました!

- [第411回　VNCサーバー／クライアントを使用してみる：Ubuntu Weekly Recipe｜gihyo.jp … 技術評論社](http://gihyo.jp/admin/serial/01/ubuntu-recipe/0411 "第411回　VNCサーバー／クライアントを使用してみる：Ubuntu Weekly Recipe｜gihyo.jp … 技術評論社")
- [Ubuntu Weekly Recipe 第411回　VNCサーバー／クライアントを使用してみる - いくやの斬鉄日記](http://blog.goo.ne.jp/ikunya/e/c3c538801364674d1885b5f88e9e86e9 "Ubuntu Weekly Recipe 第411回　VNCサーバー／クライアントを使用してみる - いくやの斬鉄日記")

> 今回はVinoと同じく現在ログインしているデスクトップを転送するため，TigerVNCサーバーはユーザー権限で動作し，新しい仮想ディスプレイは作成しません。TigerVNCにはそのような便利なコマンドが用意されています。

> 続いて次のコマンドを実行し，TigerVNCを起動します。
>
> `$ x0vncserver -display :0 -passwordfile ~/.vnc/passwd`

TigerVNCに含まれる`x0vncserver`を使うといけるようです．

しかしDebianにもパッケージがないようです．

```
$ w3m -dump 'https://packages.debian.org/search?suite=all&section=all&arch=any&searchon=names&keywords=tigervnc'|grep 'tigervnc を名前に含むパッケージを' -A4
tigervnc を名前に含むパッケージを、すべてのスイート、すべてのセクション、すべて
のアーキテクチャで検索しました。

残念ながら、検索結果はありませんでした

```

- [Debian -- パッケージ検索結果 -- tigervnc](https://packages.debian.org/search?suite=all&section=all&arch=any&searchon=names&keywords=tigervnc "Debian -- パッケージ検索結果 -- tigervnc")


Ubuntu 14.04環境が別にあるのでそちらにいくやさんの用意されたリポジトリを用意して導入してみます．

しかし，`add-apt-reposotory`が見当たりません．環境がおかしくなっている?

```
$ sudo add-apt-reposotory ppa:ikuya-fruitsbasket/tigervnc
sudo: add-apt-reposotory: command not found
$ add-apt-reposotory
コマンド 'add-apt-reposotory' は見つかりませんでした。もしかして:
 コマンド 'add-apt-repository' - パッケージ 'software-properties-common' (main)
add-apt-reposotory: コマンドが見つかりません
$ sudo apt install software-properties-common
$ add-apt-reposotory -h
コマンド 'add-apt-reposotory' は見つかりませんでした。もしかして:
 コマンド 'add-apt-repository' - パッケージ 'software-properties-common' (main)
add-apt-reposotory: コマンドが見つかりません
$ apt-file update
$ apt-file search add-apt-reposotory
$ dpkg -L software-properties-common|grep add-apt-reposotory
```

見つからない?

- [Ubuntu – パッケージ内容検索結果 -- add-apt-reposotory](http://packages.ubuntu.com/search?searchon=contents&keywords=add-apt-reposotory&mode=&suite=trusty&arch=any "Ubuntu – パッケージ内容検索結果 -- add-apt-reposotory")

あ，スペルミスだ
`~s/add-apt-reposotory/add-apt-repository/g`ですね．よく見ましょうorz

```
$ dpkg -L software-properties-common |grep bin
/usr/bin
/usr/bin/add-apt-repository
/usr/bin/apt-add-repository
$ add-apt-repository
エラー: root で実行する必要があります
$ add-apt-repository
エラー: root で実行する必要があります
```

ということでこんな感じでいけました．

```
$ sudo apt install software-properties-common
$ sudo add-apt-repository ppa:ikuya-fruitsbasket/tigervnc
$ sudo apt update && sudo apt upgrade
$ sudo apt install tigervncserver
```

よし早速!

```
$ x0vncserver -display :0 -passwordfile ~/.vnc/passwd
x0vncserver: unable to open display ":0"
~ImageCleanup called
```

そういえば起動してませんでした．この端末はX入れているけどX転送でたまにアプリを使うくらいなので起動していないのです．
slimを起動(確かデフォルトはlightdm)してloginして

```
$ sudo service slim start
```

再度

```
$ x0vncserver -display :0 -passwordfile ~/.vnc/passwd
```

そしてRemminaで接続……出来ない．fwのせいでした．とりあえず`ssh -L` で5900を転送してlocalhostに接続しました．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/25385316181/in/dateposted-public/" title="20160304_04:03:41-24566"><img src="https://farm2.staticflickr.com/1466/25385316181_5d284b1fc6_n.jpg" width="320" height="175" alt="20160304_04:03:41-24566"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

うまく行きました :)

しかし，クライアント側もTigerVNCで繋いでみたいところ．とりあえずi686とx86_64についてはバイナリ配布もされているようなのでこれを利用してみます．

- [tigervnc.bphinz.com/nightly/](http://tigervnc.bphinz.com/nightly/ "tigervnc.bphinz.com/nightly/")

とりあえず適当なディレクトリに展開して実行で動きました．

```
$ wget http://tigervnc.bphinz.com/nightly/xc/x86_64/tigervnc-Linux-x86_64-1.6.80-20160302gitf34a386c.tar.gz
$ md5sum tigervnc-Linux-x86_64-1.6.80-20160302gitf34a386c.tar.gz
ea9ea2396d42819d4ea588a6598f583b  tigervnc-Linux-x86_64-1.6.80-20160302gitf34a386c.tar.gz
$ tar xf tigervnc-Linux-x86_64-1.6.80-20160302gitf34a386c.tar.gz -C ~/tmp
$ ~/tmp/usr/bin/vncviewer &
```



ちなみに
`vnc4server`にも`x0vnc4server`というそれらしいものがあるようです．そのうち試してみよう．
