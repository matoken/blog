手元のUSBメモリに導入してあるTailsを最新にしようと思い立ちダウンロードしつつ導入ページを見てみると新しくなっていました．

- [Tails - Privacy for anyone anywhere](https://tails.boum.org/ "Tails - Privacy for anyone anywhere")
  - [Tails - Which operating system are you installing Tails from?](https://tails.boum.org/install/os/index.en.html "Tails - Which operating system are you installing Tails from?")

これまではisoファイルを入手した後USBメモリその1に導入．その後USBメモリその1から起動したTailsから運用したいUSBメモリその2に導入する必要がありました．
（USBメモリその1はカスタマイズなどが出来なかったはず）

次のページがその方法の解説ページです．

- [Tails - Other Linux (Red Hat, Fedora, etc.)](https://tails.boum.org/install/linux/usb/overview/index.en.html "Tails - Other Linux (Red Hat, Fedora, etc.)")

しかし，DebianとUbuntu向けにインストーラが用意されていました．これを利用するとインストールのためのUSBメモリが必要なくisoファイルからUSBメモリに導入するだけでOKになっていました．

- [Tails - Install from Debian or Ubuntu](https://tails.boum.org/install/debian/usb/overview/index.en.html "Tails - Install from Debian or Ubuntu")
- [Tails - Installing Tails on a USB stick from Debian or Ubuntu](https://tails.boum.org/install/debian/usb/index.en.html "Tails - Installing Tails on a USB stick from Debian or Ubuntu")
- [Tails - Tails Installer](https://tails.boum.org/blueprint/bootstrapping/installer/ "Tails - Tails Installer")

これは便利と早速試してみました．

## Tailsの入手

次のページから入手出来ます．私はbittorrent経由で入手しました．ダウンロードの完了に1時間と少し掛かりました．

- [Tails - Download, verify and install](https://tails.boum.org/download/index.en.html "Tails - Download, verify and install")

ダウンロードが済んだら署名確認をします．
公開鍵を持っていない場合はダウンロードページの下の`Tails signing key`という画像から入手してインポートします．

```
$ wget -qO - https://tails.boum.org/tails-signing.key | gpg --import /dev/stdin
gpg: 鍵58ACD84F: 公開鍵"Tails developers (offline long-term identity key) <tails@boum.org>"をインポートしました
gpg:           処理数の合計: 1
gpg:             インポート: 1  (RSA: 1)
gpg: 最小の「ある程度の信用」3、最小の「全面的信用」1、PGP信用モデル
gpg: 深さ: 0  有効性:  15  署名:  53  信用: 0-, 0q, 0n, 0m, 0f, 15u
gpg: 深さ: 1  有効性:  53  署名:  60  信用: 53-, 0q, 0n, 0m, 0f, 0u
gpg: 次回の信用データベース検査は、2016-02-19です
```

その後署名確認をします．

```
$ gpg --verify tails-i386-2.0.iso.sig
gpg: 署名されたデータが'tails-i386-2.0.iso'にあると想定します
gpg: 2016年01月26日 09時07分15秒 JSTにRSA鍵ID 752A3DB6で施された署名
gpg: "Tails developers (offline long-term identity key) <tails@boum.org>"からの正しい署名
gpg:                 別名"Tails developers <tails@boum.org>"
gpg: *警告*: この鍵は信用できる署名で証明されていません!
gpg:       この署名が所有者のものかどうかの検証手段がありません。
主鍵フィンガー・プリント: A490 D0F4 D311 A415 3E2B  B7CA DBB8 02B2 58AC D84F
副鍵フィンガー・プリント: BA2C 222F 44AC 00ED 9899  3893 98FE C6BC 752A 3DB6
```

## tails-installerの導入

tails-installerパッケージを導入します．tails-installerパッケージはDebianではjessie-backports以降に用意されています．

- [Debian Package Tracking System - tails-installer](https://packages.qa.debian.org/t/tails-installer.html "Debian Package Tracking System - tails-installer")

jessieでbackportsを設定していない人は次のページを参考に設定して下さい．

- [Debian Backports](http://backports.debian.org/ "Debian Backports")
  - [Instructions](http://backports.debian.org/Instructions/ "Instructions")

今回はstretch(testing)なのでパッケージの導入だけです．

```
$ sudo apt install tails-installer
```

## tails-installertails-installerを使ったTailsの導入

メニューからやコマンドラインからtails-installer-launcherを実行します．ちなみにawesome wm環境だと認証がうまく動かないようです．

＜脱線＞

```
以下のコマンドの実行に問題がありました: `/usr/bin/pkexec /usr/bin/syslinux  -d syslinux /dev/sdb1`。
詳細なエラーログは  '/tmp/tails-installer-ddjZmh' に書かれています。
Tails installation failed!
以下のコマンドの実行に問題がありました: `/usr/bin/pkexec /usr/bin/syslinux  -d syslinux /dev/sdb1`。
詳細なエラーログは  '/tmp/tails-installer-ddjZmh' に書かれています。
```

このログを見ると以下のようなエラーが記録されていました．syslinux実行時に/dev/ttyが見つからないと言っているようです．

```
/usr/bin/pkexec /usr/bin/syslinux  -d syslinux /dev/sdb1
Error creating textual authentication agent: Error opening current controlling terminal for the process (`/dev/tty'): No such device or address
```

端末から実行すると認証が走りました．多分上のエラーはこの認証を呼ぼうとしてどこに呼べばいいのか解らなかったんでしょうね．

```
$ LANG=C tails-installer-launcher
    :
==== AUTHENTICATING FOR org.freedesktop.policykit.exec ===
Authentication is needed to run `/usr/bin/syslinux' as the super user
Authenticating as: KenichiroMATOHARA,,, (mk)
Password:
==== AUTHENTICATION FAILED ===
```

しかし3回ほど試しましたがここの認証も正しいパスワードを入力しても失敗してしまうよう……．

＜／脱線＞

とりあえずgksudo経由で実行すればOKです．

```
$ gksudo tails-installer-launcher
```

InstallとUpgradeが選択できます．`Install`を選択した場合は新規インストールで既存のデータは消されるので注意が必要です．`Upgrade`を選択すると既に持っているTailsのメディアのアップグレードが出来ます．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/24902683795/in/dateposted-public/" title="20160209_05:02:03-18344"><img src="https://farm2.staticflickr.com/1675/24902683795_b38048854a_z.jpg" width="525" height="285" alt="20160209_05:02:03-18344"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

署名確認をしたisoファイルと導入先のデバイス(USBメモリ等)を指定して`Install Tails`を選択します．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/24902683805/in/dateposted-public/" title="20160209_05:02:22-18509"><img src="https://farm2.staticflickr.com/1607/24902683805_0cca42e447_z.jpg" width="584" height="498" alt="20160209_05:02:22-18509"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

確認画面が出て，`OK`ボタンを押すと初期化や展開が始まります．しばらく待ちましょう．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/24274540764/in/dateposted-public/" title="20160209_05:02:28-18561"><img src="https://farm2.staticflickr.com/1601/24274540764_be7d02d15f.jpg" width="488" height="148" alt="20160209_05:02:28-18561"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

以下の画面が表示されたら完了です．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/24535066039/in/dateposted-public/" title="20160209_06:02:51-24618"><img src="https://farm2.staticflickr.com/1589/24535066039_532c39bb09.jpg" width="485" height="131" alt="20160209_06:02:51-24618"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>


UNetbootin並にお手軽です．Debian/UbuntuとTailsを利用している方におすすめです :)  
＃そういえばUNetbootinはjessie以降から居なくなってる． > [Debian Package Tracking System - unetbootin](https://packages.qa.debian.org/u/unetbootin.html "Debian Package Tracking System - unetbootin")

- [Debian Package Tracking System - tails-installer](https://packages.qa.debian.org/t/tails-installer.html "Debian Package Tracking System - tails-installer")
  - [#814190 - tails-installer: tails installer fails to authentication - Debian Bug report logs](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=814190 "#814190 - tails-installer: tails installer fails to authentication - Debian Bug report logs")

<!--

私は"awesome wm"を使っています．
"Mod4+r" で"tails-installer-launcher"を入力して実行します．
"Install Tails"ボタンを押して"OK"ボタンを押した後しばらくするとエラーになります．

I have been using the "awesome wm".
"Mod4 + r" in entering the "tails-installer-launcher" and run.
Press the "Install Tails" button and press the "OK" button from. It will be for a while to an error.

```
Installing bootloader...
Synchronizing data on disk...
Unmounting mounted filesystems on '/dev/sdb1'
There was a problem executing the following command: `/usr/bin/pkexec /usr/bin/syslinux  -d syslinux /dev/sdb1`.
A more detailed error log has been written to '/tmp/tails-installer-mU_Zx2'.
Tails installation failed!
There was a problem executing the following command: `/usr/bin/pkexec /usr/bin/syslinux  -d syslinux /dev/sdb1`.
A more detailed error log has been written to '/tmp/tails-installer-mU_Zx2'.
```

ファイル(/tmp/tails-installer-mU_Zx2)の中に次のエラーが記録されていました．
全ての記録はこちら -> http://paste.debian.net/379225/

The following error in the file had been recorded.
All recordings here -> http://paste.debian.net/379225/
```
/usr/bin/pkexec /usr/bin/syslinux  -d syslinux /dev/sdb1
Error creating textual authentication agent: Error opening current controlling terminal for the process (`/dev/tty'): No such device or address
```

次にcommand lineで実行しました．すると認証を求められます．しかし失敗します．

Then I ran in the command line. Then, authentication in the command line is required. But it will fail.

```
==== AUTHENTICATING FOR org.freedesktop.policykit.exec ===
Authentication is needed to run `/usr/bin/syslinux' as the super user
Authenticating as: KenichiroMATOHARA,,, (mk)
Password:
==== AUTHENTICATION FAILED ===
```

Installer messages.

```
Installing bootloader...
Synchronizing data on disk...
Unmounting mounted filesystems on '/dev/sdb1'
There was a problem executing the following command: `/usr/bin/pkexec /usr/bin/syslinux  -d syslinux /dev/sdb1`.
A more detailed error log has been written to '/tmp/tails-installer-FxWHh4'.
Tails installation failed!
There was a problem executing the following command: `/usr/bin/pkexec /usr/bin/syslinux  -d syslinux /dev/sdb1`.
A more detailed error log has been written to '/tmp/tails-installer-FxWHh4'.
```

The following error in the file had been recorded.
All recordings here -> http://paste.debian.net/379229/
```
/usr/bin/pkexec /usr/bin/syslinux  -d syslinux /dev/sdb1
polkit-agent-helper-1: error response to PolicyKit daemon: GDBus.Error:org.freedesktop.PolicyKit1.Error.Failed: No session for cookie
Error executing command as another user: Not authorized
```


ひとまずの対処として私は "gksudo" を使いました．ひとまずの対処として私は "gksudo" を使いました．

I used the "gksudo" as the workaround.


-->
