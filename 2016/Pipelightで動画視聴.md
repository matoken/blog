<!--
Debian stretch環境でSilverlight動画を視聴
-->

確か1話はニコニコ動画で見たと思うハルチカですが，2話からは有料のよう．どこかで見られないかなーとON AIR情報を調べてみると「Rakuten SHOWTIME」というところで最新話が1週無料のようです．

> Rakuten SHOWTIME
（最新各話、配信後1週間無料）	http://video.rakuten.co.jp/

- [放送情報 | アニメ『ハルチカ〜ハルタとチカは青春する〜』公式サイト](http://haruchika-anime.jp/onair/ "放送情報 | アニメ『ハルチカ〜ハルタとチカは青春する〜』公式サイト")

早速見に行くと，
> 非対応デバイスのため、動画を再生できません。

ブラウザのUser-Agentを変更してアクセスしてみると，Silverlightでした．

> 動画を再生するためには、Silverlightのインストールが必要です。こちらから最新版のインストールをお願いいたします。

Silverlightでの動画視聴は以前Pipelightを使って試したことがありました．

<iframe src="//www.slideshare.net/slideshow/embed_code/key/4RMZxJrmrgiuvu" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 50%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/matoken/pipelight-silverlight-raspberrypi-3" title="Pipelight でSilverlightを / RaspberryPi でリアルタイム動画配信をその3" target="_blank">Pipelight でSilverlightを / RaspberryPi でリアルタイム動画配信をその3</a> </strong> from <strong><a href="//www.slideshare.net/matoken" target="_blank">Kenichiro MATOHARA</a></strong> </div>

このときは動画と音声がどんどんずれていって動画視聴は辛かったです．今なら改善されているかもと再度試してみました．

以前はPipelightはDebian pkgにあったのですが今はなくなっているようです．

```
$ w3m -dump 'https://packages.debian.org/search?keywords=pipelight&searchon=all&exact=1&suite=all&section=all﻿'|grep 'で検索しました' -A5
すべてのアーキテクチャで検索しました。

残念ながら、検索結果はありませんでした

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
```

公式ページを見ると導入手順が書かれていました．

- [Pipelight | Installation](http://pipelight.net/cms/installation.html "Pipelight | Installation")

今回はDebian stretchなのでこちらのページを参考に導入しました．
- [Pipelight | Installation - Debian](http://pipelight.net/cms/install/installation-debian.html "Pipelight | Installation - Debian")

i386パッケージを有効にします．

```
$ sudo dpkg --add-architecture i386
```

リポジトリの鍵を入手して登録します．

```
wget http://repos.fds-team.de/Release.key
sudo apt-key add Release.key
```

pipelightのsource listを追加します．

```
$ sudo apt edit-sources pipelight
$ sudo cat /etc/apt/sources.list.d/pipelight.list
deb http://repos.fds-team.de/stable/debian/ stretch main
```

パッケージ情報を更新してPipelightを導入します．

```
$ sudo apt update && sudo apt upgrade
$ sudo apt install pipelight-multi
```

プラグインデータベースを最新にします．

```
$ sudo pipelight-plugin --update
```

とりあえずPipelightの導入は完了です．続いてプラグインの有効化を行います．
ここからは以下のページを参考にします．
[Pipelight | Installation](http://pipelight.net/cms/installation.html#section_2 "Pipelight | Installation")

pipelight-pluginコマンドで利用できるプラグインの種類が確認できます．

```
$ pipelight-plugin

Usage: pipelight-plugin [OPTIONS ...] COMMAND

Environment variables:
  MOZ_PLUGIN_PATH             overwrite plugin path

Options:
  --accept                    accept all licenses

User commands:
  --enable  PLUGIN            enable plugin
  --disable PLUGIN            disable plugin
  --disable-all               disable all plugins
  --list-enabled              list enabled plugins for the current user
  --list-enabled-all          list all enabled plugins
  --show-license              print license for plugin
  --system-check              do a system check
  --help                      shows this help
  --version                   shows the current version of Pipelight

Global commands (require root rights):
  --create-mozilla-plugins    create copies of libpipelight.so
  --remove-mozilla-plugins    remove copies of libpipelight.so
  --unlock PLUGIN             unlocks an additional plugin
  --lock   PLUGIN             locks an additional plugin
  --update                    update the dependency-installer script


Supported standard plugins:
  silverlight5.1
  silverlight5.0
  silverlight4
  flash
  unity3d
  widevine

Additional plugins (experimental):
  shockwave
  foxitpdf
  grandstream
  adobereader
  hikvision
  npactivex
  roblox
  vizzedrgr
  viewright-caiway
  x64-unity3d
  x64-flash

```

今回はSilverlightを使いたいので`silverlight5.1`を有効にします．

```
$ sudo pipelight-plugin --enable silverlight5.1
```

ブラウザ(Iceweasel)で利用できるようにします，

```
$ sudo pipelight-plugin --create-mozilla-plugins
```

ブラウザ(Iceweasel)を起動して，[about:plugins](about:plugins)にアクセスしてプラグインが読み込まれているか確認します．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/24651613942/in/dateposted-public/" title="20160202_16:02:32-30774"><img src="https://farm2.staticflickr.com/1445/24651613942_b6fa9f8552_o.jpg" width="605" height="256" alt="20160202_16:02:32-30774"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

うまくいったようなので動画視聴を試してみます．2番組試してみましたが以前と違いズレも発生せず快適に視聴できました．機能も一通り試してみましたが，最大化も含め全て問題なく動いているようです．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/24743206686/in/dateposted-public/" title="20160202_15:02:03-9643"><img src="https://farm2.staticflickr.com/1701/24743206686_5ed4a3064c.jpg" width="500" height="281" alt="20160202_15:02:03-9643"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

ということでハルチカが見られるようになりました :)
