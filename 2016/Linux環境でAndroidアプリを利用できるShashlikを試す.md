<!--
Linux環境でAndroidアプリを利用できるShashlikを試す
-->

Linuxデスクトップ環境でAndroidアプリを利用できるShashlikを試してみました．
現在は0.9.1が最新で，Kubuntu(KDE Plasma 5)向けとなっています．Shashlikを導入してAndroidパッケージの.apkを導入すると通常のLinuxアプリケーションのように呼び出せるというものです．
ただし，.apkはx86サポートのものでないといけないようです．

- [Shashlik | Android Simulated Environment](http://www.shashlik.io/ "Shashlik | Android Simulated Environment")
  - [Shashlik 0.9.0 Release – Deb Kubuntu Package | Shashlik](http://www.shashlik.io/news/2016/02/18/shashlik-0-9-0-kubuntu-package/ "Shashlik 0.9.0 Release – Deb Kubuntu Package | Shashlik")
  - [Shashlik 0.9.1 | Shashlik](http://www.shashlik.io/news/2016/02/22/shashlik-0-9-1/ "Shashlik 0.9.1 | Shashlik")

以下はShashlikを使ってFlappy Birdが動いている動画です．

<iframe width="560" height="315" src="https://www.youtube.com/embed/9SC6c_ih_Ac" frameborder="0" allowfullscreen></iframe>

## Shashlikの導入

今は.debが配布されているのでこれをダウンロードして導入します．入手場所は以下のページです．140MBと結構大きいです．

- [Shashlik 0.9.1 | Shashlik](http://www.shashlik.io/news/2016/02/22/shashlik-0-9-1/ "Shashlik 0.9.1 | Shashlik")

```
$ wget http://static.davidedmundson.co.uk/shashlik/shashlik_0.9.1.deb
$ sudo dpkg -i shashlik_0.9.1.deb
```

`/opt/shashlik`以下に導入されます．`qemu`で動くみたいですね．

```
$ dpkg -L shashlik
/.
/opt
/opt/shashlik
/opt/shashlik/android
/opt/shashlik/android/emulator-user.ini
/opt/shashlik/android/system
/opt/shashlik/android/system/build.prop
/opt/shashlik/android/config.ini
/opt/shashlik/android/kernel-qemu
/opt/shashlik/android/system.img
/opt/shashlik/android/ramdisk.img
/opt/shashlik/android/userdata.img
/opt/shashlik/bin
/opt/shashlik/bin/aapt
/opt/shashlik/bin/ddms
/opt/shashlik/bin/shashlik-install
/opt/shashlik/bin/bios.bin
/opt/shashlik/bin/constants.py
/opt/shashlik/bin/shashlik-run
/opt/shashlik/bin/adb
/opt/shashlik/bin/vgabios-cirrus.bin
/opt/shashlik/bin/emulator64-x86
/opt/shashlik/data
/opt/shashlik/data/shashlik-apps.directory
/opt/shashlik/data/shashlik.png
/opt/shashlik/lib
/opt/shashlik/lib/libc++.so
/opt/shashlik/lib64
/opt/shashlik/lib64/lib64GLES_V2_translator.so
/opt/shashlik/lib64/lib64EGL_translator.so
/opt/shashlik/lib64/lib64OpenglRender.so
/opt/shashlik/lib64/libGLES_V2_translator.so
/opt/shashlik/lib64/lib64GLES_CM_translator.so
/opt/shashlik/lib64/libGLES_CM_translator.so
/opt/shashlik/lib64/libEGL_translator.so
/opt/shashlik/lib64/libut_rendercontrol_dec.so
/opt/shashlik/lib64/libOpenglRender.so
```

## Androidアプリのパッケージを入手

Shashlikの上で動かすAndroidアプリのパッケージを入手します．

> Once installed grab any Android application package (APK) from the net.
For example our favourite app, Flappy Bird, can be found here: http://beste-apps.chip.de/android/app/flappy-bird-apk-android-app,cxo.66885070/

ということでここから`Flappy Bird`を入手しました．

## Androidアプリの導入

`shashlik-install`コマンドを使ってAndroidアプリを導入します．

```
$ /opt/shashlik/bin/shashlik-install ~/Downloads/com.dotgears.flappybird.apk
Successfully installed Flappy Bird
Connecting to deprecated signal QDBusConnectionInterface::serviceOwnerChanged(QString,QString,QString)
QDBusConnection: session D-Bus connection created before QCoreApplication. Application may misbehave.
QDBusConnection: session D-Bus connection created before QCoreApplication. Application may misbehave.
kbuildsycoca4 running...
kbuildsycoca4(19354) VFolderMenu::loadDoc: Parse error in  "/home/mk/.config/menus/applications-merged/xdg-desktop-menu-dummy.menu" , line  1 , col  1 :  "unexpected end of file"
QDBusConnection: session D-Bus connection created before QCoreApplication. Application may misbehave.
QDBusConnection: session D-Bus connection created before QCoreApplication. Application may misbehave.
Successfully installed Flappy Bird
```

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/25200802325/in/dateposted-public/" title="20160222_17:02:57-19417"><img src="https://farm2.staticflickr.com/1521/25200802325_10025e4827_o.jpg" width="265" height="99" alt="20160222_17:02:57-19417"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
これでKDE Plasma 5環境ならスタートメニューに登録されるはずです．

ちなみに，x86じゃない.apkを導入しようとするとこんな感じで怒られました．
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/25107571441/in/dateposted-public/" title="20160222_18:02:13-5944"><img src="https://farm2.staticflickr.com/1467/25107571441_2a76009cde.jpg" width="500" height="79" alt="20160222_18:02:13-5944"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

## Androidアプリの起動

KDE Plasma 5環境ならスタートメニューに登録されているはずですが，今の環境はawesomeなのでメニューに出てきません．
コマンドラインから起動します．

```
$ /opt/shashlik/bin/shashlik-run com.dotgears.flappybird FlappyBirds
```

起動にはそこそこ時間がかかります．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/25174478406/in/dateposted-public/" title="20160222_18:02:35-30707"><img src="https://farm2.staticflickr.com/1633/25174478406_7446e27e97_o.jpg" width="320" height="480" alt="20160222_18:02:35-30707"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/24570144224/in/dateposted-public/" title="20160222_18:02:52-31990"><img src="https://farm2.staticflickr.com/1560/24570144224_d82e6a707a_o.jpg" width="320" height="480" alt="20160222_18:02:52-31990"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/24570144284/in/dateposted-public/" title="20160222_18:02:15-344"><img src="https://farm2.staticflickr.com/1688/24570144284_a68d0acdd1_o.jpg" width="320" height="480" alt="20160222_18:02:15-344"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

もたつきなどもなく普通に動く感じです．
ただ，真っ黒な画面のままで起動に失敗することがあるようです．起動が速くなって安定して動くようになるととても便利そうです．
