# Adobe Flash Player NPAPI for Linux が復活ぽい

<blockquote class="twitter-tweet" data-lang="ja"><p lang="en" dir="ltr"><a href="https://twitter.com/hashtag/Adobe?src=hash">#Adobe</a> Returns To Updating NPAPI/ <a href="https://twitter.com/hashtag/Linux?src=hash">#Linux</a> Flash Player <a href="https://t.co/FZtlQbz9zG">https://t.co/FZtlQbz9zG</a></p>&mdash; Phoronix (@phoronix) <a href="https://twitter.com/phoronix/status/772779840076251136">2016年9月5日</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet" data-lang="ja"><p lang="ja" dir="ltr">艦これユーザー大歓喜？</p>&mdash; いくや (AWASHIRO Ikuya) (@ikunya) <a href="https://twitter.com/ikunya/status/772781970417778688">2016年9月5日</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

- [Adobe Returns To Updating NPAPI/Linux Flash Player - Phoronix](http://www.phoronix.com/scan.php?page=news_item&px=Adobe-Will-Update-Linux-Flash "Adobe Returns To Updating NPAPI/Linux Flash Player - Phoronix")
- [Beta News – Flash Player NPAPI for Linux](https://blogs.adobe.com/flashplayer/ "Beta News – Flash Player NPAPI for Linux")
- [Download Adobe Flash Player 23 Beta for Desktops - Adobe Labs](http://labs.adobe.com/downloads/flashplayer.html "Download Adobe Flash Player 23 Beta for Desktops - Adobe Labs")

てことでちょっと試してみました．

update-flashplugin-nonfree では見えないようです．ベータだししょうがないですね．
```
$ sudo /usr/sbin/update-flashplugin-nonfree --status
Flash Player version installed on this system  : 11.2.202.632
Flash Player version available on upstream site: 11.2.202.632
flash-mozilla.so - auto mode
  link best version is /usr/lib/browser-plugin-freshplayer-pepperflash/libfreshwrapper-flashplayer.so
  link currently points to /usr/lib/browser-plugin-freshplayer-pepperflash/libfreshwrapper-flashplayer.so
  link flash-mozilla.so is /usr/lib/mozilla/plugins/flash-mozilla.so
/usr/lib/browser-plugin-freshplayer-pepperflash/libfreshwrapper-flashplayer.so - priority 70
/usr/lib/flashplugin-nonfree/libflashplayer.so - priority 50
```

ということで手動で．何時もの感じで動きました．

```
$ wget https://fpdownload.macromedia.com/pub/labs/flashruntimes/flashplayer/linux64/libflashplayer.so
$ mv libflashplayer.so ~/.mozilla/plugins/
$ sha256sum ~/.mozilla/plugins/libflashplayer.so 
413a82524b218ce49869bd9756fe4e85de0feb43e50737f05e5632599394f488  /home/mk/.mozilla/plugins/libflashplayer.so
$ firefox-esr about:plugins
```

とりあえず艦これが動くのは確認．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/29476220995/in/dateposted/" title="20160905_22:09:07-5170"><img src="https://c4.staticflickr.com/9/8161/29476220995_c744d92ceb.jpg" width="500" height="83" alt="20160905_22:09:07-5170"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/29476221655/in/photostream/" title="20160905_22:09:23-7621"><img src="https://c8.staticflickr.com/9/8123/29476221655_20c1b38001.jpg" width="500" height="303" alt="20160905_22:09:23-7621"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

消すときはこんな感じで

```
$ rm ~/.mozilla/plugins/libflashplayer.so
```


ブラウザ側ではサポート打ち切っていく方針だと思うのですが Adobe はどういう心変わりなんでしょうね?
Google ChromeのPPAPIはどうなるかな?
<!--
ちなみにWindows版のAdobe Flash Playerを動かすPipelightも普通に動く感じなのでサービスが打ち切られる場合にはこっちに移行しようと思っていました．
-->
