BadUSBなどの不正なUSBデバイスからPCが守れないかなちょっと設定すればできそう?そもそも作っている人がいるんじゃ?ということで探したらUSBGuardというソフトウェアを見つけました．

* [home \| USBGuard](https://dkopecek.github.io/usbguard/)

このソフトウェアを導入しておくと，設定したルールに合致しないUSBデバイスはblockされます．
デスクトップ利用の場合はGUIのアプレットを利用してUSB接続時にウィンドウがポップアップして接続するか選べたりもします．
便利．  
＃USB KILLERは……USB配線を外したりUSBコネクタを塞ぐしかないですよね多分．

## 導入

Debian/Ubuntuにはパッケージがあるのでそちらから導入します．

```
$ sudo apt install usbguard usbguard-applet-qt
```

## 設定

一般ユーザから利用する場合は設定ファイルを編集してユーザかグループを追加してデーモンの再起動を行います．
設定ファイルは*/etc/usbguard/usbguard-daemon.conf*です．

```
diff --git a/usbguard/usbguard-daemon.conf b/usbguard/usbguard-daemon.conf
index 4a54ca0..7b3a165 100644
--- a/usbguard/usbguard-daemon.conf
+++ b/usbguard/usbguard-daemon.conf
@@ -65,7 +65,7 @@ PresentControllerPolicy=keep
#
# IPCAllowedUsers=username1 username2 ...
#
-IPCAllowedUsers=root
+IPCAllowedUsers=root user1 user2

#
# Groups allowed to use the IPC interface.
@@ -75,7 +75,7 @@ IPCAllowedUsers=root
#
# IPCAllowedGroups=groupname1 groupname2 ...
#
-IPCAllowedGroups=root
+IPCAllowedGroups=root users

#
# Generate device specific rules including the "via-port"
```

ユーザの場合は*IPCAllowedUsers*，グループの場合は*IPCAllowedGroups*にスペース区切りで書いていきます．

## デーモンの再起動

設定を反映するためにデーモンを再起動します．

```
$ sudo service usbguard restart
```

## CUIでの利用例

usbguardが起動した後に接続されたデバイスはblockされています．

```
$ usbguard list-devices| tail -2
9: allow id 8087:0024 serial "" name "" hash "Zx7v0FMQEjScKSAFENAiobEs1OGPPB0YWR+yXDCVE04=" parent-hash "WwvSEwd+7257rAqUGLMQjffF7zyqygmmLeQTYnR9QzQ=" via-port "4-1" with-interface 09:00:00
11: block id 1004:631c serial "03a809c94b4befd4" name "LGE Android Phone" hash "P5dSK5xxK4R5QTRzd7KlD8Agf/+28pztL077j1oWqPI=" parent-hash "Zx7v0FMQEjScKSAFENAiobEs1OGPPB0YWR+yXDCVE04=" via-port "4-1.1" with-interface ff:ff:00
```

blockされている11番目のデバイスを許可してみます．（これは一時的です）

```
$ usbguard allow-device 11
$ usbguard list-devices| tail -1
11: allow id 1004:631c serial "03a809c94b4befd4" name "LGE Android Phone" hash "P5dSK5xxK4R5QTRzd7KlD8Agf/+28pztL077j1oWqPI=" parent-hash "Zx7v0FMQEjScKSAFENAiobEs1OGPPB0YWR+yXDCVE04=" via-port "4-1.1" with-interface ff:ff:00
```

## 状況の確認

USBデバイスの認識状況を監視します．

```
$ usbguard watch
[IPC] Connected
[device] Inserted: id=12 hash=vi38heJ4vKEdayxiqrQFylpwa3xkVYYUkZi2zbu3sWs= name=Mass Storage Device product_id=1336 serial=00000000000006 vendor_id=048d interface={ 08:06:50} rule_match=0 rule_id=4294967295
[device] Blocked: id=12 name=Mass Storage Device product_id=1336 vendor_id=048d rule_match=0 rule_id=4294967295
[device] Allowed: id=12 name=Mass Storage Device product_id=1336 vendor_id=048d rule_match=0 rule_id=4294967295
```

## GUI(usbguard-applet-qt)の利用例

CUIだとちょっと面倒ですが，デスクトップ利用の場合は*usbguard-applet-qt*が便利です．

usbguard-applet-qtが起動した状態でUSBデバイスを接続すると以下のようなダイヤログが表示されます．Allowボタンを押すことで利用できるようになります．
このとき，*Make the decision permanent*にチェックを入れておくと，設定が永続化されます．この設定は*/etc/usbguard/rules.conf*に記録されます．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/30173864805/in/dateposted/" title="20161007_02:10:22-9078"><img src="https://c6.staticflickr.com/6/5023/30173864805_f2e3e26eec_o.jpg" width="423" height="386" alt="20161007_02:10:22-9078"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

ダイヤログはタイムアウトすると消えてしまいますが，アプレットのウィンドウから設定画面を呼び出すことができます．ここから設定変更ができます．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/30173861395/in/dateposted/" title="20161007_16:10:49-4107"><img src="https://c4.staticflickr.com/9/8398/30173861395_ab4bab9a89.jpg" width="500" height="136" alt="20161007_16:10:49-4107"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

とりあえずはこれくらいで良さそうですが，ルールを書くといろいろ応用が効きそうです．

* [Rule Language \| USBGuard](https://dkopecek.github.io/usbguard/documentation/rule-language.html)

とりあえずはどうもデーモン起動までに接続されていたデバイスは許可されるようなので内臓デバイスを明示的に許可してその他をblockしようと思います．

