<!--
% LingrのCallback URLにhttpsは使えないかも
% Kenichiro MATOHARA(@matoken)
% 2016-03-25
-->

以前以下のroomにMLに投稿があったら通知する簡易botを設定していたのですが，

- [鹿児島らぐ – Lingr](http://lingr.com/room/kagolug "鹿児島らぐ – Lingr")

発言をするとこんな感じでエラーを出すようになっていました．

```
matoken
test
KagolugML_bot
[Error: Bot responded HTTP status code: 302]
```

`HTTP status code: 302` てことは一時的な移動．https化した時にmod_rewriteでhttpをhttpsに飛ばすようにしたんでした．
＃今回関係ないけど301出すように変えたほうがいいですね．

ということはbotのCallback URLを転送先のhttpsにしたらいいのかなと設定しようとするとエラーに．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/25952521441/in/dateposted/" title="20160324_10:03:05-16615"><img src="https://farm2.staticflickr.com/1483/25952521441_8af06c025b_m.jpg" width="199" height="128" alt="20160324_10:03:05-16615"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

httpsは受け付けないようです．しょうがないのでhttpの領域を作ってそちらに持って行きました．
てことでLingrのCallback URLにhttpだけしか使えないかもという話でした．

- [Lingr Bot API · lingr/lingr Wiki](https://github.com/lingr/lingr/wiki/Lingr-Bot-API "Lingr Bot API · lingr/lingr Wiki")


<iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=4777515362" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe><iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=4873116864" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>
