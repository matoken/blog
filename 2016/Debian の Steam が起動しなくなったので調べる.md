<!--
Debian の Steam が起動しなくなったので調べる
-->

Debian stretch testing amd64 で `steam:i386` パッケージで導入した Steam が起動しなくなっていました．

端末で直に叩くとこんな感じで怒られていました．

```
$ steam
find: The relative path '~/usr/local/bin' is included in the PATH environment variable, which is insecure in combination with the -execdir action of find.  Please remove that entry from $PATH
```

PATH から `~/usr/local/bin` を抜くとOK
PATH を `/home/user/usr/local/bin` としてもOK
てことで ~/.profile の ~ を $HOME に書き換えました．

```
- PATH="~/usr/local/bin:$PATH"
+ PATH="$HOME/usr/local/bin:$PATH"
```

とりあえずこれで動くようになりました．

Steam では最近は Unturned とかで遊んでいます．最近は Linux で動くゲームも増えて嬉しいです :)

- [Steam へようこそ](http://store.steampowered.com/ "Steam へようこそ")
  - [Steam コミュニティ :: matoken](http://steamcommunity.com/id/matoken "Steam コミュニティ :: matoken")
