<!--
title:コマンドスニペットを手軽に調べられるコマンドのborg
-->

borgはOK borgというsiteのスニペットをコマンドライン上から検索したり編集できるコマンドのようです．borgはgo製でApache License Version 2.0のソフトウェアです．

- [borg/docs at master · ok\-borg/borg · GitHub](https://github.com/ok-borg/borg/tree/master/docs)
- [OK borg \- the quickest solution to your bash woes](https://ok-b.org/)


## 導入

導入方法は如何から対応バイナリを入手して適当な場所に置き，実行権をつけるだけです．arm linuxとか*BSDとか結構いろいろそろっています．

- [Releases · ok\-borg/borg · GitHub](https://github.com/ok-borg/borg/releases)

今回はこんな感じで導入しました．

```shell
$ wget https://github.com/ok-borg/borg/releases/download/v0.0.1/borg_linux_amd64 -O ~/usr/local/bin/borg && chmod +x ~/usr/local/bin/borg
```

## 利用方法

基本的にコマンドの後ろに調べたいキーワードをつけるだけです．規定値では5つの例が表示されます．
bashでloopどう書くんだっけ?

```shell
$ borg "bash loop"
(1) Bash Shell Do While Loop Infinite loop?
         [11] while [ `prog -some flags` = "Another instance of this program is running, please exit it first" ]
              -
              bay=$(prog -some flags)
              while [ $bay = "Another instance of this program is running, please exit it first" ]
              do
              echo "Awaiting Access to program"
              bay=$(prog -some flags)
              done
              .....
         [12] while true
              do
         ...  

(2) Bash foreach loop
         [21] xargs cat <filenames.txt
              -
              for fn in `cat filenames.txt`; do
                  echo "the next file is $fn"
                  cat $fn
              done
         [22] for fn in `cat filenames.txt`; do cat "$fn"; done
         [23] while read filename
              do
                  echo "Printing: $filename"
                  cat "$filename"
         ...

(3) Bash loop ping successful
         [31] ((count = 100))                            # Maximum number to try.
              while [[ $count -ne 0 ]] ; do
                  ping -c 1 8.8.8.8                      # Try once.
                  rc=$?
                  if [[ $rc -eq 0 ]] ; then
                      ((count = 1))                      # If okay, flag to exit loop.
                  fi
                  ((count = count - 1))                  # So we don't go forever.
              done
              if [[ $rc -eq 0 ]] ; then                  # Make final determination.
         ...

(4) Limit for bash loop
        [41] for(( i=1; i <= 1000; i++ )); do
                 name=$(date --date="$i day ago" +%Y%m%d%H%M%S)
                 mkdir -p "$name" &&
                 touch "$name/${name}_file" ||
                 break
             done

(5) Bash 'for' loop syntax?
         [51] for (($i=0...
              -
              for ((i=0;i<10;i++))
         [52] for i in `seq 0 9`
              do
                  echo "the i is $i"
              done
         [53] for i in {0..9}
                do
                  echo $i
                done
         ...
```

画像をタイルに結合って?

```shell
$ borg "image tile"
(1) ImageMagick crop huge image
         [11] $ time convert -crop 512x512 +repage huge.tif x/image_out_%d.tif
              real    0m5.623s
              user    0m2.060s
              sys     0m2.148s
              $ time vips dzsave huge.tif x --depth 1 --tile-size 512 --overlap 0 --suffix .tif
              real    0m1.643s
              user    0m1.668s
              sys     0m1.000s
         [12]  convert -monitor -limit area 2mb myLargeImg.tif myLargeImg.mpc
              -
               #!/bin/bash
         ...

(2) Set clipboard to image - pbcopy
        [21] cat image.png | impbcopy -
        [22] # Copy image to clipboard
             uuencode SomeFile.jpg - | pbcopy
             -
             # Paste from clipboard to image file
             pbpaste | uudecode -o AnotherFile.jpg


(3) Using Amazon MapReduce/Hadoop for Image Processing
         [31] and should be able to be done using Bash
         [32] #!/usr/bin/env bash
              # NLineInputFormat gives a single line: key is offset, value is Isotropic Url
              read offset isofile
              # Retrieve file from Isotropic server to local disk
              echo "reporter:status:Retrieving $isofile" >&2
              target=`echo $isofile | awk '{split($0,a,"/");print a[5] a[6]}'`
              filename=$target.tar.bz2
              #$HADOOP_INSTALL/bin/hadoop fs -get $isofile ./$filename
              curl  $isofile -o $filename
         ...

(4) Convert multipage PDF to a single image
        [41] convert in.pdf -append out%d.png
             -
             convert *.png output.pdf
             -
             convert foo?.png output.pdf
        [42] convert in.pdf +append out%d.png

(5) bash cgi won't return image
        [51] echo -ne "Content-type: image/png\n\n"
             -
             echo -e "Content-type: image/png\n"
             -
               -n     do not output the trailing newline
```

とかとか．

オプションはこんな感じとりあえず省略されないように`-f`をつけてページャに渡すと良さそう．

```
$ borg --help
Usage of borg:
-f  (= false)
    Print full results, ie. no more '...'
-h (= "borg.crufter.com")
    Server to connect to
-l  (= 5)
    Result list limit. Defaults to 5
-p  (= false)
    Private search. Your search won't leave a trace. Pinky promise. Don't use this all the time if you want to see the search result relevancy improved
```

検索だけでなくスニペットの追加や編集もできるようです．

- [borg/docs at master · ok\-borg/borg · GitHub](https://github.com/ok-borg/borg/tree/master/docs)

サーバも選べるので自分用のメモを蓄積することもできますね．
ちょっと惜しいのはサーバと通信して結果を表示するのでオフラインでは使えないというところ．
オフラインで使いたい場合はローカルにサーバを立てるか別の方法を使うしかなさそうです．

