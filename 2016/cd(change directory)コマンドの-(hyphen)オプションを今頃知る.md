<!--
cd(change directory)コマンドの-(hyphen)オプションを今頃知る
-->

以下の記事を読んで知ったのですが，

- [【連載】にわか管理者のためのLinux運用入門 [9] cdコマンドを使いこなす](https://news.mynavi.jp/itsearch/article/hardware/1138 "【連載】にわか管理者のためのLinux運用入門 [9] cdコマンドを使いこなす")
> 「さっきいた場所に戻る」の呪文を覚えておきたい。それは「cd -」だ。
画面のように、cdコマンドに引数として「-」のみを指定すると、直前にいたディレクトリに戻ることができる。

毎日何十回も叩いているであろうコマンドなのに知らなかったですorz

```
mk@x220:~$ cd usr/local/bin
mk@x220:~/usr/local/bin$ cd
mk@x220:~$ cd -
/home/mk/usr/local/bin
mk@x220:~/usr/local/bin$
```

なるほど確かに．
これは便利ですね．ちなみに同じようなことをするのに`pushd/popd`を使っていました．組み合わせて使っても大丈夫みたいですね．

```
mk@x220:~/usr/local/bin$ pushd /tmp
/tmp ~/usr/local/bin
mk@x220:/tmp$ cd
mk@x220:~$ cd -
/tmp
mk@x220:/tmp$ cd /
mk@x220:/$ popd
~/usr/local/bin
mk@x220:~/usr/local/bin$ cd
```

手元の環境で`GNU bash 4.3.42`, `Zsh 5.2`, `DASH 0.5.8`, `BusyBox v1.22.1`の`built-in shell (ash)`で動作するのを確認しました．  
＃そして`pushd/popd`が`DASH`, `ash`に無いのを確認orz


しかしこういうの未だいっぱいあるんだろうなーorz
