<!--
OpenSSH 7.2のsshdに入ったauthorized_keysの新オプションを試す
-->

OpenSSH 7.2がリリースされました :)

- [[openssh-unix-announce] Announce: OpenSSH 7.2 released](http://lists.mindrot.org/pipermail/openssh-unix-announce/2016-February/000125.html "[openssh-unix-announce] Announce: OpenSSH 7.2 released")
- [春山 征吾のくけー : OpenSSH 7.2 がリリースされました - livedoor Blog（ブログ）](http://haruyama.blog.jp/archives/52047049.html "春山 征吾のくけー : OpenSSH 7.2 がリリースされました - livedoor Blog（ブログ）") リリースノートの日本語訳があります．何時も助かります_o_

ということで気になった以下の新機能を試してみます．

>   \* sshd(8): add a new authorized_keys option "restrict" that includes
   all current and future key restrictions (no-*-forwarding, etc.).
   Also add permissive versions of the existing restrictions, e.g.
   "no-pty" -> "pty". This simplifies the task of setting up
   restricted keys and ensures they are maximally-restricted,
   regardless of any permissions we might implement in the future.

>    sshd(8): authorized_keys のオプションに "restrict" を追加する.
   これは, (no-*-forwarding などの) 現在と将来のすべての制限を含む.
   さらに, 現在の制限の許可バージョンも追加する. つまり,
   "no-pty" -> "pty". これにより, 制限付きの鍵の設定のタスクが
   単純化し, 将来実装するすべての許可に関係なく
   鍵が最大に制限されていることを圃場できる.


prefix以外は規定値のままでbuid
```
$ sudo apt install build-essentialbuild-essential
$ sudo apt build-dep openssh
$ git pull
$ ./configure --prefix=$HOME/usr/local/openssh-portable
$ make
$ make install
```

適当に7.2のデーモンを起動
```
$ `pwd`/sbin/sshd -D -p 22222
$ ps -ef|grep openssh-portable
mk       21788 14885  0 00:46 pts/7    00:00:00 /home/mk/usr/local/openssh-portable/sbin/sshd -D -p 22222
```

適当な鍵を作って`~/.ssh/authorized_keys`に登録してログインテスト．

```
$ ssh-keygen -t ed25519 -N '' -f ./testkey
$ cat testkey.pub >> ~/.ssh/authorized_keys
$ ssh localhost -p 22222 -i ./testkey
```

authorized_keysの該当鍵行の頭に

```
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFm5sR98q060FFlT1cpBVbwm0caShCYGl39D5k9PCenB mk@x220
```

`restrict`を追記して
```
restrict ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFm5sR98q060FFlT1cpBVbwm0caShCYGl39D5k9PCenB mk@x220
```

ssh 7.2で接続を試します．
```
$ /home/mk/usr/local/openssh-portable/bin/ssh localhost -p 22222 -i ./testkey
PTY allocation request failed
printenv|grep -i ssh
SSH_CLIENT=::1 56910 22222
SSH_CONNECTION=::1 56910 ::1 22222
exit
Shared connection to localhost closed.
```

sshd側の機能なのでssh 7.1で繋いでも制限されます．
```
$ ssh localhost -p 22222 -i ./testkey
PTY allocation request failed
printenv|grep -i ssh
SSH_CLIENT=::1 56910 22222
SSH_CONNECTION=::1 56910 ::1 22222
Shared connection to localhost closed.
```

`restrict,pty` のようにptyを追記して`-X/-Y`を試すとptyが使えてXは制限されているのが確認できました．
```
$ grep restrict authorized_keys
restrict,pty ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFm5sR98q060FFlT1cpBVbwm0caShCYGl39D5k9PCenB mk@x220
$ ssh localhost -p 22222 -i ./testkey -X
X11 forwarding request failed
mk@x220:~$ xeyes
Error: Can't open display:
```

これまではずらずらと制限を書く必要がったのがシンプルに書けるようになりそうです :)

以下の件は別エントリにて
<!-- Place this tag in your head or just before your close body tag. -->
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<!-- Place this tag where you want the widget to render. -->
<div class="g-post" data-href="https://plus.google.com/+KenichiroMATOHARA/posts/3wUMXyjf2gU"></div>

<iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=4873112877" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>
