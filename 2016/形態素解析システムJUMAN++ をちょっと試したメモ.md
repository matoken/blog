# 形態素解析システムJUMAN++ をちょっと試したメモ

京都大学 黒橋・河原研究室より2016-09-23にリリースされた新しい形態素解析システムだそうです．とりあえず手元の環境で動かしてみたのでそのメモです．

* [JUMAN++ - KUROHASHI-KAWAHARA LAB](http://nlp.ist.i.kyoto-u.ac.jp/index.php?JUMAN++)

## 環境

Debian stretch amd64 / Ubuntu 16.10 amd64 で確認しました．どちらも以下の手順でOKでした．開発元ではCent OS 6.7で動作確認をしているようです．

## build

必要なパッケージを導入してmake．

```shell
$ sudo apt install libboost-dev build-essential
$ wget http://lotus.kuee.kyoto-u.ac.jp/nl-resource/jumanpp/jumanpp-1.01.tar.xz
$ sha256sum jumanpp-1.01.tar.xz 0d587416a3eb7123638f9c1e30a649b72dfb483448839168dcb48be572c5919a  jumanpp-1.01.tar.xz
$ tar tvf ./jumanpp-1.01.tar.xz
$ tar xf ./jumanpp-1.01.tar.xz
$ ./configure --prefix=${HOME}/usr/local
$ make
$ make install
```

## 動作確認

適当に文章を入れてみたり．

```shell
$ ~/usr/local/bin/jumanpp
こんにちはJUMANPP++
こんにち こんにち こんにち 名詞 6 時相名詞 10 * 0 * 0 "代表表記:今日/こんにち カテゴリ:時間"
は は は 助詞 9 副助詞 2 * 0 * 0 NIL
JUMANPP JUMANPP JUMANPP 未定義語 15 その他 1 * 0 * 0 "品詞推定:名詞"
+ + + 未定義語 15 その他 1 * 0 * 0 "品詞推定:名詞"
+ + + 未定義語 15 その他 1 * 0 * 0 "品詞推定:名詞"
EOS
```

青空文庫の古典を流し込んでみたり．

```shell
$ wget -O - http://www.aozora.gr.jp/cards/000160/files/2617_ruby_23916.zip | zcat | iconv -f SJIS -t UTF-8 - | ~/usr/local/bin/jumanpp
三十 三十 三十 名詞 6 数詞 7 * 0 * 0 "カテゴリ:数量"
年 ねん 年 接尾辞 14 名詞性名詞助数辞 3 * 0 * 0 "代表表記:年/ねん 準内容語 カテゴリ:時間"
後 ご 後 接尾辞 14 名詞性名詞接尾辞 2 * 0 * 0 "代表表記:後/ご 内容語"
の の の 助詞 9 接続助詞 3 * 0 * 0 NIL
世界 せかい 世界 名詞 6 普通名詞 1 * 0 * 0 "代表表記:世界/せかい カテゴリ:場所-その他"
      :
```

古典とかよりTwitterとかのほうがいいかもしれない．

```shell
$ curl 'https://twitter.com/search?f=tweets&vertical=default&q=lang%3Aja%20near%3A%22%E6%97%A5%E6%9C%AC%22%20within%3A15mi&src=typd&lang=ja' | grep 'class="TweetTextSize  js-tweet-text tweet-text"' | lynx -stdin -dump -nolist | ~/usr/local/bin/jumanpp
```

ちなみにちょっと試したいだけであれば導入しなくても以下のページで試せます．ラティス表示も楽しい．

* [Juman\+\+](http://tulip.kuee.kyoto-u.ac.jp/demo/jumanpp_lattice?text=%E5%A4%96%E5%9B%BD%E4%BA%BA%E5%8F%82%E6%94%BF%E6%A8%A9%E3%81%AB%E5%AF%BE%E3%81%99%E3%82%8B%E8%80%83%E3%81%88%E6%96%B9%E3%81%AE%E9%81%95%E3%81%84%E3%80%82)