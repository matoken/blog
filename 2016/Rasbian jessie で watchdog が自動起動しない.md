<!--
Rasbian jessie で watchdog を設定する
-->

Rasbian jessie 環境でも設定したのでメモとして．

## パッケージの導入

```
$ sudo apt install watchdog
```

## 設定ファイル修正

`/etc/watchdog.conf` を以下のように編集
```
-#min-memory              = 1
+min-memory              = 5000

-#watchdog-device       = /dev/watchdog
+watchdog-device        = /dev/watchdog
```

`min-memory` はページサイズ．今回は4096*5000で約20MB．このサイズ(swapを含めた容量)を切ったら再起動するはず．大きいような気もするけどまずは動作確認のためにこのくらいにしてみる．


`/etc/default/watchdog` を以下のような感じに編集して `bcm2708_wdog` モジュールを読みこむようにする．

```
-watchdog_module="none"
+watchdog_module="bcm2708_wdog"
```

## 起動設定

```
$ sudo update-rc.d watchdog defaults
$ sudo update-rc.d watchdog enable
```

## 動作確認

再起動して

```
$ sudo reboot
```

モジュールの読み込み，デバイスの確認，デーモンの確認をする．

```
$ lsmod|grep dog
bcm2708_wdog            3926  1
$ ls -l /dev/watchdog
crw------- 1 root root 10, 130  1月 26 23:32 /dev/watchdog
$ ps -ef|grep dog
root       642     1  0 23:32 ?        00:00:00 /usr/sbin/watchdog
```

問題無さそうなら swap を無効にしてファイルシステムを read only や sync を有効にしておいてから Forkbomb で動作確認をしてみる．以下の例ではbashでのForkbomb

```
$ sudo swapoff -a
$ sync
$ sudo mount -o remount,ro /boot
$ sudo mount -o remount,sync /
$ :(){ :|:& };:
```

うまく再起動が走ったら後は `/etc/watchdog.conf` を詰めていく﻿．

- [Fork bomb - Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/Fork_bomb "Fork bomb - Wikipedia, the free encyclopedia")[Fork bomb - Wikipedia, the free encyclopedia](https://en.wikipedia.org/wiki/Fork_bomb "Fork bomb - Wikipedia, the free encyclopedia")


## kernel panic 時の設定

`bcm2708_wdog` を使っているのでkernel panic 時にも watchdog は効くはずだけど念の為設定しておく．

`/etc/sysctl.conf` に以下を追記

```
kernel.panic = 180
kernel.panic_on_oops = 1
```

`sysctl` や再起動で反映．

```
$ sudo sysctl -p
kernel.panic = 60
kernel.panic_on_oops = 1
```
