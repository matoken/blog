FreeMindをDebian jessieに導入しようとしたらパッケージが見当たりません．
探してみるとメンテナンスされてないからjessie/sidから消されたようです．

* [#807682 - RM: freemind -- ROM; replaced by freeplane; buggy; unmaintained - Debian Bug report logs](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=807682 "#807682 - RM: freemind -- ROM; replaced by freeplane; buggy; unmaintained - Debian Bug report logs")

> We have Freeplane which is actively maintained and roughly equivalent
> in features.

でもFreeplaneというメンテされている似たものがあるよということでこれを試してみることにしました．

＃ちなみにFreeMindのsiteを見るとstableのリリースが2013年で1.0.1．その後1.1.0-Beta1が2015年1.1.0-Beta2が2016年に出ていて，Debianでは0.9.0だったようです．

* [Main Page - FreeMind](http://freemind.sourceforge.net/wiki/index.php/Main_Page "Main Page - FreeMind")

FreeplaneはどうもFreeMindのフォークのようです．見た目も使い勝手も似ています．JAVA製でマルチプラットホームなのも同じです．

* [Home - Freeplane - free mind mapping and knowledge management software](http://www.freeplane.org/wiki/index.php/Main_Page "Home - Freeplane - free mind mapping and knowledge management software")

導入はパッケージがあるのでそれを利用しました．

```
$ sudo apt install freeplane
```

起動ロゴ……何だろうこの虫?は．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/29488428274/in/dateposted/" title="20161005_01:10:30-30616"><img src="https://c3.staticflickr.com/6/5123/29488428274_bba8c9bf72.jpg" width="500" height="131" alt="20161005_01:10:30-30616"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

初回起動時にはWhats Newが表示されました．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/29488427924/in/dateposted/" title="20161005_01:10:09-30241"><img src="https://c5.staticflickr.com/6/5500/29488427924_4afcc36241.jpg" width="500" height="274" alt="20161005_01:10:09-30241"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

少し日本語が豆腐になってしまっている場所がありましたがフォントを日本語フォントに変更することで解決しました．
後はチュートリアルとかに目を通しておくと良さそうです．

チュートリアルなどのドキュメントは*/usr/share/freeplane/doc/*辺りにありました．

```
$ ls -1 /usr/share/freeplane/doc/*_ja.mm
/usr/share/freeplane/doc/freeplaneFunctions_ja.mm
/usr/share/freeplane/doc/freeplaneTutorial_ja.mm
/usr/share/freeplane/doc/freeplane_ja.mm
```

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/30116688685/in/dateposted/" title="20161005_01:10:53-29876"><img src="https://c6.staticflickr.com/6/5316/30116688685_a8bcb928db.jpg" width="500" height="274" alt="20161005_01:10:53-29876"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/29488427214/in/dateposted/" title="20161005_01:10:09-28858"><img src="https://c7.staticflickr.com/6/5272/29488427214_348b2b8645.jpg" width="500" height="274" alt="20161005_01:10:09-28858"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/29488365093/in/dateposted/" title="20161005_01:10:32-27112"><img src="https://c6.staticflickr.com/9/8396/29488365093_bea526ac43.jpg" width="500" height="273" alt="20161005_01:10:32-27112"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

未だそんなに使っていませんが，FreeMindと同じような操作性だし以前書いた.mmも読めるようなのでそのまま移行できそうです．


