<!--
ownCloudの同期が動かなくなって困る
-->

ownCloudを便利に使っているのですが，ふと気づくと同期されていない．ステータスを見ると最後の同期が8時間前．タスクマネージャーのアイコンは!マークが付いて「不明な状態」となっています．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/25272081775/in/dateposted-public/" title="20160226_19:02:52-398"><img src="https://farm2.staticflickr.com/1515/25272081775_0f875cc14e_m.jpg" width="220" height="178" alt="20160226_19:02:52-398"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

ログを見てもよくわからない．

<!--
```
02-26 19:55:16:361 32535 OCC::Updater::getSystemInfo: Sys Info size:  114
02-26 19:55:16:491 32535 OCC::ConnectionValidator::checkServerAndAuth: Checking server and authentication
02-26 19:55:16:491 32535 OCC::ConnectionValidator::checkServerAndAuth: Trying to look up system proxy
02-26 19:55:16:492 32565 OCC::SystemProxyRunnable::run: virtual void OCC::SystemProxyRunnable::run() Starting system proxy lookup
02-26 19:55:16:495 32535 OCC::ConnectionValidator::systemProxyLookupDone: No system proxy set by OS
02-26 19:55:16:495 32535 unknown: QObject::connect: Cannot connect (null)::stateChanged(QNetworkSession::State) to QNetworkReplyHttpImpl::_q_networkSessionStateChanged(QNetworkSession::State)
02-26 19:55:16:496 32535 OCC::AbstractNetworkJob::start: !!! OCC::CheckServerJob created for "https://example.org/owncloud" + "status.php" "OCC::ConnectionValidator"
02-26 19:55:16:503 32535 OCC::AccountState::checkConnectivity: ConnectionValidator already running, ignoring "matoken@example.org"
02-26 19:55:16:505 32535 OCC::AccountState::checkConnectivity: ConnectionValidator already running, ignoring "matoken@example.org"
02-26 19:55:16:505 32535 OCC::AccountState::checkConnectivity: ConnectionValidator already running, ignoring "matoken@example.org"
02-26 19:55:16:506 32535 OCC::AccountState::checkConnectivity: ConnectionValidator already running, ignoring "matoken@example.org"
02-26 19:55:16:513 32535 OCC::AccountManager::wantsAccountSavedSlot: Saving account "https://example.org/owncloud"
02-26 19:55:16:514 32535 OCC::AccountManager::save: Saving  1  unknown certs.
02-26 19:55:16:514 32535 OCC::AccountManager::save: Saving cookies.
02-26 19:55:16:516 32535 OCC::CookieJar::save: "/home/mk/.local/share/data//ownCloud//cookies.db"
02-26 19:55:16:517 32535 OCC::AccountManager::wantsAccountSavedSlot: Saved account settings, status: 0
02-26 19:55:16:591 32535 OCC::CheckServerJob::finished: status.php returns:  QMap(("edition", QVariant(QString, ""))("installed", QVariant(bool, true))("maintenance", QVariant(bool, false))("version", QVariant(QString, "8.2.2.2"))("versionstring", QVariant(QString, "8.2.2")))   QNetworkReply::NetworkError(NoError)  Reply:  QNetworkReplyHttpImpl(0x563ef1fe6f30)
02-26 19:55:16:592 32535 OCC::ConnectionValidator::slotStatusFound: ** Application: ownCloud found:  QUrl("https://example.org/owncloud/status.php")  with version  "8.2.2" ( "8.2.2.2" )
02-26 19:55:16:594 32535 OCC::AccountState::slotConnectionValidatorResult: AccountState connection status change:  "Undefined" -> "Credentials Wrong"
02-26 19:55:16:596 32535 OCC::AccountState::setState: AccountState state change:  "切断しました" -> "設定エラー"
02-26 19:55:16:601 32535 OCC::FolderMan::slotAccountStateChanged: Account "matoken@example.org" disconnected, terminating or descheduling sync folders
02-26 19:55:16:625 32535 OCC::ConnectionValidator::checkServerAndAuth: Checking server and authentication
02-26 19:55:16:626 32535 OCC::ConnectionValidator::checkServerAndAuth: Trying to look up system proxy
02-26 19:55:16:627 32565 OCC::SystemProxyRunnable::run: virtual void OCC::SystemProxyRunnable::run() Starting system proxy lookup
02-26 19:55:16:629 32535 OCC::ConnectionValidator::systemProxyLookupDone: No system proxy set by OS
02-26 19:55:16:630 32535 unknown: QObject::connect: Cannot connect (null)::stateChanged(QNetworkSession::State) to QNetworkReplyHttpImpl::_q_networkSessionStateChanged(QNetworkSession::State)
02-26 19:55:16:630 32535 OCC::AbstractNetworkJob::start: !!! OCC::CheckServerJob created for "https://example.org/owncloud" + "status.php" "OCC::ConnectionValidator"
02-26 19:55:16:691 32535 OCC::CheckServerJob::finished: status.php returns:  QMap(("edition", QVariant(QString, ""))("installed", QVariant(bool, true))("maintenance", QVariant(bool, false))("version", QVariant(QString, "8.2.2.2"))("versionstring", QVariant(QString, "8.2.2")))   QNetworkReply::NetworkError(NoError)  Reply:  QNetworkReplyHttpImpl(0x563ef22c23d0)
02-26 19:55:16:692 32535 OCC::ConnectionValidator::slotStatusFound: ** Application: ownCloud found:  QUrl("https://example.org/owncloud/status.php")  with version  "8.2.2" ( "8.2.2.2" )
02-26 19:55:16:693 32535 OCC::ConnectionValidator::checkAuthentication: # Check whether authenticated propfind works.
02-26 19:55:16:694 32535 OCC::AbstractNetworkJob::start: !!! OCC::PropfindJob created for "https://example.org/owncloud" + "/" "OCC::ConnectionValidator"
02-26 19:55:16:699 32535 unknown: QObject::connect: Cannot connect (null)::stateChanged(QNetworkSession::State) to QNetworkReplyHttpImpl::_q_networkSessionStateChanged(QNetworkSession::State)
02-26 19:55:17:084 32535 unknown: QObject::connect: Cannot connect (null)::stateChanged(QNetworkSession::State) to QNetworkReplyHttpImpl::_q_networkSessionStateChanged(QNetworkSession::State)
02-26 19:55:17:087 32535 OCC::AbstractNetworkJob::start: !!! OCC::JsonApiJob created for "https://example.org/owncloud" + "ocs/v1.php/cloud/capabilities" "OCC::ConnectionValidator"
02-26 19:55:17:243 32535 OCC::ConnectionValidator::slotCapabilitiesRecieved: Server capabilities QVariant(QVariantMap, QMap(("core", QVariant(QVariantMap, QMap(("pollinterval", QVariant(qulonglong, 60)))))("files", QVariant(QVariantMap, QMap(("bigfilechunking", QVariant(bool, true))("undelete", QVariant(bool, true))("versioning", QVariant(bool, true)))))("files_sharing", QVariant(QVariantMap, QMap(("api_enabled", QVariant(bool, true))("federation", QVariant(QVariantMap, QMap(("incoming", QVariant(bool, true))("outgoing", QVariant(bool, true)))))("public", QVariant(QVariantMap, QMap(("enabled", QVariant(bool, true))("expire_date", QVariant(QVariantMap, QMap(("days", QVariant(QString, "3"))("enabled", QVariant(bool, true))("enforced", QVariant(bool, false)))))("password", QVariant(QVariantMap, QMap(("enforced", QVariant(bool, false)))))("send_mail", QVariant(bool, true))("upload", QVariant(bool, true)))))("resharing", QVariant(bool, true))("user", QVariant(QVariantMap, QMap(("send_mail", QVariant(bool, true))))))))))
02-26 19:55:17:245 32535 OCC::AccountState::slotConnectionValidatorResult: AccountState connection status change:  "Credentials Wrong" -> "Connected"
02-26 19:55:17:247 32535 OCC::AccountState::setState: AccountState state change:  "設定エラー" -> "接続しました"
02-26 19:55:17:248 32535 OCC::FolderMan::slotAccountStateChanged: Account "matoken@example.org" connected, scheduling its folders
02-26 19:55:19:637 32535 OCC::UpdaterScheduler::slotTimerFired: void OCC::UpdaterScheduler::slotTimerFired() Skipping update check because of config file
02-26 19:55:48:166 32535 OCC::ConnectionValidator::checkAuthentication: # Check whether authenticated propfind works.
02-26 19:55:48:169 32535 OCC::AbstractNetworkJob::start: !!! OCC::PropfindJob created for "https://example.org/owncloud" + "/" "OCC::ConnectionValidator"
02-26 19:55:48:170 32535 unknown: QObject::connect: Cannot connect (null)::stateChanged(QNetworkSession::State) to QNetworkReplyHttpImpl::_q_networkSessionStateChanged(QNetworkSession::State)
02-26 19:56:20:172 32535 OCC::ConnectionValidator::checkAuthentication: # Check whether authenticated propfind works.
02-26 19:56:20:173 32535 OCC::AbstractNetworkJob::start: !!! OCC::PropfindJob created for "https://example.org/owncloud" + "/" "OCC::ConnectionValidator"
02-26 19:56:20:174 32535 unknown: QObject::connect: Cannot connect (null)::stateChanged(QNetworkSession::State) to QNetworkReplyHttpImpl::_q_networkSessionStateChanged(QNetworkSession::State)
```
-->

WebやAndroidそして別のLinuxマシンは問題ないようです．ということは多分この端末の問題．
問題の環境はDebian stretch testing amd64の`owncloud 7.0.12~dfsg-2`です．


一旦ownCloudを終了して設定ファイルだけ退避（対象ファイルはそのまま）して設定を作りなおしてみました．

```
$ mv ./.local/share/data/ownCloud ./.local/share/data/ownCloud-backup
$ owncloud --logwindow
```

とりあえず同期出来たようです．


<iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B01BM4WA1O" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe><iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00944PQMK" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>
