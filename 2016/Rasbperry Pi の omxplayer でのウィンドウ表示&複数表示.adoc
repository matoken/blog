# Rasbperry Pi の omxplayer でのウィンドウ表示&複数表示

展示でRaspberry Piのでぅくトップを賑やかにしたいなと思い，livestreamerでストリーミングを表示したら良さそうと思ったのですが，Raspberry Piのomxplayerだと基本的に全画面です．
man(1)を見ると--winというものを使うとウィンドウ表示が使えそう．

----
               --win ´x1 y1 x2 y2´     Set position of video window
               --win x1,y1,x2,y2       Set position of video window
----

現在の解像度を確認します．1366x768でした．

----
$ xdpyinfo | grep dimensions
  dimensions:    1366x768 pixels (361x203 millimeters)
----

livestreamer から omxplayer を呼び出すようにするとうまく行きました．
この例では右下1/4がlivestreamerで取得したストリーミング動画で埋まります．
直接描画されるのでX関係なくXの上に描画されます．

----
$ /usr/local/bin/livestreamer --player "omxplayer --win '684 387 1366 768'" --fifo http://www.ustream.tv/channel/iss-hdev-payload --default-stream best
----

以下のように同時に4枚表示とかも行けました．試してませんが9枚も行けるかも．

----
$ omxplayer --win '0 0 684 387' 2016-06-07.ts | \
omxplayer --win '683 0 1366 387' 2016-06-07.ts | \
omxplayer --win '0 386 684 768' 2016-06-07.ts | \
omxplayer --win '684 387 1366 768' 2016-06-07.ts﻿
----

監視カメラとかにいいかもしれません．
