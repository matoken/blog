<!--
old-old-stableのDebian squeeze LTSのサポートがもうすぐ終了
-->

<blockquote class="twitter-tweet" data-lang="ja"><p lang="ja" dir="ltr">【重要】通常3年＋追加2年でそろそろsqueeze-ltsでのサポートが終了します。squeeze使ってるようなところはとっととwheezyあるいはjessieに移行して下さい【安全】</p>&mdash; Debian こうほうチーム (@debianjp) <a href="https://twitter.com/debianjp/status/687981795451617280">2016, 1月 15</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet" data-lang="ja"><p lang="ja" dir="ltr">【再掲な】通常3年＋追加2年でそろそろsqueeze-ltsでのサポートが終了します。squeeze使ってるようなところはとっととwheezyあるいはjessieに移行して下さい【スゴイダイジ】</p>&mdash; Debian こうほうチーム (@debianjp) <a href="https://twitter.com/debianjp/status/694833633471782912">2016, 2月 3</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

- [Debian -- News -- Debian 6.0 Long Term Support reaching end-of-life](https://www.debian.org/News/2016/20160212 "Debian -- News -- Debian 6.0 Long Term Support reaching end-of-life")
> The Debian Long Term Support (LTS) Team hereby announces that Debian 6.0 (squeeze) support will reach its end-of-life on February 29, 2016, five years after its initial release on February 6, 2011.
>
> There will be no further security support for Debian 6.0.

- [LTS - Debian Wiki](https://wiki.debian.org/LTS "LTS - Debian Wiki")
> Debian 6 “Squeeze” until February 2016
Debian 7 “Wheezy” from February 2016 to May 2018
Debian 8 “Jessie“ from May 2018 to April/May 2020

LTS開始時のアナウンスなど

- [Debian -- ニュース -- Debian 6.0 の長期サポート告知](https://www.debian.org/News/2014/20140424 "Debian -- ニュース -- Debian 6.0 の長期サポート告知")

- [[SECURITY] [DSA 2938-1] Availability of LTS support for Debian 6.0 / squeeze](https://lists.debian.org/debian-security-announce/2014/msg00119.html "[SECURITY] [DSA 2938-1] Availability of LTS support for Debian 6.0 / squeeze")
  - [Squeeze-ltsのアナウンス粗訳 - ポケットを空にして。(2014-05-28)](http://d.ma-aya.to/?date=20140528#p01 "Squeeze-ltsのアナウンス粗訳 - ポケットを空にして。(2014-05-28)")

- [Debian squeeze LTS リリース | matoken's meme](http://matoken.org/blog/blog/2014/05/30/debian-squeeze-lts/ "Debian squeeze LTS リリース | matoken&apos;s meme")

ということでDebian old-old-stableの長期サポート版squeeze-LTSが今月いっぱいでEOLです．当初は2/6までとなっていましたが1月近く伸びたようですね．  
old-stableのwheezyかstableのjessieに移行しましょう．(国際宇宙ステーションのsqueezeはアップグレードされたのかな?)  
Debianのアップグレードは簡単ですけど動作しているアプリケーションはバージョンが上がるので要設定だったりとちょっと面倒ですが必要な作業です．

今動いているバージョンがわからないよという方は以下のようなコマンドを実行して`6`若しくはそれより前のバージョンであればアップグレード対象です．

```
$ cat /etc/debian_version
6.0.10
$ lsb_release -a
No LSB modules are available.
Distributor ID: Debian
Description:    Debian GNU/Linux 6.0.10 (squeeze)
Release:        6.0.10
Codename:       squeeze
```

アップグレードの具体的な手順はリリースノートの第4章が参考になるでしょう．

- [Debian 7.0 (wheezy) リリースノート (32 ビット PC 用)](https://www.debian.org/releases/wheezy/i386/release-notes/index.ja.html "Debian 7.0 (wheezy) リリースノート (32 ビット PC 用)")
  - [第4章 Debian 6.0 (squeeze) からのアップグレード](https://www.debian.org/releases/wheezy/i386/release-notes/ch-upgrading.ja.html "第4章 Debian 6.0 (squeeze) からのアップグレード")
- [Debian 7.0 (wheezy) リリースノート (64 ビット PC 用)](https://www.debian.org/releases/wheezy/amd64/release-notes/index.ja.html "Debian 7.0 (wheezy) リリースノート (64 ビット PC 用)")
  - [第4章 Debian 6.0 (squeeze) からのアップグレード](https://www.debian.org/releases/wheezy/amd64/release-notes/ch-upgrading.ja.html "第4章 Debian 6.0 (squeeze) からのアップグレード")

万が一の時のためにバックアップを取得してから作業を行いましょう!  
[＃作業依頼もお待ちしています．](mailto:matoken+work@gmail.com)
