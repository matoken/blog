<!--
内田美奈子「サーキットワンダラーズ」全話公開中
-->

<blockquote class="twitter-tweet" data-lang="ja"><p lang="ja" dir="ltr">画楽.magで好評連載をしていたサーキットワンダラーズ(内田美奈子)が、<br>描き下ろし２本(６話と７話)を加えて本日全話一挙公開!!<br>兄を探して仮想都市に入ったミケの運命は!? <a href="https://t.co/WF4ps9aVlE">pic.twitter.com/WF4ps9aVlE</a></p>&mdash; 画楽ノ杜 (@garakunomori) <a href="https://twitter.com/garakunomori/status/703250358533910529">2016年2月26日</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

コミック化を待ってたのでやったーと思ったのですが，

<blockquote class="twitter-tweet" data-lang="ja"><p lang="ja" dir="ltr">【サーキットワンダラーズ・画楽ノ杜でまとめて公開】この作品は未完にて一旦終了となります。私の力不足のため諸条件揃わず、読者様には大変申し訳ありません。過去掲載分に加え描き下ろしもありますのでご覧頂けましたら幸いに存じます。 <a href="https://t.co/fTbeTzM36c">https://t.co/fTbeTzM36c</a></p>&mdash; 内田美奈子 (@zerra01) <a href="https://twitter.com/zerra01/status/703300905483919360">2016年2月26日</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

orz  
目当ての作品があるときはやっぱり雑誌を買ってアンケート送らないとダメですね……．

- [サーキットワンダラーズ | 画楽ノ杜](http://www.garakunomori.com/?manga=cw "サーキットワンダラーズ | 画楽ノ杜")

とりあえず気を取り直して読みました．
BOOM TOWNの後のお話でBOOM TOWNのことなんかはあまり説明はされてないので未読の方はBOOM TOWNも読むといいかもしれません．マンガ図書館Zで公開されてますし．

- [マンガ図書館Z](http://www.mangaz.com/title/index?query=BOOMTOWN "マンガ図書館Z") BOOM TOWN(1~4+コミック未収録話)
- [マンガ図書館Z](http://www.mangaz.com/title/?query=%E5%86%85%E7%94%B0%20%E7%BE%8E%E5%A5%88%E5%AD%90 "マンガ図書館Z") 最近赤赤丸も公開され始めました!

あまり現実世界は出てこなくて仮想世界での話が中心．BOOM TOWNでのアシスタント(小さなリタ)やデバッガたちのような能力を得たところまででした．ここで終わるのは勿体無いな．

ちなみにBOOM TWONは雑誌掲載時からのファンで紙本も初版(乱丁)，乱丁直った版，布教用と3セット持ってたりも．クラウドファンディングでPDFも入手しているので本の痛みも気にしなくて良くなりました．
個人的にxxのテンションが下がった時に読んでテンションを上げる本というのが幾つかあって，BOOM TOWMはコンピュータ関係のテンションを上げたい時に読む本になってます．ネオアキバ行きたい．ハッセイとちまちまデバッグしたりしたい．

<iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00NWQI49S" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe><iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00NWQI4D4" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe><iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00NWQI4CA" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe><iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00NWQI4DE" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe><iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00NWQI4N4" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>
