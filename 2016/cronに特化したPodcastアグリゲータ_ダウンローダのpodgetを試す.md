<!--
cronに特化したPodcastアグリゲータ/ダウンローダのpodgetを試す
-->

先日試したpodracerは何気に10年前のソフトでメンテナンスもされていないし，起動中に強制終了するとゴミが残って次から起動しなくなったりとかとか結構不満点が出てきました．

- [CLI な podcast aggregator/downloader な podracer を試してみる | matoken's meme](http://matoken.org/blog/blog/2016/01/28/try-the-cli-of-podcat-aggregator-downloader-of-podracer/ "CLI な podcast aggregator/downloader な podracer を試してみる | matoken&apos;s meme")


今は前処理を以下のようにして，

```
if [ $(pgrep podracer) ] ; then
  echo 'running podracer.'
  exit -1
fi

echo run podracer
if [ -f ~/.podracer/tempsub ] ; then
  rm ~/.podracer/tempsub
  echo 'rm tempsub'
fi
```

プレイリストもこんな感じで別に作ってます．

```
cd ~/podcasts
PODCASTLIST=($(cd ~/podcasts ; find . -mmin -$LISTTIME -type f -print0 | xargs -0n1 file | grep -i audio | cut -f1 -d:))
if [ ${#PODCASTLIST[@]} -eq 0 ] ; then
  echo 'podcast not found.'
  exit -1;
fi

echo num = ${#PODCASTLIST[@]}
ls -1tr ${PODCASTLIST[@]} > ~/podcasts/todayspodcast.m3u
```

後者はDebianの方にはバグ報告をしていますが音沙汰がない感じです．

他にないかなーと探して(`apt-cache search podcast`)`podget`というものを見つけました．

```
$ apt show podget | grep -A99 Description:

Description: cron 用に最適化された Podcast アグリゲータ/ダウンローダ
 Podget はシンプルな podcast アグリゲータであり、定期的なバックグランドジョブ
 (すなわち cron) として起動するために最適化されています。RSS および XML フィード
 からの podcast のダウンロード、ファイルをソートしてフォルダやカテゴリごとに格納、
 iTunes PCAST ファイルおよび OPML リストからの URL のインポート、M3U および ASX
 プレイリストの自動生成、そして古くなったファイルの自動クリーンアップのサポート
 が特徴です。また、MS Windows サーバ上にホストされた podcast の UTF-16 自動変換も
 特徴です。
 podget を一旦起動すると、ユーザ設定ファイルを $HOME/.podget にインストール
 しますので、そのファイルをカスタマイズできます。
```

cronに特化していて便利そうなので試してみます．

## 導入

`Debian stretch testing/Rasbian jessie/Ubuntu 14.04` で確認しました．

```
$ sudo apt install podget
```

## 初期化

```
$ podget -h

Usage /usr/bin/podget [options]

    -c --config <FILE>           Name of configuration file.
    -C --cleanup                 Skip downloading and only run cleanup loop.
    --cleanup_simulate           Skip downloading and simulate running
                                 cleanup loop.
                                 Display files to be deleted.
    --cleanup_days               Number of days to retain files.  Anything
                                 older will be removed.
    -d --dir_config <DIRECTORY>  Directory that configuration files are
                                 stored in.
    -f --force                   Force download of items from each feed even
                                 if they have already been downloaded.
    --import_opml <FILE or URL>  Import servers from OPML file or
                                 HTTP/FTP URL.
    --import_pcast <FILE or URL> Import servers from iTunes PCAST file or
                                 HTTP/FTP URL.
    -l --library <DIRECTORY>     Directory to store downloaded files in.
    -p --playlist-asx            In addition to the default M3U playlist,
                                 create an ASX Playlist.
    -r --recent <count>          Download only the <count> newest items from
                                 each feed.
    --serverlist <list>          Serverlist to use.
    -s --silent                  Run silently (for cron jobs).
    --verbosity <LEVEL>          Set verbosity level (0-4).
    -v                           Set verbosity to level 1.
    -vv                          Set verbosity to level 2.
    -vvv                         Set verbosity to level 3.
    -vvvv                        Set verbosity to level 4.
    -h --help                    Display help.
```

初回起動時に設定ファイルなどが作成されます．引数無しで実行すると初期設定の中のpodcastをダウンロードするので`--cleanup_simulate`オプションを付けて実行すると良さそうです．
設定ファイルは規定値では`~/.podget`以下に作成されます．

```
$ podget --cleanup_simulate
$ ls -lA ~/.podget
合計 8
-rw-rw-r-- 1 mk mk 3652  2月 19 05:34 podgetrc
-rw-rw-r-- 1 mk mk 1131  2月 19 05:34 serverlist
```

- podgetrc : 設定ファイル
- serverlist : podcastのリスト

## 設定ファイルの用意

お好みで設定ファイルの`~/podgetrc`を編集します．私は以下の辺りを書き換えました．

ログファイルを有効に
```
# Directory to store logs in
dir_log=/home/mk/POD/LOG
```

プレイリストの年月日の形式を書き換え
```
# Date format for new playlist names
date_format=+%Y-%m-%d
```

Podcastの取得数を3つに

```
# Most Recent
# 0  == download all new items.
# 1+ == download only the <count> most recent
most_recent=3
```

## サーバリストの作成

取得するPodcastを`~/.podget/serverlist`に書いていきます．

書式はスペース区切りで

> URL カテゴリー 番組名

になっています．podgetを実行した時に
`カテゴリー/番組名/番組`というふうにディレクトリが掘られます．
具体的にはこんな感じで書いていきます．

```
http://feeds.feedburner.com/weblogs/csc tech 電脳空間カウボーイズ
http://feeds.backspace.fm/backspacefm tech backspace.fm
http://www.joqr.co.jp/science-podcast/index.xml science 日立ハイテクプレゼンツ 大村正樹のサイエンスキッズ
http://www.tbsradio.jp/life/rss.xml etc 文化系トークラジオ Life
http://www.tbsradio.jp/cycle-r/index.xml bike ミラクル・サイクル・ライフ
http://sokoani.com/feed anime そこあに
  :
```

他のアプリケーションらの移行でOPMLファイルのエクスポートが可能な場合はそのファイルやURLを元にインポートできるようです．※未確認

```
$ podget --import_opml <FILE or URL>
```

若しくはPCAST形式(iTunes向け?)も同様に利用できるようです．※未確認

```
$ podget --import_pcast <FILE or URL>
```

OPML書き出しの機能もあります．但しこの機能は
Ubuntu 14.04の0.6.9にはなく，
Rasbian jessieの0.7.3，
Debian stretchの0.7.9には存在しました．

```
$ podget --export_opml /tmp/podcast.opml
podget

Session file not found.  Creating podget.22189 .

Export serverlist to OPML file: /tmp/podcast.opml

Closing session and removing lock file.
$ head /tmp/podcast.opml
<?xml version="1.0" encoding="utf-8" ?>
<opml version="1.0">
<head/>
<body>
<outline text="tech"><outline text="電脳空間カウボーイズ" type="rss" xmlUrl="http://feeds.feedburner.com/weblogs/csc" /></outline>
<outline text="tech"><outline text="backspace.fm" type="rss" xmlUrl="http://feeds.backspace.fm/backspacefm" /></outline>
<outline text="science"><outline text="日立ハイテクプレゼンツ 大村正樹のサイエンスキッズ" type="rss" xmlUrl="http://www.joqr.co.jp/science-podcast/index.xml" /></outline>
<outline text="etc"><outline text="文化系トークラジオ Life" type="rss" xmlUrl="http://www.tbsradio.jp/life/rss.xml" /></outline>
<outline text="bike"><outline text="ミラクル・サイクル・ライフ" type="rss" xmlUrl="http://www.tbsradio.jp/cycle-r/index.xml" /></outline>
<outline text="anime"><outline text="そこあに" type="rss" xmlUrl="http://sokoani.com/feed" /></outline>
```

## 実行

とりあえず引数無しで実行することでPodcastが取得できます．

```
$ podget
-------------------------------------------------
Category: anime         Name: そこあに
Downloading feed index from http://sokoani.com/feed
2016-02-19 07:37:15 URL:http://sokoani.com/feed [673868] -> "-" [1]

Downloading 0_s413.mp3 from http://sokoani.com/podpress_trac/feed/9818/0/
 :
 :
```

Podcastは規定値では`~/POD`以下に`カテゴリ/番組名/番組ファイル`の形で保存されます．

```
$ find ~/POD -type f | tail -3
/home/mk/POD/etc/JUNK 伊集院光 深夜の馬鹿力/files_20160216.mp3
/home/mk/POD/etc/JUNK 伊集院光 深夜の馬鹿力/files_20160202.mp3
/home/mk/POD/etc/JUNK 伊集院光 深夜の馬鹿力/files_20160209.mp3
```

また，~/POD/*.m3uとステプレイリストも作成されます．これは実行単位で作られるのかな?

```
$ ls -l ~/POD/*.m3u
-rw-rw-r-- 1 mk mk  946  2月 19 07:14 /home/mk/POD/New-2016-02-19.m3u
-rw-rw-r-- 1 mk mk 1924  2月 19 07:50 /home/mk/POD/New-2016-02-19.r2.m3u
```

うまく動作するようなら`-s`オプションを付けてcronに登録してあげると良さそうです．以下の例では毎時3分にpodgetを実行しています．

```
$ crontab -e
$ crontab -l | tail -2
# get podcast
3 * * * *       podget -s
```

podracerはPodcastを番組関係なく実行日のディレクトリに保存されてPodcast番組を探したりするのに不便でしたがpodgetだとカテゴリと番組でディレクトリが分かれるので便利です．
しばらく併用してみようと思います．
