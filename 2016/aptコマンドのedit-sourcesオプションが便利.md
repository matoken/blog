<!--
aptコマンドのedit-sourcesオプションが便利.md
-->

Rasbian の更新でjaistを指定していたのだけど最近遅いのでmirror listを見て

- [RaspbianMirrors - Raspbian](https://www.raspbian.org/RaspbianMirrors "RaspbianMirrors - Raspbian")
```
$ w3m -dump https://www.raspbian.org/RaspbianMirrors|grep -i japan -A1
Asia^*    Japan       JAIST                    (http|rsync)://ftp.jaist.ac.jp/pub/
                                               Linux/raspbian-archive/raspbian
--
Asia^*    Japan       WIDE Project Tsukuba NOC raspbian/raspbian/
                                               rsync://ftp.tsukuba.wide.ad.jp/
--
Asia^*    Japan       Yamagata University      http://ftp.yz.yamagata-u.ac.jp/pub/
                                               linux/raspbian/raspbian/
```

`/etc/apt/sources.list`をWIDEに変更した．

```
-deb http://ftp.jaist.ac.jp/pub/Linux/raspbian-archive/raspbian/ jessie main contrib non-free rpi
-deb-src http://ftp.jaist.ac.jp/pub/Linux/raspbian-archive/raspbian/ jessie main contrib non-free rpi
+deb http://ftp.tsukuba.wide.ad.jp/Linux/raspbian/raspbian/  jessie main contrib non-free rpi
+deb-src http://ftp.tsukuba.wide.ad.jp/Linux/raspbian/raspbian/  jessie main contrib non-free rpi
```

ところで`apt`コマンドの`edit-sources`オプションが便利なのでぜひ使うべき．visudoとかみたいに間違えると教えてくれる．

```
$ sudo apt edit-sources
E: Type 'eb' is not known on line 3 in source list /etc/apt/sources.list
Failed to parse /etc/apt/sources.list. Edit again?  [Y/n]
```

引数を付けて`/etc/apt/sources.list.d/`以下の編集も出来る

```
$ ls /etc/apt/sources.list.d/
google-chrome.list  gyazo_gyazo-for-linux.list  owncloud-client.list
$ sudo apt edit-sources owncloud-client
```

Debian jessie以降，Rasbian jessie以降，Ubuntu vivid(15.04)以降でそれぞれ利用可能だと思う．

```
apt (0.9.13.1) unstable; urgency=low

  [ Colin Watson ]
  * fix "apt-get  --purge build-dep" (closes: #720597)
  * fix regression that APT::Keep-Fds is not honored (closes: #730490)

  [ Michael Vogt ]
  * add "-f" option to "build-dep" as sbuild is using it to fix
    regression with cross-building (LP: #1255806)
  * add autopkgtest support for the integration testsuite
  * merge mvo/feature/short-list
  * merge mvo/feature/edit-sources
  * fix segfault in pkgDepCache::SetCandidateRelease() (closes: #709560)
  * reset terminal on error (closes: #730795)
  * fix apport report writing (LP: #1254499)

 -- Michael Vogt <mvo@debian.org>  Fri, 29 Nov 2013 20:50:17 +0100
```
