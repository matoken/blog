kernel buildしようと現在のconfigをコピーしようとしたところ/boot以下にも/proc/config.gzも見当たらない．こういう場合`configs`というモジュールを読み込むと/proc/config.gzが出来るらしい．

```
$ ls -l /proc/config.gz
ls: cannot access /proc/config.gz: No such file or directory
$ ls -la /boot/config-*
ls: cannot access /boot/config-*: No such file or directory
$ sudo modprobe configs
$ lsmod|grep configs
configs                30516  0
$ ls -l /proc/config.gz
-r--r--r-- 1 root root 29305 Feb  4 18:34 /proc/config.gz
$ zcat /proc/config.gz > /usr/src/linux/.config
$ sudo rmmod configs
$ ls -l /proc/config.gz
ls: cannot access /proc/config.gz: No such file or directory
```

これはRasbina jessieで遭遇しました．
Debian stretch amd64でも試してみたところmoduleがありませんでした．でも普通に/boot/config-x.x.xがあるので困らないです．

```
$ sudo modprobe configs
modprobe: FATAL: Module configs not found in directory /lib/modules/4.3.0-1-amd64
```

menuconfigではこの辺みたいです．

```
General setup
  [*] Kernel .config support
    [*]   Enable access to .config through /proc/config.gz[*]   Enable access to .config through /proc/config.gz
```
