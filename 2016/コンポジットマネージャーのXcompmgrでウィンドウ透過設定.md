<!--
% コンポジットマネージャーのXcompmgrでウィンドウ透過設定
% Kenichiro MATOHARA
% 2016-03-20
-->

久々にMATE環境を使ってみています．
awesomeだとショートカットキーでウィンドウの配置を設定できるので簡単にウィンドウを並べることができるので動画を見ながら別のことをすると行ったことが簡単に出来ました．しかしMATEで同じことをしようとすると面倒．以前はGnome端末で背景を透過させていましたが機能がなくなっていました．

- [Bug 695371 – Transparent option disappears in 3.7.x](https://bugzilla.gnome.org/show_bug.cgi?id=695371 "Bug 695371 – Transparent option disappears in 3.7.x")

Gnome端末から無くなった後もMATE端末には透過機能が残っていましたがいつの間にかこちらからもなくなっていました．もう復活することは無さそうなので他の端末アプリを幾つか試してみましたが擬似透過しか見当たりません．

ということで今回は`Xcompmgr`を試してみました．

- [xorg/app/xcompmgr - Compositing manager using RENDER](https://cgit.freedesktop.org/xorg/app/xcompmgr/ "xorg/app/xcompmgr - Compositing manager using RENDER")

## 前提

xorgで`Composite`, `RENDER` が有効になっている必要がああります．以下OKの例．

```
$ xdpyinfo | grep -i "render\|composite"
    Composite
    RENDER
```

## 必要パッケージの導入

コンポジットマネージャーの`xcompmgr`と，透過の設定をする`transset`の含まれた`x11-apps`を導入します．

```
$ sudo apt install xcompmgr x11-apps
```

## コンポジットマネージャの起動

```
$ xcompmgr -c &
```

とか

```
$ xcompmgr -cf &
```

とか

```
$ xcompmgr -c -C -t-5 -l-5 -r4.2 -o.55 &
```

とかお好みで．

## transsetでの透過

`xcompmgr`が起動した状態で`transset`を使うとウィドウを透過させることが出来ます．
引数には0~1の値を設定します．0が完全に透明．1が不透明です．

```
$ transset 0.6
```

として実行するとマウスカーソルが+マークになり，クリックしたウィンドウが透過されます．
以下はブラウザフルスクリーンでニコ生を再生した上に透過設定したMATE端末を表示しています．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/25889930596/in/dateposted/" title="Screenshot at 2016-03-19 07:06:39"><img src="https://farm2.staticflickr.com/1716/25889930596_c40d59e552.jpg" width="500" height="281" alt="Screenshot at 2016-03-19 07:06:39"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

## 自動起動設定

毎回手動で起動&設定するのは面倒なのでこんな感じで自動起動設定をします．
コンポジットマネージャーは「システム」->「設定」>「ユーザ向け」->「自動起動するアプリ」から．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/25287113083/in/dateposted/" title="Screenshot at 2016-03-20 06:27:22"><img src="https://farm2.staticflickr.com/1559/25287113083_c27e565628.jpg" width="340" height="180" alt="Screenshot at 2016-03-20 06:27:22"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

MATE端末の透過設定は`~/.bashrc`に以下のように書いてbash起動時にMATE端末だったら`transset`を起動するようにしてみました．sleepを付けないとうまく動きませんでした．sleepの値は環境によって変わると思います．

```
$ tail -4 ~/.bashrc
case "$COLORTERM" in
    mate-terminal) sleep 0.3 && transset -a 0.6 > /dev/null;;
esac﻿

```


## ショートカット設定

MATE端末だけを透過設定する場合はここまでの設定で大丈夫ですが，他のウィンドウや透過度を変更するのにショートカット設定をしておくと便利そうです．

「システム」->「設定」->「ハードウェア」->「キーボードショートカット」で`transset`を登録してショートカットを設定します．

ここで設定した`transset`のオプションは`-a`はアクティブウィンドウの指定，`--inc/--dec`はインクリメント，デクリメントのオプションです．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/25796133982/in/dateposted/" title="Screenshot at 2016-03-20 07:48:08"><img src="https://farm2.staticflickr.com/1624/25796133982_919c267426.jpg" width="262" height="135" alt="Screenshot at 2016-03-20 07:48:08"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script><a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/25891009386/in/photostream/" title="Screenshot at 2016-03-20 07:48:03"><img src="https://farm2.staticflickr.com/1462/25891009386_30a9f95205.jpg" width="262" height="135" alt="Screenshot at 2016-03-20 07:48:03"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/25889930966/in/dateposted/" title="Screenshot at 2016-03-20 06:25:27"><img src="https://farm2.staticflickr.com/1571/25889930966_d197c74acc_n.jpg" width="320" height="179" alt="Screenshot at 2016-03-20 06:25:27"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

これで未設定のアプリで透過させたり，ちょっと透過度を調整したいと行った時にショートカットですぐ設定できるようになりました :)

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/25283118734/in/dateposted/" title="Screenshot at 2016-03-20 06:30:06"><img src="https://farm2.staticflickr.com/1489/25283118734_effbdb784a.jpg" width="500" height="281" alt="Screenshot at 2016-03-20 06:30:06"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
