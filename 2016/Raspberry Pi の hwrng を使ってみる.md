<!--
Raspberry Pi の hwrng を使ってみる
-->

## 通常の速度
```
pi@pizero:~ $ dd if=/dev/random of=/dev/null count=1
0+1 records in
0+1 records out
6 bytes (6 B) copied, 95.2255 s, 0.0 kB/s
```

## `bcm2708-rng` を読み込む．`/dev/hwrng` が出来る．
```
pi@pizero:~ $ sudo modprobe bcm2708-rng
pi@pizero:~ $ lsmod|grep rng
bcm2708_rng             1220  0 
pi@pizero:~ $ ls -l /dev/hwrng 
crw------- 1 root root 10, 183 Jan  8 06:05 /dev/hwrng
```

## `/dev/hwrng` を叩いてみる
```
pi@pizero:~ $ sudo dd if=/dev/hwrng bs=128 count=1 | od -xc
0000000    3152    748b    ca3a    ec6f    88bc    eb05    84f4    6e49
          R   1 213   t   : 312   o 354 274 210 005 353 364 204   I   n
0000020    9cad    a825    fa8c    a38e    6a96    6830    682a    b34d
        255 234   % 250 214 372 216 243 226   j   0   h   *   h   M 263
0000040    c540    461d    b614    3ff9    bff3    2b83    033b    4f06
          @ 305 035   F 024 266 371   ? 363 277 203   +   ; 003 006   O
0000060    3045    e6f3    71a5    5e19    544b    8906    44d2    5706
          E   0 363 346 245   q 031   ^   K   T 006 211 322   D 006   W
0000100    07c8    95b9    22d6    8414    6aff    5809    80b4    73d9
        310  \a 271 225 326   " 024 204 377   j  \t   X 264 200 331   s
0000120    9b5f    8093    023b    61a6    0169    0277    6bb5    8953
          _ 233 223 200   ; 002 246   a   i 001   w 002 265   k   S 211
0000140    db0d    a796    8a90    02f6    341e    6615    1265    6665
         \r 333 226 247 220 212 366 002 036   4 025   f   e 022   e   f
0000160    797e    45a8    5f81    8f6c    cc93    2fd7    cca4    c63e
          ~   y 250   E 201   _   l 217 223 314 327   / 244 314   > 306
1+0 records in
1+0 records out
128 bytes (128 B) copied, 0.0159928 s, 8.0 kB/s
0000200
pi@pizero:~ $ sudo dd if=/dev/hwrng of=/dev/null count=1
1+0 records in
1+0 records out
512 bytes (512 B) copied, 0.00742793 s, 68.9 kB/s
pi@pizero:~ $ sudo dd if=/dev/hwrng of=/dev/null count=1024
1024+0 records in
1024+0 records out
524288 bytes (524 kB) copied, 4.71214 s, 111 kB/s
pi@pizero:~ $ sudo dd if=/dev/hwrng of=/dev/null bs=1024 count=1024
1024+0 records in
1024+0 records out
1048576 bytes (1.0 MB) copied, 9.41426 s, 111 kB/s
```
/dev/hwrng が 111kB/s くらいで出てくるようです．

## `rng-tools` の導入

/dev/hwrng を /dev/random で使うために `rng-tools` を導入します．導入するとデーモンの rngd が動作するようになります．

```
pi@pizero:~ $ sudo apt install rng-tools
pi@pizero:~ $ ps -ef|grep rng
root     26025     1  0 20:45 ?        00:00:00 /usr/sbin/rngd -r /dev/hwrng
```

## `/dev/random` が速くなったのを確認

rngd が動いている状態で /dev/random を出力してみます．0.0 kB/s から 22kB/s くらいに大幅に速くなりました．

```
pi@pizero:~ $ dd if=/dev/random of=/dev/null count=1
0+1 records in
0+1 records out
88 bytes (88 B) copied, 0.00385096 s, 22.9 kB/s
pi@pizero:~ $ dd if=/dev/random of=/dev/null bs=1 count=1024
1024+0 records in
1024+0 records out
1024 bytes (1.0 kB) copied, 0.0697543 s, 14.7 kB/s
pi@pizero:~ $ dd if=/dev/random of=/dev/null bs=1 count=65536
65536+0 records in
65536+0 records out
65536 bytes (66 kB) copied, 2.94678 s, 22.2 kB/s
```

## 再起動後も有効に

このままだと次回起動時に `bcm2708-rng` モジュールが読み込まれないので自動的に読み込まれるようにしておきます．再起動後に `lsmod` で読み込まれていて `/dev/hwrng` が存在すればOKです．

```
pi@pizero:~ $ echo bcm2708-rng | sudo tee /etc/modules
bcm2708-rng
pi@pizero:~ $ tail -1 /etc/modules
bcm2708-rng
```

## 実利用

Raspberry Pi Zero だとかなりコンパクトです．`/dev/hwrng` を `/dev/ttyACM0` に繋げてUSB-TTLアダプタ経由でhwrng を渡せるようにしてちょっと大きなUSB hwrng ドングルが作れそうじゃないかなと思ったりしています．電源もUSBから取ればすっきりしますし．電源断に耐えられるようにしたりLEDなんかも付けてデバイスの状態を知らせる機能もほしいですね．



