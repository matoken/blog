<!--
Title: マルチプラットホーム&スライドに特化したMarkdown WriterのMarpを試す
Date: 2016-12-03 01:08:16
Modified: 2016-12-03 01:08:16
Tags: remote, Mumble, Etherpad
Category: 
Slug: 
Authors: matoken
Summary: 
-->

最近勉強会の発表資料はスライドにせずにMarkdownやAsciidocで書いてhtmlやpdfに書き出してました．これをSlideshareとかに置いているのですが，スライド形式に比べてアクセスが1桁少ないです．
Markdownでスライドにする方法はいろいろあるのですが，1ページの分量がまちまちになってはみ出したりと言ったことをよくやってしまいあまりつかっていませんでした．

そんなところにプレゼンテーション向けのMarkdown Writerがあったので試してみました．

- [Marp \- Markdown Presentation Writer](https://yhatt.github.io/marp/)

マルチプラットホームでElectron製です．PDFファイルへの書き出しも可能です．

雰囲気は以下の画像でだいたいわかるんじゃないかと思います．

![https://yhatt.github.io/marp/images/marp-cast.gif](https://yhatt.github.io/marp/images/marp-cast.gif)

左ペインでMarkdownを書いて右ページにプレビューが表示されます．
プレビューは3種類選べますが，"1:1 slide/Slide List"のどちらかだとスライド形式でプレビューが表示されるのでレイアウトを確認しながら書くことができます．

試しにちょっと書いてみました．
するとMarpのプレビュー，Evinceでは問題なかったのですが，SlideshareにPDFをアップロードすると背景画像やフッターが上下反転しています……．
試しにSpikerdeckにもアップロードしてみましたがこちらは問題なし．
恐らくSlideshareの問題ではないかと思います．

<iframe src="//www.slideshare.net/slideshow/embed_code/key/EurpyidW1LNACo" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/matoken/markdown-writermarp" title="スライド特化のMarkdown writerのMarpを試す" target="_blank">スライド特化のMarkdown writerのMarpを試す</a> </strong> from <strong><a target="_blank" href="//www.slideshare.net/matoken">Kenichiro MATOHARA</a></strong> </div>

Electron製ということで降るmのマシンでは重いかなと思っていたのですが，Core2Duoのマシンでも試してみましたが一気にスクロールとかしなければ重く感じるところはなく実用範囲内に感じました．
