USBGuardはそのままだとデーモン起動時に接続されていたデバイスは全て許可されるので不正なデバイスが起動時に既に取り付けられていた場合許可されてしまいます．
そこで手動で利用可能デバイスをallowで設定に，そしてそれ以外をblockにした．blockでなくrejectの方がセキュリティは上がるけど許可したい場合面倒なのでとりあえずブロックに．
これでallow設定されていないデバイスを繋ぐとblock．そのデバイスを利用したい場合は手動で*allow-device <id>*や*usbguard-applet-qt*でallowにすれば使えます．

※USBGuardは次のページの設定(ユーザ，グループ設定)が済んでいる前提です．

* [不正なUSBデバイスからPCを守るUSBGuardを試す | matoken's meme](http://matoken.org/blog/blog/2016/10/08/try-usbguard-to-protect-the-pc-from-unauthorized-usb-device/ "不正なUSBデバイスからPCを守るUSBGuardを試す | matoken&apos;s meme")


## ルールファイルを作成

まずは*generate-policy*で現在の状態をファイルに書き出して編集します．
ここでは内蔵カメラもblockにしています．

```
$ usbguard generate-policy > rules.conf
$ vi rules.conf
$ sudo cat /etc/usbguard/rules.conf
allow id 1d6b:0002 serial "0000:00:1a.0" name "EHCI Host Controller" hash "MwANH+QnAvclGgMNHjzanbOGkp3bPmwqoyAEZZ6QXTQ=" parent-hash "uvJm0y/N2iYeJgfKJsJqWKTJts/duhYZ7W2zzAYk7Y8=" with-interface 09:00:00
allow id 8087:0024 serial "" name "" hash "kv3v2+rnq9QvYI3/HbJ1EV9vdujZ0aVCQ/CGBYIkEB0=" parent-hash "MwANH+QnAvclGgMNHjzanbOGkp3bPmwqoyAEZZ6QXTQ=" via-port "3-1" with-interface 09:00:00
allow id 0a5c:21e6 serial "2016D8DA016E" name "BCM20702A0" hash "C4Os63DCRvIuWJYU/U+1PXrvWlXa2PmpRUQhp+C5eeE=" parent-hash "kv3v2+rnq9QvYI3/HbJ1EV9vdujZ0aVCQ/CGBYIkEB0=" with-interface { ff:01:01 ff:01:01 ff:01:01 ff:01:01 ff:01:01 ff:01:01 ff:01:01 ff:ff:ff fe:01:01 }
allow id 17ef:100a serial "" name "" hash "dMjTmGpj5dFGqH51kQpO/LVBQxE6JkwibVRJQkFCCuU=" parent-hash "kv3v2+rnq9QvYI3/HbJ1EV9vdujZ0aVCQ/CGBYIkEB0=" via-port "3-1.5" with-interface { 09:00:01 09:00:02 }
block id 04f2:b217 serial "" name "Integrated Camera" hash "BxFRAwzjkHO55cQGR8oMRm6bq+Ps2qQtU88jE1Uk5KE=" parent-hash "kv3v2+rnq9QvYI3/HbJ1EV9vdujZ0aVCQ/CGBYIkEB0=" via-port "3-1.6" with-interface { 0e:01:00 0e:02:00 0e:02:00 0e:02:00 0e:02:00 0e:02:00 0e:02:00 0e:02:00 0e:02:00 }
allow id 1d6b:0002 serial "0000:05:00.0" name "xHCI Host Controller" hash "IV7wk04gfQJink/IY4TiGVdcmTzuc09WcSe6k57kWrs=" parent-hash "3TIXKJ1dp4XFV6VxxWU11xbI0yLS0VmRZIaxdsLZDx4=" with-interface 09:00:00
allow id 1d6b:0003 serial "0000:05:00.0" name "xHCI Host Controller" hash "VlZK5oVuQQAlBH76Ekgc+KaZZDL0BAsF9tEf1ynb154=" parent-hash "3TIXKJ1dp4XFV6VxxWU11xbI0yLS0VmRZIaxdsLZDx4=" with-interface 09:00:00
allow id 1d6b:0002 serial "0000:00:1d.0" name "EHCI Host Controller" hash "WwvSEwd+7257rAqUGLMQjffF7zyqygmmLeQTYnR9QzQ=" parent-hash "FjkaT8Rp/Bh++KC4RQhk++hWack2wTDa1a1G5yXqYys=" with-interface 09:00:00
allow id 8087:0024 serial "" name "" hash "Zx7v0FMQEjScKSAFENAiobEs1OGPPB0YWR+yXDCVE04=" parent-hash "WwvSEwd+7257rAqUGLMQjffF7zyqygmmLeQTYnR9QzQ=" via-port "4-1" with-interface 09:00:00
block
```

## ルールファイルを反映して動作確認

設定ファイルをコピーしてデーモンを再起動して反映．

```
$ sudo install -m 0600 -o root -g root rules.conf /etc/usbguard/rules.conf
$ sudo systemctl restart usbguard
```

これでallow設定されていないUSBデバイスを接続した状態で起動してもblockに，新しいUSBデバイスを接続してもblockになるようになります．


## 新しいデバイスを手動で許可する

設定されていないデバイスを利用したい場合は以下のように*list-devices*でidを確認して*allow-device <id>*や*usbguard-applet-qt*でallowにして使えるようにします．

```
$ usbguard list-devices | grep block
15: block id 04f2:b217 serial "" name "Integrated Camera" hash "BxFRAwzjkHO55cQGR8oMRm6bq+Ps2qQtU88jE1Uk5KE=" parent-hash "kv3v2+rnq9QvYI3/HbJ1EV9vdujZ0aVCQ/CGBYIkEB0=" via-port "3-1.6" with-interface { 0e:01:00 0e:02:00 0e:02:00 0e:02:00 0e:02:00 0e:02:00 0e:02:00 0e:02:00 0e:02:00 }
20: block id 1004:631c serial "03a809c94b4befd4" name "LGE Android Phone" hash "0qSmncueL3SVg+z6yyVNMG2l+KxlAsMZWRfpPvCp0oU=" parent-hash "Zx7v0FMQEjScKSAFENAiobEs1OGPPB0YWR+yXDCVE04=" via-port "4-1.1" with-interface ff:ff:00
$ usbguard allow-device 20
```

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/29918228940/in/dateposted/" title="20161010_00:10:37-11962"><img src="https://c5.staticflickr.com/9/8417/29918228940_2aa0318acc_o.jpg" width="531" height="394" alt="20161010_00:10:37-11962"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

とりあえずこれで良さそうですが，たまにSSDを差し替えることがあります．そうすると前もって設定し直しておかないとデバイスが認識せず不味いことが起こりそうです．ということで設定ファイルにhostnameを付けてそれを使うようにしてみました．


## hostごとの設定ファイルを作る

起動時に設定ファイルは指定できますが，ルールファイルは指定できないようです．両方のファイルをホストごとに用意します．

```
$ sudo cp -p /etc/usbguard/usbguard-daemon.conf /etc/usbguard/usbguard-daemon-`hostname`.conf
$ sudo vi /etc/usbguard/usbguard-daemon-x220.conf
$ diff /etc/usbguard/usbguard-daemon.conf /etc/usbguard/usbguard-daemon-x220.conf
10c10
< RuleFile=/etc/usbguard/rules.conf
---
> RuleFile=/etc/usbguard/rules-x220.conf
$ sudo cp -p /etc/usbguard/rules.conf /etc/usbguard/rules-`hostname`.conf
```

## systemdの起動ファイルを修正してホスト名付きの設定ファイルを使うようにする

Systemdの設定ファイルを用意して設定ファイルをホスト名付きで呼ぶように修正しました．

```
$ sudo vi /etc/systemd/system/usbguard.service
$ cat /etc/systemd/system/usbguard.service
[Unit]
Description=USBGuard daemon
Wants=systemd-udevd.service local-fs.target
Documentation=man:usbguard-daemon(8)

[Service]
Type=simple
ExecStartPre=/bin/bash -c "/bin/systemctl set-environment hostname=$(/bin/hostname)"
ExecStart=/usr/sbin/usbguard-daemon -k -c /etc/usbguard/usbguard-daemon-${hostname}.conf
Restart=on-failure

[Install]
WantedBy=basic.target
```

起動し直して確認してみるとホスト名付きのファイルを呼んでいます．

```
$ sudo systemctl daemon-reload
$ sudo service usbguard restart
$ hostname
x220
$ ps -ef|grep -i usbguard-daemon
root     28545     1  0 00:05 ?        00:00:00 /usr/sbin/usbguard-daemon -k -c /etc/usbguard/usbguard-daemon-x220.conf
```

ちょっと面倒ですがとりあえずこれで行けそうです．

