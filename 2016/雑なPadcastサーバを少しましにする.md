<!--
雑なPadcastサーバを少しましにする
-->

先日作ったPodcastサーバですがあれからずっと動かしていますが安定して動いているようです．雑さをましにするために少し修正しました．

- [Raspberry Piで雑いpodcastサーバを作った | matoken's meme](http://matoken.org/blog/blog/2016/01/31/i-made-a-sloppy-podcast-server-in-the-raspberry-pi/ "Raspberry Piで雑いpodcastサーバを作った | matoken&apos;s meme")

プレイヤー部分はこんな感じで少し変わりました．

```
@reboot while true; do cvlc --ignore-config --extraintf=http --http-port=8081 --http-password='raspberry' ~/podcasts/todayspodcast.m3u --play-and-exit --norm-max-level=2.0 --sout='#transcode{acodec=mp3,ab=64,channels=1}:standard{access=http,mux=ts,dst=:8080}' ; done
40 * * * *      podracer ; cd ~/podcasts ; find . -mmin -1440 -type f -print0 | xargs -0n1 file | grep -i audio | cut -f1 -d: | xargs ls -1tr > ~/podcasts/todayspodcast.m3u
10 0 * * *  rm -rf ~/podcasts/`date --date '1 weeks ago' +\%F`
```

- killせずに1周毎にVLCを呼ぶようにしてプレイリスト更新
- Podcast取得を1時間に1度に変更
- ボリュームのノーマライズ
- ビットレートを64kにリアルタイムエンコーディング
- プレイリストの作成も1時間に一度
- プレイリストの順序を時間順にソート
- プレイリストの内容はプレイリスト作成時から過去1日分(1440分)
- 音泉の番組に対応(RSS化してpodracerに登録．簡単だけど公開すると怒られそうな気がする)
  - [インターネットラジオステーション＜音泉＞](http://www.onsen.ag/ "インターネットラジオステーション＜音泉＞")

## VLCのオプション

- `--ignore-config`  
設定を読み込みません
- `--extraintf=http`  
ウェブコントローラーを有効に
- `--http-port=8081`  
ウェブコントローラーの待ち受けポートを8081番に
- `--http-password='raspberry'`  
ウェブコントローラーの認証パスワードを`raspberry`に
- `~/podcasts/todayspodcast.m3u`  
再生ファイル(プレイリストを指定)
- `--play-and-exit`  
再生後終了する．これがないとプレイリストを再生後も起動しっぱなしになって次の周に入らない．(`vlc://quit`でも大丈夫だが，`vlc://quit`だとプレイリストにも表示される)
- `--norm-max-level=2.0`  
ボリュームのノーマライズ．この値はもう少しいじったほうがいいかも?
- `--sout='#transcode{acodec=mp3,ab=64,channels=1}`  
モノラル64kのmp3に変換している．Podcastを調べると殆どの番組が64kで一部40,96,128,160が混じっている．1日試したが32kでも大丈夫そう．
- `standard{access=http,mux=ts,dst=:8080}`  
8080番ポートでHTTPライブストリーミング配信

VLCのオプションはmanにはあまり載ってなくて困っていたのですが，`vlc -H`に大量のオプションが載っているのに気づいてからはかどりました．

```
$ vlc -h| tail -1
網羅的なヘルプを表示するためには、'-H'オプションを指定してください。
$ vlc -h | wc -l
372
$ vlc -H | wc -l
5674
```

その他細かいところで
- 設置場所の変更
- DNSに名前を登録して固定IPに(Androidでも名前でアクセス可能に)
- 合わせてプレイリスト等のurlを名前に
- hostnameの変更
- sshd設定
- iptable設定
- OSの自動更新設定
- watch dogの設定

といった辺りも設定しましたがこの辺は何時もの設定なので今回は省略．



と，こんな感じでだいたいやりたいことは出来たかなーという感じですが，VLCでなくMPD辺りで曲管理して再生，配信はまた別アプリでやったほうがいいような気もしてきました．VLCは1つで何でもできるしウェブコントローラーもあっていいのですが，MPDの方がリモート管理がしやすそうなので気が向いたらそっちも組むかもです．

今100円ショップのイヤホンを使って聞いているのでその部分が一番ストレスな感じです．
