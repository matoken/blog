<!--
gdm3とlightdmでログイン時のユーザを表示しないようにする
-->

## gdm3

先ずはgdm3から．
環境は，`Debian stretch testing gdm3 3.18.0-2`です．

`/etc/gdm3/greeter.dconf-defaults`を編集して，`[org/gnome/login-screen]`セクションの`disable-user-list=true`のコメントを外して有効にする．

```
[org/gnome/login-screen]
disable-user-list=true
```

その後gdm3を再起動．

```
$ sudo systemctl stop gdm
$ sudo systemctl start gdm
```

＃初め`/etc/gdm3/greeter.dconf-defaults`はテンプレートなのかと思って`/etc/gdm3/greeter.dconf`にコピーして編集して反映されないなーとやっていましたorz


## LightDM

LightDMの場合．
環境は，`Debian stretch testing lightdm 1.16.6-1`です．

`/etc/lightdm/lightdm.conf`の`[LightDM]`セクションの`greeter-hide-users=true`のコメントを外して有効にする．

```
greeter-hide-users=true
```

`--show-config`オプションで設定が確認できます．

```
$ /usr/sbin/lightdm --show-config
   [Seat:*]
A  greeter-session=lightdm-greeter
A  greeter-hide-users=true
A  session-wrapper=/etc/X11/Xsession
B  user-session=kde-plasma-kf5

   [LightDM]
C  greeter-hide-users=True
C  allow-guest=False

   [XDMCPServer]
C  enabled=true

Sources:
A  /usr/share/lightdm/lightdm.conf.d/01_debian.conf
B  /usr/share/lightdm/lightdm.conf.d/40-kde-plasma-kf5.conf
C  /etc/lightdm/lightdm.conf
```

その後LightDMの再起動．

```
$ sudo systemctl stop lightdm
$ sudo systemctl start lightdm
```


## ディスプレイマネージャの切り替え

`dpkg-reconfigure`に導入済みのディスプレイマネージャのパッケージ名(どれでも)を付けて起動すると，導入済みのディプレイマネージャから選択できます．

```
$ sudo dpkg-reconfigure gdm3
 ┌─────────────────────────────────────────────────────────────────────┤ gdm3 を設定しています ├─────────────────────────────────────────────────────────────────────┐  
 │ ディスプレイマネージャとは、X Window System 上でのグラフィカルなログイン機能を提供するものです。                                                                  │  
 │                                                                                                                                                                   │  
 │ ひとつの X サーバを管理できるのはひとつのディスプレイマネージャだけですが、ディスプレイマネージャパッケージが複数インストールされています。どのディスプレイマネ   │  
 │ ージャをデフォルトで起動させるか選択して下さい。                                                                                                                  │  
 │                                                                                                                                                                   │  
 │ 異なるサーバを担当するように設定すれば、複数のディスプレイサーバは同時に動作できます。そのようにするには、/etc/init.d にある各ディスプレイマネージャの初期化スク  │  
 │ リプトを編集し、デフォルトディスプレイマネージャのチェックを無効にして下さい。                                                                                    │  
 │                                                                                                                                                                   │  
 │ デフォルトのディスプレイマネージャ:                                                                                                                               │  
 │                                                                                                                                                                   │  
 │                                                                              gdm3                                                                                 │  
 │                                                                              lightdm                                                                              │  
 │                                                                              sddm                                                                                 │  
 │                                                                                                                                                                   │  
 │                                                                                                                                                                   │  
 │                                                                              <了解>                                                                               │  
 │                                                                                                                                                                   │  
 └───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘  
                                                                                                                                                                        
```