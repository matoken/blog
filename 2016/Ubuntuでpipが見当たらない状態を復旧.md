# Ubuntuでpipが見当たらない状態を復旧.

鹿児島らぐのオフ会で @seoponさんからpipで*janome*入れてみましょうって時に，以前*sudo hogehoge*してからpip使えなくなっちゃって方が．
見てみるとpkgは入っているけどpipコマンド自体がいなくなってる感じみたいでした．恐らくオペミスで消したかパスのないところに移動しちゃった?
(以下は多分こんなだったというのを再現．)

```
$ pip
プログラム 'pip' はまだインストールされていません。 次のように入力することでインストールできます:
sudo apt install python-pip
$ sudo apt install python-pip
パッケージリストを読み込んでいます... 完了
依存関係ツリーを作成しています
状態情報を読み取っています... 完了
python-pip はすでに最新バージョン (8.1.1-2ubuntu0.2) です。
アップグレード: 0 個、新規インストール: 0 個、削除: 0 個、保留: 0 個。
$ which pip
$ dpkg -L python-pip | grep \/pip$
/usr/bin/pip
/usr/lib/python2.7/dist-packages/pip
$ ls -l /usr/bin/pip
ls: '/usr/bin/pip' にアクセスできません: そのようなファイルやディレクトリはありません
```

pkgをreinstallして解決しました．

```
$ sudo apt install --reinstall python-pip
パッケージリストを読み込んでいます... 完了
依存関係ツリーを作成しています
状態情報を読み取っています... 完了
アップグレード: 0 個、新規インストール: 0 個、再インストール: 1 個、削除: 0 個、保留: 0 個。
144 kB のアーカイブを取得する必要があります。
この操作後に追加で 0 B のディスク容量が消費されます。
取得:1 http://jp.archive.ubuntu.com/ubuntu xenial-updates/universe amd64 python-pip all 8.1.1-2ubuntu0.2 [144 kB]
144 kB を 0秒 で取得しました (571 kB/s)
(データベースを読み込んでいます ... 現在 902816 個のファイルとディレクトリがインストールされています。)
.../python-pip_8.1.1-2ubuntu0.2_all.deb を展開する準備をしています ...
python-pip (8.1.1-2ubuntu0.2) で (8.1.1-2ubuntu0.2 に) 上書き展開しています ...
man-db (2.7.5-1) のトリガを処理しています ...
python-pip (8.1.1-2ubuntu0.2) を設定しています ...
mk@x201s:~$ !ls
ls -l /usr/bin/pip
-rwxr-xr-x 1 root root 292  8月 12 06:59 /usr/bin/pip
$ which pip
/usr/bin/pip
$ pip -V
pip 8.1.2 from /usr/local/lib/python2.7/dist-packages (python 2.7)
```

とりあえず．


