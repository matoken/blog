<!--
GNU xargsの区切り文字を指定できる-d optionが便利
-->

よくスペース混じりのファイルなんかの処理に`find -print0`や`xargs -0`でNULL区切り文字を使いますが，

```
$ find . -type f | xargs ls
ls: ./ho にアクセスできません: そのようなファイルやディレクトリはありません
ls: ge にアクセスできません: そのようなファイルやディレクトリはありません
./fuga  ./piyo
$ find . -type f -print0 | xargs -0 ls
./fuga  ./ho ge  ./piyo
```

今回間に更に処理を入れて使えませんでした．
逐次実行でよかったら`xargs -n1 -I{} rm '{}'`みたいにして括ればいいんですが，

```
$ find . -type f -print | grep -v fuga | xargs -n1 -I{} ls '{}'
./ho ge
./piyo
```

今回は一度に処理したかったのでこの方法は使えません．xargsのmanを見るとdelimiterを指定できるのに気づきました．

>
       --delimiter=delim, -d delim
              Input  items are terminated by the specified character.  The specified delimiter may be a single character, a C-style character escape such as \n, or
              an octal or hexadecimal escape code.  Octal and hexadecimal escape codes are understood as for the printf command.    Multibyte  characters  are  not
              supported.  When processing the input, quotes and backslash are not special; every character in the input is taken literally.  The -d option disables
              any end-of-file string, which is treated like any other argument.  You can use this option when the input consists of simply newline-separated items,
              although it is almost always better to design your program to use --null where this is possible.


ということで改行(`\n`)を指定してやりたかったことが実現できました :)

```
$ find . -mmin -1440 -type f -print0 | xargs -0n1 file | grep -i audio | cut -f1 -d: | xargs -d\\n ls -1tr
```

<iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00TQMFOCW" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe><iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00PID9DWA" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe><iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=9888381407" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>
