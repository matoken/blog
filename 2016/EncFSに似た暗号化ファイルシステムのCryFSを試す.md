<!--
EncFSに似た暗号化ファイルシステムのCryFSを試す.md
-->

EncFSのMLで見かけたのですが，EncFSに似た暗号化FSのCryFSというものがあるようです．

- [CryFS: A cryptographic filesystem for the cloud](https://www.cryfs.org/ "CryFS: A cryptographic filesystem for the cloud")
  - [cryfs/cryfs: Cryptographic filesystem for the cloud](https://github.com/cryfs/cryfs "cryfs/cryfs: Cryptographic filesystem for the cloud")

EncFSは平文のファイルと暗号化ファイルが一対一で対応づいているしタイムスタンプやパーミッションも引き継いでいるので推測されやすいという問題があります．CryFSはその辺りも隠蔽できるようです．
面白そうなので少し試してみました．

初めDebian stretchに導入しようと思ったのですが，jessieのapt-lineは用意されているのですが，これをstretchに導入しようとするとバージョンチェックで対応してないと言われ，sourceから導入しようと思ったら`bii`という恐らくstretchのpkgに存在しないコマンドを要求されるので手っ取り早くUbuntu 14.04 LTSで試しました．

```
$ linuxlogo -L ubuntu$ linuxlogo -L ubuntu

              .-.
        .-'``(|||)
     ,`\ \    `-`.                 88                         88
    /   \ '``-.   `                88                         88
  .-.  ,       `___:      88   88  88,888,  88   88  ,88888, 88888  88   88
 (:::) :        ___       88   88  88   88  88   88  88   88  88    88   88
  `-`  `       ,   :      88   88  88   88  88   88  88   88  88    88   88
    \   / ,..-`   ,       88   88  88   88  88   88  88   88  88    88   88
     `./ /    .-.`        '88888'  '88888'  '88888'  88   88  '8888 '88888'
        `-..-(   )
              `-`


Linux Version 3.13.0-77-generic, Compiled #121-Ubuntu SMP Wed Jan 20 10:50:42 UTC 2016
      Two 800MHz AMD Athlon Processors, 7.9GB RAM, 5191.48 Bogomips Total
                                     micro
```
＃screenfetchがpkgに無いな

## 導入

このscriptで鍵やapt-lineの設定とCryFSの導入が行われます．
```
$ wget -O - https://www.cryfs.org/install.sh | sudo bash
```

```
$ cryfs -h
CryFS Version 0.8.5
WARNING! This version is not considered stable. Please backup your data frequently!

Usage: cryfs [options] rootDir mountPoint [-- [FUSE Mount Options]]

Allowed options:
  -h [ --help ]          show help message
  -c [ --config ] arg    Configuration file
  -f [ --foreground ]    Run CryFS in foreground.
  --cipher arg           Cipher to use for encryption. See possible values by
                         calling cryfs with --show-ciphers
  --show-ciphers         Show list of supported ciphers.
  --unmount-idle arg     Automatically unmount after specified number of idle
                         minutes.
  --extpass arg          External program to use for password input
  --logfile arg          Specify the file to write log messages to. If this is
                         not specified, log messages will go to stdout, or
                         syslog if CryFS is running in the background.

```

## 利用例

基本的な使い方はEncFSと同じ感じです．初回起動時は簡単な設定が必要．
```
$ mkdir encdir
$ mkdir mnt
$ cryfs encdir mnt
CryFS Version 0.8.5
WARNING! This version is not considered stable. Please backup your data frequently!

Use default settings?
Your choice [y/n]: y

Generating secure encryption key...done
Password:
Confirm Password:
Creating config file...done

Mounting filesystem. To unmount, call:
```

アンマウントはfuseなのでfusermount -uで
```
$ fusermount -u "/tmp/mnt"
$ ls -lA encdir mnt
encdir:
合計 40
-rw-rw-r-- 1 mk mk 32816  2月  7 04:08 0C2B03AEBC6D01C0AAB861907CE361A6
-rw-rw-r-- 1 mk mk  1134  2月  7 04:08 cryfs.config

mnt:
合計 0
```

`cryfs.config`という設定ファイルが作成されます

```
$ file encdir/cryfs.config
encdir/cryfs.config: data
$ od -xc encdir/cryfs.config|head
0000000    7263    6679    2e73    6f63    666e    6769    303b    733b
          c   r   y   f   s   .   c   o   n   f   i   g   ;   0   ;   s
0000020    7263    7079    0074    0000    0008    0000    0000    0001
          c   r   y   p   t  \0  \0  \0  \b  \0  \0  \0  \0  \0 001  \0
0000040    0000    0001    0000    0020    0000    0000    0000    a86f
         \0  \0 001  \0  \0  \0      \0  \0  \0  \0  \0  \0  \0   o 250
0000060    eac9    f71c    a592    8034    2f04    a2cb    c19e    78db
        311 352 034 367 222 245   4 200 004   / 313 242 236 301 333   x
0000100    e526    e951    1548    6c59    8a1e    beaf    db19    c7ff
          & 345   Q 351   H 025   Y   l 036 212 257 276 031 333 377 307
```

タイムスタンプは残らないようです．これはちょっと困る．

```
$ cryfs encdir mnt
$ cd mnt
$ touch a b c d e f
touch: `a' のタイムスタンプを設定中です: サポートされていない操作です
touch: `b' のタイムスタンプを設定中です: サポートされていない操作です
touch: `c' のタイムスタンプを設定中です: サポートされていない操作です
touch: `d' のタイムスタンプを設定中です: サポートされていない操作です
touch: `e' のタイムスタンプを設定中です: サポートされていない操作です
touch: `f' のタイムスタンプを設定中です: サポートされていない操作です
$ ls -lA
合計 0
-rw-rw-r-- 1 mk mk 0  1月  1  1970 a
-rw-rw-r-- 1 mk mk 0  1月  1  1970 b
-rw-rw-r-- 1 mk mk 0  1月  1  1970 c
-rw-rw-r-- 1 mk mk 0  1月  1  1970 d
-rw-rw-r-- 1 mk mk 0  1月  1  1970 e
-rw-rw-r-- 1 mk mk 0  1月  1  1970 f
```

10MBのファイルを作ってみると32kくらいで分割されたファイル群になっているようです．

```
$ dd if=/dev/zero of=mnt/10M.dd bs=1M count=10
$ ls -lA encdir mnt

encdir:
合計 11848
-rw-rw-r-- 1 mk mk 32816  2月  7 04:12 0015B9E50D707A660AC59BF6ABA4588B
-rw-rw-r-- 1 mk mk 32816  2月  7 04:12 00AB6551CD86FE4A5129ED330C86B7ED
-rw-rw-r-- 1 mk mk 32816  2月  7 04:12 00BBCEFD0A4150AD15842F356E184F94
-rw-rw-r-- 1 mk mk 32816  2月  7 04:12 00F8B1CCE5770E2D3DC4C47FA0583B80
-rw-rw-r-- 1 mk mk 32816  2月  7 04:12 048860D118BFD0F43A86B8F858456965
-rw-rw-r-- 1 mk mk 32816  2月  7 04:12 049D06A30EF07D80988B6D948E72250A
-rw-rw-r-- 1 mk mk 32816  2月  7 04:12 04FF722DB4ADFBD7F567966EC1244BD1
-rw-rw-r-- 1 mk mk 32816  2月  7 04:12 0599D02B12B1C9EAB554525482531D5F
-rw-rw-r-- 1 mk mk 32816  2月  7 04:12 05D56C792B76894E7194FB28A02F0FAB
-rw-rw-r-- 1 mk mk 32816  2月  7 04:12 06048941FF5F7E185BE3372400580A2C
-rw-rw-r-- 1 mk mk 32816  2月  7 04:12 077FE2E17E29826C6AB407578C5312CB
〜中略〜
-rw-rw-r-- 1 mk mk 32816  2月  7 04:12 FE5879AFA516A816008C9ED4AEB847B5
-rw-rw-r-- 1 mk mk 32816  2月  7 04:12 FE6BB5A582CF60F7329645CF3FAB365B
-rw-rw-r-- 1 mk mk 32816  2月  7 04:12 FFD97AF585CD8DEE23C722076FF8CB94
-rw-rw-r-- 1 mk mk 32816  2月  7 04:12 FFFA9CCA10C0F7A455DA18B109024AA7
-rw-rw-r-- 1 mk mk  1134  2月  7 04:08 cryfs.config

mnt:
合計 10240
-rw-rw-r-- 1 mk mk 10485760  1月  1  1970 10M.dd
-rw-rw-r-- 1 mk mk        0  1月  1  1970 a
-rw-rw-r-- 1 mk mk        0  1月  1  1970 b
-rw-rw-r-- 1 mk mk        0  1月  1  1970 c
-rw-rw-r-- 1 mk mk        0  1月  1  1970 d
-rw-rw-r-- 1 mk mk        0  1月  1  1970 e
-rw-rw-r-- 1 mk mk        0  1月  1  1970 f
$ rm mnt/10M.dd
```

ディレクトリを掘ってもファイルとして保存されるので覗かれてもわかりません．

```
$ mkdir mnt/dir
$ echo hoge > mnt/dir/hoge
$ ls -lA  encdir/
合計 328
-rw-rw-r-- 1 mk mk 32816  2月  7 04:11 0A332D1A5E0B5C36C9FBCEAB81E6320A
-rw-rw-r-- 1 mk mk 32816  2月  7 04:38 0C2B03AEBC6D01C0AAB861907CE361A6
-rw-rw-r-- 1 mk mk 32816  2月  7 04:11 40C765A5F1D681FFAEC781502836F444
-rw-rw-r-- 1 mk mk 32816  2月  7 04:11 64CF16B73827485463657F2A6928346D
-rw-rw-r-- 1 mk mk 32816  2月  7 04:11 78AA36DEE59FE330861A6D9B1218B16E
-rw-rw-r-- 1 mk mk 32816  2月  7 04:40 A9A48DEB85E2619C2ECBF7490BF3F8C9
-rw-rw-r-- 1 mk mk 32816  2月  7 04:11 DB99D4AAAAA21302B400DA1F9E370EA4
-rw-rw-r-- 1 mk mk 32816  2月  7 04:40 E5FA0435A166231035B54AC0E4723D83
-rw-rw-r-- 1 mk mk 32816  2月  7 04:11 F9589623B409B2142141F79614A865BF
-rw-rw-r-- 1 mk mk  1134  2月  7 04:08 cryfs.config
$ rm mnt/*
rm: `mnt/dir' を削除できません: ディレクトリです
$ ls -lA  encdir
合計 112
-rw-rw-r-- 1 mk mk 32816  2月  7 04:41 0C2B03AEBC6D01C0AAB861907CE361A6
-rw-rw-r-- 1 mk mk 32816  2月  7 04:40 A9A48DEB85E2619C2ECBF7490BF3F8C9
-rw-rw-r-- 1 mk mk 32816  2月  7 04:40 E5FA0435A166231035B54AC0E4723D83
-rw-rw-r-- 1 mk mk  1134  2月  7 04:08 cryfs.config
```

EncFSはファイル名自体にファイル名のメタデータを含むので利用できるファイル名長が短くなりますが，CryFSだとファイル中にメタデータを含むので元ファイルシステムの最大長のファイル名が利用できるようです．
```
$ touch mnt/012345678890123456788901234567889012345678890123456788901234567889012345678890123456788901234567889012345678890123456788901234567889012345678890123456788901234567889012345678890123456788901234567889012345678890123456788901234567889012345678890123456789012
$ ls -lA encdir
合計 40
-rw-rw-r-- 1 mk mk 32816  2月  7 04:51 0C2B03AEBC6D01C0AAB861907CE361A6
-rw-rw-r-- 1 mk mk  1134  2月  7 04:08 cryfs.config
```

パーミッションの変更やオーナーやグループを変更してもメタデータ内に吸収されるようです．

```
$ chmod 777 mnt/012345678890123456788901234567889012345678890123456788901234567889012345678890123456788901234567889012345678890123456788901234567889012345678890123456788901234567889012345678890123456788901234567889012345678890123456788901234567889012345678890123456789012
$ sudo chown www-data.www-data mnt/012345678890123456788901234567889012345678890123456788901234567889012345678890123456788901234567889012345678890123456788901234567889012345678890123456788901234567889012345678890123456788901234567889012345678890123456788901234567889012345678890123456789012
$ ls -lA encdir mnt
encdir:
合計 40
-rw-rw-r-- 1 mk mk 32816  2月  7 04:51 0C2B03AEBC6D01C0AAB861907CE361A6
-rw-rw-r-- 1 mk mk  1134  2月  7 04:08 cryfs.config

mnt:
合計 0
-rwxrwxrwx 1 www-data www-data 0  2月  7 04:53 012345678890123456788901234567889012345678890123456788901234567889012345678890123456788901234567889012345678890123456788901234567889012345678890123456788901234567889012345678890123456788901234567889012345678890123456788901234567889012345678890123456789012
```

オプションを少し見てみます．
`--show-ciphers`で利用できる暗号が確認できます．規定値では`aes-256-gcm`のようです．

```
$ fusermount -u mnt
$ ls -lA mnt
合計 0
$ cryfs --show-ciphers
CryFS Version 0.8.5
WARNING! This version is not considered stable. Please backup your data frequently!

aes-256-gcm
aes-256-cfb
aes-128-gcm
aes-128-cfb
twofish-256-gcm
twofish-256-cfb
twofish-128-gcm
twofish-128-cfb
serpent-256-gcm
serpent-256-cfb
serpent-128-gcm
serpent-128-cfb
cast-256-gcm
cast-256-cfb
mars-448-gcm
mars-448-cfb
mars-256-gcm
mars-256-cfb
mars-128-gcm
mars-128-cfb
```

`--extpass`オプションで外部のプログラムからパスワードが受け取れます．

```
$ cat << __EOF__ > pass
#!/bin/bash
echo 'passwd'
__EOF__
$ chmod +x pass
$ ./pass
passwd
$ cryfs --extpass ./pass encdir mnt
CryFS Version 0.8.5
WARNING! This version is not considered stable. Please backup your data frequently!

Loading config file...done

Mounting filesystem. To unmount, call:
$ fusermount -u "/tmp/mnt"

```

`--unmount-idle`で一定時間利用されていない時に自動アンマウントされます．

```
$ cryfs --extpass ./pass --unmount-idle 1 encdir mnt
```

1分後

```
$ ls -l mnt
合計 0
```

今のところベータで未実装の昨日もありますし，こういう怖いメッセージも出力される状態です．

> `WARNING! This version is not considered stable. Please backup your data frequently!`

＃そういえば数年前nilfs2もmount時に似たような警告出してましたね
```
mount.nilfs2: WARNING! - The NILFS on-disk format may change at any time.
mount.nilfs2: WARNING! - Do not place critical data on a NILFS filesystem.
```
今はもう何も言われない :)
```
[1109102.058384] NILFS version 2 loaded
[1109102.077659] segctord starting. Construction interval = 5 seconds, CP frequency < 30 seconds
```

実装が済んで安定すればEncFSの競合として便利に使えそうな感じです．
興味のある方はCryFSのページのフォームでメールアドレスを登録しておくといいかもしれません．
> Let us notify you when CryFS is stable!

- [CryFS: A cryptographic filesystem for the cloud](https://www.cryfs.org/ "CryFS: A cryptographic filesystem for the cloud")
