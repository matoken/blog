<!--
CLI な podcast aggregator/downloader な podracer を試してみる
-->

podcast aggregator に `rhythmbox` を利用しているのですが，たまにファイルが存在するのに何度やってもダウンロードに失敗したり NotePC で動かしているので容量の問題で古いのをファイルサーバに持って行ったりといった処理が面倒です．
CIL で動くものがあればファイルサーバで入手するようにして新しい物だけ手元に or ファイルサーバ経由で視聴するようにしら便利かもということで CLI で動く podcast aggregator が無いか探してみました．

```
$ apt-cache search podcast aggregator
gpodder - podcast client and feed aggregator
podracer - podcast aggregator/downloader
```

`gpodder` は GUI と CLI のセットで，`podracer` は CLI のみのようです．今回は `podracer` を試してみます．

## 導入

導入は apt 一発です．

```
$ sudo apt install podracer
```

## 設定ファイルの用意

`~/.podracer/subscriptions` というファイルに入手したいフィードを１フィード１行で書いておきます．`#` 行はコメントになるようです．

```~/.podracer/subscriptions
# 電脳空間カウボーイズ
http://feeds.feedburner.com/weblogs/csc
# backspace.fm
http://feeds.backspace.fm/backspacefm
：
```

## 実行

podracer コマンドを実行すると，`~/.podracer/subscriptions` からpodcast をダウンドードしてくれます．ダウンロードが完了すると自動的に終了します．

```
$ podracer
```

初回実行時には過去のものも入手するので時間が掛かるでしょう．特に画面には何も表示されないのでログファイル( `~/.podracer/podcast.log` ) を確認するといいと思います．

```
$ tail -f ~/.podracer/podcast.log
```

取得した podcast は `~/podcasts` 以下の日付ファイルの下に保存されます．この日付は実行日時です．

```
$ ls ~/podcasts
2016-01-27  2016-01-28
$ ls -lA ~/podcasts/2016-01-28
合計 59408
-rw-r--r-- 1 mk mk        0  1月 28 14:03 2016-01-28-podcasts.m3u
-rw-r--r-- 1 mk mk 60833627  1月 28 14:02 jlEV91AshKrq
$ file  ~/podcasts/2016-01-28/*
/home/mk/podcasts/2016-01-28/2016-01-28-podcasts.m3u: empty
/home/mk/podcasts/2016-01-28/jlEV91AshKrq:            Audio file with ID3 version 2.3.0
```

後は各種プレイヤーで再生できます．

```
$ mplayer ~/podcasts/2016-01-28/jlEV91AshKrq
```

ファイル名は podcast 提供者の物のままで同じディレクトリに格納されてしまうので名前が衝突したら多分新しい方で上書きされてしまいます．それとこのままではどのファイルがどの podcast なのかとかわからないですね．オプションも特に無いようなので沢山の podcast を保管しようという用途で使うのには向いていないかもしれません．
毎日実行して今日の分をまとめて再生して消してというように使う感じなのかもしれません．


## 追記)プレイリストの不具合?

`podracer` 実行時に `2016-01-28-podcasts.m3u` のようなプレイリストが作られますが，`mp3/ogg/m4a/m4b` 以外の拡張子は登録されません．こんな感じで作り直すと良さそう．
```
$ ls -1A ~/podcasts/2016-01-28 | grep -v \.m3u$ > ~/podcasts/2016-01-28/2016-01-28-podcasts.m3u
```

この方がいいかな
```
$ ls -1A ~/podcasts/`date +%F` | grep -v \.m3u$ > ~/podcasts/`date +%F`/`date +%F`-podcasts.m3u
```

`podracer` 自体を修正してもいいかも．~~雑いけどこんなとか．~~
これだとサムネイル画像なども含まれてしまう．
```
$ diff -u /usr/bin/podracer podracer
--- /usr/bin/podracer   2015-07-04 22:39:26.000000000 +0900
+++ podracer    2016-01-28 18:34:40.317391930 +0900
@@ -415,10 +415,7 @@
                then
                (
                cd "$poddir"
-               find . -name \*.mp3 > "$m3u"
-               find . -name \*.ogg  >> "$m3u"
-               find . -name \*.m4a  >> "$m3u"
-               find . -name \*.m4b  >> "$m3u"
+               find . -name \* -type f | grep -v "$m3u" > "$m3u"
                )
        fi
 fi
```


追記2)  
上の修正だとサムネイル画像なども登録されてしまう．
以下はその他のファイルを file command で調べて audio という文字列が入っていたら登録するもの．
```
$ diff -u /usr/bin/podracer ./podracer
--- /usr/bin/podracer   2015-07-04 22:39:26.000000000 +0900
+++ ./podracer  2016-01-28 19:12:31.980446668 +0900
@@ -419,6 +419,7 @@
                find . -name \*.ogg  >> "$m3u"
                find . -name \*.m4a  >> "$m3u"
                find . -name \*.m4b  >> "$m3u"
+               find . | egrep -v "\.mp3$|\.ogg$|\.m4a$|\.m4b$" | xargs file | grep -i audio | cut -f1 -d: >> "$m3u"
                )
        fi
 fi
```

- [Podracer / Bugs](http://sourceforge.net/p/podracer/bugs/)
- [Bugs in package podracer (version 1.4-3) in unstable -- Debian Bug report logs](https://bugs.debian.org/cgi-bin/pkgreport.cgi?pkg=podracer;dist=unstable)
  - [#812980 - podracer: .mp3/.ogg/.m4a/.m4b than the extension of the file will not be registered in the play list. - Debian Bug report logs](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=812980 "#812980 - podracer: .mp3/.ogg/.m4a/.m4b than the extension of the file will not be registered in the play list. - Debian Bug report logs")
