<!--
GNU coreutilsのmktempを試す
-->


あるscriptを見ていたら`mktemp`というコマンドを見かけました．GNU coreutilsの中の1つのようです．ユニークなテンポラリファイルやディレクトリが作成できるようです．

同じようなことをやるのにこれまではこんな感じで年月日やエポックにPIDを付けたりしてました><

```
$ TMP_FILE=`date +hoge-%F_%T-$$`
$ touch $TMP_FILE
$ ls -l $TMP_FILE
-rw-r--r-- 1 mk mk 0  2月 14 18:37 hoge-2016-02-14_18:36:59-13299
$ TMP_FILE=`date +hoge-%s-$$`
$ touch $TMP_FILE
$ ls -l $TMP_FILE
-rw-r--r-- 1 mk mk 0  2月 14 18:50 hoge-1455443448-13299
```

`mktemp`の場合は規定値で10桁のランダムな文字が付くようです．更にファイル存在チェックもしているので衝突も起こらないという感じみたいです．

```
$ mktemp
/tmp/tmp.E66dzdpxMa
$ ls -l /tmp/tmp.E66dzdpxMa
-rw------- 1 mk mk 0  2月 14 18:25 /tmp/tmp.E66dzdpxMa
```

`-d`オプションでディレクトリが作成できます．

```
$ mktemp -d
/tmp/tmp.EFmqt7L0nY
$ ls -la /tmp/tmp.EFmqt7L0nY
合計 0
drwx------ 1 mk   mk      0  2月 14 18:27 .
drwxrwxrwt 1 root root 2920  2月 14 18:27 ..
```

ファイル名のテンプレートもカスタマイズできます．`X`がランダム部になります．3文字以上無いと怒られます．
ランダム部のX部分が複数あると最後尾がランダムになるようです

```
$ mktemp XXX
zfG
$ mktemp XX
mktemp: テンプレート `hoge-XX' に含まれている X の数が少なすぎます
$ mktemp hoge-XXX
hoge-60h
$ mktemp hoge-XXXXXXXXXXXXXXXXXXXXXXXXXXXX
hoge-26CXkffROkyzwuUhej3Lza4KcdbC
$ mktemp XXX-XXX
XXX-m01
$ mktemp XXX-XXX-XXX
XXX-XXX-wqd
```

拡張子の指定は`--suffix`かテンプレートで指定できます．

```
$ mktemp --suffix=.log hoge-XXXX
hoge-LqWm.log
$ mktemp hoge-XXXX.log
hoge-lxtg.log
```

`-u`オプションでドライランです．でも存在チェックのタイミングがずれるのでこのファイル名を使うのは安全ではありません．

```
$ mktemp -u
/tmp/tmp.hdiPeJef1s
$ ls -l /tmp/tmp.hdiPeJef1s
ls: /tmp/tmp.hdiPeJef1s にアクセスできません: そのようなファイルやディレクトリはありません
```

`-p`オプションで作成されるディレクトリの変更が出来ます．(規定値は`/tmp`)  
存在しないディレクトリを指定すると怒られるようです．

```
$ mktemp -p $HOME/tmp
/home/mk/tmp/tmp.dlx75Z6Wyn
$ mktemp -p `mktemp -u -d -p $HOME/tmp`
mktemp: テンプレート `/home/mk/tmp/tmp.jhGdkmp5SG/tmp.XXXXXXXXXX' からファイルを作成できません: そのようなファイルやディレクトリはありません
```

mktemp(3)もあるよう．

詳細は`man mktemp`, `info mktemp`を．


しかし`GNU coreutils`みたいな基本的なコマンドでさえ知らない使い方のわからないコマンドが結構あるのでダメですねorz  
調べなければ……
```
$ dpkg -L coreutils | grep bin/
/usr/sbin/chroot
/usr/bin/hostid
/usr/bin/link
/usr/bin/printf
/usr/bin/truncate
/usr/bin/pr
/usr/bin/sha1sum
/usr/bin/nice
/usr/bin/tee
/usr/bin/realpath
/usr/bin/tac
/usr/bin/printenv
/usr/bin/arch
/usr/bin/logname
/usr/bin/sha384sum
/usr/bin/fold
/usr/bin/users
/usr/bin/yes
/usr/bin/paste
/usr/bin/factor
/usr/bin/pathchk
/usr/bin/basename
/usr/bin/dircolors
/usr/bin/du
/usr/bin/shuf
/usr/bin/sha224sum
/usr/bin/head
/usr/bin/tty
/usr/bin/join
/usr/bin/test
/usr/bin/runcon
/usr/bin/base64
/usr/bin/sha512sum
/usr/bin/id
/usr/bin/dirname
/usr/bin/numfmt
/usr/bin/nl
/usr/bin/install
/usr/bin/split
/usr/bin/od
/usr/bin/groups
/usr/bin/env
/usr/bin/tr
/usr/bin/comm
/usr/bin/md5sum
/usr/bin/nproc
/usr/bin/pinky
/usr/bin/uniq
/usr/bin/ptx
/usr/bin/sha256sum
/usr/bin/cksum
/usr/bin/who
/usr/bin/cut
/usr/bin/[
/usr/bin/csplit
/usr/bin/expand
/usr/bin/unexpand
/usr/bin/seq
/usr/bin/stdbuf
/usr/bin/unlink
/usr/bin/chcon
/usr/bin/timeout
/usr/bin/tsort
/usr/bin/expr
/usr/bin/stat
/usr/bin/tail
/usr/bin/mkfifo
/usr/bin/sort
/usr/bin/nohup
/usr/bin/fmt
/usr/bin/whoami
/usr/bin/sum
/usr/bin/wc
/usr/bin/shred
/bin/cp
/bin/dd
/bin/false
/bin/readlink
/bin/echo
/bin/vdir
/bin/mknod
/bin/rm
/bin/df
/bin/rmdir
/bin/sleep
/bin/true
/bin/date
/bin/stty
/bin/ln
/bin/mktemp
/bin/cat
/bin/ls
/bin/uname
/bin/chmod
/bin/touch
/bin/mv
/bin/sync
/bin/mkdir
/bin/dir
/bin/chgrp
/bin/chown
/bin/pwd
/usr/bin/md5sum.textutils
```
