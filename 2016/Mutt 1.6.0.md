<!--
% Mutt 1.6.0
% Kenichiro MATOHARA
% 2016-04-05
-->

`Mutt 1.6.0`がリリースされました．安定版としては2007年のMutt 1.4.2.3以来9年ぶり!?

- [The Mutt E-Mail Client](http://www.mutt.org/ "The Mutt E-Mail Client")

Muttはターミナルで動作するメーラーです．似たものにAlpine(旧pine)があります．emacs使いだとMewを使う方も居ますね．
MuttとAlpineについては以下の記事が参考になると思います．

- [第27回　あえてターミナルを使う（3）：端末で動くメーラー; Alpine, Mutt：Ubuntu Weekly Recipe｜gihyo.jp … 技術評論社](http://gihyo.jp/admin/serial/01/ubuntu-recipe/0027 "第27回　あえてターミナルを使う（3）：端末で動くメーラー; Alpine, Mutt：Ubuntu Weekly Recipe｜gihyo.jp … 技術評論社")


使いはじめるまでの速さはAlpineが一番だと思います．
GPGやらカスタマイズやらのしやすさはMuttかな?（個人の感想です)

最近Alpineで1000000通を超えるメールボックスで1000000通目までしか見られないという現象やSubjectに♪などがあると動作や表示が崩れるといった問題に遭ったのでMutt環境を作りなおしたところでした．せっかくなのでMutt 1.6.0もBuild
ざっとこんな感じでbuildしました．はじめprefixしか指定せずにbuildしてimapに繋がらないとかやってしまいましたorz

```
$ #stretchのMutt Version
$ mutt -v|head -1
Mutt 1.5.24 (2015-08-30)
$ # stretchのMutt configure 確認
$ mutt -v|grep Configure\ 
Configure options: '--prefix=/usr' '--sysconfdir=/etc' '--mandir=/usr/share/man' '--with-docdir=/usr/share/doc' '--with-mailpath=/var/mail' '--disable-dependency-tracking' '--enable-compressed' '--enable-debug' '--enable-fcntl' '--enable-hcache' '--enable-gpgme' '--enable-imap' '--enable-smtp' '--enable-pop' '--with-curses' '--with-gnutls' '--with-gss' '--with-idn' '--with-mixmaster' '--with-sasl' '--without-gdbm' '--without-bdb' '--without-qdbm' '--build' 'x86_64-linux-gnu' 'build_alias=x86_64-linux-gnu' 'CFLAGS=-g -O2 -fstack-protector-strong -Wformat -Werror=format-security -Wall' 'LDFLAGS=-Wl,-z,relro' 'CPPFLAGS=-Wdate-time -D_FORTIFY_SOURCE=2 -I/usr/include/qdbm'
$ sudo apt-get build-dep mutt
$ wget ftp://ftp.mutt.org/pub/mutt/mutt-1.6.0.tar.gz ftp://ftp.mutt.org/pub/mutt/mutt-1.6.0.tar.gz.asc
$ gpg --verify mutt-1.6.0.tar.gz.asc 
gpg: 署名されたデータが'mutt-1.6.0.tar.gz'にあると想定します
gpg: 2016年04月03日 03時22分52秒 JSTにRSA鍵ID 80316BDAで施された署名
gpg: "Kevin J. McCarthy <kevin@8t8.us>"からの正しい署名
gpg: *警告*: この鍵は信用できる署名で証明されていません!
gpg:       この署名が所有者のものかどうかの検証手段がありません。
主鍵フィンガー・プリント: 8975 A9B3 3AA3 7910 385C  5308 ADEF 7684 8031 6BDA
$ tar tvf mutt-1.6.0.tar.gz |lv
$ tar xf mutt-1.6.0.tar.gz
$ cd mutt-1.6.0
$ ./configure --prefix=$HOME/usr/local --disable-dependency-tracking --enable-compressed --enable-fcntl --enable-hcache --enable-gpgme --enable-imap --enable-smtp --enable-pop --with-curses --with-gnutls --with-gss --with-idn --with-mixmaster --with-sasl --with-slang --without-gdbm --without-bdb --without-qdbm --build x86_64-linux-gnu
$ make
$ make install
```

Mutt 1.6.0にしても設定は1.5.24のものがそのまま使えているようです．
1000000通目以降も読めます．1034161通までは確認．Subjectに♪があると崩れたりするのはMutt 1.6.0でも同じでした．これはS-Langとかの問題なのかな?configureとかもちゃんと確認したほうがいいかもですね．


普通にGUIのメーラーを使えばいいんじゃ?と思われるかもしれませんがメールボックスが大きくなってくると辛くなってきます．そういう環境でもサーバで起動しっぱなしにしてるとサクサク動くしsshさえ使えればメールの読み書きもできるしということで使っています．
ということで今はWeb版gmail/Sylpeed/Alpine/Muttを併用中です．


<iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=6132392661" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>

