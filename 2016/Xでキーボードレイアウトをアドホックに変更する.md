<!--
% Xでキーボードレイアウトをアドホックに変更する
% Kenichiro MATOHARA
% 2016-03-31
-->

今日本語キーボードのNotePCを設定しています，これまではこのマシンはi386のテストや横に置いて動画再生などをする程度にしか使っていなかったのですが，X220の調子が悪いので外に持ち出せるマシン出来ないかと設定中です．

小さなA5サイズのマシンな上に日本語配列(購入時は英語配列となっていたが日本語配列のものが届いた……)なので英語配列の外部キーボードを利用したいところです．

こんな感じでGUIで設定してもいいのですが，繋ぎ直すたびにこれをするのは面倒です．
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/25544106244/in/dateposted/" title="Screenshot at 2016-03-31 18:27:09"><img src="https://farm2.staticflickr.com/1710/25544106244_5a5c611050_n.jpg" width="320" height="319" alt="Screenshot at 2016-03-31 18:27:09"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

コンソールでは`loadkeys`というコマンドでアドホックにキーボードレイアウト変更が出来ます．

- [コンソールのキーマップをお手軽に変更できる`loadkeys' - matoken’s meme -hatena-](http://d.hatena.ne.jp/matoken/20090905/1252164012 "コンソールのキーマップをお手軽に変更できる`loadkeys&apos; - matoken’s meme -hatena-")

同じような感じでXでも設定できないか探してみました．
`setxkbmap`コマンドで実現できそうです．しかし，以下のようにうまく行く時とエラーになる場合があります．

```
$ setxkbmap -layout us
$ setxkbmap -layout jp
Error loading new keyboard description
```

少し試行錯誤してこんな感じでエラーが出なくなりました．

```
$ setxkbmap -model pc105 -layout us,jp
$ setxkbmap -model pc105 -layout jp,us
```

適当なキーにこれを割り当てておくと便利な感じです．


## 余録

現在の設定の確認

```
$ setxkbmap -print -verbose 10

Setting verbose level to 10
locale is C
Trying to load rules file ./rules/evdev...
Trying to load rules file /usr/share/X11/xkb/rules/evdev...
Success.
Applied rules from evdev:
rules:      evdev
model:      pc105
layout:     us
Trying to build keymap using the following components:
keycodes:   evdev+aliases(qwerty)
types:      complete
compat:     complete
symbols:    pc+us+inet(evdev)
geometry:   pc(pc105)
xkb_keymap {
	xkb_keycodes  { include "evdev+aliases(qwerty)"	};
	xkb_types     { include "complete"	};
	xkb_compat    { include "complete"	};
	xkb_symbols   { include "pc+us+inet(evdev)"	};
	xkb_geometry  { include "pc(pc105)"	};
};
```



