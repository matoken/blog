Linux で色温度変更の書きかけメモ

## linux color temperature

```
% apt-cache search color temperature
gnome-shell-extension-redshift - redshift extension for GNOME Shell
irssi-scripts - collection of scripts for irssi
gtk-redshift - Adjusts the color temperature of your screen with GTK+ integration
redshift - Adjusts the color temperature of your screen
redshift-plasmoid - Adjusts the color temperature of your screen, KDE plasmoid
```

irssi のは多分違う．
gnome を使っているならgnome-shell-extension-redshift が良さそう．

- [tommie-lie/gnome-shell-extension-redshift · GitHub](https://github.com/tommie-lie/gnome-shell-extension-redshift "tommie-lie/gnome-shell-extension-redshift · GitHub")

```
% sudo apt install gnome-shell-extension-redshift
% cat /usr/share/gnome-shell/extensions/redshift@tommie-lie.de/README.md gnome-shell-extension-redshift
==============================

redshift integration for GNOME Shell

```
X に入りなおして，Tweak Tool でRedshift をOn にして，
<a href="https://www.flickr.com/photos/119142834@N05/19169686245" title="Screenshot from 2015-06-26 16-33-59 by Kenichiro MATOHARA, on Flickr"><img src="https://c1.staticflickr.com/1/304/19169686245_6465f88f3c.jpg" width="500" height="368" alt="Screenshot from 2015-06-26 16-33-59"></a>

設定して，

<a href="https://www.flickr.com/photos/119142834@N05/18982020300" title="Screenshot from 2015-06-26 16-34-21 by Kenichiro MATOHARA, on Flickr"><img src="https://c1.staticflickr.com/1/266/18982020300_d4e4053fb4.jpg" width="500" height="333" alt="Screenshot from 2015-06-26 16-34-21"></a>
<a href="https://www.flickr.com/photos/119142834@N05/19169686315" title="Screenshot from 2015-06-26 16-35-07 by Kenichiro MATOHARA, on Flickr"><img src="https://c1.staticflickr.com/1/345/19169686315_85d330d241.jpg" width="500" height="331" alt="Screenshot from 2015-06-26 16-35-07"></a>

右上からOn にしたら行けそうだけどOn にならない?

<a href="https://www.flickr.com/photos/119142834@N05/19163924332" title="Screenshot from 2015-06-26 16-38-25 by Kenichiro MATOHARA, on Flickr"><img src="https://c1.staticflickr.com/1/560/19163924332_7a390f999c.jpg" width="376" height="395" alt="Screenshot from 2015-06-26 16-38-25"></a>

一旦消す．

```
% sudo apt purge gnome-shell-extension-redshift
```

redshift の方を試してみる．

```
% redshift -h
Usage: redshift -l LAT:LON -t DAY:NIGHT [OPTIONS...]

Set color temperature of display according to time of day.

  -h            Display this help message
  -v            Verbose output
  -V            Show program version

  -b DAY:NIGHT  Screen brightness to apply (between 0.1 and 1.0)
  -c FILE       Load settings from specified configuration file
  -g R:G:B      Additional gamma correction to apply
  -l LAT:LON    Your current location
  -l PROVIDER   Select provider for automatic location updates
                (Type `list' to see available providers)
  -m METHOD     Method to use to set color temperature
                (Type `list' to see available methods)
  -o            One shot mode (do not continuously adjust color temperature)
  -O TEMP       One shot manual mode (set color temperature)
  -p            Print mode (only print parameters and exit)
  -x            Reset mode (remove adjustment from screen)
  -r            Disable temperature transitions
  -t DAY:NIGHT  Color temperature to set at daytime/night

The neutral temperature is 6500K. Using this value will not
change the color temperature of the display. Setting the
color temperature to a value higher than this results in
more blue light, and setting a lower value will result in
more red light.

Default values:

  Daytime temperature: 5500K
  Night temperature: 3500K

Please report bugs to <https://github.com/jonls/redshift/issues>
```

昼が5500K，夜が3500Kになっているよう．

```
% redshift
Trying location provider `geoclue'...
Started Geoclue provider `Geoclue Master'.
Using provider `geoclue'.

** (process:25394): WARNING **: Provider does not have a valid location available.
Unable to get location from provider.
```

うまく動かない?
標準では位置情報から外の明るさから設定が変わるようになっているようでその位置情報の取得部分でこけているよう．関連パッケージは導入されているようだけど……．
この辺がそれっぽいけど

- [Bug #888661 “redshift crashed with SIGSEGV in geoclue_master_cli...” : Bugs : redshift package : Ubuntu](https://bugs.launchpad.net/ubuntu/+source/redshift/+bug/888661)
- [Redshift (with geoclue provider) crashes when started outside of an X session · Issue #58 · jonls/redshift](https://github.com/jonls/redshift/issues/58)
- [GeoClue when not in X by maandree · Pull Request #64 · jonls/redshift](https://github.com/jonls/redshift/pull/64)


```
% redshift -V
redshift 1.10
```

v.1.10 は2015-01-04 にリリース．上のバグはv.1.8 辺りで取り込まれているよう．

こっちぽい．
- [general - Bug#789725: gnome-settings-daemon: Super+L shortcut does not work since last upgrade - msg#34098 - Recent Discussion OSDir.com](http://osdir.com/ml/general/2015-06/msg34098.html)
- [Bug#789599: gnome-settings-daemon: fn+fx buttons does not work after update](http://www.mail-archive.com/debian-bugs-dist@lists.debian.org/msg1332002.html)


とりあえず1回だけ動かすとうまく動くよう．2200K とか適当に設定してもそれらしく動く．

```
% redshift -v -O 3500
Brightness: 1.00:1.00
Gamma (Daytime): 1.000, 1.000, 1.000
Gamma (Night): 1.000, 1.000, 1.000
Using method `randr'.
Color temperature: 3500K
% redshift -v -O 5500
Brightness: 1.00:1.00
Gamma (Daytime): 1.000, 1.000, 1.000
Gamma (Night): 1.000, 1.000, 1.000
Using method `randr'.
Color temperature: 5500K
```

このメッセージの Using method `randr'. を見てそういえばxrandr でも行けるんじゃ?と思ったけど


```
% xrandr
Screen 0: minimum 8 x 8, current 1440 x 900, maximum 32767 x 32767
LVDS1 connected primary 1440x900+0+0 (normal left inverted right x axis y axis) 261mm x 163mm
   1440x900      50.00*+
   1360x768      59.80    59.96
   1152x864      60.00
   1024x768      60.00
   800x600       60.32    56.25
   640x480       59.94
DP1 disconnected (normal left inverted right x axis y axis)
HDMI1 disconnected (normal left inverted right x axis y axis)
VGA1 disconnected (normal left inverted right x axis y axis)
VIRTUAL1 disconnected (normal left inverted right x axis y axis)
% xrandr --output LVDS1 --brightness 0.6
```

近いのはbrightness の設定くらい?0~1の設定が可能で規定値は1
ちなみにこの設定をすると最大輝度がこの値になるようで，gnomeやキーボード・ショートカットでの操作ではこれ以上上げられなくなった．

```
% xrandr --output LVDS1 --brightness 0.6
```
﻿
