<!--
% 鹿児島Linux勉強会2016.03に参加
% Kenichiro MATOHARA
% 2016-03-31
-->

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/25748231426/in/dateposted/" title="DP1M1045"><img src="https://farm2.staticflickr.com/1636/25748231426_f1c23c2631.jpg" width="333" height="500" alt="DP1M1045"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
ちょっと前の話になりますが，3/12(sat)に「鹿児島Linux勉強会 2016.03」に参加してきました．


- [鹿児島Linux勉強会 2016.03 - connpass](http://kagolug.connpass.com/event/28459/ "鹿児島Linux勉強会 2016.03 - connpass")
- [鹿児島らぐ(Linux User's Group)](https://kagolug.org/ "鹿児島らぐ(Linux User&apos;s Group)")


今回の参加者は東海地方から飛行機でやって来た(!)@kapper1224さんや飛び入りの方を含め8名でした．8名ならTENONさん予約可能だったなーとか．
（予約できないってことは埋まってた場合さすらう羽目になるので毎回不安だったり．）

私はRaspberry Piにdiskless modeのalpine linux導入の話をしました．

<iframe src="//www.slideshare.net/slideshow/embed_code/key/5MoeJZIvW7RUV" width="510" height="420" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/matoken/raspberry-pidiskless-modealpine-linux" title="Raspberry Piにdiskless modeのalpine linuxを導入してみる" target="_blank">Raspberry Piにdiskless modeのalpine linuxを導入してみる</a> </strong> from <strong><a target="_blank" href="//www.slideshare.net/matoken">Kenichiro MATOHARA</a></strong> </div>

他の方の発表等は@kapper1224さんのblogやTogetterまとめをどうぞ．

- [鹿児島らぐ2016年3月鹿児島中央　LT大会 　#kagolug: Kapperのブログ　新館](http://kapper1224.sblo.jp/article/174455696.html "鹿児島らぐ2016年3月鹿児島中央　LT大会 　#kagolug: Kapperのブログ　新館")
- [鹿児島Linux勉強会 2016.03の記録 - Togetterまとめ](http://togetter.com/li/949724 "鹿児島Linux勉強会 2016.03の記録 - Togetterまとめ")


Twitterやblogに投稿してくれる方が居るといいですね． 一人だと全然追いつきませんし． 毎回こんな感じだと中継なんかも再開できるかもですが……．


次回04月も開催予定です．今のところ4/15(Fri)に鹿児島市谷山方面で調整中です．
5月以降も俺が主催やってやるぜって方や，会場も募集中です．

状況を知りたい方は鹿児島らぐのMLを購読したりしてみてください．
（LingrやTwitterでもMLの内容配信してます）

- [鹿児島らぐ(Linux User's Group)](https://kagolug.org/ "鹿児島らぐ(Linux User&apos;s Group)")


