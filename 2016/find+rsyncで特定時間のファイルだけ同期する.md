<!--
find+rsyncで特定時間のファイルだけ同期する
-->

小ネタですが．
`~mk/POD`以下から内容更新日時が1440分(1日)以内のファイルを探して同期しています．

```
cd ~mk/POD ; find . -mmin -1440 -type f -print0 | rsync --delete --progress --files-from=- --from0 ~mk/POD/ /var/www/owncloud/data/matoken/files/podcast/
```

findコマンドで欲しいファイルを探し，rsyncでその結果を同期しています．
--files-fromに`-`をつけることでfindで検索したファイルを標準入力から受け取っています．
便利 :)

一応オプションの詳細

- find
-mmin -1440 : 内容更新日時が1440分(1日)以内
-type f : ファイルを検索
-print0 : 区切り文字をNULLにする

- rsync
--delete--delete : 無くなったファイルは削除する
--progress : プログレスを表示
--files-from=- : STDINから同期元ファイルリストを読み込む
--from0 : 区切り文字をNULLとして扱う


<iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00J4KDYV4" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe><iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=4873112672" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe><iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B019O8NFDC" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>


<!--
```
cd ~mk/POD ; find . -mmin -1440 -type f -print0 | xargs -n1 -I{} ln {} /var/www/owncloud/data/matoken/files/podcast//var/www/owncloud/data/matoken/files/podcast/{}
```
-->
