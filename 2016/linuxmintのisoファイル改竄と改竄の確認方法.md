<!-- Place this tag in your head or just before your close body tag. -->
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<!-- Place this tag where you want the widget to render. -->
<div class="g-post" data-href="https://plus.google.com/+JunNOGATA/posts/RigYSk1DfzT"></div>

うわぁ……
怖すぎる．

- [Linux Mint Website Hacked, Users Tricked Into Downloading ISOs with Backdoors](http://news.softpedia.com/news/linux-mint-website-hacked-users-pointed-to-download-isos-with-backdoors-in-them-500707.shtml?utm_content=bufferd0a75&utm_medium=social&utm_source=plus.google.com&utm_campaign=buffer "Linux Mint Website Hacked, Users Tricked Into Downloading ISOs with Backdoors")
- [The Linux Mint Blog » Blog Archive » Beware of hacked ISOs if you downloaded Linux Mint on February 20th!](http://blog.linuxmint.com/?p=2994 "The Linux Mint Blog » Blog Archive » Beware of hacked ISOs if you downloaded Linux Mint on February 20th!")

2/20に入手した人はisoファイルを削除．もしOS導入済みの人はネットワークから遮断して必要なデータがあればバックアップの上初期化して導入しなおし(勿論再度入手しなおしたisoファイルで)，メールやWebサービス等を利用していたならそちらのパスワード等の変更もとのこと．

入手したisoファイルの確認方法がMD5の方法しか書かれていませんね．
署名ファイルはないのでしょうか?

ftp siteを覗いてみると，

```
$ w3m -dump http://mirrors.kernel.org/linuxmint/stable/17.3/
Index of /linuxmint/stable/17.3/

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

../
linuxmint-17.3-cinnamon-32bit.iso                  30-Nov-2015 10:14      1G
linuxmint-17.3-cinnamon-64bit.iso                  28-Nov-2015 18:18      1G
linuxmint-17.3-cinnamon-nocodecs-32bit.iso         30-Nov-2015 21:06      1G
linuxmint-17.3-cinnamon-nocodecs-64bit.iso         30-Nov-2015 18:10      1G
linuxmint-17.3-cinnamon-oem-64bit.iso              01-Dec-2015 09:31      2G
linuxmint-17.3-kde-32bit.iso                       05-Jan-2016 22:57      2G
linuxmint-17.3-kde-64bit.iso                       05-Jan-2016 21:26      2G
linuxmint-17.3-mate-32bit.iso                      30-Nov-2015 10:31      1G
linuxmint-17.3-mate-64bit.iso                      28-Nov-2015 18:19      2G
linuxmint-17.3-mate-nocodecs-32bit.iso             01-Dec-2015 02:43      1G
linuxmint-17.3-mate-nocodecs-64bit.iso             01-Dec-2015 01:01      2G
linuxmint-17.3-mate-oem-64bit.iso                  01-Dec-2015 10:42      2G
linuxmint-17.3-xfce-32bit.iso                      05-Jan-2016 16:41      1G
linuxmint-17.3-xfce-64bit.iso                      05-Jan-2016 15:48      1G
md5sum.txt                                         06-Jan-2016 16:00     958
sha256sum.txt                                      06-Jan-2016 16:03    1406
sha256sum.txt.gpg                                  06-Jan-2016 16:09     181

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
```

`sha256sum.txt.gpg`はあるよう．恐らく`sha256sum.txt`への署名ですね．これを入手して確認してみます．

先ずはファイルの入手

```
$ wget http://mirrors.kernel.org/linuxmint/stable/17.3/sha256sum.txt wget http://mirrors.kernel.org/linuxmint/stable/17.3/sha256sum.txt.gpg
```

署名を確認してみると，`DSA鍵ID 0FF405B2`で署名されているよう．

```
$ gpg --verify sha256sum.txt.gpg sha256sum.txt
gpg: 2016年01月07日 01時06分20秒 JSTにDSA鍵ID 0FF405B2で施された署名
gpg: 署名を検査できません: 公開鍵が見つかりません
```

鍵サーバから鍵を検索してインポートします．

```
$ gpg --search-keys 0FF405B2
gpg: "0FF405B2"をhkpサーバkeys.gnupg.netから検索
(1)     Clement Lefebvre (Linux Mint Package Repository v1) <root@linuxmint.co
          1024 bit DSA key 0FF405B2, 作成: 2009-04-29
Keys 1-1 of 1 for "0FF405B2".  番号(s)、N)次、またはQ)中止を入力してください >1
gpg: 鍵0FF405B2をhkpからサーバkeys.gnupg.netに要求
gpg: 鍵0FF405B2: 公開鍵"Clement Lefebvre (Linux Mint Package Repository v1) <root@linuxmint.com>"をインポートしました
gpg: 最小の「ある程度の信用」3、最小の「全面的信用」1、PGP信用モデル
gpg: 深さ: 0  有効性:  15  署名:  52  信用: 0-, 0q, 0n, 0m, 0f, 15u
gpg: 深さ: 1  有効性:  52  署名:  59  信用: 52-, 0q, 0n, 0m, 0f, 0u
gpg: 次回の信用データベース検査は、2018-07-07です
gpg:           処理数の合計: 1
gpg:             インポート: 1
```

署名を検証すると，`DSA鍵ID 0FF405B2`で署名された鍵ということが検証できました．
※ちなみにこの鍵IDと鍵指紋の確認もしたいのですが，その情報の載っているであろうlinuxmintのsiteは今落ちているようで未確認です．

```
$ gpg --verify sha256sum.txt.gpg sha256sum.txt
gpg: 2016年01月07日 01時06分20秒 JSTにDSA鍵ID 0FF405B2で施された署名
gpg: "Clement Lefebvre (Linux Mint Package Repository v1) <root@linuxmint.com>"からの正しい署名
gpg: *警告*: この鍵は信用できる署名で証明されていません!
gpg:       この署名が所有者のものかどうかの検証手段がありません。
主鍵フィンガー・プリント: E1A3 8B8F 1446 75D0 60EA  666F 3EE6 7F3D 0FF4 05B2
```

チェックサムファイルの署名が正しいことが確認できたら，isoファイルのチェックサムを確認します．これでダウンロードされたisoファイルが正しくダウンロードされたことがわかります．
今回はisoファイルは1つしか入手していません．
入手した`linuxmint-17.3-xfce-32bit.iso`の確認が行われ，`完了`と言われています．

```
$ ls linuxmint*.iso
linuxmint-17.3-xfce-32bit.iso
$ sha256sum -c sha256sum.txt
sha256sum: linuxmint-17.3-cinnamon-32bit.iso: そのようなファイルやディレクトリはありません
linuxmint-17.3-cinnamon-32bit.iso: オープンまたは読み込みに失敗しました
sha256sum: linuxmint-17.3-cinnamon-64bit.iso: そのようなファイルやディレクトリはありません
linuxmint-17.3-cinnamon-64bit.iso: オープンまたは読み込みに失敗しました
sha256sum: linuxmint-17.3-mate-32bit.iso: そのようなファイルやディレクトリはありません
linuxmint-17.3-mate-32bit.iso: オープンまたは読み込みに失敗しました
sha256sum: linuxmint-17.3-mate-64bit.iso: そのようなファイルやディレクトリはありません
linuxmint-17.3-mate-64bit.iso: オープンまたは読み込みに失敗しました
sha256sum: linuxmint-17.3-cinnamon-nocodecs-32bit.iso: そのようなファイルやディレクトリはありません
linuxmint-17.3-cinnamon-nocodecs-32bit.iso: オープンまたは読み込みに失敗しました
sha256sum: linuxmint-17.3-cinnamon-nocodecs-64bit.iso: そのようなファイルやディレクトリはありません
linuxmint-17.3-cinnamon-nocodecs-64bit.iso: オープンまたは読み込みに失敗しました
sha256sum: linuxmint-17.3-mate-nocodecs-32bit.iso: そのようなファイルやディレクトリはありません
linuxmint-17.3-mate-nocodecs-32bit.iso: オープンまたは読み込みに失敗しました
sha256sum: linuxmint-17.3-mate-nocodecs-64bit.iso: そのようなファイルやディレクトリはありません
linuxmint-17.3-mate-nocodecs-64bit.iso: オープンまたは読み込みに失敗しました
sha256sum: linuxmint-17.3-cinnamon-oem-64bit.iso: そのようなファイルやディレクトリはありません
linuxmint-17.3-cinnamon-oem-64bit.iso: オープンまたは読み込みに失敗しました
sha256sum: linuxmint-17.3-mate-oem-64bit.iso: そのようなファイルやディレクトリはありません
linuxmint-17.3-mate-oem-64bit.iso: オープンまたは読み込みに失敗しました
sha256sum: linuxmint-17.3-kde-32bit.iso: そのようなファイルやディレクトリはありません
linuxmint-17.3-kde-32bit.iso: オープンまたは読み込みに失敗しました
sha256sum: linuxmint-17.3-kde-64bit.iso: そのようなファイルやディレクトリはありません
linuxmint-17.3-kde-64bit.iso: オープンまたは読み込みに失敗しました
linuxmint-17.3-xfce-32bit.iso: 完了
sha256sum: linuxmint-17.3-xfce-64bit.iso: そのようなファイルやディレクトリはありません
linuxmint-17.3-xfce-64bit.iso: オープンまたは読み込みに失敗しました
sha256sum: 警告: 一覧にある 13 個のファイルが読み込めませんでした

```

ちなみにダウンロードが途中で終了してしまったり何らかの理由で壊れてしまった場合などは以下のように`失敗`と言われます．

```
$ echo 'a'>> linuxmint-17.3-xfce-32bit.iso
$ sha256sum -c sha256sum.txt
  :
linuxmint-17.3-xfce-32bit.iso: 失敗
  :
sha256sum: 警告: 1 個の計算したチェックサムが一致しませんでした
```

このあたりのことは以下のページの説明が解りやすいと思います．

- [Debian CD の信頼性の検証](https://www.debian.org/CD/verify "Debian CD の信頼性の検証")
