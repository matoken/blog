<!--
ストリーミング配信補助ソフトのCastawesomeを試す
-->

3/5はOpenDataDayです．
去年までは鹿児島市で募ってMapCafeと称してOSMする集まりをしていたのですが人が集まらないので今年は家で地図を書こうかと．そしてデスクトップをストリーミングしてみようかなと．

ということデスクトップストリーミングの話です．

いつものようにffmpegでRTMP配信なのですが，今回は`Castawesome`というGUIの皮を見つけたので試してみました．これを使うとオプションなどがGUIで入力するだけで埋まっていくので便利です．

- [TheSamsai/Castawesome: A GUI wrapper for FFmpeg and avconv for Livestreaming](https://github.com/TheSamsai/Castawesome "TheSamsai/Castawesome: A GUI wrapper for FFmpeg and avconv for Livestreaming")

導入はcloneした後`make`するだけ．システムに導入したいなら`make install`も．(Raspberry Piでも動きました．)  
ffmpeg or avconvとPython 3, GTK-3ライブラリなどが必要です．(make時に足りないものがある場合`DEPENDS`が参考になります)

```
$ git clone -b 0.16.0 https://github.com/TheSamsai/Castawesome.git
$ cd Castawesome
$ make
```

の後

```
$ ./castawesome.py
```

or

```
$ sudo make install
```
してスタートメニューから実行．

こんな画面が出てきてフォームを埋めていくだけでOKです．
配信先はTwitch/YouTube/Hitbox/Pocarto/Local/Customに対応．多分Customを使えばUstream.tvも行けますね．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/24894812514/in/dateposted-public/" title="20160305_22:03:24-21661"><img src="https://farm2.staticflickr.com/1459/24894812514_1e95a0fc69.jpg" width="500" height="497" alt="20160305_22:03:24-21661"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

ただ，細かい設定は苦手だったりエラーチェックも甘く配信されないことも多いのでそういう場合にはターミナルから起動して配信（「録画」ボタン）した時の以下のようなログを利用して`ffmpeg`で配信するようにするのが良さそうです．

```
ffmpeg -f x11grab -show_region 0 -s 1366x768 -framerate " 8" -i :0.0+0,0 -f pulse -ac 1 -i  -vcodec h264 -s 798x449 -preset medium -acodec mp3 -ar 44100 -threads 4
-qscale 3 -b:a 128k -b:v 300k -minrate 300k -g 16 -pix_fmt yuv420p -f flv "rtmp://a.rtmp.youtube.com/live2/<_stream_key_>"
```

設定ファイルは`~/.config/castawesome`以下に保存されます．`key`はYouTubeのものを設定しても`.twitch_key`というファイルに保存されていました．

```
$ ls -A ~/.config/castawesome/
.twitch_key  config.txt
```

よし，これで配信準備出来たと思ったのですが体調悪くスクリーンセーバーを垂れ流しながら寝落ちしてしまっていましたorz

<iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B01613FT1O" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe><iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B0065G4GH6" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe><iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00L8AY5WU" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>
