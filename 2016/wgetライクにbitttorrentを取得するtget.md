title:wgetライクにbitttorrentを取得するtget

wgetコマンドのような使い勝手のBittorentクライアントのtgetを試してみました．
node製MITライセンスのアプリケーションです．

- [jeffjose/tget: tget is wget for torrents](https://github.com/jeffjose/tget "jeffjose/tget: tget is wget for torrents")

導入は`npm`で．`tget`ではなく`t-get`なので注意．

```shell
$ npm install -g t-get
```

torrentファイル，マグネットリンクの他torrent URLでもダウンロードできるようです．
実行すると以下のようなプログレスが表示されてダウンロードが始まります．行幅はハードコードされているようです．

```shell
$ tget https://downloads.raspberrypi.org/raspbian_latest.torrent
 downloading 1 files (1.4GB) [============================= ] 97% 64.0s 588.8KB/s 99 peers
```

ダウンロード完了時は以下のような感じ．ダウンロード完了後直ちに終了します．

```shell
$ tget https://downloads.raspberrypi.org/raspbian_latest.torrent
------------------
2016-11-25-raspbian-jessie.zip 1.4GB
------------------
 downloaded 1 files (1.4GB)
```

ファイルの保存先はカレントディレクトリで，カレントディレクトリに書き込めない場合はエラーにならずファイルが消えてしまうようです．

便利だけどアップロード関係なく終了してしまうのでBittorrentへの貢献度は低そうです．
ちなみに最近はリモートのTransmissionをfile serverで動作させて，Transmission-remote-gtkで操作しています．