<!--
Raspberry Piで雑いpodcastサーバを作った
-->
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/24361279779/in/dateposted-public/" title="IMGP2678"><img src="https://farm2.staticflickr.com/1641/24361279779_e5c4e6e77c.jpg" width="500" height="331" alt="IMGP2678"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>


先日試した`podracer`で
> 毎日実行して今日の分をまとめて再生して消してというように使う感じなのかもしれません．

- [CLI な podcast aggregator/downloader な podracer を試してみる | matoken's meme](http://matoken.org/blog/blog/2016/01/28/try-the-cli-of-podcat-aggregator-downloader-of-podracer/ "CLI な podcast aggregator/downloader な podracer を試してみる | matoken&apos;s meme")

ということで自動取得，自動再生すると家庭内ラジオのようにできるのではと思い試してみました．

環境は`Raspberry Pi 1B (512MB)`に`Rasbian jessie`を導入したもので有線LAN 利用です．

## 導入パッケージ

podcast の取得のために`podracer`，プレイリストの配信に`boa`を利用，音声配信の為に`vlc-nox`を導入しました．

```
$ sudo apt install podracer boa vlc-nox
```

## podcast 取得の準備

- [CLI な podcast aggregator/downloader な podracer を試してみる | matoken's meme](http://matoken.org/blog/blog/2016/01/28/try-the-cli-of-podcat-aggregator-downloader-of-podracer/ "CLI な podcast aggregator/downloader な podracer を試してみる | matoken&apos;s meme")

を参考にpodracerの設定をして下さい．`~/.podracer/subscriptions`を用意した後1度`catchup mode`で実行しておきます．

```
$ podracer -c
```

これでRSS Feedをひと舐めして過去のpodcastを取得しないようにします．

## VLC での配信テスト

以下の例ではpodracerで作成された今日のプレイリストをそのままhttp 8080ポートで配信して更にloopさせています．

```
$ cvlc  ~/podcasts/`date +\%F`/`date +\%F`-podcasts.m3u --sout '#standard{access=http,mux=ts,dst=:8080}' --loop
```

この状態で再生したい端末で `$ cvlc http://raspberrypi.local:8080/` とか `mplayer http://raspberrypi.local:8080/` などとしてアクセスすると再生されるはずです．
1曲毎に停まってしまい再度再生し直す必要がありますがこれは後で解決することにします．


## VLC の設定

VLCで配信しますが，コントローラーも利用したいです．`--extraintf=http` オプションでwebコントローラーが利用できます．規定値では localhost 以外からアクセスできないので家のネットワークのどこからでもコントロールできるように `/usr/share/vlc/lua/http/.hosts` に利用可能なネットワークを登録しておきます．

```
192.168.1.0/24
192.168.2.0/24
```

以下のように実行するとwebコントローラーが8081ポートで起動します．パスワードは `raspberry` です．適当なブラウザで `http://:raspberry@raspberrypi.local:8081/` な感じでアクセスするとコントローラが利用できると思います．

```
$ cvlc --extraintf=http --http-port=8081 --http-password='raspberry' ~/podcasts/`date +\%F`/`date +\%F`-podcasts.m3u --sout '#standard{access=http,mux=ts,dst=:8080}' --loop
```

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/24609440592/in/dateposted-public/" title="Screenshot_2016-01-31-19-04-03"><img src="https://farm2.staticflickr.com/1682/24609440592_5d8917585d_n.jpg" width="180" height="320" alt="Screenshot_2016-01-31-19-04-03"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

パスワードを設定しないとパスワードの設定を促す画面が表示されて利用できないようです．


## http の設定(プレイリストの作成)

boaの標準では `/var/www` 以下がDocumentRootになっています．ここにメニュー用の `index.html` とプレイリストの `podcast.m3u` を用意しておきます．

権限は `www-data.www-data` なので，以下のような感じで権限を設定して編集するといいかもしれません．

```
$ sudo chown www-data.www-data /var/www
$ sudo -u www-data vi /var/www/index.html
   :
```

- `/var/www/index.html` の例

```
<html>
<body>
<ul>
<li><a href="./podcast.m3u">m3u</a></li>
<li><a href="http://:raspberry@192.168.2.200:8081/">VLC control</a></li>
</ul>
</body>
</html>
```

※AndroidではAvahiでの名前解決がうまく行かないのでIP アドレスで書いています．環境に合わせて変更して下さい．

- `/var/www/podcast.m3u` の例

```
http://192.168.2.200:8080/
http://192.168.2.200/podcast.m3u
```

1行目だけだと1番組分で再生が停止してしまうので，2行目で自分自身を呼び出して再起しています．これで2番組以降でも続けて再生されます．
※AndroidではAvahiでの名前解決がうまく行かないのでip アドレスで書いています．環境に合わせて変更して下さい．

## 自動起動とpodcast更新処理

crontab で起動時に自動的に再生が始まるように&定期的にpodcast更新&再生リスト更新&古いpodcastの削除処理をします．

`crontab -e` コマンドで編集します．

```
@reboot cvlc --extraintf=http --http-port=8081 --http-password='raspberry' ~/podcasts/`date +\%F`/`date +\%F`-podcasts.m3u --sout '#standard{access=http,mux=ts,dst=:8080}' --loop
3 */6 * * *     podracer;killall vlc;cvlc --extraintf=http --http-port=8081 --http-password='raspberry' ~/podcasts/`date +\%F`/`date +\%F`-podcasts.m3u --sout '#standard{access=http,mux=ts,dst=:8080}' --loop
10 0 * * *  rm -rf ~/podcasts/`date --date '1 weeks ago' +\%F`
```

1行目は `@reboot` で起動時にVLCを自動起動します．
2行目は6時間毎にpodcastを更新してVLCを起動しなおしています．
3行目で1週間前のpodcastデータを削除しています．

## 利用方法

ウェブブラウザで Raspberry Pi にアクセスします．
> http://raspberrypi.local

以下のようなメニューが出て来ます．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/24431803200/in/dateposted-public/" title="Screenshot_2016-01-31-20-49-47"><img src="https://farm2.staticflickr.com/1583/24431803200_7f1e9eaf1a_n.jpg" width="180" height="320" alt="Screenshot_2016-01-31-20-49-47"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

>   • m3u  
>   • VLC control

`m3u` をクリックするとプレイリストがダウンロードされます．VLC 等の関連付けられたアプリケーションなどで再生できると思います．  
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/24099054654/in/dateposted-public/" title="Screenshot_2016-01-31-19-03-53"><img src="https://farm2.staticflickr.com/1635/24099054654_98aa660d54_n.jpg" width="180" height="320" alt="Screenshot_2016-01-31-19-03-53"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

`VLC control` をクリックするとVLCのウェブコントローラーが表示されます．URLに認証情報を埋め込んでいるので認証はスキップされます．

## 問題点や改善点など

- ブロードキャスト配信の断念． はじめブロードキャスト配信を試したのですが，かなりの頻度でパケットロスしてそのたびに音が途切れました．音が途切れるのはかなりストレスなので諦めました．プレイリストがなくても連続再生可能なのは良かったのですが……．
- podcast更新時の番組強制終了．更新時にVLCをkillしています．番組の途中で切れてしまうので改善したいです．
- podcast の音量や音質を揃える．試しにmp3/128kbpsや64kbpsにリアルタイムエンコーディングしつつ配信も試してみましたが Raspberry Pi でもCPU 25%前後なので実用的な感じです．
帯域を絞ったストリームも同時配信するようにして外で聴きやすくするのもありかもしれません．
- VLC から他のアプリケーションに変更．今回お手軽なので VLC を利用しましたが Gstreamer や ffmpeg などを試すのもありかもしれません．
- スピーカーでも再生．環境によっては同時にスピーカーで再生してもいいかもしれません．
- 番組情報の配信．今は未知のアーティストなどと表示される．アートワークも含めて改善したい．  
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/24359741339/in/dateposted-public/" title="Screenshot_2016-01-31-20-50-06"><img src="https://farm2.staticflickr.com/1688/24359741339_3db8426744_n.jpg" width="180" height="320" alt="Screenshot_2016-01-31-20-50-06"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
- 更新間隔の調整．ニュース番組など定時のある番組があるのでそれらに合わせて更新処理を行うようにすると便利かもしれません．
- 名前解決．AndroidでAvahiが利用できないので家の中のDNSサーバに名前を登録してあげると便利かもしれません．
- pifmを使ってFMラジオでも視聴．日本だと電波法違反になるので自重しました．
<!-- Place this tag in your head or just before your close body tag. -->
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<!-- Place this tag where you want the widget to render. -->
<div class="g-post" data-href="https://plus.google.com/+KenichiroMATOHARA/posts/QJxqf5GBknS"></div>


現在は主にAndroidアプリのVLCで再生しています．AndroidとPCはBluetooth A2DPで繋がっていて，PCで聞いています．PCから離れるときはAndroidにイヤホンジャックを差し替えるとシームレスに移動できます．
