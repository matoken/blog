ファイルからswapを設定しようとして失敗しました．

```
$ fallocate -l 500M test
$ /sbin/mkswap test
mkswap: test: insecure permissions 0644, 0600 suggested.
Setting up swapspace version 1, size = 500 MiB (524283904 bytes)
no label, UUID=aaf26c11-4653-40d5-b279-faa7d8c8625d
$ sudo chmod 600 test
$ sudo chown 0.0 test
$ ls -l test
-rw------- 1 root root 524288000 10月  3 07:26 test
$ sudo swapon test
swapon: /home/mk/test: swapon failed: 無効な引数です
$ sudo swapon -v /home/mk/test
swapon: /home/mk/test: found signature [pagesize=4096, signature=swap]
swapon: /home/mk/test: pagesize=4096, swapsize=524288000, devsize=524288000
swapon /home/mk/test
swapon: /home/mk/test: swapon failed: 無効な引数です
$ LANG=C sudo swapon -v /home/mk/test
swapon: /home/mk/test: found signature [pagesize=4096, signature=swap]
swapon: /home/mk/test: pagesize=4096, swapsize=524288000, devsize=524288000
swapon /home/mk/test
swapon: /home/mk/test: swapon failed: Invalid argument
```

検索するとBtrfsでswapファイルがサポートされていないとか．知らなかったorz

* [Does btrfs support swap files?](https://btrfs.wiki.kernel.org/index.php/FAQ#Does_btrfs_support_swap_files.3F "Does btrfs support swap files?")

性能低下するけどloop deviceを使えばいけるよというワークアラウンドが．

_A workaround, albeit with poor performance, is to mount a swap file via a loop device._

試してみます．

```
$ /sbin/losetup -l
$ sudo /sbin/losetup loop0 ./test
$ /sbin/losetup -l
NAME SIZELIMIT OFFSET AUTOCLEAR RO BACK-FILE DIO
/dev/loop0 0 0 0 0 /home/mk/test 0
$ sudo swapon -v /dev/loop0
swapon: /dev/loop0: found signature [pagesize=4096, signature=swap]
swapon: /dev/loop0: pagesize=4096, swapsize=536870912, devsize=536870912
swapon /dev/loop0
$ /sbin/swapon -s
Filename Type Size Used Priority
/dev/dm-2 partition 16658428 0 -1
/dev/loop0 partition 524284 0 -2
```

うまく行きました．
もとに戻しておきます．

```
$ sudo swapoff /dev/loop0
$ sudo /sbin/losetup -d /dev/loop0
$ sudo rm ./test
```

