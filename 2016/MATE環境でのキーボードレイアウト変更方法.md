<!--
% MATE環境でのキーボードレイアウト変更方法
% Kenichiro MATOHARA
% 2016-04-03
-->

先日`setxkbmap`でXのキーボードレイアウトをアドホックに変更できるようにしたのですが，

- [Xでキーボードレイアウトをアドホックに変更する | matoken's meme](http://matoken.org/blog/blog/2016/03/31/x%e3%81%a7%e3%82%ad%e3%83%bc%e3%83%9c%e3%83%bc%e3%83%89%e3%83%ac%e3%82%a4%e3%82%a2%e3%82%a6%e3%83%88%e3%82%92%e3%82%a2%e3%83%89%e3%83%9b%e3%83%83%e3%82%af%e3%81%ab%e5%a4%89%e6%9b%b4%e3%81%99%e3%82%8b/ "Xでキーボードレイアウトをアドホックに変更する | matoken&apos;s meme")

MATE環境だと切替後暫くすると設定が戻ってしまいます．
`setxkbmap`の代わりにdconfの設定を書き換えればいいのではと思い`dconf dump /`してそれらしい項目を探して書き換えてみると設定の見た目は変わるのですが実際に入力してみると設定が反映されません．

```
$ dconf write /org/mate/desktop/peripherals/keyboard/kbd/layouts "['us', 'jp']"
```

dconfには他にそれらしい設定は無さそうです．

```

$ dconf dump /|grep us
start-with-status-bar=true
custom-format=''
button-lid-ac='suspend'
button-suspend='suspend'
button-lid-battery='suspend'
layouts=['us', 'jp']
mousekeys-accel-time=1200
mousekeys-max-speed=750
mousekeys-enable=false
mousekeys-init-delay=160
$ dconf dump /|grep jp
layouts=['us', 'jp']
```

GUIで設定変更をして設定が反映された前と後でも`/org/mate/desktop/peripherals/keyboard/kbd/layouts`部分しか変わっていません．dconf以外のどこかにも設定がありそうです．

```
$ dconf dump / > /tmp/before
$ # GUIで設定変更
$ dconf dump / > /tmp/after
$ diff /tmp/before /tmp/after
 
193c193
< layouts=['us', 'jp']
---
> layouts=['jp', 'us']﻿
```

ここでGUIの設定画面(「システム」->「設定」->「ハードウェア」->「キーボード」の「レイアウト」タブ）を見ると複数のレイアウトが登録できます．ここに日本語と英語を登録して，「オプション」ボタンを押して「Switching to another layout」に適当なショートカットを選ぶとこのショートカットでレイアウト変更ができるのに気づきました．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/26175590336/in/dateposted/" title="Screenshot at 2016-04-03 12:48:47"><img src="https://farm2.staticflickr.com/1487/26175590336_b216bde285_n.jpg" width="300" height="320" alt="Screenshot at 2016-04-03 12:48:47"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/25928636430/in/photostream/" title="Screenshot at 2016-04-03 12:48:35"><img src="https://farm2.staticflickr.com/1651/25928636430_ebff85e75f_n.jpg" width="320" height="249" alt="Screenshot at 2016-04-03 12:48:35"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

ということで最近の環境だとこの機能で切り替えるのが良さそうです．



