<!--
title:H-IIB F6によるこうのとり6号機打ち上げ見学
-->

- [宇宙ステーション補給機「こうのとり」6号機特設サイト | ファン!ファン!JAXA!](http://fanfun.jaxa.jp/countdown/htv6/ "宇宙ステーション補給機「こうのとり」6号機特設サイト | ファン!ファン!JAXA!")

ということでせっかくなので佐多岬の辺りまで出て見てきました．移動はmotercycleで寒いかなと着込んでいったのですが案外暖か，14度位あったようです．でも帰りは寒く家に着いたときは5度台でした．
移動は片道1時間ちょい．見学したのはさたでい号のそばの田尻海水浴場．

<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://www.openstreetmap.org/export/embed.html?bbox=130.66497623920444%2C31.006731907728962%2C130.67088782787326%2C31.00950898623532&amp;layer=mapnik&amp;marker=31.00812045709507%2C130.66793203353882" style="border: 1px solid black"></iframe><br/><small><a href="http://www.openstreetmap.org/?mlat=31.00812&amp;mlon=130.66793#map=18/31.00812/130.66793">大きな地図を表示</a></small>
＃スカスカなのでサーベイしておくんだった……．

もう少し足を伸ばして佐多岬展望台というのも考えたのですが，展望場所はちょっと狭いし，そこまでの道も街灯もなくアップダウンのある道を歩かないといけないし最近工事しててどんな感じになっているかもわからないしで以前も見学していて確実なこちらに．

この辺りは今はDocomoもAUもLTEが入ってネットワーク環境もいい感じです．Youtubeの中継を聞きながら撮影準備に．用意しておいた三脚を忘れてしまいました．手持ちのものでカメラの仰角を取って試し撮り．今回は，

- Pentax K-5
- SAMYANG 8mm F3.5
- 自作レリーズケーブル

<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B00I9UUX14&linkId=d74d24b7eb39ead1602185790dc6364f"></iframe><iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B0043M4X86&linkId=0184719af4c32c3bc165ba185ea2fd65"></iframe><iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B000FHU7PQ&linkId=477b2fa82fc729ef2ae25da8fcf083df"></iframe>

でISO6400 / ss2s / F3.5 な感じで連射してみました．ss2じゃカメラの処理が追いつかず途中から遅れていきました．もう少し絞ってssを伸ばせばよかったです．

後はうまく撮れるかわからないけどとスマホ(SO-02F)でも動画撮影を試してみました．動画撮影を試していると未だ余裕があると思っていたのにオレンジの点が．慌ててレリーズスイッチを押して撮影開始しました．

流石に音は全く聞こえません．昼間条件がいいとSRB-A分離もわかったりしますが夜はその辺りは全くわかりませんでした．その代わり長く追えます．見えなくなるまで見て5分位追えました．

周りは他に2グループくらい居た感じです．しかしせっかちなグループは撤収が早く車を避けたりでレンズに写り込んでしまったり><
これは三脚を用意して岸壁の海側で撮影しないとですね……．

てことで家に帰ってからStar Trailとtimelapsにしてみました．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/30697353904/in/photostream/lightbox/" title="H-IIB F6によるこうのとり6号機打ち上げ佐多田尻海水浴場より(StarTrail)"><img src="https://c1.staticflickr.com/1/464/30697353904_c0fa0060f3.jpg" width="500" height="331" alt="H-IIB F6によるこうのとり6号機打ち上げ佐多田尻海水浴場より(StarTrail)"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<iframe width="500" height="281" src="https://www.youtube.com/embed/bAKPWPi_tF8?rel=0" frameborder="0" allowfullscreen></iframe>

<iframe width="500" height="281" src="https://www.youtube.com/embed/_mefy9DNBl0?rel=0" frameborder="0" allowfullscreen></iframe>

Star TrailはStarStaXというマルチプラットホームなクローズドソフトウェアを利用，timelapsはFfmpegで作成しました．

- [Software - StarStaX](http://www.markus-enzweiler.de/software/software.html "Software - StarStaX")
- ​[FFmpeg](https://www.ffmpeg.org/ "FFmpeg")

timelapsの方は今回はこんな感じでpv9のwebmにしてみたのですが，すごく小さくなりました．画質は落ちてる?画質は後で試して差し替えるかもしれません．

```
$ ffmpeg -r 10 -pattern_type glob -i 'IMGP????.JPG' out.webm
```

ちなみにVGAのアニメーションGIFで21MBを超えました(21863089byte)が，この動画は366KB(366305byte)でした．
元の静止画は1枚10MBちょっとで135枚．
不安になる小ささですね．

次は12/20に内之浦から打ち上げ予定のepsilon2/ERGの見学に行きたいと思っています．見学場所は迷っていて定番の内之浦漁港か宮原ロケット見学場と岸良の間かなと思っています．夜だし内之浦漁港の方が安全そう．でもライトアップするみたいなので撮影には向かないかも……．一度ロケハンに行きたいところ．

宮原ロケット見学場は一般人が唯一射場を望めるのですが，今回バスツアーのみになっていて一番近い鹿屋からだと割高感があって申し込みませんでした．国分鹿児島体とむしろ安く感じるんですけどね．

- [ジオスペース探査衛星（ERG）/イプシロンロケット2号機 特設サイト | ファン!ファン!JAXA!](http://fanfun.jaxa.jp/countdown/epsilon2/ "ジオスペース探査衛星（ERG）/イプシロンロケット2号機 特設サイト | ファン!ファン!JAXA!")
- [JAXA | イプシロンロケット2号機によるジオスペース探査衛星（ERG）の打上げについて](http://www.jaxa.jp/press/2016/11/20161115_epsilon2_j.html "JAXA | イプシロンロケット2号機によるジオスペース探査衛星（ERG）の打上げについて")
