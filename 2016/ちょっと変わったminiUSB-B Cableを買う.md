<!--
ちょっと変わったminiUSB-B Cableを買う
-->

デモの時やスマホなどでこの折りたたみキーボード( Targus PA875　)が重宝しています．確か秋葉原で結構前に3千円くらいで買ったような気がします．最近はBluetoothが主流ですが有線も安定してていい感じです．

<!-- Place this tag in your head or just before your close body tag. -->
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<!-- Place this tag where you want the widget to render. -->
<div class="g-post" data-href="https://plus.google.com/+KenichiroMATOHARA/posts/NpudEqxo9PU"></div>

久しぶりに使おうとしたらケーブル皮膜が剥けてしまっています．断線はしていないようで今のところ利用できますが出先で使えないと致命的．しかしコネクタは一般的なUSB形状とは違い四角いもの．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/25599882945/in/dateposted-public/" title="IMG_20160308_074032"><img src="https://farm2.staticflickr.com/1502/25599882945_6ae9cf1b68_m.jpg" width="240" height="103" alt="IMG_20160308_074032"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script><a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/24973202503/in/dateposted-public/" title="IMG_20160308_074044"><img src="https://farm2.staticflickr.com/1472/24973202503_77e2922ee9_m.jpg" width="240" height="180" alt="IMG_20160308_074044"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script><a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/25481311242/in/dateposted-public/" title="IMG_20160308_073704"><img src="https://farm2.staticflickr.com/1620/25481311242_6641675337_m.jpg" width="240" height="180" alt="IMG_20160308_073704"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script><a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/25573797326/in/dateposted-public/" title="IMG_20160308_073652"><img src="https://farm2.staticflickr.com/1629/25573797326_87432f07c6_m.jpg" width="240" height="180" alt="IMG_20160308_073652"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script><a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/25599897715/in/dateposted-public/" title="IMG_20160308_073524"><img src="https://farm2.staticflickr.com/1492/25599897715_2ed6aa6da9_m.jpg" width="240" height="180" alt="IMG_20160308_073524"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

以下のページの「ヒロセ４P：四角（４Ｐ）」と同じようです．

- [ミニUSBの規格を明確にしたい！！調査にご協力を](<http://www.h4.dion.ne.jp/~pond/new_page_300.htm> "www.h4.dion.ne.jp/~pond/new_page_300.htm")

こういうコネクタに強いCOMONのページをみるとありました．

- [www.comon.co.jp/EC-HABH.htm](<http://www.comon.co.jp/EC-HABH.htm> "www.comon.co.jp/EC-HABH.htm")

秋葉原に行ければ千石(確か巻取りじゃないのも店頭に置いてあったはず)やコンピュエース辺りで買うのですが遠いので通販を．(千石はひと通り試せるようになっています)最近はこの手のはメール便を使えるフタバヤさんをよく利用しています．前回はAmazonのマーケットプレイス経由で買いましたが今回はポイントが残ってたので楽天店から購入．

- [【楽天市場】【メール便対応】USB 2.0対応 収縮 USBケーブルケーブル【80cm】USB Aタイプ (オス)⇔USB B ハーフコネクタ (オス)ブラック COMON(カモン) EC-HABH【収縮】【A-Hiroseタイプ】：フタバヤ楽天市場店](<http://item.rakuten.co.jp/futabaya-one/ec-habh/> "【楽天市場】【メール便対応】USB 2.0対応 収縮 USBケーブルケーブル【80cm】USB Aタイプ (オス)⇔USB B ハーフコネクタ (オス)ブラック COMON(カモン) EC-HABH【収縮】【A-Hiroseタイプ】：フタバヤ楽天市場店")
- [カモン ＥＣ－ＨＡＢＨ ＵＳＢ（２．０）　ｍｉｎｉＢ（ヒロセタイプ）　巻取ケーブル　０．８ｍ](<https://www.sengoku.co.jp/mod/sgk_cart/detail.php?code=3AUA-JJKL># "カモン ＥＣ－ＨＡＢＨ ＵＳＢ（２．０）　ｍｉｎｉＢ（ヒロセタイプ）　巻取ケーブル　０．８ｍ")
- [【楽天市場】COMON(カモン)　USB-2.0リールケーブル　A⇔ミニB4pin(ヒロセ)タイプ 0.8m [EC-HABH]：地球館　楽天市場店](<http://item.rakuten.co.jp/auc-eco-p/3151/> "【楽天市場】COMON(カモン)　USB-2.0リールケーブル　A⇔ミニB4pin(ヒロセ)タイプ 0.8m [EC-HABH]：地球館　楽天市場店")

そして届いたのがこれです．
普通に使えました．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/25573788036/in/dateposted-public/" title="IMG_20160308_043453"><img src="https://farm2.staticflickr.com/1479/25573788036_963be8936b.jpg" width="375" height="500" alt="IMG_20160308_043453"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

ロゴも張り替えたのでぱっと見同じです．これで安心して利用できます．
ということで，Targus PA875のUSBはヒロセ４PやHIROSE Typeと呼ばれているもので，COMON EC-HABHが適合するよという情報でした．

おまけ?

```
$ dmesg | tail
   :
[125061.384207] CPU1: Package temperature/speed normal
[125491.780430] usb 2-1.1: new low-speed USB device number 4 using ehci-pci
[125491.880961] usb 2-1.1: New USB device found, idVendor=0a81, idProduct=0101
[125491.880969] usb 2-1.1: New USB device strings: Mfr=1, Product=2, SerialNumber=0
[125491.880975] usb 2-1.1: Product: USB Keyboard
[125491.880980] usb 2-1.1: Manufacturer: CHESEN
[125491.886178] input: CHESEN USB Keyboard as /devices/pci0000:00/0000:00:1d.0/usb2/2-1/2-1.1/2-1.1:1.0/0003:0A81:0101.0003/input/input21
[125491.940690] hid-generic 0003:0A81:0101.0003: input,hidraw0: USB HID v1.10 Keyboard [CHESEN USB Keyboard] on usb-0000:00:1d.0-1.1/input0
[125491.947369] input: CHESEN USB Keyboard as /devices/pci0000:00/0000:00:1d.0/usb2/2-1/2-1.1/2-1.1:1.1/0003:0A81:0101.0004/input/input22
[125492.000811] hid-generic 0003:0A81:0101.0004: input,hidraw1: USB HID v1.10 Device [CHESEN USB Keyboard] on usb-0000:00:1d.0-1.1/input1
$ lsusb -d 0a81:0101
Bus 002 Device 005: ID 0a81:0101 Chesen Electronics Corp. Keyboard
```

- [sudo lsusb -vvv -d 0a81:0101.log](https://gist.github.com/matoken/ff7b8653d5efc2f96e22 "sudo lsusb -vvv -d 0a81:0101.log")
