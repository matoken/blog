<!--
Title: Mumbleでリモート会議
Date: 2016-12-03 01:08:16
Modified: 2016-12-03 01:08:16
Tags: remote, Mumble, Etherpad
Category: 
Slug: 
Authors: matoken
Summary: 
-->

先週ですが，FSIJ 月例会に参加していました． 
といっても東京に出て行ったわけではなくネットワーク開催だったので家からの参加です．

- [11月の技術講座](http://www.fsij.org/monthly-meetings/2016/Nov.html)
    - _"テーマ: Mumble と Etherpad で会議をしよう"_

ということでMumbleというソフトウェアで音声チャット + Etherpad(ウェブブラウザベース)でテキスト交換という環境でした．
Mumbleを前もって試していればよかったのだけど直前に入れてなかなか参加できず途中からの参加になりましたorz~

MumbleはDebian stretch amd64環境ではパッケージが存在する( `mumble` )のでこれを導入するだけでした．マルチプラットホームで各種環境向けにも存在して，Androidなどでも利用できるようです．
- [Mumble, the open source VoIP solution](https://wiki.mumble.info/wiki/Main_Page)
- [Plumble \- Mumble VOIP \(Free\) \- Google Play の Android アプリ](https://play.google.com/store/apps/details?id=com.morlunk.mumbleclient.free)
- [Plumble \- Mumble VOIP \- Google Play の Android アプリ](https://play.google.com/store/apps/details?id=com.morlunk.mumbleclient)

今回使用機器は以下のような感じ．でしたが，スピーカーは試行錯誤している時に繋いだもので後で試すと本体のスピーカーで大丈夫でした．

- PC : Thinkpad X220(Debian stretch amd64)
- マイク : 3.5mmジャックに直接挿せるもの(型番不明)
- スピーカー : ELECOM LBT-SPTR01AV(Bluetooth)

このときMumble利用時に嵌ったところをメモしておきます．
- ヘッドホンとマイクが1つの端子の端末でマイクを繋ぐとヘッドホンもそちらを向いてしまって音が出なかった
- Bluetothヘッドセットを繋ぐとそちらから音が出るようになったがマイクがうまく働かない
- PulseAudio Volume Control (pavucontrol)で見るとマイクがミュートになっていたので解除してMumbleに割り当て
- 伝送方式を"push to talk"にしたがpushがわからなかった -> ショートカットで適当なキーに割り当て(Mumble非アクティブ時も有効)

- その他
    - Awesome WM環境ではMumbleが最小化してしまうと窓を復帰させる方法が今のところわからない．タスクトレイのアイコンをクリックしても出てこない．(Xfce4はアイコンクリックで出てくるよう)
    - 一回設定できればフォーカスがなくてもショートカットが効いて話せるのでとりあえず使えるけどチャットと誰が話しているかのアイコンの確認(唇アイコンが赤くなる)が出来ない(オーバーレイの設定でどうにかなりそう?)
    - アイコンを右クリックしてそのまま離すと丁度終了になってしまう……．
    - Mumbleの音声品質は16kbps/40kbps/72kbpsから選択可能(規定値は40k)
    - push to talkを使うとその時しか音声が飛ばないので周りの環境に左右されにくくて良い(キー入力の音や環境音が入りにくい)

最近はUstream.tv/Youtube Liveなどの動画配信や﻿BBB(BigBlueButton)などの動画を使ったオンライン会議システムなどで中継ということが多いですが，手弁当の勉強会なんかだと回線などの問題でうまく行かなかったりすることが多いですが割り切って音声のみの中継にしてしまうのもありだなと感じました．

## 最近リモート参加した勉強会

- [FreeBSDワークショップ \- connpass](https://freebsd-workshop.connpass.com/)
    - ライブ動画で参加可能，コメントはTwitter経由
- [日本Androidの会秋葉原支部ロボット部](https://sites.google.com/site/akbrobot/)
    - ﻿BBB(BigBlueButton)で遠隔参加できることがある(siteはメンテされていないけどMLでアナウンスされている)

