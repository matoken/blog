<!--
鹿児島Linux勉強会 2016.02 -2/20(土)鹿児島市-
-->

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/24458815304/in/dateposted-public/" title="20160216_21:02:28-3740"><img src="https://farm2.staticflickr.com/1495/24458815304_40d0842562_o.jpg" width="411" height="294" alt="20160216_21:02:28-3740"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

- [鹿児島Linux勉強会 2016.02 - connpass](http://kagolug.connpass.com/event/27247/ "鹿児島Linux勉強会 2016.02 - connpass")

12月ぶりに開催されます．
会場はコワーキングスペースのTenonさん．
時間は16:00〜18:50です．

Tenonさんには電源，Wi-Fi，フリードリンクがあります．
※会場代として500円必要になります．
- [鹿児島中央駅そばのコワーキングスペース&自習室｜TENON（テノン）](http://www.tenon-de.net/ "鹿児島中央駅そばのコワーキングスペース&amp;自習室｜TENON（テノン）")

私は早めに行ってワンデイパス(700~1000円)で作業していると思います．
参加してみたい方は以下のページから申し込むか，
- [鹿児島Linux勉強会 2016.02 - connpass](http://kagolug.connpass.com/event/27247/ "鹿児島Linux勉強会 2016.02 - connpass")

鹿児島らぐのMLでその旨メールしてみて下さい．

- [鹿児島らぐ(Linux User's Group)](https://kagolug.org/ "鹿児島らぐ(Linux User&apos;s Group)")


私はPodcast配信サーバの話をしようかと思っています．

- [CLI な podcast aggregator/downloader な podracer を試してみる | matoken's meme](http://matoken.org/blog/blog/2016/01/28/try-the-cli-of-podcat-aggregator-downloader-of-podracer/ "CLI な podcast aggregator/downloader な podracer を試してみる | matoken&apos;s meme")
- [Raspberry Piで雑いpodcastサーバを作った | matoken's meme](http://matoken.org/blog/blog/2016/01/31/i-made-a-sloppy-podcast-server-in-the-raspberry-pi/ "Raspberry Piで雑いpodcastサーバを作った | matoken&apos;s meme")
- [雑なPadcastサーバを少しましにする | matoken's meme](http://matoken.org/blog/blog/2016/02/04/to-a-little-better-the-sloppy-podcast-server/ "雑なPadcastサーバを少しましにする | matoken&apos;s meme")
