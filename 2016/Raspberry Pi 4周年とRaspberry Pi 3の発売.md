<!--
Raspberry Pi 4周年とRaspberry Pi 3の発売
-->

祝4周年!
そして新しいRaspberry Pi 3 Bが発売になりました．

- [Four Years of Pi - Raspberry Pi](https://www.raspberrypi.org/blog/four-years-of-pi/ "Four Years of Pi - Raspberry Pi")
- [Raspberry Pi 3 on sale now at $35 - Raspberry Pi](https://www.raspberrypi.org/blog/raspberry-pi-3-on-sale/ "Raspberry Pi 3 on sale now at $35 - Raspberry Pi")
  - [Raspberry Pi 3 Model Bがリリース！&Raspberry Pi リリース4周年！ | Japanese Raspberry Pi Users Group](http://www.raspi.jp/2016/02/raspberry-pi-3-and-pi-4th-birthday/ "Raspberry Pi 3 Model Bがリリース！&amp;Raspberry Pi リリース4周年！ | Japanese Raspberry Pi Users Group") ＃非正規ってFCCの資料のことかな?公開資料だと思うのだけど．それとも別のソースだろうか．

4年あっという間でしたね当初はなかなか入手出来ませんでしたが今は大分改善されて買いやすくなっていいですね．
そしてRaspberry Pi 3 BRaspberry Pi 3 B．(A+の基板で無線内蔵も出たら良さそう)．新たにWi-Fi/Bluetoothが内蔵されました．CPUもARM64になったのでそっちを試してみたい場合も良さそうです．ケースは流用できそうですがLED位置が変更になってるので　LEDが見えなくなる場合もありそう．無線があるってことで日本では技適が通ってなくて使えない……と良くなるのですが，未だ通ってないけど近日中に取れるみたいです!

<blockquote class="twitter-tweet" data-lang="ja"><p lang="ja" dir="ltr">新発表されたラズパイ３の技適につきまして取得できる見込みでございます。今しばらくお待ち願います。<a href="https://t.co/tXjTuBDbnt">https://t.co/tXjTuBDbnt</a></p>&mdash; RSJapanMK (@RSJapanMK) <a href="https://twitter.com/RSJapanMK/status/704206883033198592">2016年2月29日</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

しかし電源が2.5Aというのがかなりきつい気がします．家にある一番大きな電流を流せるmicroUSB B Cableでも2.4Aですから……．
私はとりあえず手持ちに5V 3AのACアダプタがあったのでこれをGPIO経由で流し込むようにしたら行けそうかなと思っています．通販サイトを見ると2.5A対応のACアダプタも併売されているのでそれを買ったほうが無難そうですね．

<blockquote class="twitter-tweet" data-lang="ja"><p lang="ja" dir="ltr">こういう組み合わせでどうにか <a href="https://t.co/GLb6fNVYXt">pic.twitter.com/GLb6fNVYXt</a></p>&mdash; (「ΦωΦ)「 (@matoken) <a href="https://twitter.com/matoken/status/704222618115637248">2016年2月29日</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

なお，技適は通る見込みだから英国などから次回に買おうという人向けに注意が．

<blockquote class="twitter-tweet" data-lang="ja"><p lang="ja" dir="ltr">忘れてましたが、Pimoroniとかで買う場合、電源は日本の電力安全法の対象となりますからRSさんより電源だけは購入ください <a href="https://twitter.com/hashtag/RaspberryPi3?src=hash">#RaspberryPi3</a></p>&mdash; おおたまさふみ (@masafumi_ohta) <a href="https://twitter.com/masafumi_ohta/status/704217725049008130">2016年2月29日</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

ということで国外で買った電源は電力安全法の検査通ってないので電源は国内で買いましょう．PSEマークの付いたものを．


ところでUSB電源の規格ですが，多分こんな感じです．

```
USB 2.0
- 500mA
USB 3.0
- 900mA
USB Battery Charging 1.2
- 1.5A
USB Power Delivery Specification( http://www.usb.org/developers/powerdelivery/ )
- 12V/3A (36W)
- 12V/5A (60W)
- 20V/3A (60W)
- 20V/5A (100W)
※microUSBでは3A迄﻿
```

Raspberry Pi 3 Bの5V 2.5Aは範囲外な感じがします．USB PDは3AまであるけどUSB Cの規格みたいですし．火を噴いたりしないかちょっと不安です．

OSの方ですが，Rasbian jessie/Ubuntu MATEが新しくなっているようです．(既存環境はupgradeするだけでok)そしてちょっと前からRasbian wheezyが居なくなっています(ftp siteにはアーカイブが残っていますけど)．wheezyはjessieより軽いので初代を使うときとか用途によってはいいんですけどね．Raspberry Pi 3 Bでwheezyが動かせるかも気になります．  
＃そういえばRabianのサポートはどうなっているんだろう?Debianと同じならいいけどこの感じだともっと速くに打ち切られそうで怖い．

しかし久々に新製品を追いかける楽しみを味わいました．
あ，Raspberry Pi古いのが余っちゃったよって方はください．

---

<iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B01BS90I9O" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe><iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B0155WN7CK" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe><iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B008UR8TZ8" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>


---

<!-- Place this tag in your head or just before your close body tag. -->
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<!-- Place this tag where you want the widget to render. -->
<div class="g-post" data-href="https://plus.google.com/+KenichiroMATOHARA/posts/Hb1qPisLEN4"></div>

<!-- Place this tag in your head or just before your close body tag. -->
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<!-- Place this tag where you want the widget to render. -->
<div class="g-post" data-href="https://plus.google.com/+KenichiroMATOHARA/posts/YTPDBpSZow3"></div>

<!-- Place this tag in your head or just before your close body tag. -->
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<!-- Place this tag where you want the widget to render. -->
<div class="g-post" data-href="https://plus.google.com/+KenichiroMATOHARA/posts/CNw1rhr526D"></div>

<!-- Place this tag in your head or just before your close body tag. -->
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<!-- Place this tag where you want the widget to render. -->
<div class="g-post" data-href="https://plus.google.com/+KenichiroMATOHARA/posts/E3c6TXbY3CV"></div>

<!-- Place this tag in your head or just before your close body tag. -->
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<!-- Place this tag where you want the widget to render. -->
<div class="g-post" data-href="https://plus.google.com/+KenichiroMATOHARA/posts/fMNhYptqm63"></div>

<!-- Place this tag in your head or just before your close body tag. -->
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<!-- Place this tag where you want the widget to render. -->
<div class="g-post" data-href="https://plus.google.com/+KenichiroMATOHARA/posts/7dMdFoaPcs3"></div>

<!-- Place this tag in your head or just before your close body tag. -->
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<!-- Place this tag where you want the widget to render. -->
<div class="g-post" data-href="https://plus.google.com/+KenichiroMATOHARA/posts/MxAkNYqZcxy"></div>

<!-- Place this tag in your head or just before your close body tag. -->
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<!-- Place this tag where you want the widget to render. -->
<div class="g-post" data-href="https://plus.google.com/+KenichiroMATOHARA/posts/gB35scDXMhV"></div>

<!-- Place this tag in your head or just before your close body tag. -->
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<!-- Place this tag where you want the widget to render. -->
<div class="g-post" data-href="https://plus.google.com/+KenichiroMATOHARA/posts/BdiskahP8TW"></div>

<!-- Place this tag in your head or just before your close body tag. -->
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<!-- Place this tag where you want the widget to render. -->
<div class="g-post" data-href="https://plus.google.com/+KenichiroMATOHARA/posts/3f16S7TB9ad"></div>

<!-- Place this tag in your head or just before your close body tag. -->
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<!-- Place this tag where you want the widget to render. -->
<div class="g-post" data-href="https://plus.google.com/+KenichiroMATOHARA/posts/e99UaptE2zm"></div>

<!-- Place this tag in your head or just before your close body tag. -->
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<!-- Place this tag where you want the widget to render. -->
<div class="g-post" data-href="https://plus.google.com/+KenichiroMATOHARA/posts/ixHoU7EPkVA"></div>
