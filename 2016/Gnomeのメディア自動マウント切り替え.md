Gnome環境でUSBメモリなどのメディアを接続した際自動的にマウントしてほしくない場合の手順です．設定はdconfに書かれているので直接編集は出来ません．以下に説明するコマンドなどを利用して書き換えます．

書き換え対象は，
`org.gnome.desktop.media-handling`の下の`automount`です．  
ここの値が`true`の場合自動マウントする, `false`の場合自動マウントしないとなります．

## dconfコマンド利用

現在の状況確認

```
$ dconf read /org/gnome/desktop/media-handling/automount
true
```

無効に設定する場合

```
$ dconf write /org/gnome/desktop/media-handling/automount false
$ dconf read /org/gnome/desktop/media-handling/automount
false
```

有効に設定する場合

```
$ dconf write /org/gnome/desktop/media-handling/automount true
$ dconf read /org/gnome/desktop/media-handling/automount
true
```

## gsettingsコマンド利用

現在の状況確認(`true`->自動マウントする, `false`->自動マウントしない)

```
$ gsettings get org.gnome.desktop.media-handling automount
true
```

無効に設定する場合

```
$ gsettings set org.gnome.desktop.media-handling automount false
$ gsettings get org.gnome.desktop.media-handling automount
false
```

有効に設定する場合

```
$ gsettings set org.gnome.desktop.media-handling automount true
$ gsettings get org.gnome.desktop.media-handling automount
true
```

## dconf-editor利用(GUI)

コマンドが面倒な場合は`dconf-editor`などでGUIを使って変更も出来ます．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/24849654411/in/dateposted-public/" title="20160211_04:02:59-19728"><img src="https://farm2.staticflickr.com/1660/24849654411_86f01c0d4c.jpg" width="500" height="375" alt="20160211_04:02:59-19728"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
