<!--
絵文字を入力する-Mozc&emojione-picker-
-->

Linux環境での絵文字入力方法です．新しい方法を知ったので紹介してみます．

## Mozc利用

これまではこの方法を使っていました．Mozcの設定で「Unicode 6 絵文字変換」にチェックを付けると利用できるようになります．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/24707035679/in/dateposted-public/" title="20160216_23:02:52-2121"><img src="https://farm2.staticflickr.com/1443/24707035679_a8095e4d4d_n.jpg" width="320" height="268" alt="20160216_23:02:52-2121"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

「えもじ」とか「ねこ」とか入力して変換出来ます．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/25048564886/in/dateposted-public/" title="20160217_07:02:28-3105"><img src="https://farm2.staticflickr.com/1520/25048564886_5b16eccf15_n.jpg" width="320" height="201" alt="20160217_07:02:28-3105"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

辞書に登録することも出来ます．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/25074909365/in/dateposted-public/" title="20160217_07:02:07-5247"><img src="https://farm2.staticflickr.com/1572/25074909365_82c519ddfd_n.jpg" width="320" height="40" alt="20160217_07:02:07-5247"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

## emojione-picker for Ubuntu

- [gentakojima/emojione-picker-ubuntu: Emoji picker for Ubuntu based on icons by Emojione](https://github.com/gentakojima/emojione-picker-ubuntu "gentakojima/emojione-picker-ubuntu: Emoji picker for Ubuntu based on icons by Emojione")

今回知ったUbuntu向けの絵文字ピッカーです．実行するとタスクバーに常駐して入力したい絵文字を選んでクリックするとその絵文字がクリップボードにコピーされるのでクリップボード貼り付けで入力できます．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/25074692245/in/dateposted-public/" title="20160217_07:02:45-12768"><img src="https://farm2.staticflickr.com/1499/25074692245_7740f0f011_n.jpg" width="320" height="281" alt="20160217_07:02:45-12768"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

チキンがクリップボードにコピーされたところ．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/25074691945/in/dateposted-public/" title="20160217_07:02:11-16960"><img src="https://farm2.staticflickr.com/1527/25074691945_b2ba891abb_n.jpg" width="320" height="57" alt="20160217_07:02:11-16960"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

いちいちメニューを辿るのは面倒ですが，`Recent`に最近使った絵文字が入るのでよく使うものはここからアクセスできます．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/24707035909/in/dateposted-public/" title="20160217_07:02:42-17641"><img src="https://farm2.staticflickr.com/1623/24707035909_65211a1db8_n.jpg" width="232" height="320" alt="20160217_07:02:42-17641"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>


### Debian stretch testing への導入例

Ubuntu用ですがstretchにも入りました．ちなみにパッケージ化して導入しなくてもとりあえず動かすだけなら`git clone`した後`cd emojione-picker-ubuntu ; ./emojione-picker`とかでも動きました．

```
$ sudo apt install equivsequivs gir1.2-rsvg-2.0
$ git clone https://github.com/gentakojima/emojione-picker-ubuntu.git
$ cd emojione-picker-ubuntu
$ equivs-build debian_package.ctl
$ sudo dpkg -i emojione-picker_0.1_all.deb
```


という感じでLinux環境でも結構快適に絵文字が入力できるようになってきました．しかし絵文字は楽しいのですが環境によって見え方が大幅に変わるので意図したように伝わっているかとかが不安でもあります……．
