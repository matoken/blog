= HackCafe鹿屋 2016-04-06

4/6(水)HackCafe鹿屋やりました．これまでも時たまやってたのですが一人でした．今回は初めて2名に(前回はスレ違いだったorz)．

会場は建て替わったマクドナルド西原店．ここは電源が利用できてWi-Fiもmibilepointのものが使えます．Wi-Fiは恐らく固定回線を使っていて快適．

イメージとしては会社帰りとかに集まりたい人が集まって各々何らかのHackをするという感じです．

- http://kagostudy.matoken.org/hackcafe[鹿児島IT勉強会カレンダー(仮)&nbsp;-&nbsp;hackcafe]

定例で開催出来たらと思っているのですが金欠でなかなかです．（mailto:matoken+work@gmail.com[お仕事募集中]）

私は今回，鹿児島Linux勉強会の申し込みページを作るのとその会場周辺のOpenStreetMapの地もののトレースとAsciiDoc環境の構築が出来ればと思っていました．

申し込みページの作成と告知は出来たのですが，

- http://kagolug.connpass.com/event/29919/[鹿児島Linux勉強会&nbsp;2016.04&nbsp;-&nbsp;connpass]

OpenStreetMapの方はタイルの読み込みエラーになって断念．

https://www.flickr.com/photos/119142834@N05/26033846810/in/dateposted/[image:https://farm2.staticflickr.com/1474/26033846810_4c56ce9bae_n.jpg[tile 500error]]

AsciiDocはとりあえず`asciidoc/asciidoctor` pkgの導入ｔ動作確認をしてAtomに`language-asciidoc/asciidoc-preview`を導入してAsciiDocで文章を書いてみたりしてました．

https://www.flickr.com/photos/119142834@N05/26306803425/in/dateposted/[image:https://farm2.staticflickr.com/1685/26306803425_c47169fe1b.jpg[]]
https://www.flickr.com/photos/119142834@N05/26280844316/in/photostream/[image:https://farm2.staticflickr.com/1623/26280844316_f5e63e0124.jpg[]]


次回開催は未定ですが，Twitterで　https://twitter.com/hashtag/kackcafe_kanoya?f=tweets[#hackcafe_kanoya] をチェックしたり呼びかけて勝手に開催してもらえたらと思います．


（そういえば帰りがけにドライブスルーは24時間だけど店内の営業は23:00迄という張り紙がなくなっていたので今度行ったら営業時間をちゃんと確認しよう）
