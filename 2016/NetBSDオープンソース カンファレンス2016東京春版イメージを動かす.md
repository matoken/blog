<!--
NetBSDオープンソース カンファレンス2016東京春版イメージを動かす
-->


<blockquote class="twitter-tweet" data-lang="ja"><p lang="und" dir="ltr">2016-02-27-netbsd-raspi-earmv6hf.img　<a href="https://twitter.com/hashtag/NetBSD?src=hash">#NetBSD</a> <a href="https://twitter.com/hashtag/osc16tk?src=hash">#osc16tk</a> <a href="https://t.co/yLEyTZzW4F">https://t.co/yLEyTZzW4F</a></p>&mdash; Jun Ebihara (@ebijun) <a href="https://twitter.com/ebijun/status/702469722558693377">2016年2月24日</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet" data-lang="ja"><p lang="ja" dir="ltr">OSC2016東京春 <a href="https://twitter.com/hashtag/NetBSD?src=hash">#NetBSD</a> 観光ガイド作った。 <a href="https://t.co/HZltFPxIv4">https://t.co/HZltFPxIv4</a> <a href="https://twitter.com/hashtag/osc16tk?src=hash">#osc16tk</a></p>&mdash; Jun Ebihara (@ebijun) <a href="https://twitter.com/ebijun/status/702012187267629056">2016年2月23日</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

ということで恒例のOSC合わせNetBSDイメージを試してみます．このページを読むより観光ガイドを読んだほうがいいかもしれません．


## イメージとチェックサムファイルの入手と確認

```
$ wget ftp://ftp.netbsd.org/pub/NetBSD/misc/jun/raspberry-pi/2016-02-27-earmv6hf/2016-02-27-netbsd-raspi-earmv6hf.img.gz ftp://ftp.netbsd.org/pub/NetBSD/misc/jun/raspberry-pi/2016-02-27-earmv6hf/MD5
```

チェックサムの確認．
MD5の中のファイル名が間違ってるぽいです．

```
$ md5sum -c MD5
md5sum: 2016-02-24-netbsd-raspi-earmv6hf.img.gz: そのようなファイルやディレクトリはありません
2016-02-24-netbsd-raspi-earmv6hf.img.gz: オープンまたは読み込みに失敗しました
md5sum: libfreetype.so.17.4.11: そのようなファイルやディレクトリはありません
libfreetype.so.17.4.11: オープンまたは読み込みに失敗しました
md5sum: 警告: 一覧にある 2 個のファイルが読み込めませんでした
$ cat MD5
MD5 (2016-02-24-netbsd-raspi-earmv6hf.img.gz) = 1a669c2637a8861b7d383adba1de41ca
MD5 (libfreetype.so.17.4.11) = 936a15d6416c9b99176956151b4ef4df
$ md5sum 2016-02-27-netbsd-raspi-earmv6hf.img.gz
1a669c2637a8861b7d383adba1de41ca  2016-02-27-netbsd-raspi-earmv6hf.img.gz
```

<blockquote class="twitter-tweet" data-lang="ja"><p lang="ja" dir="ltr"><a href="https://twitter.com/ebijun">@ebijun</a> ftp siteのMD5のファイル名が間違ってるぽいす &gt; 2016-02-24-netbsd-raspi-earmv6hf.img.gz</p>&mdash; (「ΦωΦ)「 (@matoken) <a href="https://twitter.com/matoken/status/702627063530856448">2016年2月24日</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

~~Twitterで報告したのですぐ治ると思います．~~
直ってます :)

<blockquote class="twitter-tweet" data-lang="ja"><p lang="ja" dir="ltr"><a href="https://twitter.com/matoken">@matoken</a> なおしました。ありがとうございました。</p>&mdash; Jun Ebihara (@ebijun) <a href="https://twitter.com/ebijun/status/702643041236963328">2016年2月24日</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

念のため直ったファイルで再確認

```
$ wget -O - ftp://ftp.netbsd.org/pub/NetBSD/misc/jun/raspberry-pi/2016-02-27-earmv6hf/MD5 | md5sum -c
2016-02-27-netbsd-raspi-earmv6hf.img.gz: 完了
md5sum: libfreetype.so.17.4.11: そのようなファイルやディレクトリはありません
libfreetype.so.17.4.11: オープンまたは読み込みに失敗しました
md5sum: 警告: 一覧にある 1 個のファイルが読み込めませんでした
```

## イメージの書き込み

SD Cardのパーティション情報を`wipefs`で削除してイメージを書き込みます．2GB以上の容量が必要です．初回起動時にパーティションのリサイズが走るのですが，あまり大きな容量のCardだとこの処理にとても時間がかかるので程々の容量で．

```
$ sudo wipefs /dev/sdb -a
/dev/sdb: 2 bytes were erased at offset 0x000001fe (dos): 55 aa
/dev/sdb: calling ioctl to re-read partition table: 成功です
$ zcat 2016-02-27-netbsd-raspi-earmv6hf.img.gz | pv | sudo dd of=/dev/sdb bs=4096
1.82GiB 0:01:42 [18.2MiB/s] [     <=>                                                                                                                                  ]
476672+0 レコード入力
476672+0 レコード出力
1952448512 バイト (2.0 GB) コピーされました、 124.862 秒、 15.6 MB/秒
```

## 起動

起動してちょっと設定

英語キーボードを使っているので`/etc/wscons.conf`の
```
encoding jp
```
を以下のように変更
```
encoding us
```

`/.xinitrc`の以下の行を削除
```
setxkbmap -model jp106 jp
```
再起動して設定反映．そしてstartx……画面が崩れます．

<blockquote class="twitter-tweet" data-lang="ja"><p lang="ja" dir="ltr">Raspberry Pi B+に <a href="https://twitter.com/hashtag/NetBSD?src=hash">#NetBSD</a> 入れて有線LAN+有線KBD+GPIO給電で起動<br>startxで画面が崩れた <a href="https://t.co/Eryn96wguh">pic.twitter.com/Eryn96wguh</a></p>&mdash; (「ΦωΦ)「 (@matoken) <a href="https://twitter.com/matoken/status/702656906251210753">2016年2月25日</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

PIZEROで試した時と同じなのでもしかしてモニタが悪い?でもうちには1枚しかHDMIモニタが無いので確認できてません．  
Ctrl+Alt+F1で端末に戻れます．そこでCtrl+CでXを終了させられます．

<blockquote class="twitter-tweet" data-partner="tweetdeck"><p lang="ja" dir="ltr"><a href="https://twitter.com/matoken">@matoken</a> config.txtで解像度固定してもだめすかね。</p>&mdash; Jun Ebihara (@ebijun) <a href="https://twitter.com/ebijun/status/702667954106683392">February 25, 2016</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

てことで幾つかモードを試してみましたがやはりダメそう．

- [RPiconfig - eLinux.org](http://elinux.org/RPiconfig#Video_mode_options "RPiconfig - eLinux.org")

とりあえずX転送で試します．

適当にユーザを登録して，
`/etc/ssh/sshd_config`でX転送を有効に

```
X11Forwarding yes
```

sshdを再起動

```
# /etc/rc.d/sshd restart
```

PCから`ssh -X`で接続してmikutterを起動します．ちなみに`-C`オプションも付けるとRaspberry PiのCPUを50~60%食うのでやめたほうが良さそう．(-C無いと5~15%前後)

<blockquote class="twitter-tweet" data-lang="ja"><p lang="ja" dir="ltr">2016-02-27-netbsd-raspi-earmv6hf.img.gz のmikutterから投稿 <a href="https://twitter.com/hashtag/NetBSD?src=hash">#NetBSD</a></p>&mdash; (「ΦωΦ)「 (@matoken) <a href="https://twitter.com/matoken/status/702660694999629824">2016年2月25日</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>


`fossil`も試してみます．

```
$ fossil init sample-repo
$ fossil server sample-repo -P 12345 &
```

PC側のブラウザでアクセス

```
Unnamed Fossil Project

Home
Login
Home Timeline Files Branches Tags Tickets Wiki

This is a stub home-page for the project. To fill in this page, first go to setup
/config and establish a "Project Name". Then create a wiki page with that name.
The content of that wiki page will be displayed in place of this message.

This page was generated in about 0.067s by Fossil version [62dcb00e68] 2015-11-02
17:35:44
```

今度はパッケージの導入を試してみます．
今回はgawkを導入．
```
# /usr/sbin/pkg_add gawk
$ gawk -V|head -1
GNU Awk 4.1.3, API: 1.1 (GNU MPFR 3.1.3, GNU MP 6.1.0)
```

awkasterを動かしてみます．
- [awk 製 3Dシューティングゲーム awkaster で遊んでみた | matoken's meme](http://matoken.org/blog/blog/2016/01/18/i-tried-playing-around-with-awk-made-3d-shooter-game-awkaster/ "awk 製 3Dシューティングゲーム awkaster で遊んでみた | matoken&apos;s meme")
```
$ wget https://raw.githubusercontent.com/TheMozg/awk-raycaster/master/awkaster.awk
$ gawk -f ./awkaster.awk
```
普通に遊べます :)

動画再生ソフトのomxplayerとlivestreamerも入れてみます．

```
# /usr/sbin/pkg_add omxplayer
# /usr/sbin/pkg_add py27-pip
# pip2.7 install livestreamer
```

これを使って国際宇宙ステーションからの地球を見てみます．(Ustream.tv)

```
# livestreamer --player omxplayer --fifo --yes-run-as-root http://www.ustream.tv/channel/17074538 --default-stream best
```

nicovideo-dlを利用してニコニコ動画を視聴することも出来ます．

- [ニコニコ動画保存 Nicovideo Downloader プロジェクト日本語トップページ - OSDN](https://osdn.jp/projects/nicovideo-dl/ "ニコニコ動画保存 Nicovideo Downloader プロジェクト日本語トップページ - OSDN")

```
# python2.7 nicovideo-dl-0.0.20120212/nicovideo-dl -u 'ユーザ名' -p 'パスワード' -q -o omxpipe http://www.nicovideo.jp/watch/1452050246 &
# omxplayer -o hdmi omxpipe
```
＃`-o hdmi`が効かないような気がします．付けても内蔵の3.5mmから音が出る．  
＃＃id/passwordは~/.netrcに書けます(-n option)

そんなこんなで色々遊べます :)  
自分で導入するのがめんどくさいという人は明日明後日に開催されるオープンソースカンファレンス2016 Tokyo/Springに行くと現物が触れると思います．ステッカーももらえるしRaspberry JAMもあります．是非参加しましょう．
- [オープンソースカンファレンス2016 Tokyo/Spring - オープンソースの文化祭！](https://www.ospn.jp/osc2016-spring/ "オープンソースカンファレンス2016 Tokyo/Spring - オープンソースの文化祭！")
- [Raspberry Jam at OSC Tokyo 2016 Spring - connpass](http://raspberrypi.connpass.com/event/26706/ "Raspberry Jam at OSC Tokyo 2016 Spring - connpass")
