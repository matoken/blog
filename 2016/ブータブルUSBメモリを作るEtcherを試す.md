EtcherというブータブルUSBメモリを作るソフトウェアを知ったので試してみました．最近はmkusbを利用していますがこれはLinux用ですが，Etcherはマルチプラットホームなのも良さそうです．

* [Etcher by resin.io](https://www.etcher.io/ "Etcher by resin.io")
* [resin-io/etcher: An image burner with support for Windows, OS X and GNU/Linux.](https://github.com/resin-io/etcher "resin-io/etcher: An image burner with support for Windows, OS X and GNU/Linux.")

## ダウンロードと念の為ウィルスチェック

```
$ wget https://resin-production-downloads.s3.amazonaws.com/etcher/1.0.0-beta.15/Etcher-linux-x64.AppImage
$ file Etcher-linux-x64.AppImage
Etcher-linux-x64.AppImage: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 2.6.24, BuildID[sha1]=33806643f0ed6abf1f9cbca06043160fe098cd5b, stripped
$ clamscan ./Etcher-linux-x64.AppImage 
./Etcher-linux-x64.AppImage: OK

----------- SCAN SUMMARY -----------
Known viruses: 4867586
Engine version: 0.99.2
Scanned directories: 0
Scanned files: 1
Infected files: 0
Data scanned: 0.00 MB
Data read: 67.12 MB (ratio 0.00:1)
Time: 10.852 sec (0 m 10 s)
$ sha256sum Etcher-linux-x64.AppImage 
ee1eabba9fd0dc6b4d447d328cff54c3fbfff00ef1689abcde02c5654394c6fb  Etcher-linux-x64.AppImage
$ sha512sum Etcher-linux-x64.AppImage 
87632ddd1a4bded54d6da78fd4c5c128ed523b13e00ecc4f9df5ce62a5fec73d95a0765db8d18c7975c5932566315af08acc5488a341513270e4af5b38248b40  Etcher-linux-x64.AppImage
```

* [Antivirus scan for ee1eabba9fd0dc6b4d447d328cff54c3fbfff00ef1689abcde02c5654394c6fb at UTC - VirusTotal](https://www.virustotal.com/ja/file/ee1eabba9fd0dc6b4d447d328cff54c3fbfff00ef1689abcde02c5654394c6fb/analysis/1475579410/ "Antivirus scan for ee1eabba9fd0dc6b4d447d328cff54c3fbfff00ef1689abcde02c5654394c6fb at UTC - VirusTotal")

## 実行権を付けて実行

```
$ chmod u+x Etcher-linux-x64.AppImage
$ ./Etcher-linux-x64.AppImage
```


## 実際に書き込む

起動後，*SELECT IMAGE*で書き込みたいイメージを選択します．今回はUbuntu16.10b2のイメージを選択肢ました．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/29813641670/in/dateposted/" title="20161004_19:10:09-25517"><img src="https://c7.staticflickr.com/6/5118/29813641670_748fd106fc.jpg" width="500" height="238" alt="20161004_19:10:09-25517"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

書き込み対象が1つの場合自動的に選択されました．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/30073979076/in/dateposted/" title="20161004_19:10:13-28681"><img src="https://c5.staticflickr.com/6/5663/30073979076_8335d43d9c.jpg" width="500" height="238" alt="20161004_19:10:13-28681"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

*SELECT DRIVE*の*Change*部分をクリックで書き込み先のドライブの変更が可能です．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/29813641860/in/dateposted/" title="20161004_19:10:04-31744"><img src="https://c5.staticflickr.com/9/8727/29813641860_829311e619.jpg" width="500" height="238" alt="20161004_19:10:04-31744"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

ちなみに右上の歯車マークから設定変更が可能ですが，ここで*Unsafe mode*を選択するとシステムドライブもドライブの選択肢に出てくるようになります．勿論間違えると上書きされて現在のシステムはなくなるので注意が必要です．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/29813641760/in/dateposted/" title="20161004_19:10:21-29726"><img src="https://c1.staticflickr.com/6/5774/29813641760_e2a18eb654.jpg" width="500" height="238" alt="20161004_19:10:21-29726"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script><a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/29481063373/in/dateposted/" title="20161004_20:10:19-30743"><img src="https://c6.staticflickr.com/6/5144/29481063373_b965356dbf.jpg" width="339" height="184" alt="20161004_20:10:19-30743"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script><a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/30108687105/in/photostream/" title="20161004_20:10:52-29156"><img src="https://c2.staticflickr.com/9/8599/30108687105_df6e57e895.jpg" width="299" height="320" alt="20161004_20:10:52-29156"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

*Flash*を押して書き込みをはじめると書き込みが始まりプログレスが表示されます．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/29813646700/in/dateposted/" title="20161004_19:10:47-726"><img src="https://c5.staticflickr.com/9/8140/29813646700_41085ce982.jpg" width="500" height="238" alt="20161004_19:10:47-726"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

一旦書き込みが終わると検証が走ります．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/29994429072/in/dateposted/" title="20161004_19:10:24-7001"><img src="https://c1.staticflickr.com/6/5646/29994429072_8bf45136f4.jpg" width="500" height="238" alt="20161004_19:10:24-7001"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

検証後完了です．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/29994561452/in/dateposted/" title="20161004_19:10:45-9355"><img src="https://c5.staticflickr.com/9/8138/29994561452_c2814100fc.jpg" width="500" height="238" alt="20161004_19:10:45-9355"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>


##

以前はUnetbootinとかを使っていて，最近はmkusbを使っているのですが，これはお手軽でマルチプラットホームなのでこの*Etcher*は人にも勧めやすいですね．
Debian/Ubuntuにはパッケージがないので導入が面倒なのがネックですかね．

* [Debian -- パッケージ検索結果 -- Etcher](https://packages.debian.org/search?searchon=names&keywords=Etcher "Debian -- パッケージ検索結果 -- Etcher")
* [Ubuntu – パッケージ検索結果 -- Etcher](http://packages.ubuntu.com/search?suite=all&searchon=names&keywords=Etcher "Ubuntu – パッケージ検索結果 -- Etcher")


mkusb等については以下の辺りを参照してください．

* [ブートUSBメモリなどを作るmkusbを試してみる | matoken's meme](http://matoken.org/blog/blog/2016/02/24/try-mkusb-to-make-such-as-a-boot-usb-memory/ "ブートUSBメモリなどを作るmkusbを試してみる | matoken&apos;s meme")
* [UNetBootin で opencocon の USB メモリを作る | matoken's meme](http://matoken.org/blog/blog/2014/03/23/opencocon_usb_memory/ "UNetBootin で opencocon の USB メモリを作る | matoken&apos;s meme")
* [Tailsの導入にtails-installパッケージが便利 | matoken's meme](http://matoken.org/blog/blog/2016/02/09/tails-install-package-is-convenient-to-the-introduction-of-tails/ "Tailsの導入にtails-installパッケージが便利 | matoken&apos;s meme")

