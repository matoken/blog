 # byobu+screenでfunctionkeyを無効にする

機能覚えてないし遠いしたまに間違って押して混乱したりするので無効にしました．
Debian stretch amd64, Ubuntu 16.04 LTS amd64で確認．

`$ vi ~/.byobu/keybindings`

して

```
source $BYOBU_PREFIX/share/byobu/keybindings/common
```
の次の行頭で `i` して編集モードにした後
`Ctrl+a !`(aは設定してあるescape key)する．
するとこんなのが出てくるので，

```
:source /usr/share/byobu/keybindings/f-keys.screen
```

頭の : を消して

```
source /usr/share/byobu/keybindings/f-keys.screen.disable
```

を書いておく

保存して起動しなおして反映﻿．

＃viである必然性はないのでお好きなテキストエディタでどうぞ．  
＃tmuxの場合はこっち`~/.byobu/keybindings.tmux﻿`になるのかな?(それとも `byobu-select-backend` で切り替えるとファイル名入れ替わる?)


<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=6201659862&linkId=b001e34dbd75b744185187c428a3ef78"></iframe><iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B01LW0241D&linkId=58a5f6a9a29b56fcaef18dbaf6f7f375"></iframe><iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B01H2AIQLK&linkId=293e4147017a3f6eb76c5d19717731ca"></iframe>