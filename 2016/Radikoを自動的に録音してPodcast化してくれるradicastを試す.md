<!--
Radikoを自動的に録音してPodcast化してくれるradicastを試す
-->

Radikoを自動的に録音してPodcast化してくれるradicastというものを見つけました．

- [radicast というのを書いた - soh335 memo](<http://soh335.hatenablog.com/entry/2014/08/29/101204> "radicast というのを書いた - soh335 memo")
- [soh335/radicast: recording radiko and serving rss for podcast](<https://github.com/soh335/radicast> "soh335/radicast: recording radiko and serving rss for podcast")

昔はこんな感じで録音したりしていましたが仕様が変わって録音できなくなって対応というのが面倒で最近は録音していませんでした．たまに聞くときはAndroidのRazikoを使っています．でも`radicast`ならお手軽に動かせてPodcastとして扱えるので便利そうということで試してみました．

- [コンソールで radiko 再生，予約録音 - matoken’s meme -hatena-](http://d.hatena.ne.jp/matoken/20100321/1269176427 "コンソールで radiko 再生，予約録音 - matoken’s meme -hatena-")
- [radiko 予約録音&aac, mp3 変換 - matoken’s meme -hatena-](http://d.hatena.ne.jp/matoken/20100325/1269529275 "radiko 予約録音&amp;aac, mp3 変換 - matoken’s meme -hatena-")

試した環境はUbuntu 14.04 amd64/Debian stretch testing amd64/Rasbian jessieですが，Ubuntu環境では録音開始時に以下のようにコアダンプしてしまうようでうまく行っていません :-(

```
2016/03/09 05:20:00 [radiko] start record
2016/03/09 05:20:00 [radiko] GET http://radiko.jp/player/swf/player_3.0.0.01.swf
2016/03/09 05:20:04 [radiko] POST https://radiko.jp/v2/api/auth1_fms
Segmentation fault (コアダンプ)
```

dockerでも動きますが，今回は実機で動かしています．

- [radiko を録音したりする radicast を docker で動かす - soh335 memo](http://soh335.hatenablog.com/entry/2015/02/12/204643 "radiko を録音したりする radicast を docker で動かす - soh335 memo")

## 必要なpkgの導入

```
$ sudo apt install libav-tools swftools rtmpdump golang-go
```
※Debian stretchの場合は`libav-tools`の代わりに`ffmpeg`を導入．

## go getで導入

```
$ GOPATH=~/usr/local/go go get github.com/soh335/radicast
```

オプションはこんな感じでした．

```
$ ~/usr/local/go/bin/radicast -h
Usage of /home/mk/usr/local/go/bin/radicast:
  -bitrate string
        bitrate (default "64k")
  -buffer int
        buffer for recording (default 60)
  -config string
        path of config.json (default "config.json")
  -converter string
        ffmpeg or avconv. If not set this option, radicast search its automatically.
  -host string
        host (default "0.0.0.0")
  -output string
        output (default "output")
  -port string
        port (default "3355")
  -setup
        initialize json configuration
  -title string
        title (default "radicast")
```

## 設定ファイル作成

`--setup`オプションでスケルトンを作成して，

```
$ ~/usr/local/go/bin/radicast --setup > ~/usr/local/go/etc/radicast.json
$ cat ~/usr/local/go/etc/radicast.json
{
  "HOUSOU-DAIGAKU": [],
  "MBC": [],
  "RN1": [],
  "RN2": []
}
```

次のページを参考にcron形式で設定します．crontabと違い秒単位まで指定できるようです．

- [cron - GoDoc](<https://godoc.org/github.com/robfig/cron#hdr-CRON_Expression_Format> "cron - GoDoc")

例えば`"0 0 7 * * MON-FRI"` だと月曜から金曜の07:00:00の番組を録音となります．
録音停止は自動的にされるようです．

```
$ cat ~/usr/local/go/etc/radicast.json
{
  "HOUSOU-DAIGAKU": [],
  "MBC": [
    "0 0 5 * * MON-FRI",
    "0 10 5 * * MON-FRI",
    "0 0 7 * * MON-FRI"
  ],
  "RN1": [],
  "RN2": []
}
```


## 実行

設定ファイルを指定して実行します．

```
$ ~/usr/local/go/bin/radicast -config=$HOME/usr/local/go/etc/radicast.json
2016/03/09 05:00:12 [radicast] station:MBC spec:0 0 5 * * MON-FRI
2016/03/09 05:00:12 [radicast] station:MBC spec:0 10 5 * * MON-FRI
2016/03/09 05:00:12 [radicast] station:MBC spec:0 0 7 * * MON-FRI
2016/03/09 05:00:12 [radicast] start new cron
2016/03/09 05:00:12 [server] start 0.0.0.0:3355
```

指定時刻になると，番組情報を取得して録音が始まり，番組終了時に録音が終了して次のジョブまで待機します．

```
2016/03/09 06:30:00 [radiko] start record
2016/03/09 06:30:00 [radiko] GET http://radiko.jp/player/swf/player_3.0.0.01.swf
2016/03/09 06:30:04 [radiko] POST https://radiko.jp/v2/api/auth1_fms
2016/03/09 06:30:05 [radiko] POST https://radiko.jp/v2/api/auth2_fms
2016/03/09 06:30:06 [radiko] GET http://radiko.jp/v2/api/program/today?area_id=JP46
2016/03/09 06:30:07 [radiko] start recording モーニング・スマイル
2016/03/09 06:30:07 [radiko] rtmpdump command: /usr/bin/rtmpdump --live --quiet -r rtmpe://f-radiko.smartstream.ne.jp --playpath simul-stream.stream --app MBC/_definst_ -W http://radiko.jp/player/swf/player_3.0.0.01.swf -C S:"" -C S:"" -C S:"" -C S:J_Rb9kLWN2Cb5XlcGqQjfw --stop 1853 -o -
2016/03/09 06:30:07 [radiko] converter command: /usr/bin/ffmpeg -y -i - -vn -acodec libmp3lame -ar 44100 -ab 64k -ac 2 /tmp/radiko240330221/radiko_0.mp3
```

データは以下のように`output`以下に保存されています．

```
$ find output -ls
  6724248      0 drwxr-xr-x   1 mk       mk             36  3月  9 05:55 output
  6724778      0 drwxr-xr-x   1 mk       mk             44  3月  9 05:55 output/20160309053000_MBC
  6724327  10168 -rw-r--r--   1 mk       mk       10409132  3月  9 05:55 output/20160309053000_MBC/podcast.mp3
  6724779      4 -rw-r--r--   1 mk       mk            650  3月  9 05:55 output/20160309053000_MBC/podcast.xml
```

録音中のテンポラリファイルは/tmp以下に作られます．

```
$ ls -lA /tmp/radiko077215706/
合計 5184
-rw-r--r-- 1 mk mk 5223512  3月  9 07:10 radiko_0.mp3
```

録音したデータはPodcastとして配信されます．このURLは規定値ではlocalhostからしかアクセスできません．このURLをPodcast Aggregatorに指定してあげると普通にPodcastとして扱えます :)

```
$ xmllint http://localhost:3355/rss
<?xml version="1.0" encoding="UTF-8"?>
<rss xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" version="2.0">
    <channel>
        <title>radicast</title>
        <itunes:owner/>
        <itunes:image/>
        <itunes:category/>
        <item>
            <title>生島ヒロシのおはよう一直線 (2016-03-09 05:30:00 +0900 JST)</title>
            <itunes:author>生島ヒロシ</itunes:author>
            <itunes:summary>　&lt;br /&gt;&lt;A href="http://www.mbc.co.jp/radio/" target="_blank"&gt;&lt;img src="http://www.mbc.co.jp/radio/radiko/bn/mbcradio_pr1410.png" alt="MBCradio" /&gt;&lt;/A&gt;&lt;br /&gt;&lt;br /&gt;twitterハッシュタグは「&lt;a href="http://twitter.com/#!/search/%23mbc1107"&gt;#mbc1107&lt;/a&gt;」</itunes:summary>
            <itunes:image/>
            <enclosure url="http://localhost:3355/podcast/20160309053000_MBC.mp3" length="10409132" type="audio/mpeg"/>
            <pubDate>Wed, 9 Mar 2016 05:55:45 +0900</pubDate>
        </item>
    </channel>
</rss>
```

Podcast Aggregatorとこのサーバが別の場合以下のように`-host`オプションを指定してインターフェイスに割り当てたipを指定するとそのネットワークからアクセスできるようになります．

```
$ ~/usr/local/go/bin/radicast -config=$HOME/usr/local/go/etc/radicast.json -host 192.168.2.203
  :
2016/03/09 07:42:34 [server] start 192.168.2.203:3355
```


##proxy

は使えない感じかな?

```
$ w3m -dump http://radiko.jp/area
document.write('<span class="JP46">KAGOSHIMA JAPAN</span>');
$ tsocks w3m -dump http://radiko.jp/area
document.write('<span class="JP13">TOKYO JAPAN</span>');
$ ALL_PROXY=socks5h://localhost:8080 ~/usr/local/go/bin/radicast -setup
2016/03/09 07:56:00 [radiko] GET http://radiko.jp/player/swf/player_3.0.0.01.swf
2016/03/09 07:56:01 [radiko] POST https://radiko.jp/v2/api/auth1_fms
2016/03/09 07:56:02 [radiko] POST https://radiko.jp/v2/api/auth2_fms
2016/03/09 07:56:02 [radiko] GET http://radiko.jp/v2/api/program/today?area_id=JP46
{
  "HOUSOU-DAIGAKU": [],
  "MBC": [],
  "RN1": [],
  "RN2": []
}
$ tsocks ~/usr/local/go/bin/radicast -setup
   :
  "MBC": [],
   :
$ tsocks ~/usr/local/go/bin/radicast -setup
   :
  "MBC": [],
   :
$ ALL_PROXY=http://localhost:8080 ~/usr/local/go/bin/radicast -setup
   :
  "MBC": [],
   :
```
