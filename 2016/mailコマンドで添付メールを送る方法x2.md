<!--
mailコマンドで添付ファイル付きメールを送る方法x2
-->

Google+でmailコマンドで添付ファイル付きメールを送ろうとしている方の投稿のコメントにbase64でデコードして送るscriptを使ってますとか書いたのですが，これperl4でcgi全盛な大昔からやってる手法なので楽なものがあるのではと思い探してみました．

## Debian stretch testingのs-nailを使う

Debian stretch testing/sid ではmailコマンドがbsd-mailxからs-nailに変わって添付ファイルオプションがあるようです．  

>
     -a file     Attach the given file to the message.  The same filename conventions as described in the section COMMANDS apply: shell word expansion is
                 restricted to the tilde ‘~’ and variables.  Shall file not be accessible but contain a ‘=’ character, then anything after the ‘=’ is assumed to
                 specify the input character set and anything before ‘=’ the filename: this is the only option to specify the input character set (and don't per‐
                 form any character set conversion) for text attachments from the command line, not using the ~@ tilde escape command.


こんな感じで使えました．

```
$ echo 'attach test(binary)' | mail -s "attach test mail" -a ./text.xz matoken@gmail.com
```
＃はじめ.zipを送ろうとしてgmailサーバに弾かれました．そういえばそうだった……  
＃＃`sendmail: 552 5.7.0 content and attachment content guidelines. xu1sm1469539pab.31 - gsmtp`

普通にメール送るのに加えて`-a`オプションでファイルを指定する感じです．
SMTPサーバの指定も出来るようなのでMTAの設定をしていないような場合でも使えそうです．

- [Debian Package Tracking System - s-nail](https://packages.qa.debian.org/s/s-nail.html "Debian Package Tracking System - s-nail")

## Debian jessieでbiabamを使う

`s-nail`はstretch testingからでjessieには入っていないようです．代わりに入っているのは`bsd-mailx`です．manをちょっと見た感じでは添付ファイルの扱いは載っていなさそう．でもきっと便利な何かがあるのでは?と`apt-cache search mail attach`とかして探してみると`biabam`というbash script製のものを発見．小さいし良さそうと試してみました．

> biabam - bash attachment mailer

こんな感じで使えました．

```
$ sudo apt install biabam
$ echo 'attach test' | biabam ./test.xz -s "attach test from jessie" matoken@gmail.com
```

- [Debian Package Tracking System - biabam](https://packages.qa.debian.org/b/biabam.html "Debian Package Tracking System - biabam")  
＃13年位前(!)からあるみたい．


ちなみにmailコマンドで添付ファイルの方法を検索すると大抵mutt使う例が出てきますが，muttを既に設定済みの人でないと敷居が高いと思います．そういう場合に今回の方法はいいのではないかと思います．

しかし恐らく多国言語対応はして居なさそうなので日本語をSubjectやBODYに使う場合は素直にScriptを書いたほうがいいかもしれません……．
定形なら適当なMUAでメールのテンプレートファイルを作っておいて流し込んでmailコマンドに投げるようにしてもいいですね．
