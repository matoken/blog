<!--
Coreutils 8.25でlsの出力が変わったのを試してみる
-->
DebuanのMLで知ったのですが，`Coreutils 8.25`の`ls`の出力結果が変わっています．

- [[DNG] Coreutils 8.25 ls output](https://lists.dyne.org/lurker/message/20160216.140735.97dba147.en.html "[DNG] Coreutils 8.25 ls output")
- [Coreutils - GNU core utilities](http://www.gnu.org/software/coreutils/coreutils.html "Coreutils - GNU core utilities")
  - [coreutils-8.25 released [stable]](http://lists.gnu.org/archive/html/coreutils/2016-01/msg00075.html "coreutils-8.25 released [stable]")
  - [broken ls in coreutils 8.25](http://lists.gnu.org/archive/html/coreutils/2016-01/msg00089.html "broken ls in coreutils 8.25")
  - [Re: broken ls in coreutils 8.25](http://lists.gnu.org/archive/html/coreutils/2016-02/msg00000.html "Re: broken ls in coreutils 8.25")
  > The reasons we changed the default was:
  >
  >  - It only happens when outputting to terminals
  >  - It disambiguates the output for users
  >  - Output can be pasted back in the shell for further processing
  >  - Users can get back to the old format by adding -N to their ls alias

この変更は`ls`コマンドを使ったscriptなどが影響を受けそうと思ったのですが端末でのみの変更のようで一安心．  
`Debian stretch testing`でちょっと試しに動かしてみました．
Debian jessie/Ubuntu 14.04以降も同じ手順で行けると思います．

## 開発環境の導入

```
$ sudo apt install build-essential
$ sudo apt build-dep coreutils
```

## Coreutils 8.25の入手と署名確認

```
$ wget ftp://ftp.gnu.org/gnu/coreutils/coreutils-8.25.tar.xz ftp://ftp.gnu.org/gnu/coreutils/coreutils-8.25.tar.xz.sig
$ gpg --verify coreutils-8.25.tar.xz.sig
gpg: 署名されたデータが'coreutils-8.25.tar.xz'にあると想定します
gpg: 2016年01月20日 20時17分46秒 JSTにRSA鍵ID 306037D9で施された署名
gpg: "Pádraig Brady <P@draigBrady.com>"からの正しい署名
gpg:                 別名"Pádraig Brady <pixelbeat@gnu.org>"
gpg: *警告*: この鍵は信用できる署名で証明されていません!
gpg:       この署名が所有者のものかどうかの検証手段がありません。
主鍵フィンガー・プリント: 6C37 DC12 121A 5006 BC1D  B804 DF6F D971 3060 37D9
```

## 展開とビルド

```
$ tar xf coreutils-8.25.tar.xz
$ cd coreutils-8.25
$ ./configure --prefix=$HOME/usr/local
$ make
```

## 端末でのlsの動作比較

今パスの通っているのは`8.24`で，`src/ls`が`8.25`

```
$ ls --version
ls (GNU coreutils) 8.24
Copyright (C) 2015 Free Software Foundation, Inc.
ライセンス GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

作者 Richard M. Stallman および David MacKenzie。
$ src/ls --version
ls (GNU coreutils) 8.25
Copyright (C) 2016 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Written by Richard M. Stallman and David MacKenzie.
```

スペースの含まれたファイルを作成して`ls`を実行すると8.25のlsでは`'`で括られる．

```
$ touch /tmp/hoge\ fuga
$ ls /tmp/hoge\ fuga
/tmp/hoge fuga
$ src/ls /tmp/hoge\ fuga
'/tmp/hoge fuga'
```

日本語は大丈夫だが，エスケープの必要な文字も括られる．

```
$ touch /tmp/日本語 /tmp/エスケープの必要な記号\?
$ src/ls /tmp/日本語 /tmp/他の記号\?
'/tmp/他の記号?'  /tmp/日本語
```

## パイプでの動作確認

パイプを通るとこれまでどおりの動作

```
$ src/ls /tmp/hoge\ fuga | cat
/tmp/hoge fuga
```

なので，xargsなどで処理したい場合はこれまでどおり

```
$ src/ls /tmp/hoge\ fuga | xargs cat
cat: /tmp/hoge: そのようなファイルやディレクトリはありません
cat: fuga: そのようなファイルやディレクトリはありません
$ src/ls /tmp/hoge\ fuga | xargs -I{} cat "{}"
```

## 回避方法x3

```
$ src/ls --quoting-style=literal /tmp/hoge\ fuga /tmp/エスケープの必要な記号\?
/tmp/hoge fuga  /tmp/エスケープの必要な記号?
$ src/ls -N /tmp/hoge\ fuga /tmp/エスケープの必要な記号\?
/tmp/hoge fuga  /tmp/エスケープの必要な記号?
$ QUOTING_STYLE=literal src/ls /tmp/hoge\ fuga /tmp/エスケープの必要な記号\?
/tmp/hoge fuga  /tmp/エスケープの必要な記号?
```

ということでとりあえずこれまでと同じ動作をさせたい場合は`QUOTING_STYLE=literal`を設定したり`ls -N`の`alias`を設定しておくといいみたいです．

```
$ echo 'export QUOTING_STYLE=literal' >> ~/.bashrc
```
or
```
$ echo "alias ls='ls -N'" >> ~/.bashrc~/.bashrc
```


＃ところでCoreutils 8.25でまたコマンドが一つ増えていました．今回は覚えやすいけど

> ** New commands
>
>   base32 is added to complement the existing base64 command,  
>   and encodes and decodes printable text as per RFC 4648.

＜追記＞
- [#813164 - coreutils: ls suddenly quotes output - Debian Bug report logs](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=813164 "#813164 - coreutils: ls suddenly quotes output - Debian Bug report logs")

Debianでは`Severity: serious`(重要度:深刻)となってsidの`8.25-2`で`no_ls_quoting.patch`が入ってrevertされたようです．


```
$ bin/ls --version|head -1
ls (GNU coreutils) 8.25
$ bin/ls /tmp/hoge\ fuga
/tmp/hoge fuga
```

```
$ cat patches/no_ls_quoting.patch
Description: revert inconsistent ls quoting
 This is a revert of upstream commit 109b9220cead6e979d22d16327c4d9f8350431cc.
 Info changed to reflect current upstream intentions.

Bug-Debian: https://bugs.debian.org/813164

--- coreutils-8.25.orig/NEWS
+++ coreutils-8.25/NEWS
@@ -71,9 +71,6 @@ GNU coreutils NEWS
   df now prefers sources towards the root of a device when
   eliding duplicate bind mounted entries.

-  ls now quotes file names unambiguously and appropriate for use in a shell,
-  when outputting to a terminal.
-
   join, sort, uniq with --zero-terminated, now treat '\n' as a field delimiter.

 ** Improvements
--- coreutils-8.25.orig/doc/coreutils.texi
+++ coreutils-8.25/doc/coreutils.texi
@@ -7750,8 +7750,8 @@ this"} in the default C locale.  This lo

 You can specify the default value of the @option{--quoting-style} option
 with the environment variable @env{QUOTING_STYLE}@.  If that environment
-variable is not set, the default value is @samp{shell-escape} when the
-output is a terminal, and @samp{literal} otherwise.
+variable is not set, the default value is @samp{literal}, but this
+default may change to @samp{shell-escape} in a future version of this package.

 @item --show-control-chars
 @opindex --show-control-chars
--- coreutils-8.25.orig/src/ls.c
+++ coreutils-8.25/src/ls.c
@@ -1581,7 +1581,6 @@ decode_switches (int argc, char **argv)
       if (isatty (STDOUT_FILENO))
         {
           format = many_per_line;
-          set_quoting_style (NULL, shell_escape_quoting_style);
           /* See description of qmark_funny_chars, above.  */
           qmark_funny_chars = true;
         }
```

＜／追記＞
