先日の台風16号で09/19 23:00頃から09/22 12:45頃まで停電していました……．
とりあえずサーバはシャットダウンしてThinkPad X200はバッテリが4本，X220は2本，モバイルバッテリは1000mA x3という感じで停電開始．
とりあえず普通に使っていても3営業日以上は耐えられるはずです．

でも一応NotePCは必須デバイス以外は取り外して輝度を下げてpowertopで省電力モードに．

＃powertopについては以下の辺りを参考に．
<iframe src="//www.slideshare.net/slideshow/embed_code/key/2XiynlYGRWLlNs" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/matoken/bluetoothlinuxandroid/23" title="「BluetoothでLinuxマシンとAndroidを繋いで話が出来るようにした話」「台風で停電になって省電力の設定をした話」「ネットワークの設定が引き継がれなくて困ったので調べた話」" target="_blank">「BluetoothでLinuxマシンとAndroidを繋いで話が出来るようにした話」「台風で停電になって省電力の設定をした話」「ネットワークの設定が引き継がれなくて困ったので調べた話」</a> </strong> from <strong><a target="_blank" href="//www.slideshare.net/matoken">Kenichiro MATOHARA</a></strong> </div>


これで1台ずつ使っていくことに．しかし，不安点がデータのバックアップ．
何時もはワークディレクトリはownCloudで自動バックアップ&デイリーでファイルサーバへのバックアップがされています．しかし停電時はサーバが動いてない&電池の切れたPCから別のPCに作業内容が移行できない．ということでlsyncdを使ってデータをUSBメモリにコピー&ファイルシステムはNILFS2にしてスナップショットも取得できるようにしました．

まずはUSBメモリの用意．普通にNILFS2で作ります．  
*/dev/sdz*のパーティション情報を削除して全領域1パーティションで作成し，NILFS2で初期化して*/mnt*にマウントしてユーザ，グループを自分のものにしています．

```
$ sudo wipefs -a /dev/sdz
$ sudo fdisk /dev/sdz
Command (m for help): n
Partition type
   p   primary (0 primary, 0 extended, 4 free)
   e   extended (container for logical partitions)
Select (default p): p
Partition number (1-4, default 1):
First sector (51-7821311, default 51):
Last sector, +sectors or +size{K,M,G,T,P} (51-7821311, default 7821311):

Created a new partition 1 of type 'Linux' and of size 3.7 GiB.

Command (m for help): w
$ sudo mkfs.nilfs2 /dev/sdz1
$ sudo mount /dev/sdz1 /mnt
$ sudo chown `id -u`.`id -g` /mnt
$ sudo chmod 700 /mnt
```

続いてlsyncdを実行してデータを同期します．今回はアドホックにコマンドとして動かしました．同期元同期先はrsyncと同じ書き方だと思うので前もって*rsync -avn from to*みたいな感じで動作確認しておくと安心です．

```
$ lsyncd -log all -nodaemon -rsync /home/mk/ownCloud /mnt/ownCloud
```

これでlsyncdのターゲット以下のファイル，ディレクトリは自動的にUSBメモリにコピーされ，USBメモリの中では自動的にチェックポイントが作成される状態です．
作業の区切りのタイミングで明示的にスナップショットを取るようにしておくと更に安心です．（チェックポイント，スナップショットには何時でもその状態に巻き戻せる．但しチェックポイントは古いものから順に自動的に削除される．スナップショットは自動削除されない）

マシンを切り替えるときにはlsyncdを止めて念の為rsyncをdry runして同期状況を確認しておいて，新しいマシンでUSBメモリからSSDにrsyncしてlsyncdを動かすという感じでした．

ちなみにいつぞやの計画停電のときは計画的だし時間もずれないしで案外困りませんでした．でも念の為NotePCの大容量バッテリは買い増しして持ち歩いていました．(今回のバッテリの一つもそれ)

* [nilfs2 を試してみる - matoken’s meme -hatena-](http://d.hatena.ne.jp/matoken/20110616/1308240479 "nilfs2 を試してみる - matoken’s meme -hatena-")
* [バックアップに一番いいファイルシステムを頼む](http://www.slideshare.net/matoken/backup-fs-4 "バックアップに一番いいファイルシステムを頼む")

ちなみに停電から14,5時間で携帯基地局のバッテリも尽きたようで完全にオフラインになってしまいました．NILFS2は大抵標準では導入されていないし手元ではGit管理もしているのでNILFS2は使わなくても良かったかなと後で思いました．

復旧時の残容量はNotePC が1.5本/モバイルバッテリが50% x2という感じ未だ行けるなという感じでした．でもネットワークが繋がっていたらもっと利用していたでしょう．

しかしこの辺は雷やら強風やらでよく停電になります．大抵は携帯基地局が落ちるまでには復旧するのですが，台風だと今回のように長い停電に．でも復旧状況を停電情報サイトで確認していると深夜にもステータスが変わっていて24時間体制で復旧してるのかと驚きました．九州電力の関係者のみなんさんありがとうございます_o_  
（でも昼頃復旧->本日中->本日未明復旧とステータスがずれ込んでいくとはちょっとつらかったです……）

* [九州電力株式会社　停電情報](http://www.kyuden.co.jp/emergency/pc/kyusyu.html "九州電力株式会社　停電情報")

* [NILFS - Continuous Snapshotting Filesystem for Linux](http://nilfs.osdn.jp/ja/ "NILFS - Continuous Snapshotting Filesystem for Linux")
* [GitHub - axkibe/lsyncd: Lsyncd (Live Syncing Daemon) synchronizes local directories with remote targets](https://github.com/axkibe/lsyncd "GitHub - axkibe/lsyncd: Lsyncd (Live Syncing Daemon) synchronizes local directories with remote targets")

ついでに今回役にたったものを

* PETZLのワイヤ式ヘッドライト  
手元のは初代かその次くらいの古いもので暗いけど便利でした．  
<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B00UAJ2EUA&linkId=148ea2ed8145ee992e4f16870c3323aa"></iframe>

* モバイルバッテリー  
Cheerox2に秋葉原で買った謎バッテリーの3本を使っていました．基地局が落ちるまではスマホに給電．基地局が落ちてからはiPad/Kobo/Raspberry Piに給電していました．  
<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B00OXPDYGK&linkId=d3240fbeacf9ece71dc30ce4c8a6b1a2"></iframe>

* Raspberry Pi B+ とAdafruit3.5インチ液晶  
USB稼働の液晶モニタもあるのですがバッテリ消費量的にこいつのほうが食わない感じなのでちっこいですがこれで作業したりも．でもNotePCのバッテリが切れなかったのでテスト程度．
<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B00LT4BOK6&linkId=f4a2bd1c5af811eaa16a73e7e5e0cc9f"></iframe>  
[Adafruit PiTFT - 320x240 2.8 TFT+Touchscreen for Raspberry Pi ID: 1601 - $34.95 : Adafruit Industries, Unique & fun DIY electronics and kits](https://www.adafruit.com/products/1601 "Adafruit PiTFT - 320x240 2.8 TFT+Touchscreen for Raspberry Pi ID: 1601 - $34.95 : Adafruit Industries, Unique &amp; fun DIY electronics and kits")

* エネループ  
自己放電の少ない充電式乾電池です．自己放電が少ないので充電しておいておけば何時でも使えるのが良いです．電気をあまり食わないものはこいつで．
ちなみに充電器は[単三・単四 ニッケル水素充電池対応 放電機能 USB充電ポート搭載 充電器 (16本用)の通信販売【上海問屋】 | 上海問屋](https://www.donya.jp/item/22145.html "単三・単四 ニッケル水素充電池対応 放電機能 USB充電ポート搭載 充電器 (16本用)の通信販売【上海問屋】 | 上海問屋")を主に使っています．
<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B00P8SBKR6&linkId=ea10f7d7ecaef23457a5f0df924ce225"></iframe>
<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B00C48VB6I&linkId=e80e29548b60a4acb2e2cf0885c94db3"></iframe>

* ユニフレームガスバーナー  
一般的なイワタニとかのタイプのカセットコンロガスが使える(非推奨)なガスバーナーです．山向けのガスやアルコールに比べて大分ランニングコストが安く付きます．低地のみならこれで十分．(手持ちのは如くの小さい恐らく古いもの)
<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B017973KIU&linkId=d00da3ee64e8b88f8cb4a26cba09a1d6"></iframe>
