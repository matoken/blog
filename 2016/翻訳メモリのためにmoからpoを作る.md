<!--
翻訳メモリのためにmoからpoを作ったメモ
-->

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/24934252282/in/dateposted-public/" title="20160216_06:02:28-15473"><img src="https://farm2.staticflickr.com/1530/24934252282_a347399db7.jpg" width="261" height="201" alt="20160216_06:02:28-15473"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

久々に翻訳をしようと思い立ちました．そこで翻訳メモリを作ると楽が出来るはずと既存の翻訳ファイルを突っ込んだメモです．

##Debianパッケージで導入された`.mo`を一括で`.po`に変換

ここでは`/usr/share/locale/ja/LC_MESSAGES/*.mo`を利用しています．Debian/Ubuntu系なら同じでしょう．

```
$ export TMP=$(mktemp -d)
$ echo $TMP
/tmp/tmp.3ARspdBIMI
$ ls -1 /usr/share/locale/ja/LC_MESSAGES/ | egrep \.mo$ | xargs -n1 -I{} msgunfmt /usr/share/locale/ja/LC_MESSAGES/{} -o $TMP/{}.po
```

## 色々なソースを置いてあるディレクトリ以下から`ja.po`をコピー

これは私がここに置いてるからというだけです．やらなくていいです．

```
$ find ~/src -iname "ja.po" | xargs -n1 -I{} cp {} $TMP
```

## `Poedit`を起動して読み込む

- 「編集(E)」->「設定\(P\)」
- 「翻訳メモリ」タブ
- 「ファイルから学習…」ボタンを押して用意した`.po`を読み込む．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/24934252262/in/dateposted-public/" title="20160216_06:02:02-14632"><img src="https://farm2.staticflickr.com/1571/24934252262_5265456292_n.jpg" width="297" height="320" alt="20160216_06:02:02-14632"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/24684700539/in/dateposted-public/" title="20160216_06:02:30-14314"><img src="https://farm2.staticflickr.com/1582/24684700539_5265456292_n.jpg" width="320" height="255" alt="20160216_06:02:30-14314"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/24421695334/in/dateposted-public/" title="20160216_06:02:38-14388"><img src="https://farm2.staticflickr.com/1698/24421695334_3e33b623cd_n.jpg" width="320" height="132" alt="20160216_06:02:38-14388"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/25052334995/in/dateposted-public/" title="20160216_06:02:51-13986"><img src="https://farm2.staticflickr.com/1482/25052334995_8963955390_n.jpg" width="297" height="320" alt="20160216_06:02:51-13986"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

## 翻訳メモリを使う

`Poedit`で翻訳対象の`.po`を読み込んで
- 「カタログ(A)」->「翻訳メモリを使って未翻訳項目を埋める…(T)」

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/25052520405/in/dateposted-public/" title="20160216_06:02:48-22901"><img src="https://farm2.staticflickr.com/1711/25052520405_e6131be032.jpg" width="400" height="211" alt="20160216_06:02:48-22901"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/24425665823/in/dateposted-public/" title="20160216_06:02:53-22974"><img src="https://farm2.staticflickr.com/1442/24425665823_541afcd080.jpg" width="344" height="111" alt="20160216_06:02:53-22974"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/24756818230/in/dateposted-public/" title="20160216_02:02:16-28154"><img src="https://farm2.staticflickr.com/1456/24756818230_5103f62ab7.jpg" width="461" height="141" alt="20160216_02:02:16-28154"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/24934532962/in/dateposted-public/" title="20160216_06:02:12-25100"><img src="https://farm2.staticflickr.com/1657/24934532962_38b5e3edd9.jpg" width="500" height="327" alt="20160216_06:02:12-25100"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

翻訳の提案も出てきます．

大助かりです :)  
＃でもきっともっといい方法がある気がする．
