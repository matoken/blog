<!--
rc_gui を Rasbian jessie lite で build するのに必要なpkg memo
-->
`lightdm git dh-autoreconf libglib2.0-dev intltool`

```
pi@pizero:~/src/rc_gui $ ./autogen.sh
  ：
./autogen.sh: 4: ./autogen.sh: autoreconf: not found
pi@pizero:~/src/rc_gui $ sudo apt install dh-autoreconf
pi@pizero:~/src/rc_gui $ ./autogen.sh
  ：
configure.ac:157: warning: macro 'AM_GLIB_GNU_GETTEXT' not found in library
  ：
configure.ac:157: warning: macro 'AM_GLIB_GNU_GETTEXT' not found in library
configure.ac:157: error: possibly undefined macro: AM_GLIB_GNU_GETTEXT
      If this token and others are legitimate, please use m4_pattern_allow.
      See the Autoconf documentation.
autoreconf: /usr/bin/autoconf failed with exit status: 1
pi@pizero:~/src/rc_gui $ sudo apt install libglib2.0-dev
pi@pizero:~/src/rc_gui $ ./autogen.sh 
  ：
./autogen.sh: 5: ./autogen.sh: intltoolize: not found
pi@pizero:~/src/rc_gui $ sudo apt install intltool
pi@pizero:~/src/rc_gui $ ./autogen.sh 
pi@pizero:~/src/rc_gui $ ./configure --prefix=$HOME/usr/local
  ：
No package 'gtk+-2.0' found
  ：
pi@pizero:~/src/rc_gui $ sudo apt install libgtk2.0-dev
pi@pizero:~/src/rc_gui $ ./configure --prefix=$HOME/usr/local
pi@pizero:~/src/rc_gui $ make
pi@pizero:~/src/rc_gui $ make install
pi@pizero:~/src/rc_gui/src $ ~/usr/local/bin/rc_gui
```
