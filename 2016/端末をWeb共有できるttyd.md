<!--
title:端末をWeb共有できるttyd
-->

端末をWebブラウザで共有できるソフトウェエアです．
以前似たものでGoTTYを試してみましたが，ttydはGoTTYインスパイアらしいです．
ttydはc製でMITライセンスです．

- [ttyd - Share your terminal over the web](https://tsl0922.github.io/ttyd/ "ttyd - Share your terminal over the web")


## 導入

今回はDebian stretch amd64(testing)に導入しました．
`README.md`ではUbuntu 16.04での手順が書かれていますがそのまま使えました．

```
$ sudo apt install cmake g++ pkg-config git vim-common libwebsockets-dev libjson-c-dev libssl-dev
$ git clone https://github.com/tsl0922/ttyd.git
$ cd ttyd
$ mkdir build
$ cd build
$ cmake ..
$ make
```

## 動かしてみる

```
$ ./ttyd -p 8080 bash
```

としてウェブブラウザで `http://localhost:8080/` に繋ぐと利用できます．
tmuxのセッションを共有して複数のブラウザで1つの端末の操作とかdocker利用とかGoTTYと同じように利用できます．

```
$ ./ttyd -p 8080 tmux new -A -s ttyd
[2016/12/09 18:54:25:5954] NOTICE: Initial logging level 7
[2016/12/09 18:54:25:5954] NOTICE: Libwebsockets version: 2.0.3 unknown-build-hash
[2016/12/09 18:54:25:5954] NOTICE: IPV6 not compiled in
[2016/12/09 18:54:25:5954] NOTICE: libev support compiled in but disabled
[2016/12/09 18:54:25:5954] NOTICE: libuv support compiled in but disabled
[2016/12/09 18:54:25:5955] NOTICE:  Threads: 1 each 1024 fds
[2016/12/09 18:54:25:5955] NOTICE:  mem: platform fd map:  8192 bytes
[2016/12/09 18:54:25:5955] NOTICE:  Compiled with OpenSSL support
[2016/12/09 18:54:25:5955] NOTICE:  SSL disabled: no LWS_SERVER_OPTION_DO_SSL_GLOBAL_INIT
[2016/12/09 18:54:25:5955] NOTICE: Creating Vhost 'default' port 8080, 2 protocols
[2016/12/09 18:54:25:5955] NOTICE:  Listening on port 8080
[2016/12/09 18:54:25:5955] NOTICE:  mem: per-conn:          920 bytes + protocol rx buf
[2016/12/09 18:54:25:5956] NOTICE:  canonical_hostname = x220
[2016/12/09 18:54:25:5956] NOTICE: TTY configuration:
[2016/12/09 18:54:25:5956] NOTICE:   start command: tmux new -A -s ttyd
[2016/12/09 18:54:25:5956] NOTICE:   reconnect timeout: 10s
[2016/12/09 18:54:25:5956] NOTICE:   close signal: SIGHUP (1)
[2016/12/09 18:54:25:6057] NOTICE: lws_protocol_init
[2016/12/09 18:54:25:9806] NOTICE: HTTP connect from localhost (127.0.0.1), path: /
[2016/12/09 18:54:25:0015] NOTICE: HTTP connect from localhost (127.0.0.1), path: /auth_token.js
[2016/12/09 18:54:26:3474] NOTICE: client connected from localhost (127.0.0.1), total: 1
[2016/12/09 18:54:26:3548] NOTICE: started process, pid: 30365
[2016/12/09 18:54:34:2928] NOTICE: HTTP connect from localhost (127.0.0.1), path: /
[2016/12/09 18:54:34:3132] NOTICE: error on reading from skt : 104
[2016/12/09 18:54:34:3132] NOTICE: sending SIGHUP to process 30365
[2016/12/09 18:54:34:3138] NOTICE: process exited with code 256, pid: 30365
[2016/12/09 18:54:34:3138] NOTICE: client disconnected from localhost (127.0.0.1), total: 0
[2016/12/09 18:54:34:5554] NOTICE: HTTP connect from localhost (127.0.0.1), path: /auth_token.js
[2016/12/09 18:54:34:5732] NOTICE: client connected from localhost (127.0.0.1), total: 1
[2016/12/09 18:54:34:5811] NOTICE: started process, pid: 30505
[2016/12/09 18:54:40:0073] NOTICE: wsi 0x556bcbc34500: TIMEDOUT WAITING on 3 (did hdr 0, ah 0x556bcbbc0c50, wl 0, pfd events 0)
[2016/12/09 18:54:40:0074] NOTICE: lws_header_table_detach: wsi 0x556bcbc34500: ah held 6s, ah.rxpos 0, ah.rxlen 0, mode/state 0 4,wsi->more_rx_waiting 0
```

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/31526345375" title="20161209_18:12:56-1180"><img src="https://c8.staticflickr.com/1/509/31526345375_06da4a1cf5.jpg" width="500" height="330" alt="20161209_18:12:56-1180"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

GoTTYについてはこちらを．

- [端末をリアルタイムに Web で共有できる GoTTY | matoken's meme](http://matoken.org/blog/blog/2015/08/24/terminal-web-share-gotty/ "端末をリアルタイムに Web で共有できる GoTTY | matoken&apos;s meme")

<iframe src="//www.slideshare.net/slideshow/embed_code/key/dnNhjGWtxZwodE" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/matoken/2-rasbian-jessie-google-authenticator#6" title="勉強会向けサーバを作ってみる2 / Rasbian jessieを試す/ Google Authenticatorのパスコードを作る" target="_blank">勉強会向けサーバを作ってみる2 / Rasbian jessieを試す/ Google Authenticatorのパスコードを作る</a> </strong> from <strong><a target="_blank" href="//www.slideshare.net/matoken">Kenichiro MATOHARA</a></strong> </div>

