<!--
OpenSSH 7.2で入ったssh-keygenの新機能を試す
-->

OpenSSH 7.2リリースネタ続きです．

- [[openssh-unix-announce] Announce: OpenSSH 7.2 released](http://lists.mindrot.org/pipermail/openssh-unix-announce/2016-February/000125.html "[openssh-unix-announce] Announce: OpenSSH 7.2 released")
- [春山 征吾のくけー : OpenSSH 7.2 がリリースされました - livedoor Blog（ブログ）](http://haruyama.blog.jp/archives/52047049.html "春山 征吾のくけー : OpenSSH 7.2 がリリースされました - livedoor Blog（ブログ）") リリースノートの日本語訳があります．何時も助かります_o_

ここでは`ssh-keygen`の新機能を試してみます．

## 鍵のコメント変更

先ずは鍵のコメント変更．

>  \* ssh-keygen(1): allow ssh-keygen to change the key comment for all
   supported formats.

>    ssh-keygen(1): すべてのサポートする形式について ssh-keygen が
   鍵のコメントを変更できるようにする

manやhelpを見ると`-c`オプションのようです．

```
    -c      Requests changing the comment in the private and public key files.  This operation is only supported for RSA1 keys.  The program will prompt for the
            file containing the private keys, for the passphrase if the key has one, and for the new comment.
```

```
$ ~/usr/local/openssh-portable/bin/ssh-keygen --help |& grep '\-c'
       ssh-keygen -c [-P passphrase] [-C comment] [-f keyfile]
```

適当な鍵を作って書き換えてみました．`test comment`が`change comment`に書き換わりました．手で書き換えるとミスすることもあるのでいいかもしれません．

```
$ ssh-keygen -t ed25519 -N '' -C "test comment" -f ./testkey
$ cat ./testkey.pub
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHBYXP3okrQO7hejY7f0d6PvjD++puLFuvozI5Trxv8D test comment
$ ~/usr/local/openssh-portable/bin/ssh-keygen -c -C "change comment" -f ./testkey
Key now has comment 'test comment'
The comment in your key file has been changed.
$ cat testkey.pub
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHBYXP3okrQO7hejY7f0d6PvjD++puLFuvozI5Trxv8D change comment
```

## 標準入力からの鍵の読み込みと鍵指紋の表示

`-f`オプションのファイルの代わりに`-`を指定すると標準入力から鍵を読み込みます．ファイル指定の場合は秘密鍵も公開鍵もいけますが，標準入力からだと公開鍵のみのようです．

```
$ cat ./testkey.pub | ~/usr/local/openssh-portable/bin/ssh-keygen -lf -
256 SHA256:v7zpU4SoiUIIEqWhaftbuUy2t2Tuny9Ld5WVrDD2Sjs change comment (ED25519)
$ cat ./testkey | ~/usr/local/openssh-portable/bin/ssh-keygen -lf -
(stdin) is not a public key file.
$ ~/usr/local/openssh-portable/bin/ssh-keygen -lf ./testkey.pub
256 SHA256:v7zpU4SoiUIIEqWhaftbuUy2t2Tuny9Ld5WVrDD2Sjs change comment (ED25519)
$ ~/usr/local/openssh-portable/bin/ssh-keygen -lf ./testkey
256 SHA256:v7zpU4SoiUIIEqWhaftbuUy2t2Tuny9Ld5WVrDD2Sjs change comment (ED25519)
```

host鍵全部とかはこんな感じで行けますね．

```
$$ cat /etc/ssh/ssh_host_*_key.pub | ~/usr/local/openssh-portable/bin/ssh-keygen -lf -
521 SHA256:WdWtCQFxH+Xno527Cl+PomQ5ZKj81/NUzIzYOckXsWA root@x220 (ECDSA)
256 SHA256:rONiKpthZZORiwOKpfNVcrMp+FgBhfE20GuCuuNrgA8 root@x220 (ED25519)
4096 SHA256:o2xTrmNmkd2RwNybmaO+TWIFw6xeoZmXmbLdhC1gIpU root@x220 (RSA)
```

## ファイルに含まれる複数の公開鍵の指紋表示

> \* ssh-keygen(1): allow fingerprinting multiple public keys in a
   file, e.g. "ssh-keygen -lf ~/.ssh/authorized_keys" bz#1319

>    ssh-keygen(1): ファイルに含まれる複数の公開鍵の指紋表示を
   できるようにする. つまり
   "ssh-keygen -lf ~/.ssh/authorized_keys" bz#1319


こちらの場合も秘密鍵の鍵指紋は取得できないです．ちなみにエラーなどは出ずスキップされます．

```
$ ssh-keygen -t ed25519 -f /tmp/hoge_key
$ ssh-keygen -t ed25519 -f /tmp/hoge2_key
$ cat /tmp/hoge_key.pub /tmp/hoge2_key.pub > /tmp/keys.pub
$ ~/usr/local/openssh-portable/bin/ssh-keygen -l -f /tmp/keys.pub
256 SHA256:/fZluTkiB9Ogv3mT9mniaZ2YPbL2XkMwge+bQ0r74dg mk@x220(ED25519)
256 SHA256:iHSZ0ucbwTR0VLScSF5l29iBFQIT13I+fELvue/N+xM mk@x220(ED25519)
$ cat /tmp/hoge_key.pub /tmp/hoge2_key > /tmp/keys.mix
$ ~/usr/local/openssh-portable/bin/ssh-keygen -l -f /tmp/keys.mix
256 SHA256:/fZluTkiB9Ogv3mT9mniaZ2YPbL2XkMwge+bQ0r74dg mk@x220(ED25519)
$ cat /tmp/hoge_key.pub /tmp/hoge2_key /tmp/hoge2_key.pub> /tmp/keys.mix
$ ~/usr/local/openssh-portable/bin/ssh-keygen -l -f /tmp/keys.mix256 SHA256:/fZluTkiB9Ogv3mT9mniaZ2YPbL2XkMwge+bQ0r74dg mk@x220(ED25519)
256 SHA256:iHSZ0ucbwTR0VLScSF5l29iBFQIT13I+fELvue/N+xM mk@x220(ED25519)
```
＃見づらいですね><

`-v`を付けると秘密鍵のところで`not a public key`と言われるようになります．

```
$ ./ssh-keygen -l -v -f /tmp/keys.mix
256 SHA256:/fZluTkiB9Ogv3mT9mniaZ2YPbL2XkMwge+bQ0r74dg mk@x220(ED25519)
+--[ED25519 256]--+
|            ..   |
|           .  .  |
|            .o   |
|         . . .o  |
|        S o +  . |
|         . = +. .|
|          o O.Oo=|
|           *=^oX*|
|           +@E&Bo|
+--[SHA256]---+
debug1: /tmp/keys.mix:2: not a public key
debug1: /tmp/keys.mix:8: not a public key
256 SHA256:iHSZ0ucbwTR0VLScSF5l29iBFQIT13I+fELvue/N+xM mk@x220(ED25519)
+--[ED25519 256]--+
|       .+.+**+*+.|
|     . = = +o*oB.|
|    o = + o +.B.o|
|   . + + .    .+o|
|    . . S      o+|
|         o     E.|
|        .       o|
|               +.|
|               o@|
+--[SHA256]---+
```

<iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=4873112877" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe><iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00P7SKB40" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>


---

<!-- Place this tag in your head or just before your close body tag. -->
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<!-- Place this tag where you want the widget to render. -->
<div class="g-post" data-href="https://plus.google.com/+KenichiroMATOHARA/posts/WEjsMT2oa8W"></div>

<!-- Place this tag in your head or just before your close body tag. -->
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<!-- Place this tag where you want the widget to render. -->
<div class="g-post" data-href="https://plus.google.com/+KenichiroMATOHARA/posts/XspBE26nv55"></div>

<!-- Place this tag in your head or just before your close body tag. -->
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<!-- Place this tag where you want the widget to render. -->
<div class="g-post" data-href="https://plus.google.com/+KenichiroMATOHARA/posts/3wUMXyjf2gU"></div>
