<!--
So-net 0SIM の通常販売が販売開始
-->

以前書いたこの記事ですが，
「[デジモノステーション2016年2月号付録の0SIMをRaspberry Piで試す | matoken's meme](http://matoken.org/blog/blog/2015/12/29/try-0sim-of-dejimonostation-magazine-in-february-2016-degree-in-raspberry-pi/ "デジモノステーション2016年2月号付録の0SIMをRaspberry Piで試す | matoken&apos;s meme")」
付録ではなく通常販売されたようです．

- [プレスリリース | So-net 会社情報](http://www.so-net.ne.jp/corporation/release/2016/pub20160126_0003.html "プレスリリース | So-net 会社情報")
- [0 SIM | So-net モバイルサービス](http://lte.so-net.ne.jp/sim/0sim/ "0 SIM | So-net モバイルサービス")

デジモノステーションの付録との違いはプランが多いことと事務手数料が必要なかった(雑誌台は必要)のと早く試せた位?
プラン変更が可能なのかは見当たりませんでした．最低利用期間はないので解約して新規契約になるのかな?

もし沢山契約できたら IoT 的にも使えるなぁと思ってましたが，

> ※SIMカードはお一人様一枚までとなります。
※1：コラボSIMを含みます。

と流石にそういうことは出来ないよう．




個人とかで数カ所なら 0SIM に加え500円くらいのサービスをそれぞれ契約して使う感じですかね．
- FREETEL の「使った分だけ安心プラン データ専用」の 299円/月〜
- DMM mobile の「データSIMプラン ライト」の440円/月(固定料金低速200kbps)
- ServersMan SIM の「ServersMan SIM LTE」の 467円/月(固定料金低速250kbps)

IoT だとコストは増える感じがするけど沢山契約できて管理機能も充実したソラコム辺りになるんですかね．
- [株式会社ソラコム](https://soracom.jp/ "株式会社ソラコム")
- [SORACOM Air のご利用料金 - 株式会社ソラコム](https://soracom.jp/services/air/price/ "SORACOM Air のご利用料金 - 株式会社ソラコム")
12ヶ月の[無料利用枠](https://soracom.jp/services/air/price/#price6 "無料利用枠")もあるのでそこで試してみるのも良さそうです．

この分野は日本だと今のところここだけだと思うので
alternative が現れて欲しいところ．
