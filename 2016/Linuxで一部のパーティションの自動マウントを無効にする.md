<!--
Linuxで一部のパーティションの自動マウントを無効にする
-->

Linuxで外付けHDDやUSBストレージなどを接続した時に一部のパーティションの自動マウントを無効にする方法です．

Gnomeで全てのメディアの自動マウントの切り替えをしたい場合は以下を参照して下さい．

- [Gnomeのメディア自動マウント切り替え | matoken's meme](http://matoken.org/blog/blog/2016/02/11/media-auto-mount-switching-of-gnome/ "Gnomeのメディア自動マウント切り替え | matoken&apos;s meme")

## 対象パーティションの確認

対象のメディアをマウントした状態で`blkid`コマンドや`/etc/mtab`などを参考に対象のUUIDを調べる

```
$ cat /etc/mtab
    :
/dev/mmcblk0p1 /media/mk/boot vfat rw,nosuid,nodev,relatime,uid=1000,gid=1000,fmask=0022,dmask=0022,codepage=437,iocharset=utf8,shortname=mixed,showexec,utf8,flush,errors=remount-ro 0 0
/dev/mmcblk0p2 /media/mk/ec2aa3d2-eee7-454e-8260-d145df5ddcba ext4 rw,nosuid,nodev,relatime,data=ordered 0 0
$ sudo /sbin/blkid /dev/mmcblk0*
/dev/mmcblk0: PTUUID="ba2edfb9" PTTYPE="dos"
/dev/mmcblk0p1: SEC_TYPE="msdos" LABEL="boot" UUID="74BD-74CF" TYPE="vfat" PARTUUID="ba2edfb9-01"
/dev/mmcblk0p2: UUID="ec2aa3d2-eee7-454e-8260-d145df5ddcba" TYPE="ext4" PARTUUID="ba2edfb9-02"
```

ここでは

> /dev/mmcblk0p1 -> 74BD-74CF  
> /dev/mmcblk0p2 -> ec2aa3d2-eee7-454e-8260-d145df5ddcba

GPartedなどを使うのもいいかもしれません．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/24850316251/in/dateposted-public/" title="20160211_05:02:10-12031"><img src="https://farm2.staticflickr.com/1523/24850316251_f4984e2fa7_n.jpg" width="320" height="211" alt="20160211_05:02:10-12031"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>


## fatabの編集

`/etc/fstab`にnoautoオプション付きで記述します．対象パーティションをマウントした状態の`/etc/mtab`が参考になるでしょう．

```
UUID=74BD-74CF  /media/mk/pi-boot       vfat    noauto,rw,nosuid,nodev,relatime,uid=1000,gid=1000,fmask=0022,dmask=0022,codepage=437,iocharset=utf8,shortname=mixed,show
exec,utf8,flush,errors=remount-ro       0       0
UUID=ec2aa3d2-eee7-454e-8260-d145df5ddcba       /media/mk/pi-root       ext4    noauto,rw,nosuid,nodev,relatime,data=ordered   0       0
```

## マウントしたい場合

手動でmountコマンドを実行したり，

```
$ sudo mount /media/mk/pi-root
```

Nautilus等で該当パーティションのアイコンをクリックしてマウントできます．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/24316685743/in/dateposted-public/" title="20160211_05:02:05-8216"><img src="https://farm2.staticflickr.com/1509/24316685743_b3a9660906_o.jpg" width="173" height="93" alt="20160211_05:02:05-8216"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>


ちょっと大げさですね．
root権の要らないもっとお手軽な方法がありそう﻿な気がします．
