```
$ sudo apt install unattended-upgrades apt-listchanges
```

設定ファイル．
`/etc/apt/apt.conf.d/50unattended-upgrades`

以下のコメントを解除してメールが飛ぶように．(他のサーバで読みたい場合はssmtpなどを設定しておくといい)

```
Unattended-Upgrade::Mail "root";
```

以下のコマンドで自動ダウンロード&自動アップデートを設定．

```
$ sudo dpkg-reconfigure -plow unattended-upgrades
```

> Automatically download and install stable updates?                                                                                                              と効かれるので

> \<Yes\>

を選ぶ．
`/etc/apt/apt.conf.d/20auto-upgrades`が作成される．


```
$ cat /etc/apt/apt.conf.d/20auto-upgrades
APT::Periodic::Update-Package-Lists "1";
APT::Periodic::Unattended-Upgrade "1";
```

- [UnattendedUpgrades - Debian Wiki](https://wiki.debian.org/UnattendedUpgrades "UnattendedUpgrades - Debian Wiki")
- [第2章 Debian パッケージ管理](https://www.debian.org/doc/manuals/debian-reference/ch02.ja.html "第2章 Debian パッケージ管理")
