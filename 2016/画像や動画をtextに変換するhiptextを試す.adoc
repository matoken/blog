= 画像や動画をtextに変換する?hiptextを試す

* https://github.com/jart/hiptext[jart/hiptext: Turn images into text better than caca/aalib]

類似のものでlibcacaとかがありますが，

""
Turn images into text better than caca/aalib
""

とのことでもっといいものらしいです．
Texttopfootnote:[https://github.com/tombh/texttop[tombh/texttop: A fully interactive X Linux desktop rendered in TTY and streamable over SSH]]で使われているので興味を持ったのですが，そのままではBuild出来なかったのでメモです．

== build

READMEではこの辺が必要とのことです．
----
sudo apt-get install build-essential libpng12-dev libjpeg-dev \
    libfreetype6-dev libgif-dev ragel libavformat-dev libavcodec-dev \
    libswscale-dev libgflags-dev libgoogle-glog-dev
----

Debian stretch testingではこんな感じ?

----
$ sudo apt-get install build-essential libpng-dev libjpeg-dev \
      libfreetype6-dev libgif-dev ragel libavformat-dev libavcodec-dev \
      libswscale-dev libgflags-dev libgoogle-glog-dev
----

後はmakeするだけなんですが，こんなエラーが．新しいlibavに追従できてない感じ?

----
g++ -g -O3 -std=c++11 -Wall -Wextra -fno-exceptions -fno-rtti -MD  -march=native -c -o movie.o movie.cc
movie.cc: In member function ‘void Movie::PrepareRGB(int, int)’:
movie.cc:56:42: error: ‘PIX_FMT_RGB24’ was not declared in this scope
                       width_, height_, PIX_FMT_RGB24, SWS_FAST_BILINEAR,
                                          ^
In file included from graphic.h:10:0,
                 from movie.h:9,
                 from movie.cc:4:
movie.cc:64:38: error: ‘avcodec_alloc_frame’ was not declared in this scope
 CHECK(frame_ = avcodec_alloc_frame());
                                      ^
movie.cc:66:19: warning: ‘int avpicture_get_size(AVPixelFormat, int, int)’ is deprecated [-Wdeprecated-declarations]
   int rgb_bytes = avpicture_get_size(PIX_FMT_RGB24, width_, height_);
                   ^
In file included from /usr/include/x86_64-linux-gnu/libavformat/avformat.h:318:0,
                 from movie.cc:10:
/usr/include/x86_64-linux-gnu/libavcodec/avcodec.h:4898:5: note: declared here
 int avpicture_get_size(enum AVPixelFormat pix_fmt, int width, int height);
     ^
movie.cc:66:19: warning: ‘int avpicture_get_size(AVPixelFormat, int, int)’ is deprecated [-Wdeprecated-declarations]
   int rgb_bytes = avpicture_get_size(PIX_FMT_RGB24, width_, height_);
                   ^
In file included from /usr/include/x86_64-linux-gnu/libavformat/avformat.h:318:0,
                 from movie.cc:10:
/usr/include/x86_64-linux-gnu/libavcodec/avcodec.h:4898:5: note: declared here
 int avpicture_get_size(enum AVPixelFormat pix_fmt, int width, int height);
     ^
movie.cc:69:14: warning: ‘int avpicture_fill(AVPicture*, const uint8_t*, AVPixelFormat, int, int)’ is deprecated [-Wdeprecated-declarations]
   int prep = avpicture_fill(reinterpret_cast<AVPicture*>(frame_rgb_),
              ^
In file included from /usr/include/x86_64-linux-gnu/libavformat/avformat.h:318:0,
                 from movie.cc:10:
/usr/include/x86_64-linux-gnu/libavcodec/avcodec.h:4883:5: note: declared here
 int avpicture_fill(AVPicture *picture, const uint8_t *ptr,
     ^
movie.cc:69:14: warning: ‘int avpicture_fill(AVPicture*, const uint8_t*, AVPixelFormat, int, int)’ is deprecated [-Wdeprecated-declarations]
   int prep = avpicture_fill(reinterpret_cast<AVPicture*>(frame_rgb_),
              ^
In file included from /usr/include/x86_64-linux-gnu/libavformat/avformat.h:318:0,
                 from movie.cc:10:
/usr/include/x86_64-linux-gnu/libavcodec/avcodec.h:4883:5: note: declared here
 int avpicture_fill(AVPicture *picture, const uint8_t *ptr,
     ^
movie.cc: In member function ‘Graphic Movie::Next()’:
movie.cc:94:5: warning: ‘void av_free_packet(AVPacket*)’ is deprecated [-Wdeprecated-declarations]
     av_free_packet(&packet);
     ^
In file included from /usr/include/x86_64-linux-gnu/libavformat/avformat.h:318:0,
                 from movie.cc:10:
/usr/include/x86_64-linux-gnu/libavcodec/avcodec.h:4040:6: note: declared here
 void av_free_packet(AVPacket *pkt);
      ^
movie.cc:94:5: warning: ‘void av_free_packet(AVPacket*)’ is deprecated [-Wdeprecated-declarations]
     av_free_packet(&packet);
     ^
In file included from /usr/include/x86_64-linux-gnu/libavformat/avformat.h:318:0,
                 from movie.cc:10:
/usr/include/x86_64-linux-gnu/libavcodec/avcodec.h:4040:6: note: declared here
 void av_free_packet(AVPacket *pkt);
      ^
movie.cc:94:27: warning: ‘void av_free_packet(AVPacket*)’ is deprecated [-Wdeprecated-declarations]
     av_free_packet(&packet);
                           ^
In file included from /usr/include/x86_64-linux-gnu/libavformat/avformat.h:318:0,
                 from movie.cc:10:
/usr/include/x86_64-linux-gnu/libavcodec/avcodec.h:4040:6: note: declared here
 void av_free_packet(AVPacket *pkt);
      ^
<ビルトイン>: ターゲット 'movie.o' のレシピで失敗しました
make: *** [movie.o] エラー 1
----

でも以下のPRを反映したら通りました :)

- https://github.com/jart/hiptext/pull/27[Ffmpeg improvements by tombh · Pull Request #27 · jart/hiptext]

== 試してみる

サンプルとして，`balls.jpg`, `obama.jpg` があるのでこれを試してみます．
うまく動いていそうです．

https://asciinema.org/a/47418[image:https://asciinema.org/a/47418.png[]]

でも.pngはうまく動かない?

----
$ ./hiptext ~/Downloads/Tux.png
F0601 11:01:16.713266 30721 png.cc:35] Check failed: type == PNG_COLOR_TYPE_RGB || type == PNG_COLOR_TYPE_RGBA png bad type: /home/mk/Downloads/Tux.png
*** Check failure stack trace: ***
    @     0x7fa48d3c013d  google::LogMessage::Fail()
    @     0x7fa48d3c1fa3  google::LogMessage::SendToLog()
    @     0x7fa48d3bfccb  google::LogMessage::Flush()
    @     0x7fa48d3c298e  google::LogMessageFatal::~LogMessageFatal()
    @           0x41859f  LoadPNG()
    @           0x403fa8  main
    @     0x7fa489e59610  __libc_start_main
    @           0x40a229  _start
    @              (nil)  (unknown)
中止
$ convert ~/Downloads/Tux.png ./Tux.jpg
$ ./hiptext Tux.jpg
----

`.jpg`に変換したらいけてる感じですがなんか欠けてます．

https://www.flickr.com/photos/119142834@N05/26780171563/in/dateposted/[image:https://c4.staticflickr.com/8/7579/26780171563_8d301b27a4.jpg[]]

動画再生もいけます．再生のみで一時定位などの操作は出来ない感じ?

https://asciinema.org/a/47425[image:https://asciinema.org/a/47425.png[]]
_https://www.youtube.com/watch?v=1a5hlejlq9E[「ふらいんぐうぃっち ぷち」　＃１ - YouTube]より_

今のところlibcacaや動画ではvlcやmplayerなんかのほうがパッケージで導入できるのでお手軽そう．

とりあえず動くようになったので次はTexttopを試してみようと思います．
