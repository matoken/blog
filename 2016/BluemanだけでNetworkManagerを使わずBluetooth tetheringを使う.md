最近の自転車移動でThinkpad X200を運んでいるのですがちょっと重い．
PC使うかわからないようなときも持ち運んでいるのですが重いし壊しそうだしであまり良くない．
最近値崩れしたKINGJIM PORTABOOK(XMC10)や小さくて可愛いGPD-WINとかほしいのですがお金ない．てことで昔買ってあまり使っていないFujitsu FMV-U8250 + 外部キーボードで荷物の軽量化出来ないかなと．

<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B01946W1CA&linkId=f52593451ba06d7bdff7d64d15e0dd4f"></iframe>

ちなみにGPD-WINを日本で利用する人の技適問題は以下の辺りから買うと大丈夫のよう．(他は多分NG)

- [PCゲーマーへ朗報！コントローラ付ゲーム専用モバイルPC＜GPD WIN＞上陸！ コミュニケーション | クラウドファンディング - Makuake（マクアケ）](https://www.makuake.com/project/gpd-win/communication/ "PCゲーマーへ朗報！コントローラ付ゲーム専用モバイルPC＜GPD WIN＞上陸！ コミュニケーション | クラウドファンディング - Makuake（マクアケ）")

ということでとりあえずFujitsu FMV-U8250にUSB-Bluetoothアダプタを接続してBluetooth外部キーボードとBluetooth tetheringな環境を整えようと思ったのですが，OSをDevuan jessieにしてBluemanで繋ごうとすると，

```
Connection Failed: the name org.freedesktop.NetworkManager was not provided by ant .service files
```

というエラーで接続できませんでした．
NetworkManagerに依存しているようです．

しかしいまのネットワークマネージャーはWicdでNetworkManagerは入っていません．どうにかならないかなーとBluemanの設定を見るとそれらしいものを発見．

- View -> Local Service
- NetworkのPAN SupportとDUN SupportをそれぞれBluemanに

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/30591227854/in/dateposted/" title="blueman01"><img src="https://c2.staticflickr.com/6/5677/30591227854_19d5f12430_o.png" width="280" height="333" alt="blueman01"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script><a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/31412411785/in/photostream/" title="blueman02"><img src="https://c2.staticflickr.com/6/5502/31412411785_0236cc45d5_n.jpg" width="320" height="274" alt="blueman02"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

これでNetworkManagerのない環境でもBluetooth tetheringで繋がるようになりました :)

ちなみにX200(9セルバッテリ)の重量は1.7kg近く，FMV-U8250(大容量バッテリ)はで700g近く+キーボード420g……．体積は結構減ってるけど案外軽くなってませんね．キーボードが案外重いです．使い勝手も大分落ちるし微妙?

<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B00DLK4GO2&linkId=eb01bcdc52450a00cd55678b2c17be48"></iframe>

