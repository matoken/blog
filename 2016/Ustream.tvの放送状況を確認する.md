<!--
Ustream.tvの放送状況を確認する
-->

Ustream.tvの放送状況を確認して放送中なら再生するというものが作れないかと思って少し調べたメモです．

やりたいのはこんな感じ．
- Ustream.tvの「登録済みチャンネル」から放送中のチャンネルを抽出
- 放送中のものがあったら再生
- 放送中のものがなかったらNASA.TV辺りを流す


APIをざっと見てみるとチャンネルの放送状況は取得できました．

- [Ustream Developers - Ustream Developers](http://developers.ustream.tv/ "Ustream Developers - Ustream Developers")
  - [Channel API - Ustream Developers](http://developers.ustream.tv/broadcasting-api/channel.html "Channel API - Ustream Developers")

この機能はデベロッパーIDなども必要ないようです．

```
$ w3m -dump_source https://api.ustream.tv/channels/17125175.xml|grep status
        <status><![CDATA[live]]></status>
$ w3m -dump_source https://api.ustream.tv/channels/27482.xml|grep status
        <status><![CDATA[offair]]></status>﻿
```

ここで指定するのはチャンネルIDでチャンネルURLの後ろのチャンネルではありません．これは以下のAPIで取得できます．

```
$ w3m -dump_source http://api.ustream.tv/xml/channel/ospn-tv/getValueOf/id|grep results
<results>10635140</results>﻿
```

ということで「登録済みチャンネル」の取得以外は出来そうです．後はこれをlivestreamerに流し込むとUstream.tvのサイネージが作れそうな感じ．

ちなみにAPIにはLive開始時などに教えてくれる機能などもあるようですがそちらはレシーバが必要になるので今回の用途には向か無さそうなのでちゃんと調べていません．


<iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B0065G4GH6" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe><iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00APZ43BM" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe><iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00SKLFSL2" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>


<!-- Place this tag in your head or just before your close body tag. -->
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<!-- Place this tag where you want the widget to render. -->
<div class="g-post" data-href="https://plus.google.com/+KenichiroMATOHARA/posts/bgVrsqrWC53"></div>

<!-- Place this tag in your head or just before your close body tag. -->
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<!-- Place this tag where you want the widget to render. -->
<div class="g-post" data-href="https://plus.google.com/+KenichiroMATOHARA/posts/QEAh3ftzN3h"></div>

<!-- Place this tag in your head or just before your close body tag. -->
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<!-- Place this tag where you want the widget to render. -->
<div class="g-post" data-href="https://plus.google.com/+KenichiroMATOHARA/posts/RmbEPYrGgiD"></div>
