# コマンドラインからGooglecastにcast出来るcastnowを試す

見つけたので試してみました

- [GitHub - xat/castnow: commandline chromecast player](https://github.com/xat/castnow "GitHub - xat/castnow: commandline chromecast player")

Debian stretch amd64, Ubuntu 16.04 LTS amd64で確認しています．

## 導入

$HOME以下にPATH通してnpmで導入しています．/usr/local以下で良ければ`npm install castnow -g`だけでいいかも．

```
$ sudo apt install npm nodejs-legacy
$ echo 'PATH="$PATH":$HOME/node_modules/.bin' >> ~/.bashrc
$ source ~/.bashrc
$ npm install castnow
```

入りましたがChromecastがみあたらない?

```
$ castnow --help

Usage: castnow [<media>, <media>, ...] [OPTIONS]

Option                  Meaning
--tomp4                 Convert file to mp4 during playback
--device <name>         The name of the Chromecast device that should be used
--address <ip>          The IP address or hostname of your Chromecast device
--subtitles <path/url>  Path or URL to an SRT or VTT file
--subtitle-scale <scale> Subtitle font scale
--subtitle-color <color> Subtitle font RGBA color
--myip <ip>             Your local IP address
--quiet                 No output
--peerflix-* <value>    Pass options to peerflix
--ffmpeg-* <value>      Pass options to ffmpeg
--type <type>           Explicitly set the mime-type (e.g. "video/mp4")
--bypass-srt-encoding   Disable automatic UTF-8 encoding of SRT subtitles
--seek <hh:mm:ss>       Seek to the specified time on start using the format hh:mm:ss or mm:ss
--loop                  Loop over playlist, or file, forever
--help                  This help screen

Player controls

Key                     Action
space                   Toggle between play and pause
m                       Toggle mute
up                      Volume Up
down                    Volume Down
left                    Seek backward
right                   Seek forward
n                       Next in playlist
s                       Stop playback
quit                    Quit

$ castnow
Error: device not found
```

名前解決が出来なかったようでipを指定するとcast出来ました．別のマシンでは通ったのでこのマシンがおかしいようです．

```
$ avahi-browse -a -t -l -r
+ wlp3s0 IPv6 x201s                                         Remote Disk Management local
+ wlp3s0 IPv4 Chromecast-KagoLUG                            _googlecast._tcp     local
= wlp3s0 IPv6 x201s                                         Remote Disk Management local
   hostname = [x201s.local]
   address = [fe80::222:faff:fe33:456a]
   port = [22]
   txt = []
= wlp3s0 IPv4 Chromecast-KagoLUG                            _googlecast._tcp     local
   hostname = [Chromecast-KagoLUG.local]
   address = [192.168.2.206]
   port = [8009]
   txt = ["rs=" "bs=FA8FCA59198D" "st=1" "ca=4101" "fn=Chromecast-KagoLUG" "ic=/setup/icon.png" "md=Chromecast" "ve=05" "rm=A652B47431F36207" "id=7c14aaaa62a6002dd95ba94814e5c5e0"]
$ castnow --address 192.168.2.206 http://commondatastorage.googleapis.com/gtv-videos-bucket/ED_1280.mp4

  State     : Playing....

```

パイプも使えるので色々応用できそうな気がします．
（最近Chromiumがamd64/i386しかなくなったようですが，これを使えば他のアーキテクチャでもスクリーンキャストもできそう?）

- [Debian -- jessie の chromium パッケージに関する詳細](https://packages.debian.org/ja/jessie/chromium "Debian -- jessie の chromium パッケージに関する詳細")


<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B00IJBCAXC&linkId=373770b92e66dd545097a1aff492666d"></iframe><iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B00W54JNCU&linkId=c11e581f91243773a2890cdd07f7a1a6"></iframe><iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B00ZVNYLS8&linkId=287d84c6af923db3844d48127bf85027"></iframe>