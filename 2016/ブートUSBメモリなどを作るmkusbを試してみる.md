<!--
ブートUSBメモリなどを作るmkusbを試してみる
-->

USBメモリなどにisoを書き込んで起動デバイスを作れるとても便利な`UNetbootin`というツールがあります．
しかし，Debian jessie/stretchに入っていません．

- [Debian -- sid の unetbootin パッケージに関する詳細](https://packages.debian.org/sid/unetbootin "Debian -- sid の unetbootin パッケージに関する詳細")

なんでだろうとBTSを見るとうまく動かないバグがあるようです．(UEFI環境でNG?)

- [#775689 - Do NOT use unetbootin for Debian CD images - Debian Bug report logs](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=775689 "#775689 - Do NOT use unetbootin for Debian CD images - Debian Bug report logs")

ざざっと眺めていて`mkusb`という同じようなツールがあるのを見かけたので試してみました．

- [mkusb - Community Help Wiki](https://help.ubuntu.com/community/mkusb "mkusb - Community Help Wiki")

## 環境

`Debian stretch testing amd64`です．

```
$ screenfetch
         _,met$$$$$gg.           mk@x220
      ,g$$$$$$$$$$$$$$$P.        OS: Debian testing-updates sid
    ,g$$P""       """Y$$.".      Kernel: x86_64 Linux 4.3.0-1-amd64
   ,$$P'              `$$$.      Uptime: 9h 7m
  ',$$P       ,ggs.     `$$b:    Packages: 3622
  `d$$'     ,$P"'   .    $$$     Shell: bash 4.3.42
   $$P      d$'     ,    $$P     Resolution: 1366x768
   $$:      $$.   -    ,d$$'     WM: Awesome
   $$\;      Y$b._   _,d$P'      WM Theme: awesome
   Y$$.    `.`"Y$$$$P"'          CPU: Intel Core i5-2540M CPU @ 3.3GHz
   `$$b      "-.__               GPU: Mesa DRI Intel(R) Sandybridge Mobile
    `Y$$                         RAM: 9768MiB / 15934MiB
     `Y$$.                      
       `$$b.                    
         `Y$$b.                 
            `"Y$b._             
                `""""           

```

Ubuntuはppaがあります．DebianはUbuntuのppaが使えます．他のLinuxディストリビューションでは導入スクリプト( `mkusb-installer` )が用意されています．

## 導入(ppa)

source.listを登録します．`apt edit-sources`コマンドで`deb http://ppa.launchpad.net/mkusb/ppa/ubuntu trusty main`を登録します．

```
$ sudo apt edit-sources mkusb
$ sudo chmod +r /etc/apt/sources.list.d/mkusb.list
$ cat /etc/apt/sources.list.d/mkusb.list
# "mkusb/install-to-debian - Community Help Wiki"
# https://help.ubuntu.com/community/mkusb/install-to-debian
deb http://ppa.launchpad.net/mkusb/ppa/ubuntu trusty main
```

- [MKUSB PPA : “MKUSB” team](https://launchpad.net/~mkusb/+archive/ubuntu/ppa "MKUSB PPA : “MKUSB” team")
を参考にapy-key addする．

```
$ gpg --keyserver keyserver.ubuntu.com --recv-keys 54B8C8AC
gpg: 鍵54B8C8ACをhkpからサーバkeyserver.ubuntu.comに要求
gpg: 鍵54B8C8AC: 公開鍵"Launchpad PPA for MKUSB"をインポートしました
gpg:           処理数の合計: 1
gpg:             インポート: 1  (RSA: 1)
$ gpg --fingerprint 54B8C8AC
pub   4096R/54B8C8AC 2014-08-14
 フィンガー・プリント = 29D7 6ADA 2D15 A87B F4C6  8B82 3729 8274 54B8 C8AC
uid                  Launchpad PPA for MKUSB

$ gpg -a --export 54B8C8AC | sudo apt-key add -
OK
$ apt-key list|grep -A1 54B8C8AC
pub   4096R/54B8C8AC 2014-08-14
uid                  Launchpad PPA for MKUSB
```

パッケージリストの更新とmkusbパッケージの導入

```
$ sudo apt update
$ sudo apt install mkusb mkusb-nox
$ dpkg -L mkusb
/.
/usr
/usr/share
/usr/share/mkusb
/usr/share/mkusb/mkusb-start
/usr/share/mkusb/restore
/usr/share/mkusb/mkusb-st2
/usr/share/mkusb/usb-pack_efi.tar.gz
/usr/share/mkusb/grub.cfg
/usr/share/mkusb/maybe-problems.txt
/usr/share/mkusb/backup
/usr/share/icons
/usr/share/icons/mkusb.svg
/usr/share/icons/hicolor
/usr/share/icons/hicolor/scalable
/usr/share/icons/hicolor/scalable/apps
/usr/share/icons/hicolor/scalable/apps/mkusb.svg
/usr/share/icons/hicolor/48x48
/usr/share/icons/hicolor/48x48/apps
/usr/share/icons/hicolor/48x48/apps/mkusb.png
/usr/share/doc
/usr/share/doc/mkusb
/usr/share/doc/mkusb/README.Debian
/usr/share/doc/mkusb/copyright
/usr/share/doc/mkusb/changelog.Debian.gz
/usr/share/man
/usr/share/man/man8
/usr/share/man/man8/mkusb.8.gz
/usr/share/applications
/usr/share/applications/mkusb.desktop
/usr/sbin
/usr/sbin/mkusb
$ dpkg -L mkusb-nox
/.
/usr
/usr/share
/usr/share/doc
/usr/share/doc/mkusb-nox
/usr/share/doc/mkusb-nox/copyright
/usr/share/doc/mkusb-nox/changelog.Debian.gz
/usr/sbin
/usr/sbin/mkusb-nox
```

<!--
## 導入(導入script)

Ubuntu/Debian以外のディストリビューション向けに導入scriptが用意されています．これも試してみます．

scriptと署名ファイルの入手
```
$ wget http://phillw.net/isos/linux-tools/mkusb/mkusb-installer http://phillw.net/isos/linux-tools/mkusb/md5sum.txt.asc
```

署名ファイルの確認と鍵の入手

```
$ gpg --verify md5sum.txt.asc
gpg: 2016年02月07日 04時53分57秒 JSTにRSA鍵ID EB0FC2C8で施された署名
gpg: 署名を検査できません: 公開鍵が見つかりません
$ gpg --recv-keys EB0FC2C8
gpg: 鍵EB0FC2C8をhkpからサーバkeys.gnupg.netに要求
gpg: 鍵EB0FC2C8: 公開鍵"Nio Sudden Wiklund (sudodus) <nio.wiklund@gmail.com>"をインポートしました
gpg: 最小の「ある程度の信用」3、最小の「全面的信用」1、PGP信用モデル
gpg: 深さ: 0  有効性:  15  署名:  52  信用: 0-, 0q, 0n, 0m, 0f, 15u
gpg: 深さ: 1  有効性:  52  署名:  59  信用: 52-, 0q, 0n, 0m, 0f, 0u
gpg: 次回の信用データベース検査は、2018-07-07です
gpg:           処理数の合計: 1
gpg:             インポート: 1  (RSA: 1)
$ gpg --fingerprint EB0FC2C8
pub   2048R/EB0FC2C8 2012-04-08
 フィンガー・プリント = 0303 EA77 E34C 52F2 2958  47C6 BD43 C742 EB0F C2C8
uid                  Nio Sudden Wiklund (sudodus) <nio.wiklund@gmail.com>
sub   2048R/4FC1D9E7 2012-04-08

$ gpg --verify md5sum.txt.asc
gpg: 2016年02月07日 04時53分57秒 JSTにRSA鍵ID EB0FC2C8で施された署名
gpg: "Nio Sudden Wiklund (sudodus) <nio.wiklund@gmail.com>"からの正しい署名
gpg: *警告*: この鍵は信用できる署名で証明されていません!
gpg:       この署名が所有者のものかどうかの検証手段がありません。
主鍵フィンガー・プリント: 0303 EA77 E34C 52F2 2958  47C6 BD43 C742 EB0F C2C8
```

ハッシュの確認

```
$ grep mkusb-installer md5sum.txt.asc
0e0bad5fe02826e558b9f236d3ee3e8e  mkusb-installer
8002f77399074990a2b6faf8163528b5  mkusb-installer0
$ md5sum mkusb-installer
0e0bad5fe02826e558b9f236d3ee3e8e  mkusb-installer
```

導入scriptの実行

```

```
-->

## 実行例

メニューから起動するか，

```
$ sudo -H /usr/sbin/mkusb
```

のようにして起動します．書き込むイメージが決まっている場合は引数に指定できます．イメージには.gzや.xzも指定可能なので展開の手間が省けます．

```
$ sudo -H /usr/sbin/mkusb ./alpine-3.3.1-x86_64.iso
```

スプラッシュが出て，

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/25195091486/in/dateposted-public/" title="20160224_06:02:32-32496"><img src="https://farm2.staticflickr.com/1655/25195091486_d34c146b04_m.jpg" width="188" height="170" alt="20160224_06:02:32-32496"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/25103106952/in/dateposted-public/" title="20160224_06:02:36-32565"><img src="https://farm2.staticflickr.com/1689/25103106952_e513f12867_m.jpg" width="240" height="177" alt="20160224_06:02:36-32565"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

メニューが出てきます．cloneやwipeも可能です．
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/25221377045/in/dateposted-public/" title="20160224_06:02:45-32658"><img src="https://farm2.staticflickr.com/1594/25221377045_038469f76f.jpg" width="500" height="204" alt="20160224_06:02:45-32658"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

書き込み先のデバイスは間違えないようにしましょう．間違えるととても悲しいことになります．ちなみにSD Cardスロットの`/dev/mmcblk0`は検出されませんでした．USB-SDアダプタ経由だと出てきました．
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/24594563023/in/dateposted-public/" title="20160224_06:02:26-868"><img src="https://farm2.staticflickr.com/1489/24594563023_60d6255a1d.jpg" width="500" height="442" alt="20160224_06:02:26-868"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/25103107232/in/dateposted-public/" title="20160224_06:02:43-1237"><img src="https://farm2.staticflickr.com/1551/25103107232_742acb4bfd.jpg" width="500" height="326" alt="20160224_06:02:43-1237"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

確認画面でチェックを入れて「Go」ボタンで書き込みが始まります．
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/25103107482/in/dateposted-public/" title="20160224_06:02:05-2732"><img src="https://farm2.staticflickr.com/1555/25103107482_755daa0d42.jpg" width="500" height="355" alt="20160224_06:02:05-2732"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/24853685429/in/dateposted-public/" title="20160224_06:02:13-2827"><img src="https://farm2.staticflickr.com/1467/24853685429_277581670c.jpg" width="420" height="179" alt="20160224_06:02:13-2827"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/24853685489/in/dateposted-public/" title="20160224_06:02:17-2960"><img src="https://farm2.staticflickr.com/1541/24853685489_df757351af.jpg" width="500" height="326" alt="20160224_06:02:17-2960"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>


## 実行例(nox)

`mkusb-nox`コマンドを使うと端末で利用できます．nox版では複数のUSBメモリがある場合の選択方法はわかりませんでした．書き込むデバイスだけを差し込むようにするしか無いかもしれません．

```
$ sudo /usr/sbin/mkusb-nox ./alpine-3.3.1-x86_64.iso
The iso file SHOULD BE loop mounted on a temporary file READ-ONLY:
mount: /dev/loop0 is write-protected, mounting read-only
disk_name_type=label
default grsec|label grsec| _found_ in iso-file
default grsec|label grsec| _found_ in /dev/sdb
 Final checkpoint
Install to /dev/sdb? (y/n)
y
pv "alpine-3.3.1-x86_64.iso"| dd of=/dev/sdb bs=4096 ...
  82MiB 0:00:00 [ 205MiB/s] [========================================================================================================================>] 100%            
20992+0 records in
20992+0 records out
85983232 bytes (86 MB) copied, 4.84297 s, 17.8 MB/s
syncing the drive ...
The default grsec|label grsec| USB device is re-cloned  :-)
```


問題なく利用できる場合は`UNetbootin`の方がマルチプラットホームだし使いやすそうです．状況により使い分けるのがいいのかもしれません．
