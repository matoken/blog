# Screenfetch的な情報表示コマンドのNeofetch

よくデスクトップのキャプチャを取得するときに表示されているScreenfetchコマンドというものがあります．システムの情報とアスキーアートを素敵に表示してくれます．
これににたものでNetfetchというものを見かけたので試してみました．こちらは画像も扱えるようです．

- [dylanaraps/neofetch: A fast, highly customizable system info script that supports Linux, MacOS, iOS, BSD, Solaris, Android, Haiku, GNU Hurd and Windows (Cygwin/Windows 10 Linux subsystem)](https://github.com/dylanaraps/neofetch "dylanaraps/neofetch: A fast, highly customizable system info script that supports Linux, MacOS, iOS, BSD, Solaris, Android, Haiku, GNU Hurd and Windows (Cygwin/Windows 10 Linux subsystem)")

## 導入

以下のページに各種環境への導入方法が書かれています．

- [Installation · dylanaraps/neofetch Wiki](https://github.com/dylanaraps/neofetch/wiki/Installation "Installation · dylanaraps/neofetch Wiki")

Debian sid/stretchにはパッケージがあるようなことが書かれていますが，

```text
Neofetch is in Debian Unstable/Stretch's official repositories.
```

今はsidのみのようです．

- [Debian -- パッケージ検索結果 -- neofetch](https://packages.debian.org/search?keywords=neofetch&searchon=names&suite=unstable&section=all "Debian -- パッケージ検索結果 -- neofetch")

ということでひとまず`stretch`ではこんな感じで導入しました．

```shell
$ #-- /etc/apt/sources.list.d/neofetch を作る
$ sudo apt edit-sources neofetch
$ cat /etc/apt/sources.list.d/neofetch.list
deb http://dl.bintray.com/dawidd6/neofetch jessie main
$ #-- 鍵のインポート
$ wget "https://bintray.com/user/downloadSubjectPublicKey?username=bintray" -O Release-neofetch.key
$ gpg Release-neofetch.key
pub   rsa4096 2015-02-17 [SC]
      8756C4F765C9AC3CB6B85D62379CE192D401AB61
uid           Bintray (by JFrog) <bintray@bintray.com>
sub   rsa4096 2015-02-17 [E]
$ sudo apt-key add Release-neofetch.key
$ rm Release-neofetch.key
$ #-- pkg情報更新&pkg導入
$ sudo apt update
$ sudo apt install neofetch
```

## 早速実行

```
$ neofetch --help

    NEOFETCH

    USAGE: neofetch --option "value" --option "value"

    NOTE: There's also a config option for each flag below.

    Info:
    --disable infoname          Allows you to disable an info line from appearing
                                in the output.
                                NOTE: You can supply multiple args. eg.
                                'neofetch --disable cpu gpu disk shell'
                                NOTE: The arguments must all be lowercase.
    --os_arch on/off            Hide/Show OS architecture.
    --speed_type type           Change the type of cpu speed to display.
                                Possible values: current, min, max, bios,
                                scaling_current, scaling_min, scaling_max
                                NOTE: This only support Linux with cpufreq.
    --cpu_shorthand type        Shorten the output of CPU
                                Possible values: name, speed, tiny, on, off
    --cpu_cores type            Whether or not to display the number of CPU cores
                                Takes: logical, physical, off
                                NOTE: 'physical' doesn't work on BSD.
    --cpu_speed on/off          Hide/Show cpu speed.
    --cpu_temp on/off           Hide/Show cpu temperature.
                                NOTE: This only works on Linux and BSD.
                                NOTE: For FreeBSD-based systems, you need to enable coretemp
                                      kernel module.
    --distro_shorthand on/off   Shorten the output of distro (tiny, on, off)
                                NOTE: This option won't work in Windows (Cygwin)
    --kernel_shorthand on/off   Shorten the output of kernel
                                NOTE: This option won't work in BSDs (except PacBSD and PC-BSD)
    --uptime_shorthand on/off   Shorten the output of uptime (tiny, on, off)
    --refresh_rate on/off       Whether to display the refresh rate of each monitor
                                Unsupported on Windows
    --gpu_brand on/off          Enable/Disable GPU brand in output. (AMD/NVIDIA/Intel)
    --gtk_shorthand on/off      Shorten output of gtk theme/icons
    --gtk2 on/off               Enable/Disable gtk2 theme/icons output
    --gtk3 on/off               Enable/Disable gtk3 theme/icons output
    --shell_path on/off         Enable/Disable showing $SHELL path
    --shell_version on/off      Enable/Disable showing $SHELL version
    --ip_host url               Url to query for public IP
    --song_shorthand on/off     Print the Artist/Title on seperate lines
    --birthday_shorthand on/off Shorten the output of birthday
    --birthday_time on/off      Enable/Disable showing the time in birthday output
    --birthday_format format    Format the birthday output. (Uses 'date' cmd format)

    Text Formatting:
    --colors x x x x x x        Changes the text colors in this order:
                                title, @, underline, subtitle, colon, info
    --underline on/off          Enable/Disable the underline.
    --underline_char char       Character to use when underlining title
    --bold on/off               Enable/Disable bold text

    Color Blocks:
    --color_blocks on/off       Enable/Disable the color blocks
    --block_width num           Width of color blocks in spaces
    --block_height num          Height of color blocks in lines
    --block_range start end     Range of colors to print as blocks

    Bars:
    --bar_char 'elapsed char' 'total char'
                                Characters to use when drawing bars.
    --bar_border on/off         Whether or not to surround the bar with '[]'
    --bar_length num            Length in spaces to make the bars.
    --bar_colors num num        Colors to make the bar.
                                Set in this order: elapsed, total
    --cpu_display mode          Bar mode.
                                Takes: bar, infobar, barinfo, off
    --memory_display mode       Bar mode.
                                Takes: bar, infobar, barinfo, off
    --battery_display mode      Bar mode.
                                Takes: bar, infobar, barinfo, off
    --disk_display mode         Bar mode.
                                Takes: bar, infobar, barinfo, off

    Image:
    --image type                Image source. Where and what image we display.
                                Possible values: wall, ascii,
                                /path/to/img, /path/to/dir/, off
    --size 00px | --size 00%    How to size the image.
                                Possible values: auto, 00px, 00%, none
    --crop_mode mode            Which crop mode to use
                                Takes the values: normal, fit, fill
    --crop_offset value         Change the crop offset for normal mode.
                                Possible values: northwest, north, northeast,
                                west, center, east, southwest, south, southeast

    --xoffset px                How close the image will be to the left edge of the
                                window. This only works with w3m.
    --yoffset px                How close the image will be to the top edge of the
                                window. This only works with w3m.
    --bg_color color            Background color to display behind transparent image.
                                This only works with w3m.
    --gap num                   Gap between image and text.
                                NOTE: --gap can take a negative value which will
                                move the text closer to the left side.
    --clean                     Delete cached files and thumbnails.

    Ascii:
    --ascii value               Where to get the ascii from, Possible values:
                                distro, /path/to/ascii
    --ascii_colors x x x x x x  Colors to print the ascii art
    --ascii_distro distro       Which Distro's ascii art to print
                                NOTE: Arch and Ubuntu have 'old' logo varients.
                                NOTE: Use 'arch_old' or 'ubuntu_old' to use the old logos.
                                NOTE: Ubuntu has flavor varients.
                                NOTE: Change this to 'Lubuntu', 'Xubuntu', 'Ubuntu-GNOME'
                                or 'Ubuntu-Budgie' to use the flavors.
    --ascii_logo_size           Size of ascii logo.
                                Supported distros: Arch, Gentoo, Crux, OpenBSD.
    --ascii_bold on/off         Whether or not to bold the ascii logo.
    --logo | -L                 Hide the info text and only show the ascii logo.

    Screenshot:
    --scrot | -s /path/to/img   Take a screenshot, if path is left empty the screen-
                                shot function will use $scrot_dir and $scrot_name.
    --upload | -su /pth/t/img   Same as --scrot but uploads the scrot to a website.
    --image_host                Website to upload scrots to. Takes: imgur, teknik
    --scrot_cmd cmd             Screenshot program to launch

    Other:
    --config /path/to/config    Specify a path to a custom config file
    --config none               Launch the script without a config file
    --help                      Print this text and exit
    --version                   Show neofetch version
    -v                          Display error messages.
    -vv                         Display a verbose log for error reporting.
```

オプション結構多いです．
とりあえず素で実行．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/31097343580/in/dateposted/" title="20161207_05:12:12-5188"><img src="https://c5.staticflickr.com/6/5495/31097343580_fe9314350a_n.jpg" width="320" height="153" alt="20161207_05:12:12-5188"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

情報もScreenfetchより増えています．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/31353317901/in/dateposted/" title="20161207_05:12:20-9093"><img src="https://c6.staticflickr.com/6/5765/31353317901_d3b07c7898_n.jpg" width="320" height="144" alt="20161207_05:12:20-9093"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

次は画像を指定してみます．画像サイズが大きかったので25%に縮小もしています．
楽しい :)

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/31353170281/in/photostream/" title="20161207_05:12:35-3963"><img src="https://c2.staticflickr.com/6/5557/31353170281_3d9308d6c6_n.jpg" width="320" height="141" alt="20161207_05:12:35-3963"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/31468904265/in/dateposted/" title="20161207_06:12:30-16979"><img src="https://c2.staticflickr.com/6/5573/31468904265_4e58bdf5b4_n.jpg" width="320" height="132" alt="20161207_06:12:30-16979"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

画像の表示については環境によってうまく行く行かないがあるようです．うまく表示されない場合は以下のWikiを参照すると良さそうです．

- [Images in the terminal · dylanaraps/neofetch Wiki](https://github.com/dylanaraps/neofetch/wiki/Images-in-the-terminal "Images in the terminal · dylanaraps/neofetch Wiki")




