<!--
awk 製 3Dシューティングゲーム `awkaster` で遊んでみた
-->

awk 製 3Dシューティングゲームがあると聞いて遊んでみました．

- [TheMozg/awk-raycaster](https://github.com/TheMozg/awk-raycaster)
  - [Show HN: 3D shooter in your terminal using raycasting in Awk | Hacker News](https://news.ycombinator.com/item?id=10896901)

遊ぶには GNU awk 4.0.0 以上が必要です．Debian だと `gawk` というパッケージです．とりあえず遊んでみるにはこんな感じで．

```
$ sudo apt install gawk
$ wget https://raw.githubusercontent.com/TheMozg/awk-raycaster/master/awkaster.awk
$ awk -f ./awkaster.awk
```

操作はこんな感じです．

- 1234 画面表示切り替え
- WASD 移動
- JL 回転(16回で1回転)/Shiftを押しながらで高速回転(8回で1回転)
- SPACE 攻撃
- X エレベーターに乗る（ゲームクリア）
- Q 終了


以下は遊んでいる様子です．

[![asciicast](https://asciinema.org/a/33858.png)](https://asciinema.org/a/33858)

3Dシューティングというとスターフォックスを思い浮かべたのですが，単純な 3D RPG 的な感じです．Wolfenstein 3D と doom に触発されたそうです．Wolfenstein 3DWolfenstein 3D　は知らないのですが確かに doom みたいな感じです．
ターン制?なので敵の赤球が出てきても慌てず操作できます．

敵に触れるとHPが減っていって0になるとゲームオーバーでこんなメッセージが出ます．

```
GAME OVER! YOU LOSE!
Credits: Fedor 'TheMozg' Kalugin
https://github.com/TheMozg/awk-raycaster
Gameplay testing - Alex 'Yakojo' & Danya 'bogych97'
Go away!
```

うまいことエレベーターに到着して `X` を押すとゲームクリアです．
但し `ELEVATOR COMING` が `0` になっている必要があります．これは初め `1000` で1操作で1カウントダウンされます．ゴールのエレベーターの場所は見た目わかりません
敵が沢山居るところの辺りにあるので探してみて下さい．
(私は判んなくて結局 source 読んでクリアしましたorz)

```
YOU WIN! YOUR SCORE: 4900
Credits: Fedor 'TheMozg' Kalugin
https://github.com/TheMozg/awk-raycaster
Gameplay testing - Alex 'Yakojo' & Danya 'bogych97'
Go away!
```

ちなみにそこそこ広い領域が必要です．領域が狭いと画面に収まりきらずずれてしまいます．フォントサイズを小さくするなどして列と行を確保しましょう．  
GNU screen 上でも Rasbian jessie 上でも問題なく動きました．

昔こんな3D迷路とかをポケコンBASIC で作っていたのを思い出しました．最近のマイコンやRaspberry Pi でこういうゲームを作ってみるのも楽しそうですね :)  
([Arduboy](https://www.arduboy.com/) とか [POCKETC.H.I.P.](http://getchip.com/pages/pocketchip) とか )
