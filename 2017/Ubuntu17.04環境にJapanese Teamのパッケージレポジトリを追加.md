# Ubuntu17.04環境にJapanese Teamのパッケージレポジトリを追加

5/3にUbuntu 17.04 日本語 Remixがリリースされました．

* [Ubuntu 17.04 日本語 Remix リリース | Ubuntu Japanese Team](http://www.ubuntulinux.jp/News/ubuntu1704-ja-remix "Ubuntu 17.04 日本語 Remix リリース | Ubuntu Japanese Team")
* [Ubuntuの日本語環境 | Ubuntu Japanese Team](http://www.ubuntulinux.jp/japanese "Ubuntuの日本語環境 | Ubuntu Japanese Team")

デスクトップ利用しているUbuntu 17.04環境が1つあるのでUbuntu Japanese Teamのパッケージレポジトリを追加してみました．

```
$ wget https://www.ubuntulinux.jp/ubuntu-ja-archive-keyring.gpg
$ gpg ./ubuntu-ja-archive-keyring.gpg
pub   dsa1024 2005-05-24 [SC]
	  3B593C7BE6DB6A89FB7CBFFD058A05E90C4ECFEC
uid           Ubuntu-ja Archive Automatic Signing Key <archive@ubuntulinux.jp>
sub   elg2048 2005-05-24 [E]
$ sudo apt-key add ./ubuntu-ja-archive-keyring.gpg
OK
$ wget https://www.ubuntulinux.jp/ubuntu-jp-ppa-keyring.gpg
$ gpg ./ubuntu-jp-ppa-keyring.gpg 
pub   rsa1024 2009-04-05 [SC]
	  59676CBCF5DFD8C1CEFE375B68B5F60DCDC1D865
uid           Launchpad PPA for Ubuntu Japanese Team
$ sudo apt-key add ./ubuntu-jp-ppa-keyring.gpg 
OK
$ rm ./ubuntu-jp-ppa-keyring.gpg 
$ wget https://www.ubuntulinux.jp/sources.list.d/zesty.list
$ cat zesty.list 
## Ubuntu Japanese LoCo Team's repository for Ubuntu 17.04
## Please report any bugs to https://bugs.launchpad.net/ubuntu-jp-improvement
deb http://archive.ubuntulinux.jp/ubuntu zesty main
deb-src http://archive.ubuntulinux.jp/ubuntu zesty main
deb http://archive.ubuntulinux.jp/ubuntu-ja-non-free zesty multiverse
deb-src http://archive.ubuntulinux.jp/ubuntu-ja-non-free zesty multiverse
$ sudo mv ./zesty.list /etc/apt/sources.list.d/ubuntu-ja.list
$ sudo apt update
$ sudo apt upgrade
$ sudo apt install ubuntu-defaults-ja
$ apt show ubuntu-defaults-ja
Package: ubuntu-defaults-ja
Version: 17.04-0ubuntu1~ja3
Priority: optional
Section: metapackages
Maintainer: Jun Kobayashi <jkbys@ubuntu.com>
Installed-Size: 22.5 kB
Provides: ubuntu-default-settings
Depends: dconf-gsettings-backend | gsettings-backend, language-pack-ja, language-pack-gnome-ja, libreoffice-l10n-ja, libreoffice-help-ja, firefox-locale-ja, thunderbird-locale-ja, fonts-noto-cjk, fonts-takao-pgothic, fonts-takao-gothic, fonts-takao-mincho, poppler-data, cmap-adobe-japan1, fcitx, fcitx-mozc, fcitx-libs-qt5, fcitx-frontend-all, fcitx-frontend-gtk2, fcitx-frontend-gtk3, fcitx-frontend-qt4, fcitx-ui-classic, fcitx-config-gtk, mozc-utils-gui
Conflicts: im-setup-helper, ubuntu-default-settings
Replaces: im-setup-helper
Download-Size: 4,446 B
APT-Manual-Installed: yes
APT-Sources: http://archive.ubuntulinux.jp/ubuntu zesty/main amd64 Packages
Description: Default settings for Japanese/Japan
 This package contains customized default settings for the Japanese Ubuntu
 Remix.

```

