moshの1.3.0がリリースされました．

- [Mosh: the mobile shell](https://mosh.org/ "Mosh: the mobile shell")
- [[mosh-users] mosh 1.3.0 released (corrected announcement)](http://mailman.mit.edu/pipermail/mosh-users/2017-March/000341.html "[mosh-users] mosh 1.3.0 released (corrected announcement)")

moshはsshを置き換えようとしているソフトウェアで，ネットワークが遅いときにローカルエコーを表示したり，ネットワークが切り替わってもローミングしたりと便利なソフトウェアです．(NotePCで接続してサスペンドして別のネットワークでレジュームしてもすぐに使っていた端末にアクセスできる)

1.3.0でなにかおもしろい機能が増えたりしていないかなと眺めていると

```
  * Add --no-ssh-pty option for Dropbear compatibility and other issues.
```

Dropbearで使えるようになるオプションが増えているようです．
DropbearはOpenSSHに比べて大分小さなsshdです．組み込みとかで使われているのを見ます．でも機能が物足りないなと思うことも．

- [Dropbear SSH](<https://matt.ucc.asn.au/dropbear/dropbear.html> "Dropbear SSH")

mosh + Dropbearの組み合わせを試してみました．

試したのはこんな感じの環境です．

Host : Raspbian jessie(Raspberry Pi 2B)
Client : Debian sid amd64

## dropbearの準備

HostにDropbearをパッケージで導入します

```
$ sudo apt install dropbear
```

dropbearのhost鍵を用意

```
$ mkdir /tmp/dropbear
$ dropbearkey -t ecdsa -s 521 -f /tmp/dropbear/dropbear_ecdsa_host_key
```

- -t 鍵種類
- -s 鍵長
- -f 鍵ファイル

接続時に解りやすいようにバナーファイル作成

```
$ banner dropbear > /tmp/dropbear/banner
```

Dropbearを2222番ポートで起動

```
$ dropbear -r /tmp/dropbear/dropbear_ecdsa_host_key -p 2222 -b /tmp/dropbear/banner -E
```

- -r host鍵を指定
- -p sshポート番号
- -b バナーファイル
- -E 標準エラー出力にメッセージ出力

Dropbearにssh接続できるか確認

```shell
$ ssh pi@192.168.2.200 -i ~/.ssh/id_ecdsa -p 2222
Host key fingerprint is SHA256:G3u9pP4ZrkZ2kWs9e+ly9MkEtnO0f4ebvkBS0ObGJQQ
+---[ECDSA 521]---+
|          E+.    |
|           .+ .  |
|           +.+   |
|           .*o . |
|        S ..o++ .|
|         +o++oo= |
|        oo.o= *+=|
|         ..+ *o**|
|         o+o=.O=o|
+----[SHA256]-----+

 #####   #####    ####   #####   #####   ######    ##    #####
 #    #  #    #  #    #  #    #  #    #  #        #  #   #    #
 #    #  #    #  #    #  #    #  #####   #####   #    #  #    #
 #    #  #####   #    #  #####   #    #  #       ######  #####
 #    #  #   #   #    #  #       #    #  #       #    #  #   #
 #####   #    #   ####   #       #####   ######  #    #  #    #

Authenticated to 192.168.2.200 ([192.168.2.200]:2222).

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
pi@raspberrypi:~ $ 
```

## mosh 1.3.0の準備

server側はsourceからbuild

```
$ sudo apt-get build-dep mosh
$ wget https://mosh.org/mosh-1.3.0.tar.gz
$ sha256sum mosh-1.3.0.tar.gz
320e12f461e55d71566597976bd9440ba6c5265fa68fbf614c6f1c8401f93376  mosh-1.3.0.tar.gz
```
※hashとgpg署名は以下のメールに，鍵は https://mosh.org/ の一番下にある．

- [[mosh-users] mosh 1.3.0 released (corrected announcement)](http://mailman.mit.edu/pipermail/mosh-users/2017-March/000341.html "[mosh-users] mosh 1.3.0 released (corrected announcement)")

sourceを展開してbuild

```
$ tar tvf mosh-1.3.0.tar.gz
$ tar xf mosh-1.3.0.tar.gz
$ ./configure --prefix=${HOME}/usr/local
$ make
$ install
```

host側はDebian sid amd64のパッケージを利用した

```
$ sudo apt install mosh
$ dpkg-query -W mosh
mosh    1.3.0-1
```

ということでホスト側はこんな状態に

- OpenSSH : 22番ポート
- Dropbear : 2222番ポート
- mosh 1.2.4a-1+b2(pkg) : /usr/bin/mosh-server
- mosh 1.3.0 : ${HOME}/usr/local/bin/mosh-server

この状態でDropbearとmosh 1.3.0を利用するにはこんな感じ

```shell
$ mosh pi@192.168.2.200 --ssh="ssh -p 2222" --server='${HOME}'/usr/local/bin/mosh-server
```

でも実際に繋ごうとするとエラーになってしまう．

接続元のmoshではこんなエラー

```shell
/usr/bin/mosh: Did not find mosh server startup message. (Have you installed mosh on your server?)
```

接続先のDropbearではこんなエラー

```shell
[20390] Mar 28 06:37:20 Child connection from 192.168.2.205:32888
[20390] Mar 28 06:37:21 Pubkey auth attempt with unknown algo for 'pi' from 192.168.2.205:32888
[20390] Mar 28 06:37:21 Pubkey auth succeeded for 'pi' with key md5 0b:fb:21:45:d6:a0:7d:57:02:24:9b:d3:ed:c7:c6:23 from 192.168.2.205:32888
[20391] Mar 28 06:37:21 ioctl(TIOCSCTTY): Input/output error
[20391] Mar 28 06:37:21 /dev/pts/1: No such file or directory
[20391] Mar 28 06:37:21 open /dev/tty failed - could not set controlling tty: No such device or address
[20390] Mar 28 06:37:21 Exit (pi): Disconnect received
```

ということで，mosh 1.3.0で入った`--no-ssh-pty`を付けてみます．

```shell
$ mosh pi@192.168.2.200 --ssh="ssh -p 2222" --server='${HOME}'/usr/local/bin/mosh-server --no-ssh-pty
```

[![asciicast](https://asciinema.org/a/51qfzfi0kcqfyrwd3kdpiqff8.png)](https://asciinema.org/a/51qfzfi0kcqfyrwd3kdpiqff8)

うまく繋がりました．
試しに回線を切断して再接続すると自動的に繋がるのも確認 ☺

`--no-ssh-pty`を~/.ssh/configとかに書いとけるといいんですけどね．今のところ毎回指定するしかなさそう?


<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B00I1M03OC&linkId=5dbce015450eba10b97a18e812b98917"></iframe><iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B06XH1ZDBT&linkId=7ca6bd0c75ef9f49a954188862283dc4"></iframe><iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B06WRXDM52&linkId=ff10c369a7168e2811e722bd1aaf4281"></iframe>