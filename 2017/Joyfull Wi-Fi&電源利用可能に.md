<!--
Joyfull Wi-Fi&電源利用可能に
-->

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/31767105924/in/photostream/" title="IMG_20170126_000231"><img src="https://c1.staticflickr.com/1/366/31767105924_a42bae5ecf_n.jpg" width="320" height="240" alt="IMG_20170126_000231"></a>

<blockquote class="twitter-tweet" data-lang="ja"><p lang="ja" dir="ltr">む．<br>ジョイフルに独自Wi-Fi来てる?<a href="https://t.co/mgVNByQkvm">https://t.co/mgVNByQkvm</a><a href="https://twitter.com/hashtag/kagolug?src=hash">#kagolug</a></p>&mdash; 鹿児島Linux勉強会02/11 (@matoken) <a href="https://twitter.com/matoken/status/822829564334219264">2017年1月21日</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet" data-lang="ja"><p lang="ja" dir="ltr">リリースには見当たらない<br><br>&quot;新着情報｜ファミリーレストラン ジョイフル [Joyfull]&quot; <a href="https://t.co/sWhLduHpAc">https://t.co/sWhLduHpAc</a></p>&mdash; 鹿児島Linux勉強会02/11 (@matoken) <a href="https://twitter.com/matoken/status/822829833621114880">2017年1月21日</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet" data-lang="ja"><p lang="ja" dir="ltr">ジョイフルに無料wifi<br>(｡･･)ﾉぉはょぅ♪ <a href="https://t.co/F9zn3bscqf">pic.twitter.com/F9zn3bscqf</a></p>&mdash; suzukishouten (@suzukishouten) <a href="https://twitter.com/suzukishouten/status/822567582083457024">2017年1月20日</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet" data-lang="ja"><p lang="ja" dir="ltr">11月ぶりにジョイフルのFAQ見たらWifiはありませんから一部ありますになってただけじゃなくて、店内のコンセントを自由に利用してＯＫになっている･･･！！！ <a href="https://t.co/rCbdJxZvR5">pic.twitter.com/rCbdJxZvR5</a></p>&mdash; NT/fiv｜2/12大九鹿児島 (@ntfiv) <a href="https://twitter.com/ntfiv/status/816619418654380032">2017年1月4日</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet" data-lang="ja"><p lang="ja" dir="ltr">11月ぶりにジョイフルのFAQ見たらWifiはありませんから一部ありますになってただけじゃなくて、店内のコンセントを自由に利用してＯＫになっている･･･！！！ <a href="https://t.co/rCbdJxZvR5">pic.twitter.com/rCbdJxZvR5</a></p>&mdash; NT/fiv｜2/12大九鹿児島 (@ntfiv) <a href="https://twitter.com/ntfiv/status/816619418654380032">2017年1月4日</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet" data-lang="ja"><p lang="ja" dir="ltr">そうそうJoyfull肝付店のWi-Fi 5GHzしか無かったので注意(設定ミス?) <a href="https://t.co/q0qaGY5UIn">https://t.co/q0qaGY5UIn</a><a href="https://t.co/HifBTpxWXF">https://t.co/HifBTpxWXF</a></p>&mdash; 鹿児島Linux勉強会02/11 (@matoken) <a href="https://twitter.com/matoken/status/824561317923086336">2017年1月26日</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet" data-lang="ja"><p lang="ja" dir="ltr">今日は2.4GHz(Channel 11)もあった．こないだは2.4GHzの機器が落ちてたのかも?<a href="https://t.co/TjbJjipQs3">https://t.co/TjbJjipQs3</a> <a href="https://t.co/OihlpIsAda">https://t.co/OihlpIsAda</a></p>&mdash; 鹿児島Linux勉強会02/11 (@matoken) <a href="https://twitter.com/matoken/status/826074315950993408">2017年1月30日</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

てことで九州民のオアシスJoyfullの一部店舗でWi-Fiが利用可能になっているようです．更に電源も利用可能とか．

鹿屋近辺の4店舗(高山店，肝付店，鹿屋北店，鹿屋店)を回ってみたところうち1件(肝付店)で利用可能なのを確認しました．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/32457224142/in/dateposted/" title="IMG_20170125_235917"><img src="https://c1.staticflickr.com/1/567/32457224142_70b6ec7819_n.jpg" width="240" height="320" alt="IMG_20170125_235917"></a>

Wi-FiはOPENで接続後認証が必要です．認証は以下のような感じで各種SNSやメールアドレスが利用できました．

<a href="https://www.flickr.com/photos/119142834@N05/32487760661/in/dateposted/" title="Screenshot_2017-01-26-00-05-01"><img src="https://c1.staticflickr.com/1/457/32487760661_8aa0b38d86_n.jpg" width="203" height="320" alt="Screenshot_2017-01-26-00-05-01"></a><a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/31767105504/in/photostream/" title="Screenshot_2017-01-26-00-05-52"><img src="https://c1.staticflickr.com/1/725/31767105504_a08205381d_n.jpg" width="203" height="320" alt="Screenshot_2017-01-26-00-05-52"></a>

USENと株式会社ファイバーゲートのサービスのようです．
> 株式会社USENと株式会社ファイバーゲートが運営するWIFIサービスサポートセンター（以下「当社」といいます）が提供するインターネット接続等に関するサービス

- [joyfull\-wifi 株式会社 ジョイフル 利用方法](https://www.joyfull-wifi.jp/help/help_nexagreement_ja.aspx?unitname=651972_001&mac=0&inf=wlan0)

回線はASAHIネットのようです．

```
$ traceroute-nanog www.google.com|head
traceroute to www.google.com (202.224.62.89), 30 hops max, 60 byte packets
 1  gateway (192.168.150.1)  2.994 ms  2.987 ms  2.968 ms
 2  kgsnik31.asahi-net.or.jp (202.224.38.216)  7.214 ms  9.284 ms  9.515 ms
 3  tkybi3-v7.asahi-net.or.jp (202.224.38.1)  32.352 ms  32.785 ms  32.584 ms
 4  tkycr2-v2.asahi-net.or.jp (202.224.32.182)  33.519 ms  33.885 ms  34.059 ms
 5  tkycr1-v1001.asahi-net.or.jp (202.224.51.1)  33.718 ms  33.835 ms  33.382 ms
 6  cs1cr1-v1002.asahi-net.or.jp (202.224.51.6)  34.042 ms  33.787 ms  33.502 ms
 7  cs1cdn2-v1023.asahi-net.or.jp (202.224.51.90)  33.597 ms  33.592 ms  34.722 ms
 8  cs1cdn1-v1031.asahi-net.or.jp (202.224.51.121)  32.347 ms  34.007 ms  33.860 ms
 9  * * *

```

速度は上下とも20Gbps前後

− [Speedtest by Ookla](http://beta.speedtest.net/result/5996937125)

1/26の時点では5GHz帯のみで珍しいなと思っていましたが，

- [iwlist wls1 scanning(Joyfull\_kimotuki\-ten)](https://gist.github.com/matoken/498c01bfdcbe949d6e248dd95d6d03f6)

1/30には2.4GHz/5GHz両方生きてるのを確認しました．

- [iwlist wls1 scanning(Joyfull_kimotuki-ten 2017-01-30)](https://gist.github.com/matoken/0b4e6c872c057c674e937837b431a419#file-iwlist-wls1-scanning-joyfull_kimotuki-ten-2017-01-30)

26日の時点ではAP落ちてたのかな?

大隅半島でWi-Fiが利用できてゆっくり出来る飲食店は他にマクドナルド西原店，自遊空間鹿屋店位だと思うのでかなり嬉しいです．

他の店舗にも速く導入して欲しい&電源席増強して欲しいとこです．
