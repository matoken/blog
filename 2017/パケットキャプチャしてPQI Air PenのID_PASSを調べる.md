# パケットキャプチャしてPQI Air PenのID/PASSを調べる

送料込み500円だったのでポチってしまいました．X200のジャンク以来のコの手の買い物．

- [「PQI Air Pen」Linux搭載でtelnetできるワイヤレスアクセスポイントが500円](http://hitoriblog.com/?p=48779 "「PQI Air Pen」Linux搭載でtelnetできるワイヤレスアクセスポイントが500円")

ちょっと古いものですがftp/telnetなどが開いてて色々遊べるようです．PQI Air Card(初代)も持っていますが，これはバッテリー内蔵でAP機能などもあります．

## ちょっと叩いてみる

dhcpの提供されているネットワークケーブルを繋いで電源をれてちょっと叩いてみます．

まずはdhcp poolのipから探してみます．

```
$ sudo nmap -sP 192.168.2.200-
Starting Nmap 7.40 ( https://nmap.org ) at 2017-03-08 11:33 JST
  :
Nmap scan report for 192.168.2.214
Host is up (0.0012s latency).
MAC Address: 80:DB:31:01:A4:B8 (Power Quotient International)
  :
```

192.168.2.214でした．ポートスキャンしてみます．

```
$ nmap -A 192.168.2.214

Starting Nmap 7.40 ( https://nmap.org ) at 2017-03-08 11:45 JST
Nmap scan report for 192.168.2.214
Host is up (0.037s latency).
Not shown: 995 closed ports
PORT     STATE SERVICE VERSION
21/tcp   open  ftp     vsftpd 2.0.7
23/tcp   open  telnet  BusyBox telnetd 1.0
53/tcp   open  domain  dnsmasq 2.52
| dns-nsid:
|_  bind.version: dnsmasq-2.52
80/tcp   open  http    Brivo EdgeReader access control http interface
|_http-title: PQI Air Pen
8080/tcp open  http    Mongoose httpd 3.7 (directory listing)
|_http-title: Index of /
Service Info: OS: Unix; Device: security-misc

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 37.14 seconds
$ nmap -P 0-65536 192.168.2.214

Starting Nmap 7.40 ( https://nmap.org ) at 2017-03-08 11:37 JST
Nmap scan report for 192.168.2.214
Host is up (0.035s latency).
Not shown: 995 closed ports
PORT     STATE SERVICE
21/tcp   open  ftp
23/tcp   open  telnet
53/tcp   open  domain
80/tcp   open  http
8080/tcp open  http-proxy

Nmap done: 1 IP address (1 host up) scanned in 0.61 seconds
```

80番ポートにアクセスすると認証無しで設定画面にアクセスできました．8080番はファイルのアクセスが出来ます．こちらも認証なし．

ftp/telnetは流石に未認証では駄目のようです．

```
$ nc 192.168.2.214 21
220 (vsFTPd 2.0.7)
USER anonimouse
331 Please specify the password.
PASS matoken@gmail.com
530 Login incorrect.
$ nc 192.168.2.214 23
������!����          (none) login: 

(none) login: 
```

パッケージの中に入っていたマニュアルには特にID/PASSぽいものの情報はありません．
でもスマートフォン用アプリでファイルのやり取りが可能なようなのでそのパケットを覗けばわかりそうです．
＃ネットワークで検索するとID/PASSは見つかるのですが，せっかくなので?

## パケットキャプチャしてみる

適当なWi-Fiの使えるPCを用意してネットワークカードをmonitor modeにしてパケットキャプチャをします．
今回はこんな感じ．

* PC : LENOVO Thinkpad X200
* NIC : Intel Corporation PRO/Wireless 5100 AGN
* OS : Ubuntu 17.04 amd64
* Driver : iwldvm, iwlwifi

### AQI Air Penの無線チャンネルを確認しておく

* ここでは11

```
$ nmcli d wifi | egrep 'SSID|PQI'
*  SSID                モード    CHAN  レート     信号  バー  セキュリティ 
   PQI Air Pen         インフラ  11    54 Mbit/s  100   ▂▄▆█  --           
```

```
$ sudo /sbin/iwlist wls1 scanning | grep -B 5 "PQI Air Pen"
		  Cell 09 - Address: 80:DB:31:01:A4:B7
					Channel:11
					Frequency:2.462 GHz (Channel 11)
					Quality=70/70  Signal level=-28 dBm  
					Encryption key:off
					ESSID:"PQI Air Pen"
```

### phyとネットワークインターフェイスの確認

```
$ /sbin/iw dev
phy#0
        Interface wls1
                ifindex 8
                wdev 0x3
                addr 00:22:fa:33:45:6a
                type managed
                channel 8 (2447 MHz), width: 20 MHz, center1: 2447 MHz
                txpower 15.00 dBm
```

### デバイスがmonitor modeになれるか確認する

monitorになれない場合はドライバを変更すると対応できる場合もあります．

```
$ /sbin/iw phy phy0 info | lv
  :
		Supported interface modes:
				 * IBSS
				 * managed
				 * monitor
  :
		software interface modes (can always be added):
				 * monitor
```

### monitor modeのインターフェイスを作る

```
$ sudo iw phy phy0 interface add mon0 type monitor
```

### managed modeのインターフェイスを削除する

```
$ sudo iw dev wls1 del
```

### monitor modeのインターフェイス(mon0)をUpする

```
$ sudo ifconfig mon0 up
```

###  monitor modeのインターフェイスの無線チャンネルを設定する

上の方で11チャンネルだったので2462に設定します．

```
$ sudo iw dev mon0 set freq 2462
```

他のチャンネルはこんな感じ

>
ch1 : 2412  
ch2 : 2417  
ch3 : 2422  
ch4 : 2427  
ch5 : 2432  
ch6 : 2437  
ch7 : 2442  
ch8 : 2447  
ch9 : 2452  
ch10 : 2457  
ch11 : 2462  
ch12 : 2467  
ch13 : 2472  
ch14 : 2484  


### 確認

```
$ /sbin/iwconfig mon0
```

## パケットキャプチャをしながらスマートフォン公式アプリを使ってみる

※パケットがたくさん飛んでいるような場合はフィルタを書いたりWiresharkなどを使うと便利です．

パケットをキャプチャしながらスマートフォンでPQi Air Penのネットワークに繋いだ状態で公式アプリを起動して更新などを行います．

```
$ sudo tcpdump -i mon0 -n -A -s0
    :
01:27:10.970158 1.0 Mb/s 2462 MHz 11b -34dBm signal antenna 3 IP 192.168.200.1.21 > 192.168.200.102.50504: Flags [P.], seq 1:21, ack 0, win 2896, options [nop,nop,TS val 194874 ecr 35416851], length 20: FTP: 220 (vsFTPd 2.0.7)
E..Hv.@.@..-.......f...H.[.....;...P.P.....
...:..k.220 (vsFTPd 2.0.7)
...e
    :
01:26:05.791087 2462 MHz 11n -39dBm signal antenna 3 72.2 Mb/s MCS 7 20 MHz s
hort GI mixed IP 192.168.200.102.50396 > 192.168.200.1.21: Flags [P.], seq 1:
12, ack 20, win 115, options [nop,nop,TS val 35410347 ecr 178581], length 11:
 FTP: USER root
E..?O.@.@..z...f.............wu....s    ......
..Q.....USER root
...2
    :
01:26:05.792197 2462 MHz 11n -41dBm signal antenna 3 72.2 Mb/s MCS 7 20 MHz s
hort GI mixed IP 192.168.200.1.21 > 192.168.200.102.50396: Flags [P.], seq 20
:54, ack 12, win 2896, options [nop,nop,TS val 178613 ecr 35410347], length 34: FTP: 331 Please specify the password.
E..V.b@.@.\........f.....wu........P`......
......Q.331 Please specify the password.
u`a.
    :
01:27:11.238673 2462 MHz 11n -40dBm signal antenna 3 72.2 Mb/s MCS 7 20 MHz short GI mixed IP 192.168.200.102.50504 > 192.168.200.1.21: Flags [P.], seq 11:23, ack 55, win 115, options [nop,nop,TS val 35416878 ecr 194908], length 12: FTP: PASS pqiap
E..@.@@.@./....f.....H.....F.[.&...s.......
..k....\PASS pqiap
.5.Z
```

FTP接続で`root:pqiap`のようです．


### インターフェイスを戻す

```
sudo iw dev mon0 del
sudo iw phy phy0 interface add wls1 type managed
```

## ftp接続を試してみる

```
$ nc 192.168.200.1 21
220 (vsFTPd 2.0.7)
user root
331 Please specify the password.
pass pqiap
230 Login successful.
```

## telnetを試してみる

```
$ nc 192.168.200.1 23
������!����(none) login: 

(none) login: root
root
Password: pqiap



BusyBox v1.01 (2013.01.03-08:27+0000) Built-in shell (ash)
Enter 'help' for a list of built-in commands.

~ # uname -a
uname -a
Linux (none) 2.6.31.AirPen_V0.1.22-g5eca71a #319 Thu Jan 3 16:27:02 CST 2013 mips unknown
```

ということで中に入れるようになりました :)

