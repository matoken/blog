<!--
epsilon2/EPG打ち上げ見学
-->

先月のことで結構時間が立ってしまいましたが，epsilon2/EPG打ち上げを見に内之浦に行ってきました．

宮原見学所という場所があるのですが，今回はツアーバスのみの受付で国分，鹿児島市，鹿屋市発着で国分，鹿児島市は安く感じましたが家から一番近い鹿屋発着はちょっと高いように感じる&自由度が少ないのでこちらは諦めて一般で見学することにしました．
以前のepsilog1の時の内之浦港は港の際のあたりに陣取れたので人が多くても三脚を立てたり撮影しやすいだろうとこちらに．
移動はmotercycleで向かいました．



- [ジオスペース探査衛星「あらせ」（ERG）/イプシロンロケット2号機 特設サイト \| ファン\!ファン\!JAXA\!](http://fanfun.jaxa.jp/countdown/epsilon2/)
- [JAXA \| イプシロンロケット2号機によるジオスペース探査衛星（ERG）の打上げ時刻について](http://www.jaxa.jp/press/2016/12/20161218_epsilon2_j.html)
- [JAXA \| イプシロンロケット2号機によるジオスペース探査衛星（ERG）の打上げ結果について](http://www.jaxa.jp/press/2016/12/20161220_epsilon2_j.html)

- [ジオスペース探査衛星「あらせ」（ERG衛星） \| 科学衛星・探査機 \| 宇宙科学研究所](http://www.isas.jaxa.jp/missions/spacecraft/current/erg.html)
- [イプシロン \| ロケット \| JAXA 第一宇宙技術部門 ロケットナビゲーター](http://www.rocket.jaxa.jp/rocket/epsilon/)

- [宇宙作家クラブ　ニュース掲示板 その122 (No.2003-No.2017 2016年11月4日(金)23時37分-2016年12月19日(月)18時55分)](http://www.sacj.org/openbbs/bbs122.html)
- [宇宙作家クラブ　ニュース掲示板 その123](http://www.sacj.org/openbbs/bbs123.html)(予定)

