# mikutterからGooglePhotosにアップロードするやつを試す


<blockquote class="twitter-tweet" data-lang="ja"><p lang="ja" dir="ltr">というわけで、mikutterのプラグインできた。 <a href="https://t.co/6hP23pBziO">https://t.co/6hP23pBziO</a></p>&mdash; スラマイマラス (@slimymars) <a href="https://twitter.com/slimymars/status/835441244935348225">2017年2月25日</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

* [slimymars/mikutter-google-photos-uploader](https://github.com/slimymars/mikutter-google-photos-uploader "slimymars/mikutter-google-photos-uploader")

Google Photosってことは容量気にせず画像投げられる & [Google+](https://plus.google.com/u/0/+KenichiroMATOHARA)との連携もできそう?ってことで試してみました．

＃mikutterについてはこちらを．

* [mikutter](http://mikutter.hachune.net/ "mikutter")

## 関連パッケージ導入

```
$ sudo apt install ruby-oauth2
```

## mikutter-google-photos-uploader plugin導入

```
$ cd ~/.mikutter/plugin
$ git clone https://github.com/slimymars/mikutter-google-photos-uploader
$ cd mikutter-google-photos-uploader
$ bundle install
```

## mikutter起動

……認識されない．mikutterが古い所為のようです．

```
$ grep mikutter: ~/.mikutter/plugin/mikutter-google-photos-uploader/.mikutter.yml 
  mikutter: 3.5.2
$ dpkg-query -W mikutter
mikutter        3.5.0+dfsg-1
```

Debianでは`jessie-backports`から`sid`まで`3.5.0+dfsg-1`で`experimental`だけ`3.5.2+dfsg-1`でした．

- [Debian -- パッケージ検索結果 -- mikutter](https://packages.debian.org/search?keywords=mikutter&searchon=names&suite=all&section=all "Debian -- パッケージ検索結果 -- mikutter")

てことで，`experimental`から借りてきます．

`sources.list`に`experimental`がある状態で

```
$ grep experimental /etc/apt/sources.list
deb http://dennou-q.gfd-dennou.org/debian/ experimental main non-free contrib
deb-src http://dennou-q.gfd-dennou.org/debian/ experimental main non-free contrib
```

`/etc/apt/preferences.d/mikutter`を以下のような感じで用意して，

```
$ cat /etc/apt/preferences.d/mikutter
Package: mikutter
Pin: release a=experimental
Pin-Priority: 800
```

パッケージを更新して導入．

```
$ sudo apt upgrade
```

## mikutterでの設定

mikutterが3.5.2になったので認識しました．設定画面を見ると，`GooglePhotos`というタブが増えているのでそのタブの`Authrise code 取得URL`をブラウザで開いて認証し，出てきたコードを`Authrization_code`に貼り付けます．
更にmikutterからアップロードする保存先のアルバムを追加しておきます．GooglePhotosに存在しないアルバムは前もって

* [フォト - Google フォト](https://photos.google.com/ "フォト - Google フォト")

から登録しておきましょう．アルバムは複数登録できるので例えば

* ミク
* フレンズ

のようにアルバムを登録しておくと画像整理がはかどります?

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/32332433754/in/dateposted/" title="20170228_19:02:23-16584"><img src="https://c1.staticflickr.com/4/3804/32332433754_9f15cb8214.jpg" width="500" height="223" alt="20170228_19:02:23-16584"></a>

実際の画像保存方法は，mikutterの保存したい画像の上で右クリックして`Google Photosに画像をアップロード`を選び，

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/32795452290/in/dateposted/" title="shutter_17-03-01_08:33:12_001"><img src="https://c1.staticflickr.com/4/3849/32795452290_7fb996c6ea.jpg" width="341" height="427" alt="shutter_17-03-01_08:33:12_001"></a>

その後表示されるアルバムを選択するだけです．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/32332433554/in/photostream/" title="menu_018"><img src="https://c1.staticflickr.com/4/3889/32332433554_d097be7018_m.jpg" width="154" height="44" alt="menu_018"></a>

Google Photosに見に行くとアップロードされているのが確認できます．

- [フォト - Google フォト](https://photos.google.com/ "フォト - Google フォト")

Google+へのクロスポストにも使えるかなと思ったのですが，Google+の投稿画面はタイムラグがあるようで少し待たないと表示されませんでした．これはGoogle側の問題ですね．Google Photosから投稿するようにすれば良さそうです．

## 関連?Tweet

<blockquote class="twitter-tweet" data-lang="ja"><p lang="ja" dir="ltr">てことで，mikutter-google-photos-uploader動きました．Debianではruby-oauth2とexperimentalのmikutterが必要な感じです． <a href="https://t.co/qt5vhPnhjN">pic.twitter.com/qt5vhPnhjN</a></p>&mdash; 鹿児島Linux勉強会03/11 (@matoken) <a href="https://twitter.com/matoken/status/836526825769574401">2017年2月28日</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
<blockquote class="twitter-tweet" data-lang="ja"><p lang="ja" dir="ltr">これマジ！？</p>&mdash; キシワダイト (11カラット) (@toshi_a) <a href="https://twitter.com/toshi_a/status/836551364004077569">2017年2月28日</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
<blockquote class="twitter-tweet" data-lang="ja"><p lang="ja" dir="ltr">おもろいプラグイン出てきたんとちゃうか</p>&mdash; キシワダイト (11カラット) (@toshi_a) <a href="https://twitter.com/toshi_a/status/836551573371142145">2017年2月28日</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
<blockquote class="twitter-tweet" data-lang="ja"><p lang="ja" dir="ltr">みくったー・グーグル・フォトズ・うpろーだ、575では</p>&mdash; はいこん (@OBSOLETE_STD) <a href="https://twitter.com/OBSOLETE_STD/status/836551749993283588">2017年2月28日</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
<blockquote class="twitter-tweet" data-lang="ja"><p lang="ja" dir="ltr">ておくれだ……！</p>&mdash; スラマイマラス (@slimymars) <a href="https://twitter.com/slimymars/status/836565072541446144">2017年2月28日</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
<blockquote class="twitter-tweet" data-lang="ja"><p lang="ja" dir="ltr">わかりやすい <a href="https://t.co/Q6iEzgvx3e">pic.twitter.com/Q6iEzgvx3e</a></p>&mdash; 鹿児島Linux勉強会03/11 (@matoken) <a href="https://twitter.com/matoken/status/836564516250894337">2017年2月28日</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

## 環境

```
$ screenfetch
         _,met$$$$$gg.           mk@x220
      ,g$$$$$$$$$$$$$$$P.        OS: Debian 9.0 stretch
    ,g$$P""       """Y$$.".      Kernel: x86_64 Linux 4.9.0-2-amd64
   ,$$P'              `$$$.      Uptime: 4d 18h 51m
  ',$$P       ,ggs.     `$$b:    Packages: 5147
  `d$$'     ,$P"'   .    $$$     Shell: bash 4.4.11
   $$P      d$'     ,    $$P     Resolution: 1366x768
   $$:      $$.   -    ,d$$'     WM: Awesome
   $$\;      Y$b._   _,d$P'      WM Theme: default
   Y$$.    `.`"Y$$$$P"'          CPU: Intel Core i5-2540M CPU @ 3.3GHz
   `$$b      "-.__               GPU: Mesa DRI Intel(R) Sandybridge Mobile 
    `Y$$                         RAM: 13040MiB / 15934MiB
     `Y$$.                      
       `$$b.                    
         `Y$$b.                 
            `"Y$b._             
                `""""           
                                
```

<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B06WLGMNGH&linkId=5c789b68339c0e7e29d2cb816025a865"></iframe><iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B00TQMFOCW&linkId=ac84dc216e1a5f599c2e358bd64042df"></iframe>
