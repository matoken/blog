# Podcastが無くなって困る

現在Podcastを`podget`で取得して，

```
2 * * * *       podget -s
```

その中の新しいものだけをownCloudにコピーしてAndroidやPCで視聴しています．

```
10 * * * *      cd ~mk/POD ; find . -mmin -2880 -type f | egrep -v 'm3u$|LOG\/' | rsync --delete --progress --files-from=- ~mk/POD/ /var/www/owncloud/data/matoken/files/podcast/; find /var/www/owncloud/data/matoken/files/podcast/ -ctime +7 -type f -print0 | xargs -0 rm ; find /var/www/owncloud/data/matoken/files/podcast/news/ -mmin +1`
```

今日聞こうとしたらownCloud内のPodcastが0個になっていました．心当たりはUbuntu 14.04 LTSからUbuntu 16.04 LTSへのdist-upgrade．ownCloudは上げる前と後に動作確認してたんで他の部分が怪しいとログを見ると`podget`がエラーを吐いています．こんな感じ．

```shell
$ podget -s
/home/mk/.podget/podgetrc cannot be verified to be compatible with this version of podget.

It is missing the version line that is included in configuration files created by newer versions of podget.

Please create a new configuration file by running 'podget --create-config <FILENAME>',
and then converting your old configuration to the new format.  Then move the new file
in place of the old and podget will work as it used to.
```

設定ファイルが使えなくなってるみたいですね．枯れてるのかと思ってたら結構変化があったようです．
ちなみに更新前のバージョンが`podget 0.6.9-1`で現在は`podget 0.7.9-1`でした．

てことで，設定ファイルの雛形を別の場所に書き出して，

```
$ podget --create-config /tmp/podgetrc
podget

Configuration filename specified by -c or --create-config violates the following rules...

Suggestion(s):
  1. Filenames should not include any directory configuration.
     Remove the directory configuration.
     If you need to specify a directory other than the default,
     use the -d or --dir_config command line options.

Closing session.
```

……．`--dir_config`付けて再度．

```
$ podget --dir_config /tmp --create-config podgetrc
podget
  Installing default user configuration file in /tmp/podgetrc
  Installing default server list configuration.

Closing session.
```

https://gist.github.com/1b5fba2e949409a87e1955e0c779b057

diffを取ってみると設定項目自体はそう変わってない感じですが，設定値が大文字小文字が変わったりしています．
比較しながら書き換えて叩くと動き始めたようです．

```
$ podget
podget


Session directory not found, creating
Session file not found.  Creating podget.18364 .

-------------------------------------------------
Category: tech                 Name: 電脳空間カウボーイズ

Downloading feed index from http://feeds.feedburner.com/weblogs/csc
2017-04-06 06:13:13 URL:http://feeds.feedburner.com/weblogs/csc [46549] -> "-" [1]

Downloading csc_2017_design2.mp3 from http://onosendai.jp/csc
2017-04-06 06:20:43 URL:http://onosendai.jp/csc/csc_2017_design2.mp3 [51781411/51781411] -> "/home/mk/POD/tech/電脳空間カウボーイズ/csc_2017_design2.mp3" [1]
PLAYLIST: Adding tech/電脳空間カウボーイズ/csc_2017_design2.mp3 to /home/mk/POD/New-2017-04-06.m3u

Downloading csc_2017_design1.mp3 from http://onosendai.jp/csc
2017-04-06 06:25:04 URL:http://onosendai.jp/csc/csc_2017_design1.mp3 [37304885/37304885] -> "/home/mk/POD/tech/電脳空間カウボーイズ/csc_2017_design1.mp3" [1]
PLAYLIST: Adding tech/電脳空間カウボーイズ/csc_2017_design1.mp3 to /home/mk/POD/New-2017-04-06.m3u
 :
```

しかし，ダウンロード済みのファイルも取得しているような?
新規のみの設定にしているつもりなんですが，初回は舐めちゃうのかな?2度目のときに確認してみます．

```
# Force
# 0 == Only download new material.
# 1 == Force download all items even those you've downloaded before.
FORCE=0
```

とりあえず終わるまではsoundcloud辺りで聞いてます．
