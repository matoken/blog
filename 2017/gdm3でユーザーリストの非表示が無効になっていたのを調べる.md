# gdm3でユーザーリストの非表示が無効になっていたのを調べる

Debian sid amd64のgdm3の画面でユーザーリストを表示しないようにしていたのですが，表示されるようになっていました．

これまでは，
`/etc/gdm3/greeter.dconf`ファイルの以下の設定を有効にしていました．

```
disable-user-list=true
```

`$ apt-get changelog gdm3`( /usr/share/doc/gdm3/changelog.Debian.gz)を見るとこんな記述が，

```
  * greeter.dconf-defaults: this is a new file to remove the old 
    greeter.gsettings
  * Convert greeter.gsettings to greeter.dconf-defaults at postinst time 
    and use ucf to update the file.
  * Make a direct symlink to the new greeter.dconf-defaults file in the 
    dconf defaults directory.
```

gdm3 (3.12.1-1)から`greeter.dconf-defaults`に変わったようです．

```
$ ls /etc/gdm3/
Init  PostLogin  PostSession  PreSession  Xsession  daemon.conf  daemon.conf.dpkg-dist  greeter.dconf  greeter.dconf-defaults  greeter.dconf-defaults.ucf-old
```

内容はほとんど同じなようです．

```
$ diff  /etc/gdm3/greeter.dconf /etc/gdm3/greeter.dconf-defaults
26,27d25
< #logo='/usr/share/icons/gnome/48x48/places/debian-swirl.png'
< #fallback-logo='/usr/share/icons/gnome/48x48/places/debian-swirl.png'
31d28
< 
33c30
< disable-user-list=true
---
> # disable-user-list=true
37,38c34,35
< #banner-message-enable=true
< #banner-message-text='Welcome!!!!!!!!!!!!!!!!!!!!!!'
---
> # banner-message-enable=true
> # banner-message-text='Welcome'
```

ということで`/etc/gdm3/greeter.dconf-defaults`の`disable-user-list=true`を有効にしたらOKでした．

```
diff --git a/gdm3/greeter.dconf-defaults b/gdm3/greeter.dconf-defaults
index c5b0786..b0ee10c 100644
--- a/gdm3/greeter.dconf-defaults
+++ b/gdm3/greeter.dconf-defaults
@@ -27,7 +27,7 @@ logo='/usr/share/icons/hicolor/48x48/emblems/emblem-debian-white.png'
 fallback-logo='/usr/share/icons/hicolor/48x48/emblems/emblem-debian-white.png'
 
 # - Disable user list
-# disable-user-list=true
+disable-user-list=true
 # - Disable restart buttons
 # disable-restart-buttons=true
 # - Show a login welcome message
```

＃ちなみにこのとき`$ sudo service gdm3 restart`としたらなんかおかしくなってしまいました……．OSごと再起動したら治りました．

<blockquote class="twitter-tweet" data-lang="ja"><p lang="ja" dir="ltr">gdm3でdisable-user-list=trueが効かない<br>greeter.dconfからgreeter.dconf-defaultsに変わってる?defaults側でdisable-user-list=trueにしてgdm3上げ直したら酷いことに<br>遠隔から修正したorz <a href="https://t.co/TIPmlC6cWY">pic.twitter.com/TIPmlC6cWY</a></p>&mdash; 鹿児島Linux勉強会04/08 (@matoken) <a href="https://twitter.com/matoken/status/849288439279243264">2017年4月4日</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

次から気づきやすいように

```
$ sudo dpkg-reconfigure apt-listchanges
```

で「APTで表示すべき変更内容の種類を選択してください。」を「両方 - ニュースと changelog の両方」にしておきました．testing/unstable辺りはちゃんと読まないとですね……．

※apt-listchangesが入っていない場合は`$ sudo apt install apt-listchanges`

