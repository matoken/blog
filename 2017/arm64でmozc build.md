# Ubuntu 16.04 aarch64環境でmozc pkgが無いのでsource pkgからbuild

最近Ubuntu 16.04 aarch64のi3 wmにモニタとキーボードを繋いでデスクトップ利用を試しています．しかし，日本語入力のためのmozcのバイナリパッケージが見当たりません．確認すると，xenialからartfulではi386/amd64/armhfのみの対応．

- [Ubuntu ? Package Search Results \-\- mozc](https://packages.ubuntu.com/search?suite=xenial&arch=arm64&searchon=names&keywords=mozc)

Debianではarmelも対応していますが，arm64はやっぱりありません．

- [Debian \-\- Details of package mozc\-server in jessie](https://packages.debian.org/jessie/mozc-server)

armhf/armelで動いているならarm64もあまり悩まずソースパッケージからbuildできるのではと試してみました．

> ＃dpkg --add-archtecture armhfしてMultiarchにしてarmhfのMozcを使うという手でも行けそうな気はする．(未確認)


## 環境構築

必要なパッケージの導入と，ソースパッケージの入手を行います．

```shell
$ sudo apt update
$ sudo apt upgrade
$ sudo apt install build-essential devscripts
$ sudo apt build-dep fcitx-mozc
$ apt source fcitx-mozc
```

* apt build-depは指定したパッケージをbuildするのに必要なパッケージを導入できます．
* apt sourceは指定したパッケージのソースパッケージを導入して展開してくれます．

※もしソースパッケージが見つからない場合はsource.listにdeb-srcの行があるか確認してみてください．無ければapt edit-sourcesコマンドで/etc/apt/source.listファイルを編集してdeb-srcを有効にしてください．以下のページが参考になると思います．

* [https://help.ubuntu.com/community/Repositories/CommandLine](https://help.ubuntu.com/community/Repositories/CommandLine)

## Archtectureにarm64を追加してバイナリパッケージ作成

次にソースディレクトリに降りて，debian/controlファイルを開き，必要なパッケージのArchtectureにarm64を登録します．mozc-dataはallなのでそのままにしておきます．（はじめarm64をaarch64にしてうまく行かなかったですorz）

```shell
$ cd mozc-2.17.2116.102+gitfd0f5b34+dfsg
$ cp debian/control debian/control.org
$ vi debian/control
$ diff -u debian/control.org debian/control
--- debian/control.org  2017-05-30 17:06:44.000000000 +0000  
+++ debian/control      2017-05-30 17:07:19.000000000 +0000  
@@ -28,7 +28,7 @@                                            
  This open-source project originates from Google Japanese Input.
                              mozc-data                               
 Package: ibus-mozc                                          
-Architecture: i386 amd64 armel armhf                        
+Architecture: i386 amd64 armel armhf arm64                  
 Depends: ${misc:Depends}, ${shlibs:Depends}, mozc-data,     
        ibus (>= 1.2), mozc-server (= ${binary:Version}), tegaki-zinnia-japanese
 Recommends: mozc-utils-gui (= ${binary:Version})            dpkg-buildpackage
@@ -45,7 +45,7 @@                                            
  This open-source project originates from Google Japanese Input.
                                mozc-data                             
 Package: fcitx-mozc                                         
-Architecture: i386 amd64 armel armhf                        
+Architecture: i386 amd64 armel armhf arm64                  
 Depends: ${misc:Depends}, ${shlibs:Depends}, fcitx-bin, fcitx-data, fcitx-modules,
        mozc-server (= ${binary:Version}), mozc-data, tegaki-zinnia-japanese
 Recommends: fcitx, mozc-utils-gui (= ${binary:Version})     
@@ -64,7 +64,7 @@                                            
  fcitx-mozc provides client part of the Mozc input method.  
                                                             
 Package: emacs-mozc                                         
-Architecture: i386 amd64 armel armhf                        
+Architecture: i386 amd64 armel armhf arm64                  
 Depends: ${misc:Depends}, emacs | emacs23 | emacs24, emacs-mozc-bin (= ${binary:Version})
 Conflicts: emacsen-common (<< 2.0.0)                        
 Description: Mozc for Emacs                                 
@@ -76,7 +76,7 @@                                            
  This open-source project originates from Google Japanese Input.
                                                             
 Package: emacs-mozc-bin                                     
-Architecture: i386 amd64 armel armhf                        
+Architecture: i386 amd64 armel armhf arm64                  
 Depends: ${misc:Depends}, ${shlibs:Depends}, mozc-server (= ${binary:Version})
 Description: Helper module for emacs-mozc                   
  Mozc is a Japanese Input Method Editor (IME) designed for multi-platform
@@ -87,7 +87,7 @@                                            
  This open-source project originates from Google Japanese Input.
                                                             
 Package: mozc-server                                        
-Architecture: i386 amd64 armel armhf                        
+Architecture: i386 amd64 armel armhf arm64                  
 Depends: ${misc:Depends}, ${shlibs:Depends}                 
 Multi-Arch: foreign                                         
 Description: Server of the Mozc input method                
@@ -99,7 +99,7 @@                                            
  This open-source project originates from Google Japanese Input.
                                                             
 Package: mozc-utils-gui                                     
-Architecture: i386 amd64 armel armhf                        
+Architecture: i386 amd64 armel armhf arm64                  
 Depends: ${misc:Depends}, ${shlibs:Depends}, mozc-data      
 Recommends: mozc-server (= ${binary:Version})               
 Suggests: ibus-qt
```

次にdpkg-buildpackageコマンドでパッケージを作成します．手元のマシンではちょうど1時間ほどで終了しました．（あとで-j4とかすると30分ほどに）

```shell
$ dpkg-buildpackage -us -uc
$ ls ../*.debmozc-data
../emacs-mozc-bin_2.17.211archtecture6.102+gitfd0f5b34+dfsg-1ubuntu1.1_arm64.deb  ../ibus-mozc_2.17.2116.102+gitfd0f5b34+dfsg-1ubuntu1.1_arm64.deb    ../mozc-utils-gui_2.17.2116.102+gitfd0f5b34+dfsg-1ubuntu1.1_arm64.deb
../emacs-mozc_2.17.2116.102+gitfd0f5b34+dfsg-1ubuntu1.1_arm64.deb      ../mozc-data_2.17.2116.102+gitfd0f5b34+dfsg-1ubuntu1.1_all.deb
../fcitx-mozc_2.17.2116.102+gitfd0f5b34+dfsg-1ubuntu1.1_arm64.deb      ../mozc-server_2.17.2116.102+gitfd0f5b34+dfsg-1ubuntu1.1_arm64.deb
```

## 出来上がったパッケージの導入

今回はfcitxを使っていたので，fcitx-mozcを導入しました．導入した後にログインしなおして，fcitx-config-gtkを起動するとMozcが登録されて利用できるようになりました． :)

```
$ sudo apt-get install ../mozc-server_2.17.2116.102+gitfd0f5b34+dfsg-1ubuntu1.1_arm64.deb ../mozc-data_2.17.2116.102+gitfd0f5b34+dfsg-1ubuntu1.1_all.deb ../fcitx-mozc_2.17.2116.102+gitfd0f5b34+dfsg-1ubuntu1.1_arm64.deb ../mozc-utils-gui_2.17.2116.102+gitfd0f5b34+dfsg-1ubuntu1.1_arm64.deb 
```

重かったり不具合が出るようならskkあたりにしようかと思っていましたが，現在このパッケージを1週間ほど利用して特に問題なく使えています．

あまり推奨しませんが，私の作ったパッケージをしばらく以下においておきます．必要な方は試してみてください．（上の手順に + dhc -i + dpkg-buildpackage -j4したもの）

* [Yandex.Disk](https://yadi.sk/d/ul56g_qw3JjXp9 "Yandex.Disk")

ところでこれはどうすればいいのかな．Debianでも動作確認してバグレポでok?そしてUbuntuはSync mozc from Debian unstableを待つ感じ?

- [Bugs : mozc package : Ubuntu](https://launchpad.net/ubuntu/+source/mozc/+bugs?field.searchtext=&orderby=-importance&search=Search&field.status%3Alist=CONFIRMED&field.status%3Alist=TRIAGED&field.status%3Alist=INPROGRESS&field.status%3Alist=FIXCOMMITTED&field.status%3Alist=FIXRELEASED&field.status%3Alist=INCOMPLETE_WITH_RESPONSE&field.status%3Alist=INCOMPLETE_WITHOUT_RESPONSE&assignee_option=any&field.assignee=&field.bug_reporter=&field.bug_commenter=&field.subscriber=&field.tag=&field.tags_combinator=ANY&field.status_upstream-empty-marker=1&field.upstream_target=&field.has_cve.used=&field.omit_dupes.used=&field.omit_dupes=on&field.affects_me.used=&field.has_patch.used=&field.has_branches.used=&field.has_branches=on&field.has_no_branches.used=&field.has_no_branches=on&field.has_blueprints.used=&field.has_blueprints=on&field.has_no_blueprints.used=&field.has_no_blueprints=on "Bugs : mozc package : Ubuntu")
- [Bugs in package mozc-server (version 2.19.2623.102+dfsg-1) in unstable -- Debian Archived Bug report logs](https://bugs.debian.org/cgi-bin/pkgreport.cgi?archive=both;dist=unstable;package=mozc-server)

