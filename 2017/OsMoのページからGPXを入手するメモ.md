# OsMoのページからGPXを入手するメモ

土日に撮った写真にジオタグをつけようとそのときロギングしていたGPSのログを吸い出そうとしたら見当たりません．GPSロガーの設定がいつの間にかログを保存しない設定になっていました．ボタン部分のゴム部分に穴が開いてしまって防水ではなくなったので土曜の雨の時カバンに入れていたのでそのときにキーロックし忘れて設定が変わったのかもしれません．

<blockquote class="twitter-tweet" data-lang="ja"><p lang="ja" dir="ltr">保存しない設定になってたorz <a href="https://t.co/TOFcU4Df4j">pic.twitter.com/TOFcU4Df4j</a></p>&mdash; 鹿児島Linux勉強会04/08 (@matoken) <a href="https://twitter.com/matoken/status/851455496229855234">2017年4月10日</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

AndroidスマホでOsMoDroidも動作させていたのでこれがログを残していないかと見てみると古いものしかありません．

```
$ jmtpfs android/
$ ls -ltrA android/内部ストレージ/OsMoDroid | tail -1
-rw-r--r--  1 mk mk  803382  2月 25 21:35 20170225070457.gpx
$ find android -iname "*osmo*"
android/内部ストレージ/osmand/tracks/osmo
android/内部ストレージ/OsMoDroid
android/SDカード/Android/obb/net.osmand.plus/tracks/osmo
$ ls -ltrA android/内部ストレージ/OsMoDroid/ | tail -1
-rw-r--r-- 1 mk mk  803382  2月 25 21:35 20170225070457.gpx
```

自動的に生成されるTripページにルートが表示されるのでこれが取得できないかソースを見てみました．

- [Archived trip #659396 - [OsMo] OpenStreetMap Monitoring](https://osmo.mobi/h/d8VhsthhuF9rpEDUCNXjOhHxJHdmiNQNRsoR4AEmpF "Archived trip #659396 - [OsMo] OpenStreetMap Monitoring")


それっぽいURLをこんな感じで抜き出して，

```
$ w3m -dump_source 'https://osmo.mobi/h/d8VhsthhuF9rpEDUCNXjOhHxJHdmiNQNRsoR4AEmpF' | sed -n "s/^.*getJSON('\([^']*\)'.*$/\1/p"
https://api.osmo.mobi/session_get?url=d8VhsthhuF9rpEDUCNXjOhHxJHdmiNQNRsoR4AEmpF&mode=
```

そのurlの中を見るとJSONでGPXの項目があるのでjqで抜き出す

```
$ w3m -dump_source 'https://osmo.mobi/h/d8VhsthhuF9rpEDUCNXjOhHxJHdmiNQNRsoR4AEmpF' | sed -n "s/^.*getJSON('\([^']*\)'.*$/\1/p" | jq -r .gpx
https://st.osmo.mobi/htg/d/8/V/hsthhuF9rpEDUCNXjOhHxJHdmiNQNRsoR4AEmpF.gpx
```

これをDLするとGPXだった．

```
$ wget https://st.osmo.mobi/htg/d/8/V/hsthhuF9rpEDUCNXjOhHxJHdmiNQNRsoR4AEmpF.gpx
```

繋げるとこんな感じ

```
$ w3m -dump_source 'https://osmo.mobi/h/d8VhsthhuF9rpEDUCNXjOhHxJHdmiNQNRsoR4AEmpF' | sed -n "s/^.*getJSON('\([^']*\)'.*$/\1/p" | xargs w3m -dump_source | jq -r .gpx | wget -i -
```

GPX Viewerで開こうとするとファイルオープン画面から帰ってこなかったですが，GpsPruneだと開けました．ちょっと荒いけど無いよりは全然いいですね．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/33120435634/in/dateposted/" title="20170411_01:04:30-17480"><img src="https://c1.staticflickr.com/3/2858/33120435634_4bb24dd1c8.jpg" width="500" height="266" alt="20170411_01:04:30-17480"></a>