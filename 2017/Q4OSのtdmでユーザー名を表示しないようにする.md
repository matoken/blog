# Q4OSのtdmでユーザー名を表示しないようにする

`/etc/trinity/tdm/tdmrc`を編集する．設定内容は`/etc/trinity/tdm/tdmrc.doc`に載っている．

```
$ diff -u ./tdmrc /etc/trinity/tdm/tdmrc
--- ./tdmrc     2017-05-26 06:33:49.000000000 +0900
+++ /etc/trinity/tdm/tdmrc      2017-05-26 06:47:12.000000000 +0900
@@ -30,6 +30,7 @@
 StdFont=Liberation Sans,-1,14,5,0,0,0,0,0,0
 Theme=@@@ToBeReplacedByDesktopBase@@@
 WindowManager=""
+UserList=false
 
 [X-:*-Core]
 AllowNullPasswd=true
@@ -41,7 +42,7 @@
 AllowClose=true
 FocusPasswd=true
 LoginMode=DefaultLocal
-PreselectUser=Previous
+PreselectUser=""
 
 [X-:0-Core]
 AutoLoginEnable=false
```

* `UserList=false`
ユーザーリストを表示しないように
* `PreselectUser=""`
ユーザー名を非表示に



## 環境

```
$ lsb_release -a
No LSB modules are available.
Distributor ID: Debian
Description:    Debian GNU/Linux 8.8 (jessie)
Release:        8.8
Codename:       jessie
$ dpkg-query -W tdm-trinity
tdm-trinity     4:14.0.4-0debian8.0.0+0.1
```