<!--
# 鹿児島Linux勉強会 2017.01に参加
-->

- [鹿児島Linux勉強会 2017\.01 \- connpass](https://kagolug.connpass.com/event/47774/)
    - 開催日時 : 2017/01/04(水)
13:00 〜 17:00
    - 会場 : サンエールかごしま 中研修室1
    - 参加者 : 現地3人 + 遠隔1人 = 計4人

- [TitanPad: 鹿児島Linux勉強会 2017\.01](https://kagolug.titanpad.com/9) (勉強会ノート)

今回は遠隔参加を試してみました．[BigBluebutton](http://bigbluebutton.org/)などの会議システムは映像も双方向に送れていい感じなのですが，野良勉強会では回線が無かったり今使っているPCではリアルタイムエンコードが難しいという問題があります．そこで音声のみの[Mumble](http://wiki.mumble.info/wiki/Main_Page)を利用しました．

サーバはさくらのVPSに設定，会場ではBluetoothマイク兼スピーカーで……と思ったのですが，このマイクではうまく音を拾わない&ヘッドホンのプロファイルでは音質も悪いのでA2DPにしてスピーカーのみの利用として，マイクはPC直付にしました．
このマイクは一応前指向性ですが数百円の安物なので音質は悪かったと思います．
でも双方向でのやり取りが出来るのは良かったです．会議室利用の際は良さそうです．

今回は資料が事前に共有できていたので良かったですが，資料が事前に共有できない場合はやはりどうにか映像を共有する方法を考えないといけないでしょうね．
そうするとやはりBBBやYoutubeLive等が必要に……．(帯域をなるべく絞って映像のみRaspberry PiでYoutubeLive配信というのが現実的かな?)

私の発表した内容は以下の３本でした．

<script async class="speakerdeck-embed" data-id="00cbd7582d0949719c4dfd104c85c42c" data-ratio="1.33333333333333" src="//speakerdeck.com/assets/embed.js"></script>
[Raspberry PiのUSB OTGを試す // Speaker Deck](https://speakerdeck.com/matoken/raspberry-pifalseusb-otgwoshi-su)


<script async class="speakerdeck-embed" data-id="936ab091f0e54a138c2df62ec9764e0e" data-ratio="1.33333333333333" src="//speakerdeck.com/assets/embed.js"></script>
[USBを保護するUSBGuard // Speaker Deck](https://speakerdeck.com/matoken/usbwobao-hu-suruusbguard)

<script async class="speakerdeck-embed" data-id="86a6b4e61870418d9fc19f7fb58efd61" data-ratio="1.33333333333333" src="//speakerdeck.com/assets/embed.js"></script>
[ゲーミングプラットホームのLutris紹介 // Speaker Deck](https://speakerdeck.com/matoken/gemingupuratutohomufalselutrisshao-jie)

データはこちら．
- [matoken / 鹿児島Linux勉強会2017\.01 / ソース / — Bitbucket](https://bitbucket.org/matoken/linux-2017.01/src)


Raspberry PiのUSB OTGはさわりだけでちょっとあれでした．次回までにもうちょっと……．

次回は2月上旬の土日にと思っています．今年は月一ペース開催を目標にと思っています．
最近は開催場所どうしよう……となって日が過ぎていくパターンなのでとりあえず日程アナウンスはして会場見つからなかったらマクドナルドとかで開催という感じで考えています．
