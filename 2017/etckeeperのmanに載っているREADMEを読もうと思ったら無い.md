Debian sid amd64の`etckeeper`のmanに載っているREADMEを読もうと思ったらそんなファイルは無い．

```
$ man etckeeper | grep -B1 README
SEE ALSO
       /usr/share/doc/etckeeper/README.md.gz
$ lv /usr/share/doc/etckeeper/README.md.gz
/usr/share/doc/etckeeper/README.md.gz: No such file or directory
$ ls -lA /usr/share/doc/etckeeper/
合計 40
-rw-r--r-- 1 root root  4679  7月 18  2016 README.mdwn.gz
-rw-r--r-- 1 root root 11645  8月  2  2016 changelog.Debian.gz
-rw-r--r-- 1 root root  1785  7月 18  2016 copyright
-rw-r--r-- 1 root root   948  7月 18  2016 index.mdwn
-rw-r--r-- 1 root root   483  7月 18  2016 install.mdwn
-rw-r--r-- 1 root root    55  7月 18  2016 news.mdwn
-rw-r--r-- 1 root root   309  7月 18  2016 todo.mdwn
$ dpkg -L etckeeper | grep README
/etc/etckeeper/commit.d/README
/etc/etckeeper/init.d/README
/etc/etckeeper/post-install.d/README
/etc/etckeeper/pre-commit.d/README
/etc/etckeeper/pre-install.d/README
/etc/etckeeper/unclean.d/README
/etc/etckeeper/uninit.d/README
/etc/etckeeper/update-ignore.d/README
/usr/share/doc/etckeeper/README.mdwn.gz
$ dpkg-query -W etckeeper
etckeeper       1.18.5-1
```

`/usr/share/doc/etckeeper/README.mdwn.gz`が内容からしてそれぽい．
バグぽいので報告しようかと思って既存のバグを眺めると既に報告されて上流で修正済みのようでした．

* [#791566 - etckeeper: Broken symlink /usr/share/doc/etckeeper/README.md - Debian Bug report logs](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=791566 "#791566 - etckeeper: Broken symlink /usr/share/doc/etckeeper/README.md - Debian Bug report logs")


