# ディスクイメージのデバイスマップが簡単に作れるkpartxを試す

kpartxというディスクのデバイスマップを作るコマンドがあるのを知りました

- [Vine Linux Magazine - ループバックマウントとchrootで作るなんちゃって仮想マシン](https://vinelinux.org/vlmagazine/20110303.html "Vine Linux Magazine - ループバックマウントとchrootで作るなんちゃって仮想マシン")

```
そこでまず、kpartxを使って各パーティションのデバイスマップを作ります。
$ sudo /sbin/kpartx -av /opt/atde3-20100309.img 
add map loop2p1 : 0 497952 linear /dev/loop2 63
add map loop2p2 : 0 33045705 linear /dev/loop2 498015
$ ls /dev/mapper/
control  loop2p1  loop2p2
これでディスクイメージの各物理パーティションに対応したデバイスマップができました。fdiskで見えていたパーティションはそれぞれ、/dev/mapper/loop2p1 /dev/mapper/loop2p2 として参照できるようになっています。
```

これまでは以下のようにfdiskコマンドでパーティション情報を確認してmount時にoffsetを指定していました．

- [ユーザーモードエミュレーションqemu を使って Kono on Debian のイメージを Host PC で apt-get とかする | matoken's meme](http://matoken.org/blog/blog/2014/01/22/usermode-qemu-kono-on-debian/ "ユーザーモードエミュレーションqemu を使って Kono on Debian のイメージを Host PC で apt-get とかする | matoken&apos;s meme")

kpartxを使うとこの作業が簡単になりそうなので試してみました．

Debian sid amd64ではそのまま`kpartx`パッケージだったのでこれを導入します．(Ubuntu 17.04 amd64でも同様でした．)

```
$ sudo apt install kpartx
```

丁度Raspbian jessie 2017-04-10が出たのでこれで試してみます．

```
$ unzip -l 2017-04-10-raspbian-jessie-lite.zip 
Archive:  2017-04-10-raspbian-jessie-lite.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
1297862656  2017-04-10 18:58   2017-04-10-raspbian-jessie-lite.img
---------                     -------
1297862656                     1 file
$ time unzip 2017-04-10-raspbian-jessie-lite.zip
Archive:  2017-04-10-raspbian-jessie-lite.zip
  inflating: 2017-04-10-raspbian-jessie-lite.img  

real    2m58.438s
user    0m27.512s
sys     0m2.132s
 sudo /sbin/kpartx -av 2017-04-10-raspbian-jessie-lite.img
add map loop0p1 (254:3): 0 83968 linear 7:0 8192
add map loop0p2 (254:4): 0 2442728 linear 7:0 92160
$ ls -lA /dev/mapper/
合計 0
crw------- 1 root root 10, 236  4月 11 23:37 control
lrwxrwxrwx 1 root root       7  4月 12 06:07 loop0p1 -> ../dm-3
lrwxrwxrwx 1 root root       7  4月 12 06:07 loop0p2 -> ../dm-4
lrwxrwxrwx 1 root root       7  4月 11 23:37 sda3_crypt -> ../dm-0
lrwxrwxrwx 1 root root       7  4月 11 23:37 x220--vg-root -> ../dm-1
lrwxrwxrwx 1 root root       7  4月 11 23:37 x220--vg-swap_1 -> ../dm-2
```

デバイスマッピングされています．これで簡単にmount出来ました．

```
$ sudo mount -o ro /dev/mapper/loop0p1 /media/mk/pi-boot
$ sudo mount -o ro /dev/mapper/loop0p2 /media/mk/pi-root/
$ mount | grep /dev/mapper/loop0p
/dev/mapper/loop0p1 on /media/mk/pi-boot type vfat (ro,relatime,fmask=0022,dmask=0022,codepage=437,iocharset=ascii,shortname=mixed,utf8,errors=remount-ro)
/dev/mapper/loop0p2 on /media/mk/pi-root type ext4 (ro,relatime,data=ordered)
$ ls /media/mk/pi-boot
COPYING.linux     bcm2708-rpi-0-w.dtb     bcm2708-rpi-cm.dtb   bcm2710-rpi-cm3.dtb  config.txt    fixup_db.dat  kernel.img   start.elf     start_x.elf
LICENCE.broadcom  bcm2708-rpi-b-plus.dtb  bcm2709-rpi-2-b.dtb  bootcode.bin         fixup.dat     fixup_x.dat   kernel7.img  start_cd.elf
LICENSE.oracle    bcm2708-rpi-b.dtb       bcm2710-rpi-3-b.dtb  cmdline.txt          fixup_cd.dat  issue.txt     overlays     start_db.elf
$ ls /media/mk/pi-root
bin  boot  dev  etc  home  lib  lost+found  media  mnt  opt  proc  root  run  sbin  srv  sys  tmp  usr  var
```

そしてchrootしてみたり

```
$ sudo mount -o remount,rw /media/mk/pi-root
$ sudo cp -p /usr/bin/qemu-arm-static /media/mk/pi-root/usr/bin
$ sudo chroot /media/mk/pi-root/ /bin/bash
# dpkg --get-selections "*" | wc -l
427
```

```
# exit
$ sudo umount /media/mk/pi-*

```

便利ですね :)

