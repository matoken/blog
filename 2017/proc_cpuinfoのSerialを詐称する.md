/proc/cpuinfoのSerialを詐称する

```
$ cat /proc/cpuinfo | sed -e 's/Serial.*/Serial\t\t: 00000000deadbeef/' > /tmp/cpuinfo_fake
$ sudo chmod 444 /tmp/cpuinfo
$ sudo chown root.root /tmp/cpuinfo
$ sudo mount -o bind /tmp/cpuinfo_fake /proc/cpuinfo
$ tail /proc/cpuinfo
Features        : half thumb fastmult vfp edsp neon vfpv3 tls vfpv4 idiva idivt vfpd32 lpae evtstrm 
CPU implementer : 0x41
CPU architecture: 7
CPU variant     : 0x0
CPU part        : 0xc07
CPU revision    : 5

Hardware        : BCM2835
Revision        : a01041
Serial          : 00000000deadbeef
```
