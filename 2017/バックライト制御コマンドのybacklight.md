awesome wmでバックライトの制御に`xbacklight`コマンドを使っていたのですが，手元のDebian sid amd64環境では`No outputs have backlight property`というエラーで現在利用できなくなっています．


```shell
$ xbacklight -get
No outputs have backlight property
$ xbacklight = 100
No outputs have backlight property
$ xbacklight + 10
No outputs have backlight property
$ xbacklight - 10
No outputs have backlight property
```

workaroundとしてこんな感じで手で叩いていてちょっとあれです．持ち出さないPCなのであまり操作しないのでどうにかなってるけど不便です．

```
$ sudo sh -c "echo 1000 > /sys/class/backlight/intel_backlight/brightness"
```

しかし，gdm3のログイン画面では`Fn+Home/Fm+End`でバックライトの変更が出来ることに気づきました．
なんか進展あったのかな?とバグレポを眺めているとそれらしいものは見つからないけれど，

* [#833508 - xbacklight reports "No outputs have backlight property" - Debian Bug report logs](<https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=833508> "#833508 - xbacklight reports &quot;No outputs have backlight property&quot; - Debian Bug report logs")
* [96572 – xbacklight doesn't work with modesetting on intel](<https://bugs.freedesktop.org/show_bug.cgi?id=96572> "96572 – xbacklight doesn&apos;t work with modesetting on intel")

`ybacklight`というものが紹介されているのに気づきました．

* [ybacklight/ybacklight at master · yath/ybacklight](https://github.com/yath/ybacklight/blob/master/ybacklight "ybacklight/ybacklight at master · yath/ybacklight")

利用方法は`xbacklight`と同じです．

```
$ ybacklight -h
Usage: /home/mk/bin/ybacklight [options]
  Options:
  -d <driver> or -driver <driver>: Use driver <driver> (NB: -display is
                                   also supported for compatibility)
  -help: Print this help
  -set <percentage> or = <percentage>: Set backlight to <percentage>
  -inc <percentage> or + <percentage>: Increase backlight by <percentage>
  -dec <percentage> or - <percentage>: Decrease backlight by <percentage>
  -get: Get backlight percentage
  -time <ignored> or -steps <ignored>: Unsupported, ignored
  -v: Verbose output
```

読むのはいけますが，変更は出来ません．

```
$ ybacklight -get
30
$ ybacklight +10
Permissions conflict.  Can't write to: ( /sys/class/backlight/intel_backlight/brightness )
```

`/sys/class/backlight/intel_backlight/brightness`に書き込み権をつけると行けます．

```
$ ls -l /sys/class/backlight/intel_backlight/brightness
-rw-r--r-- 1 root root 4096  4月  5 06:48 /sys/class/backlight/intel_backlight/brightness
$ sudo chmod o+w /sys/class/backlight/intel_backlight/brightness
$ ls -l /sys/class/backlight/intel_backlight/brightness
-rw-r--rw- 1 root root 4096  4月  5 06:48 /sys/class/backlight/intel_backlight/brightness
$ ybacklight +10
40
$ ybacklight -10
30
```

ちょっと微妙．
