# Joypadで簡易操作を試みる

LinuxBoxで文章を読むのにJoypadで操作できないかなと思ってちょっと試してみました．

はじめは`xev`コマンドでJoypadのKeyCodeを調べて`xmodmad`コマンドで書き換えればいいだろうと思っていたのですが，`xev`コマンドでJoypadは反応しないようでした．他に方法はないかと検索すると`joy2key`というものを見つけたのでこれを試してみました．

```
$ apt show joy2key
Package: joy2key
Version: 1.6.3-2
Priority: optional
Section: x11
Maintainer: Debian QA Group <packages@qa.debian.org>
Installed-Size: 52.2 kB
Depends: libc6 (>= 2.15), libx11-6
Homepage: http://joy2key.sourceforge.net
Tag: hardware::input:joystick, hardware::input:keyboard, implemented-in::c,
 role::program, use::configuring
Download-Size: 21.8 kB
APT-Sources: http://dennou-q.gfd-dennou.org/debian sid/main amd64 Packages
Description: ジョイスティックの動きを同等のキーストロークに変換
 joy2key により、ジョイスティックの軸とボタンの動きに対してキーボードイベント
 を選択できますので、ネーティブでジョイスティックをサポートしていないアプリケーション
 でジョイスティックやゲームパッドを利用できます。
```

DebianだとWheezy以降，Ubuntuだとprecise(12.04LTS)以降にパッケージがあるようです．

* [Debian -- パッケージ検索結果 -- joy2key](https://packages.debian.org/search?keywords=joy2key&searchon=names&suite=&section=all "Debian -- パッケージ検索結果 -- joy2key")
* [Ubuntu – パッケージ検索結果 -- joy2key](http://packages.ubuntu.com/search?keywords=joy2key&searchon=names&suite=&section=all "Ubuntu – パッケージ検索結果 -- joy2key")

今回はDebian sidやjessieなので導入はパッケージを入れるだけでした．

```
$ sudo apt install joy2key
```

設定はよく解らなかったけどこの辺を参考に

* [joy2key/joy2keyrc.sample at master · joolswills/joy2key](https://github.com/joolswills/joy2key/blob/master/joy2keyrc.sample "joy2key/joy2keyrc.sample at master · joolswills/joy2key")
* [debian(etch)上でjoy2keyをつかう - 計算機と戯れる日々](http://d.hatena.ne.jp/n9d/20080210/1202648836 "debian(etch)上でjoy2keyをつかう - 計算機と戯れる日々")

こんな感じで動いた．キーマップは未だ詰めるつもりだけどとりあえず．

```
$ DISPLAY=:0 joy2key -X -buttons A B X Y L Tab Escape Return KP_9 -axis Left Right Up Down -thresh -16383 16383 -16383 16383
```

キーコードについてはxevや`/usr/include/X11/keysymdef.h`で確認できる．とりあえずここにも貼っておく．

* [/usr/include/X11/keysymdef.h](https://gist.github.com/matoken/5c2b9d2f0b92c21452cf8b4f01f7e149 "keysymdef.h")

しかしShift + Tabも使いたいのだけどShift/Tabそれぞれ単体は入力できるけど同時入力が効かない．通常のKBDでは動作するのでjoy2keyの問題だと思う．

試した環境はDebian sidのjoy2key 1.6.3-2とDebian lennyのjoy2key 1.6.3-2でJoypadはハードオフで\200くらいだったBUFFALOのスーパーファミコンみたいな見た目のBSGP801シリーズと書かれているもの．

* [sudo lsusb -d 0583:2060 -vvv](https://gist.github.com/matoken/8ecaa8c426678a76dfb50743058cfbab "sudo lsusb -d 0583:2060 -vvv")
* dmesg

```
[ 5239.625574] usb 2-1.2: authorized to connect
[ 5263.982357] usb 1-1.2: new low-speed USB device number 6 using ehci-pci
[ 5264.095408] usb 1-1.2: New USB device found, idVendor=0583, idProduct=2060
[ 5264.095410] usb 1-1.2: New USB device strings: Mfr=0, Product=2, SerialNumber=0
[ 5264.095411] usb 1-1.2: Product: USB,2-axis 8-button gamepad  
[ 5264.095545] usb 1-1.2: Device is not authorized for usage
[ 5266.168066] usb 1-1.2: authorized to connect
[ 5266.176105] hidraw: raw HID events driver (C) Jiri Kosina
[ 5266.184205] usbcore: registered new interface driver usbhid
[ 5266.184207] usbhid: USB HID core driver
[ 5266.188173] input: USB,2-axis 8-button gamepad   as /devices/pci0000:00/0000:00:1a.0/usb1/1-1/1-1.2/1-1.2:1.0/0003:0583:2060.0001/input/input19
[ 5266.188377] hid-generic 0003:0583:2060.0001: input,hidraw0: USB HID v1.10 Joystick [USB,2-axis 8-button gamepad  ] on usb-0000:00:1a.0-1.2/input0
```

<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B002B9XB0E&linkId=b440467e1c9d15b9920d82894bb7f794"></iframe><iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B004R1R9IO&linkId=12f8e6d3fb92d0c335171485c5f7a378"></iframe><iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B06XH1ZDBT&linkId=247e25a981e5eeb64aeab55ebb4e4d76"></iframe>