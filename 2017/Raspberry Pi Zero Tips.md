# Raspberry Pi Zero Tips

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/33031203146/in/dateposted/" title="Raspberry-Pi-Zero-web"><img src="https://c1.staticflickr.com/1/337/33031203146_3d2b7dcb08.jpg" width="500" height="348" alt="Raspberry-Pi-Zero-web"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
24日にKSYとスイッチサイエンスからRaspberry Pi Zero発売ってことでTipsを書いてみます．

* [Raspberry Pi Zero - Raspberry Pi](https://www.raspberrypi.org/products/pi-zero/ "Raspberry Pi Zero - Raspberry Pi")
* [Raspberry Pi Zero 取扱開始のお知らせ - Raspberry Pi Shop by KSY"](https://raspberry-pi.ksyic.com/news/page/nwp.id/46)
    * [ホーム - Raspberry Pi Shop by KSY](https://raspberry-pi.ksyic.com/main/index/pdp.id/216,217,218,219,220,221,222,223,224,225/pdp.open/222﻿) ※販売ページ
* [スイッチサイエンスで「Raspberry Pi Zero v1.3」の予約販売を開始 - スイッチサイエンス](https://www.switch-science.com/pressrelease/raspberry_pi_zero/ "スイッチサイエンスで「Raspberry Pi Zero v1.3」の予約販売を開始 - スイッチサイエンス")
    * [【予約販売】Raspberry Pi Zero [2017年2月24日正午から] - スイッチサイエンス](https://www.switch-science.com/catalog/3190/ "【予約販売】Raspberry Pi Zero [2017年2月24日正午から] - スイッチサイエンス") ※販売ページ

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/32916493402/in/dateposted/" title="Raspberry-Pi-Zero-web (コピー)"><img src="https://c1.staticflickr.com/3/2057/32916493402_3c64fe7628_n.jpg" width="320" height="180" alt="Raspberry-Pi-Zero-web (コピー)"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

* 本体の他に最低限microSD(OS領域)やmicroUSBケーブル(電源)が必要です．(microUSBへのOS導入に別途PCも必要)．
    * [Installing operating system images - Raspberry Pi Documentation](https://www.raspberrypi.org/documentation/installation/installing-images/README.md "Installing operating system images - Raspberry Pi Documentation")
    * <iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B00M55C0NS&linkId=c870b711d314abbccc974bfa9de7c53a"></iframe><iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B016LWI25K&linkId=e6cee304f951b76b73239fc1a6c82d8b"></iframe>
* HDMIディスプレイを利用したい場合は大抵の場合別途mini HDMIからフルサイズへの変換アダプタやケーブルが必要です．※PIZEROはmini HDMIです．よく見かけるのはmicro HDMIなので注意しましょう．
    * <iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B004FWEULO&linkId=2cb81ce6f53c6915ba1391a5aaabb3d9"></iframe><iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B014I8UHXE&linkId=1f9abd85eac7ad137b1bb9bb2e9269df"></iframe>
* コンポジットのディスプレイを利用したい場合はジャックなどをはんだ付けする必要があります．(**TV**のシルク)
* microUSBx2のうち片方(**PWR IN**)は電源専用です．
* もう片方(**USB**)はUSB OTGでHOSTにもGadget(SerialやEthernetはdebugに便利!)にもなれます．電源供給も可能なので電源容量が足りるならこの1本だけで運用することも可能です．microUSBケーブルやOTGケーブルはそれなりのものを使ったほうが良いです．安いものを使ったらPIZERO側のコネクタが傷んで使えなくなってしまいました．
<script async class="speakerdeck-embed" data-id="00cbd7582d0949719c4dfd104c85c42c" data-ratio="1.33333333333333" src="//speakerdeck.com/assets/embed.js"></script>
* USBを利用する場合は給電が可能なmicroUSBのHUBがデバッグに便利です．
    * <iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B010EFZISY&linkId=63b40bda5d4ff7bde0a6c8ddb4977415"></iframe>
* GPIO Pinは+以降と共通だけどピンは未実装なので利用したい場合は自分で別途ピンヘッダ等購入してはんだ付けする必要があります．カラフルなピンヘッダをASUS TINKER BOARDのようにピンの用途毎に変えると便利そうです．
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/33031202786/in/dateposted/" title="XxbHbzYFnf4AVH1a_setting_fff_1_90_end_1000"><img src="https://c1.staticflickr.com/3/2220/33031202786_f9ff623e47_q.jpg" width="150" height="150" alt="XxbHbzYFnf4AVH1a_setting_fff_1_90_end_1000"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script><a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/33031201296/in/dateposted/" title="C-06641"><img src="https://c1.staticflickr.com/1/304/33031201296_6cfd0f8b42_q.jpg" width="150" height="150" alt="C-06641"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
    * [TINKER BOARD | Single-board Computer | ASUS United Kingdom](https://www.asus.com/uk/Single-board-Computer/TINKER-BOARD/ "TINKER BOARD | Single-board Computer | ASUS United Kingdom")
    * [細ピンヘッダ　１×４０　アソートパック　（１０本入）: パーツ一般 秋月電子通商 電子部品 ネット通販](http://akizukidenshi.com/catalog/g/gC-06641/ "細ピンヘッダ　１×４０　アソートパック　（１０本入）: パーツ一般 秋月電子通商 電子部品 ネット通販")

* 純正カメラモジュールを利用する場合はRaspberry Pi Zero V1.3以降が必要(KSYもスイッチサイエンスもV1.3と書かれているので問題ない)で更にカメラモジュール付属のものとは別のPIZERO専用のケーブルが必要です．
    * <iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B01FS46E7K&linkId=087283d7221f44024137b0e006f55050"></iframe>
* KSYでは5000円以上送料無料，それ以下は一律756円の送料が掛かる．KSYを初めて使う人は形式チェックが厳しくて結構面倒なので前もってアカウントを登録しておいたほうが良いです．(単品で買うのであれば海外で買うのと変わらないか高いくらいなので早く欲しい人向け?)
* スイッチサイエンスでは送料150円．3000円以上送料無料．単品で買う場合おすすめ．
* 余録．チョコベビーの容器はケースにちょうどいいです :)
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/33073035015/in/dateposted/" title="IMG_20170216_075638"><img src="https://c1.staticflickr.com/3/2781/33073035015_6c7ec1ebae_n.jpg" width="320" height="240" alt="IMG_20170216_075638"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

