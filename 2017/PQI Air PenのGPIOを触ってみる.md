# PQI Air PenのGPIOを触ってみる

ID/PASSWORDがわかってtelnetで中に入れるようになったので中を覗いているとgpioが見えてました．

- [パケットキャプチャしてPQI Air PenのID/PASSを調べる | matoken's meme](http://matoken.org/blog/blog/2017/03/08/packet-capture-and-check-id-pass-of-pqi-air-pen/ "パケットキャプチャしてPQI Air PenのID/PASSを調べる | matoken&apos;s meme")

```
# ls -lA /proc/gpio/
ls -lA /proc/gpio/
-r--r--r--    1 root     root            0 Jan  1 07:22 gpio12_in
-r--r--r--    1 root     root            0 Jan  1 07:22 gpio22_in
-rw-r--r--    1 root     root            0 Jan  1 07:22 gpio23_out
-rw-r--r--    1 root     root            0 Jan  1 07:22 gpio27_out
-r--r--r--    1 root     root            0 Jan  1 07:22 gpio6_in
-rw-r--r--    1 root     root            0 Jan  1 07:22 gpio7_out
-r--r--r--    1 root     root            0 Jan  1 07:22 gpio8_in
```

てことでちょっと叩いてみました．

* 赤LED(🔋) : /proc/gpio/gpio7_out
    * 0 : On
    * 1 : Off

```
/proc/gpio # echo 1 > gpio7_out
echo 1 > gpio7_out
/proc/gpio # echo 0 > gpio7_out
echo 0 > gpio7_out
```

* 黄緑LED(🔃) : /proc/gpio/gpio23_out
    * 0 : Off
    * 1 : On

```
/proc/gpio # echo 0 > gpio23_out
echo 0 > gpio23_out
/proc/gpio # echo 1 > gpio23_out
echo 1 > gpio23_out
```

* 横面同期ボタン(🔃) : /proc/gpio/gpio22_in
  * 0 : On
  * 1 : Off

```
/proc/gpio # cat gpio22_in
cat gpio22_in
0
/proc/gpio # cat gpio22_in
cat gpio22_in
1
```

という感じで2つのLEDと1つのボタンは簡単に利用できました．他はちょっと叩いただけでは割らなかったです．
とりあえずこんな感じで横の同期ボタンを押すとLEDx2を光らせるということが出来ます．

```
~ # while :
> do
> if [ `cat /proc/gpio/gpio22_in` = '0' ]; then
> echo on
> echo 0 > /proc/gpio/gpio7_out
> echo 1 > /proc/gpio/gpio23_out
> break
> fi
> sleep 1
> done
```

<iframe width="560" height="315" src="https://www.youtube.com/embed/Ywan8-NJ-Dk?rel=0" frameborder="0" allowfullscreen></iframe>


<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B00BNAST0O&linkId=f0dc7ce25a352b827cce5dda94fd19cb"></iframe><iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B015J44R0U&linkId=a0a72fb28df925f647a4f9e5fde2aacb"></iframe><iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B06XH1ZDBT&linkId=7ca6bd0c75ef9f49a954188862283dc4"></iframe>