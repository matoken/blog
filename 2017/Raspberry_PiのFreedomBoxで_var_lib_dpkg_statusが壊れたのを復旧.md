# Raspberry Pi の FreedomBox で /var/lib/dpkg/status が壊れたのを復旧

Raspberry PiでFreedomBoxを試しているのですが，ストレージがmicroSDのせいか修正していたパッケージの依存関係か何かがおかしくなったせいか`/var/lib/dpkg/status`が壊れてしまいました．

```
$ sudo apt update
Hit:1 http://cdn-fastly.deb.debian.org/debian testing InRelease
Reading package lists... Error!
E: Unable to parse package file /var/lib/dpkg/status (1)
W: You may want to run apt-get update to correct these problems
E: The package cache file is corrupted
```
```
E: パッケージファイル /var/lib/dpkg/status を解釈することができません (1)
W: これらの問題を解決するためには apt-get update を実行する必要があるかもしれません
E: パッケージキャッシュファイルが壊れています
```

`apt-get update`で治るのかなと思って叩いてみましたが駄目でした．

[Debian リファレンス](https://www.debian.org/doc/manuals/debian-reference/index.ja.html "Debian リファレンス")の[第2章 Debian パッケージ管理](https://www.debian.org/doc/manuals/debian-reference/ch02.ja.html#_recovering_package_selection_data "第2章 Debian パッケージ管理")によると，

> 2.6.5. パッケージセレクションの復元
> 
> もし何らかの理由で "/var/lib/dpkg/status" の内容が腐った場合には、Debian システムはパッケージ選択データーが失われ大きな打撃を被ります。古い "/var/lib/dpkg/status" ファイルは、"/var/lib/dpkg/status-old" や "/var/backups/dpkg.status.*" としてあるので探します。
> 
> "/var/backups/" は多くの重要な情報を保持しているので、これを別のパーティション上に置くのも良い考えです。

とのことなので，`/var/lib/dpkg/status-old`から復旧をと思いましたがこれも既に壊れているバージョン．`/var/backups/dpkg.status.0`はまだ壊れていなかったのでこれで上書きして復旧したようです．

```
$ sudo cp /var/backups/dpkg.status.0 /var/lib/dpkg/status
```

