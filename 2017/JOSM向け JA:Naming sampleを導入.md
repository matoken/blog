
* [[OSM-ja] JOSM向け JA:Naming sample が更新されています](https://lists.openstreetmap.org/pipermail/talk-ja/2017-March/009649.html "[OSM-ja] JOSM向け JA:Naming sample が更新されています")

ということで導入方法を確認しました．

プリセットメニューの「プリセット設定」を選択．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/33429473641/in/dateposted/" title="menu-priset"><img src="https://c1.staticflickr.com/4/3946/33429473641_0ddf380e8e.jpg" width="348" height="137" alt="menu-priset"></a>

「利用可能なプリセット」の中から「JA:Naming sample」を選択して左矢印ボタンを押して有効にします．OKボタンを押して，

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/32714822544/in/dateposted/" title="priset_menu"><img src="https://c1.staticflickr.com/3/2865/32714822544_8c64092eef.jpg" width="496" height="500" alt="priset_menu"></a>

JOSMを再起動することで反映されます．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/33429473951/in/dateposted/" title="reboot"><img src="https://c1.staticflickr.com/3/2814/33429473951_2a7633e70b.jpg" width="423" height="138" alt="reboot"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

これでプリセットやプリセットの検索で利用できるようになります．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/33429474461/in/dateposted/" title="shutter_17-03-21_06:16:59_001"><img src="https://c1.staticflickr.com/3/2843/33429474461_85893009f9.jpg" width="500" height="330" alt="shutter_17-03-21_06:16:59_001"></a>

インクリメンタル検索の例．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/33429474581/in/photostream/" title="shutter_17-03-21_06:18:25_001"><img src="https://c1.staticflickr.com/3/2919/33429474581_b80b2da2c5.jpg" width="412" height="371" alt="shutter_17-03-21_06:18:25_001"></a>

とても便利です :)


既に導入している人はJOSM起動時に更新されるはずです．
不安な人は一旦消して再導入したり，実際のファイルの中のバージョンを確認することも出来ます．

```
$ grep JaNamigSample ~/.josm/preferences.xml
  <list key='mirror.https://josm.openstreetmap.de/josmfile?page_Presets/JaNamigSample&amp;zip_1'>
    <entry value='/home/mk/.josm/cache/mirror_https___josm.openstreetmap.de_josmfile_page_Presets_JaNamigSample_zip_1'/>
      <tag key='url' value='https://josm.openstreetmap.de/josmfile?page=Presets/JaNamigSample&amp;zip=1'/>
$ zgrep version /home/mk/.josm/cache/mirror_https___josm.openstreetmap.de_josmfile_page_Presets_JaNamigSample_zip_1
    version="2.0b7_24_2017-03-04"
```

