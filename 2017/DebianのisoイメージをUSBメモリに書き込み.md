
## ファイルダウンロード

今回はDebian stretch Debian Installer rc2のi386版のnetinst

- debian-stretch-DI-rc2-i386-netinst.iso : イメージ本体
- SHA512SUMS : isoファイルのチェックサムファイル
- SHA512SUMS.sign : SHA512SUMSの署名ファイル

```shell
$ wget http://cdimage.debian.org/cdimage/stretch_di_rc2/i386/iso-cd/debian-stretch-DI-rc2-i386-netinst.iso http://cdimage.debian.org/cdimage/stretch_di_rc2/i386/iso-cd/SHA512SUMS.sign http://cdimage.debian.org/cdimage/stretch_di_rc2/i386/iso-cd/SHA512SUMS
```

>
※i386/amd64をよく使う場合は`multi-arch`を使うと1つのUSBメモリでi386/amd64が利用できて便利

## チェックサムファイルの署名確認

チェックサムファイルのSHA512SUMSが正常なものか確認

```shell
$ gpg --verify SHA512SUMS.sign
gpg: 署名されたデータが'SHA512SUMS'にあると想定します
gpg: 2017年02月02日 07時45分30秒 JSTに施された署名
gpg:                RSA鍵DA87E80D6294BE9Bを使用
gpg: "Debian CD signing key <debian-cd@lists.debian.org>"からの正しい署名 [不明の]
gpg: *警告*: この鍵は信用できる署名で証明されていません!
gpg:       この署名が所有者のものかどうかの検証手段がありません。
 主鍵フィンガープリント: DF9B 9C49 EAA9 2984 3258  9D76 DA87 E80D 6294 BE9B
```


※`公開鍵が見つかりません(public key not found)`というエラーの場合は`gpg --keyserver keyring.debian.org --recv-keys DA87E80D6294BE9B`で鍵をインポートして再度確認．鍵のIDやフィンガープリントは以下のページでも確認できる

- [Debian CD の信頼性の検証](https://www.debian.org/CD/verify "Debian CD の信頼性の検証")


## ハッシュ確認

isoファイルが正しくダウンロードされているか確認
以下の例は`debian-stretch-DI-rc2-i386-netinst.iso`しかダウンロードしていないのでそれ以外のエラーや警告は無視する

```shell
$ sha512sum -c SHA512SUMS
sha512sum: debian-mac-stretch-DI-rc2-i386-netinst.iso: そのようなファイルやディレクトリはありません
debian-mac-stretch-DI-rc2-i386-netinst.iso: FAILED open or read
debian-stretch-DI-rc2-i386-netinst.iso: 完了
sha512sum: debian-stretch-DI-rc2-i386-xfce-CD-1.iso: そのようなファイルやディレクトリはありません
debian-stretch-DI-rc2-i386-xfce-CD-1.iso: FAILED open or read
sha512sum: 警告: 一覧にある 2 個のファイルが読み込めませんでした
```

## usbメモリへの書き込み

### USBメモリの確認

書き込み先のデバイスが正しいか確認する
USBメモリ接続直後にdmesgを確認したりfdiskコマンドやマウントして中を確認したり……

```shell
$ dmesg
  :
[414356.444121] usb 1-1.2: New USB device found, idVendor=13fe, idProduct=1a00
[414356.444128] usb 1-1.2: New USB device strings: Mfr=0, Product=11, SerialNumber=0
[414356.444131] usb 1-1.2: Product: USB 2.0 HUB
[414362.925178] usb-storage 1-1.2.1:1.0: USB Mass Storage device detected
[414362.925967] scsi host6: usb-storage 1-1.2.1:1.0
[414364.184209] sd 6:0:0:1: [sdb] 980480 512-byte logical blocks: (502 MB/479 MiB)
[414364.187102] sd 6:0:0:1: [sdb] Write Protect is off
[414364.187106] sd 6:0:0:1: [sdb] Mode Sense: 23 00 00 00
[414364.190339] sd 6:0:0:1: [sdb] No Caching mode page found
[414364.190345] sd 6:0:0:1: [sdb] Assuming drive cache: write through
[414364.201730]  sdb: sdb1
[414364.201734] sdb: p1 size 982496 extends beyond EOD, enabling native capacity
[414364.214471]  sdb: sdb1
[414364.214475] sdb: p1 size 982496 extends beyond EOD, truncated
[414364.228961] sd 6:0:0:1: [sdb] Attached SCSI removable disk
$ sudo fdisk -l /dev/sdb
Disk /dev/sdb: 478.8 MiB, 502005760 bytes, 980480 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x00000000

Device     Boot Start    End Sectors   Size Id Type
/dev/sdb1  *       32 982527  982496 479.8M  6 FAT16
```

### USBメモリのアンマウント

USBメモリをマウントしている場合はアンマウントしておく

```shell
$ sudo umount /dev/sdb1
$ mount | grep /dev/sdb
```

### パーテイション情報の削除

念の為パーテイション情報を削除しておく

```shell
$ sudo wipefs /dev/sdb
offset               type
----------------------------------------------------------------
0x1fe                dos   [partition table]

$ sudo wipefs -a /dev/sdb ; sync
/dev/sdb: 2 bytes were erased at offset 0x000001fe (dos): 55 aa
/dev/sdb: calling ioctl to re-read partition table: 成功です
```

## 書き込み

進捗状態を確認したい場合はpvコマンドを間に挟んだり，ddrescue / ddrescueなどが利用できる

- [Linux 環境での Raspberry Pi 向け OS 書き込みTips \| matoken's meme](http://matoken.org/blog/blog/2015/12/08/raspberry-pi-for-os-writing-tips-on-linux-environment/)

```shell
$ sudo dd if=./debian-stretch-DI-rc2-i386-netinst.iso of=/dev/sdb bs=4M ; sync
95+1 レコード入力
95+1 レコード出力
401604608 bytes (402 MB, 383 MiB) copied, 176.874 s, 2.3 MB/s
```

書き込みが終わったらUSBメモリを取り外してターゲットマシンで利用する

## VirtualBoxで起動確認(余録)

手軽に試せるマシンがなかったのでVirtualBoxからUSBメモリを起動して確認した

### USBメモリのディスクイメージ作成

直にUSBメモリを指定できないのでUSBメモリへアクセスするためのvmdkイメージを作成

```shell
$ sudo VBoxManage internalcommands createrawvmdk -rawdisk /dev/sdb -filename sdb.vmdk 
RAW host disk access VMDK file sdb.vmdk created successfully.
$ sudo cat sdb.vmdk
# Disk DescriptorFile
version=1
CID=1722e641
parentCID=ffffffff
createType="fullDevice"

# Extent description
RW 980480 FLAT "/dev/sdb" 0

# The disk Data Base 
#DDB

ddb.virtualHWVersion = "4"
ddb.adapterType="ide"
ddb.geometry.cylinders="972"
ddb.geometry.heads="16"
ddb.geometry.sectors="63"
ddb.uuid.image="c2c9d560-049f-4c44-bf8a-0b85e820ba12"
ddb.uuid.parent="00000000-0000-0000-0000-000000000000"
ddb.uuid.modification="00000000-0000-0000-0000-000000000000"
ddb.uuid.parentmodification="00000000-0000-0000-0000-000000000000"
```

### USBメモリにアクセス権のあるユーザでVirtualBoxを起動してUSBメモリのイメージを指定して起動

（ここいまいち……）

```
$ gksudo virtualbox
```

![](https://c1.staticflickr.com/3/2872/33528516932_97253a80a9_o.png)
