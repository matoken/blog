# awesomeのバージョンアップでコマンド実行できなくなる(3.5.9-1 -> 4.0-1)

Debian strech amd64環境のawesomeが3.5.9-1から4.0-1になりました．
するとMod4+rに割り当てていたコマンド実行を押すとこんなメッセージが表示されて動作しません．

```
Oops, an error happened!
/home/mk/.config/awesome/rc.lua:302: attempt to index field '?' (a nil value)
```

とりあえずはターミナルから呼んでいましたがちゃんと確認することに．

一応Syntax check

```
$ luac -v
Lua 5.1.5  Copyright (C) 1994-2012 Lua.org, PUC-Rio
$ luac -p ~/.config/awesome/rc.lua
```

awesome versionを確認すると4.0-1になっている
Luaは5.1で変わらず．

```
$ apt show awesome
Package: awesome
Version: 4.0-1
Priority: optional
Section: x11
Maintainer: Julien Danjou <acid@debian.org>
Installed-Size: 5,662 kB
Provides: x-window-manager
Depends: libc6 (>= 2.14), libcairo2 (>= 1.12.0), libdbus-1-3 (>= 1.9.14), libgdk-pixbuf2.0-0 (>= 2.22.0), libglib2.0-0 (>= 2.30.0), liblua5.1-0, libstartup-notification0 (>= 0.10), libx11-6, libxcb-cursor0 (>= 0.0.99), libxcb-icccm4 (>= 0.4.1), libxcb-keysyms1 (>= 0.4.0), libxcb-randr0 (>= 1.12), libxcb-render0, libxcb-shape0, libxcb-util0 (>= 0.3.8), libxcb-xinerama0, libxcb-xkb1, libxcb-xrm0 (>= 0.0.0), libxcb-xtest0, libxcb1 (>= 1.6), libxdg-basedir1, libxkbcommon-x11-0 (>= 0.5.0), libxkbcommon0 (>= 0.5.0), menu, dbus-x11, lua-lgi (>= 0.7.0), gir1.2-freedesktop, gir1.2-pango-1.0
  :
```

該当行周辺

```
299     awful.key({ modkey, "Control" }, "n", awful.client.restore),↲
300 ↲   
301     -- Prompt↲
302     awful.key({ modkey },            "r",     function () mypromptbox[mouse.screen]:run() end),↲
303 ↲
304     awful.key({ modkey }, "x",↲
```

exampleと比較してみる

```
$ dpkg -L awesome | grep -e \/rc.lua
/etc/xdg/awesome/rc.lua
$ grep mypromptbox /etc/xdg/awesome/rc.lua
    s.mypromptbox = awful.widget.prompt()
            s.mypromptbox,
    awful.key({ modkey },            "r",     function () awful.screen.focused().mypromptbox:run() end,
                    textbox      = awful.screen.focused().mypromptbox.widget,
$ diff ~/.config/awesome/rc.lua /etc/xdg/awesome/rc.lua|wc -l
703
```

ということで4.0ベースで作り直したほうが良さそう．

* [awesome API documentation](https://awesomewm.org/apidoc/documentation/89-NEWS.md.html#v4 "awesome API documentation")

```
$ cp /etc/xdg/awesome/rc.lua ~/.config/awesome/rc.lua
$ 
$ vi ~/.config/awesome/rc.lua
$ Xephyr :1 -ac -br -noreset -screen 1366x700 &
$ DISPLAY=:1 awesome -c ~/.config/awesome/rc.lua
```

な感じで編集してとりあえずこんな感じになりました．

