# Ubuntu 17.04のSynergyと互換性が無くなったのでDebian sidでexperimentalのSynergyを使う

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/32245593774/in/dateposted/" title="20170224_01-02-43-4533"><img src="https://c1.staticflickr.com/4/3770/32245593774_87d8be1454.jpg" width="500" height="275" alt="20170224_01-02-43-4533"></a>

リリース前のUbuntu 17.04をサブマシンで使用しているのですが，これをメインPCの横に置いてSynergyを使ってメインPCから操作しています．しかしUbuntu 17.04のSynergyが新しくなって繋がらなくなりました．
設定を確認するとこれまで暗号化通信にパスワードを指定する形でした．これがSSLになったようです．
GUIでもman synergys/synergycを見ても旧形式は使えなさそうで互換性が失われているようです．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/32935703692/in/dateposted/" title="20170224_01-02-53-4521"><img src="https://c1.staticflickr.com/4/3899/32935703692_8511b1a956.jpg" width="368" height="416" alt="20170224_01-02-53-4521"></a>
※Ubuntu 17.04のsynergy 1.8.7-stable+dfsg.1-2

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/32963755811/in/dateposted/" title="20170224_01:02:47-1037"><img src="https://c1.staticflickr.com/4/3876/32963755811_445515a309.jpg" width="368" height="476" alt="20170224_01:02:47-1037"></a>
※Debian sidのsynergy 1.4.16-2

Debian sidのSynergyのバージョンを確認すると，今のところsidでも1.4.16-2でexperimentalでは1.8.7-stable+dfsg.1-2でした．

* [Debian -- パッケージ検索結果 -- synergy](https://packages.debian.org/search?keywords=synergy&searchon=names&suite=&section=all)

とりあえず`experimental`から借りてくることにしました．

`sources.list`に`experimental`がある状態で

```
$ grep experimental /etc/apt/sources.list
deb <http://dennou-q.gfd-dennou.org/debian/> experimental main non-free contrib
deb-src <http://dennou-q.gfd-dennou.org/debian/> experimental main non-free contrib
```

`/etc/apt/preferences.d/synergy`を以下のような感じで用意して，

```
$ cat /etc/apt/preferences.d/synergy
Package: synergy
Pin: release a=experimental
Pin-Priority: 800
```

パッケージ情報を更新して導入．

```
$ sudo apt update
$ sudo apt install synergy
 :
以下のパッケージはアップグレードされます:
  synergy
アップグレード: 1 個、新規インストール: 0 個、削除: 0 個、保留: 22 個。
853 kB のアーカイブを取得する必要があります。
この操作後に追加で 607 kB のディスク容量が消費されます。
取得:1 <http://dennou-q.gfd-dennou.org/debian> experimental/main amd64 synergy amd64 1.8.7-stable+dfsg.1-2 [853 kB]
853 kB を 3秒 で取得しました (235 kB/s)
 :
.../synergy_1.8.7-stable+dfsg.1-2_amd64.deb を展開する準備をしています ...
synergy (1.8.7-stable+dfsg.1-2) で (1.4.16-2 に) 上書き展開しています ...
synergy (1.8.7-stable+dfsg.1-2) を設定しています ...
 :
```

という感じで導入できました．
元に戻す場合は`/etc/apt/preferences.d/synergy`を消してパッケージを更新すればいいはず．

バージョンが新しくなってからSynergyを起動するとウィザードが走ってSSL証明書が`~/.synergy/SSL/Synergy.pem`に作られます．フィンガープリントも`~/.synergy/SSL/Fingerprints/Local.txt`に作成されるので初回接続時にClient側に表示されるメッセージをSynergy Server側のこのフィンガープリントと見比べてから接続します．


```
$ cat ~/.synergy/SSL/Fingerprints/Local.txt 
2E:51:06:DC:05:42:09:81:7E:FB:3A:4F:C6:A4:A4:EE:98:ED:75:A0
```

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/32709118930/in/dateposted/" title="20170225_00:02:32-4657"><img src="https://c1.staticflickr.com/3/2098/32709118930_7b137a1ddd.jpg" width="500" height="184" alt="20170225_00:02:32-4657"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

このフィンガープリントはClientの`~/.synergy/SSL/Fingerprints/TrustedServers.txt`に保存されます．

```
$ cat ~/.synergy/SSL/Fingerprints/TrustedServers.txt
2E:51:06:DC:05:42:09:81:7E:FB:3A:4F:C6:A4:A4:EE:98:ED:75:A0
```

てことでとりあえず使えるようになりました．

## 環境

* server

```
$ lsb_release -d
Description:    Debian GNU/Linux 9.0 (stretch)
$ uname -a
Linux x220 4.9.0-2-amd64 #1 SMP Debian 4.9.10-1 (2017-02-17) x86_64 GNU/Linux
$ dpkg-query -W synergy
synergy 1.8.7-stable+dfsg.1-2
```

* client

```
$ lsb_release -d
Description:	Ubuntu Zesty Zapus (development branch)
$ uname -a
Linux x200 4.9.0-15-generic #16-Ubuntu SMP Fri Jan 20 15:31:12 UTC 2017 x86_64 x86_64 x86_64 GNU/Linux
$ dpkg-query -W synergy
synergy	1.8.7-stable+dfsg.1-2
```

<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B01NAQ5VNH&linkId=b5144afeb2c473023e1a3b69739201fb"></iframe><iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B01LYN4ODS&linkId=e57d40aee45827ea4ca8c46d7a4aed6d"></iframe><iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=as_ss_li_til&asins=B01KN6VEYG&linkId=fc10b79d2922cf417f9768ce140193a5"></iframe>