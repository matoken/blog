# i3wm入門中

最近i3 wmを使い始めました．未だ設定中なのですがとりあえずよく使うキーのチートシートを．

| awesome              | i3                                                           |                                      |
|----------------------|--------------------------------------------------------------|--------------------------------------|
| mod + Enter          | mod + Enter                                                  | 端末起動                             |
| mod + shift + c      | mod + shift + q                                              | ウィンドウを閉じる                   |
| mod + d              | mod + r                                                      | コマンド実行                         |
| mod + shift + q      | mod + shift + e                                              | ログアウト                           |
| mod + clrl+ r        | mod + shift + r                                              | 再起動                               |
| mod + h/l/j/k        | mod + j/k/l/;                                                | ウィンドウフォーカス切り替え         |
| ?                    | mod + h                                                      | 横にウィンドウを作成                 |
| ?                    | mod + v                                                      | 縦にウィンドウを作成                 |
| mod + shift + h/j/k/l| mod + shift + j/k/l/;                                        | ウィンドウの移動                     |
| mod + h/l            | mod + r でリサイズモード<br>j/k/l/;でリサイズ<br>Enterで確定 | ウィンドウリサイズ                   |
| mod + 1-9            | mod+1-0                                                      | ワークスペース切り替え               |
| mod + shift + 1-9    | mod + shift + 1-0                                            | ウィンドウを指定ワークスペースに移動 |
| mod + m              | mod + w                                                      | ウィンドウ最大化                     |
| mod + f              | mod + f                                                      | フルスクリーン表示                   |

あと，ちょっと嵌ったのがtrayの表示．モニタは1枚なので既定値のprimaryで良さそうに思ったのだけど表示されなくて，xrandrコマンドで確認して出てきた名前(default)を指定して出るように．

設定ファイルは~/.config/i3/config

```shell
  # Start i3bar to display a workspace bar (plus the system information i3status
  # finds out, if available)
  bar {
          status_command i3status
+         tray_output default
-         tray_output primary
  }
```

* [i3: i3 User’s Guide](https://i3wm.org/docs/userguide.html#_tray_output "i3: i3 User’s Guide")

