minicomでステータスラインを非表示に

- [[https://www.raspberrypi.org/forums/viewtopic.php?f=82&t=177004|Raspberry Pi • View topic - minicomからのシリアルコンソールで]]

ということでmanを見てみると`-F`というのがそれぽい

```
$ minicim -D /dev/ttyACM1 -F ''
```

としてみるとステータスラインの内容は消えるけどステータスライン自体は残っているのでちょっと違う感じ．

minicim起動中に設定画面(Ctrl+A o)の「画面とキーボード」内にそれらしい「C - ステータスライン表示   : 許可」というものがある．これを`C`を押して「禁止」にして「"dfl" に設定を保存」で設定ファイルに書き出してみる．

```
$ cat ~/.minirc.dfl 
# Machine-generated file - use setup menu in minicom to change parameters.
pu statusline       disabled
```

この状態でminicomを起動するとステータスラインは消えていて，この行をコメントアウトしてminicomを起動するとステータスラインがでてくるのを確認．


試した環境はDebian sid amd64のminicom 2.7-1+b2


