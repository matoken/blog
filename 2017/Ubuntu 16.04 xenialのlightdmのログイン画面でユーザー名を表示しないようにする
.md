Ubuntu 16.04 xenialのlightdmのログイン画面でユーザー名を表示しないようにする

```
$ sudo cat << __EOF__ > /usr/share/lightdm/lightdm.conf.d/10_my.conf
[SeatDefaults]
# Hiding the user list.
greeter-hide-users=true
__EOF__
$ cat /usr/share/lightdm/lightdm.conf.d/10_my.conf 
[SeatDefaults]
# Hiding the user list.
greeter-hide-users=true
$ sudo service lightdm restart
```
