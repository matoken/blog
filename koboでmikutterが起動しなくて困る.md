<a href="https://www.flickr.com/photos/119142834@N05/15111415595" title="IMG_20140902_071214 by Kenichiro MATOHARA, on Flickr"><img src="https://farm6.staticflickr.com/5582/15111415595_4525f85948.jpg" width="500" height="375" alt="IMG_20140902_071214"></a>
1月ほど前のオープンソースカンファレンス関西1日目にchroot で debian を起動した Kobo やAndroid を入れた Kobo を展示したりしていたのですが，mikutter を起動しておこうと思ったらエラーを吐いて起動しませんでした．
この時久々に起動したので大量のアップデートを行ったので環境が変わったのでした．デモの前に前もって確認しないとダメですよね…．
エラーはKobo のコンソールを流れてしまい，スクロールバックの方法もよくわからない．ssh 経由で確認したいけれど会場のWi-Fi 経由では無理そうだったのでその時は諦めました．(開発者も居たのに!)

てことでさっき確認したのでメモ．

ssh 経由で起動してみるとこんな感じでした．
```
$ mikutter --confroot=/tmp/mikutter
/usr/share/mikutter/core/mui/cairo_cell_renderer_message.rb: line 10
   GLib-GObject-WARNING **:Attempt to add property GtkCellRendererMessage::message-id after class was initialised
/usr/lib/ruby/vendor_ruby/gettext/text_domain.rb:100:in `=~': incompatible encoding regexp match (UTF-8 regexp with ASCII-8BIT string) (Encoding::CompatibilityError)
        from /usr/lib/ruby/vendor_ruby/gettext/text_domain.rb:100:in `block in translate_singular_message'
        from /usr/lib/ruby/vendor_ruby/gettext/text_domain.rb:99:in `each'
        from /usr/lib/ruby/vendor_ruby/gettext/text_domain.rb:99:in `translate_singular_message'
        from /usr/lib/ruby/vendor_ruby/gettext/text_domain_manager.rb:104:in `block in translate_singular_message'
        from /usr/lib/ruby/vendor_ruby/gettext/text_domain_manager.rb:84:in `block (2 levels) in each_text_domains'
        from /usr/lib/ruby/vendor_ruby/gettext/text_domain_manager.rb:83:in `each'
        from /usr/lib/ruby/vendor_ruby/gettext/text_domain_manager.rb:83:in `block in each_text_domains'
        from /usr/lib/ruby/vendor_ruby/gettext/text_domain_manager.rb:81:in `each'
        from /usr/lib/ruby/vendor_ruby/gettext/text_domain_manager.rb:81:in `each_text_domains'
        from /usr/lib/ruby/vendor_ruby/gettext/text_domain_manager.rb:103:in `translate_singular_message'
        from /usr/lib/ruby/vendor_ruby/gettext.rb:118:in `gettext'
        from /usr/share/mikutter/core/plugin/settings/basic_settings.rb:4:in `block in <top (required)>'
        from /usr/share/mikutter/core/plugin.rb:21:in `instance_eval'
        from /usr/share/mikutter/core/plugin.rb:21:in `create'
        from /usr/share/mikutter/core/plugin/settings/basic_settings.rb:3:in `<top (required)>'
        from /usr/lib/ruby/2.1.0/rubygems/core_ext/kernel_require.rb:55:in `require'
        from /usr/lib/ruby/2.1.0/rubygems/core_ext/kernel_require.rb:55:in `require'
        from /usr/share/mikutter/core/plugin/settings/settings.rb:4:in `<top (required)>'
        from /usr/share/mikutter/core/miquire_plugin.rb:138:in `load'
        from /usr/share/mikutter/core/miquire_plugin.rb:138:in `load'
        from /usr/share/mikutter/core/miquire_plugin.rb:130:in `block in load'
        from /usr/share/mikutter/core/miquire_plugin.rb:127:in `each'
        from /usr/share/mikutter/core/miquire_plugin.rb:127:in `load'
        from /usr/share/mikutter/core/miquire_plugin.rb:130:in `block in load'
        from /usr/share/mikutter/core/miquire_plugin.rb:127:in `each'
        from /usr/share/mikutter/core/miquire_plugin.rb:127:in `load'
        from /usr/share/mikutter/core/miquire_plugin.rb:97:in `block in load_all'
        from /usr/share/mikutter/core/miquire_plugin.rb:37:in `block in each_spec'
        from /usr/share/mikutter/core/miquire_plugin.rb:32:in `each'
        from /usr/share/mikutter/core/miquire_plugin.rb:32:in `each'
        from /usr/share/mikutter/core/miquire_plugin.rb:35:in `each_spec'
        from /usr/share/mikutter/core/miquire_plugin.rb:95:in `load_all'
        from /usr/share/mikutter/core/boot/load_plugin.rb:7:in `<top (required)>'
        from /usr/lib/ruby/2.1.0/rubygems/core_ext/kernel_require.rb:55:in `require'
        from /usr/lib/ruby/2.1.0/rubygems/core_ext/kernel_require.rb:55:in `require'
        from /usr/share/mikutter/core/miquire.rb:98:in `miquire_original_require'
        from /usr/share/mikutter/core/miquire.rb:95:in `file_or_directory_require'
        from /usr/share/mikutter/core/miquire.rb:76:in `block in miquire'
        from /usr/share/mikutter/core/miquire.rb:75:in `each'
        from /usr/share/mikutter/core/miquire.rb:75:in `miquire'
        from /usr/share/mikutter/core/miquire.rb:18:in `miquire'
        from /usr/share/mikutter/mikutter.rb:38:in `<main>'
```

言語環境周りぽいなてことで `LANG` を確認してみると空

> 
$ echo $LANG

`LANG` に `ja_JP.UTF-8` を入れてあげると動きました．
```
$ LANG=ja_JP.UTF-8 mikutter
/usr/lib/ruby/vendor_ruby/gtk2.rb: line 13
   Gtk-WARNING **:Locale not supported by C library.
        Using the fallback 'C' locale.
/usr/share/mikutter/core/mui/cairo_cell_renderer_message.rb: line 10
   GLib-GObject-WARNING **:Attempt to add property GtkCellRendererMessage::message-id after class was initialised
/usr/share/mikutter/core/plugin/display_requirements/display_requirements.rb:5: warning: already initialized constant CACHE_DIR
/usr/share/mikutter/core/plugin/aspectframe/aspectframe.rb:12: warning: previous definition of CACHE_DIR was here
/usr/share/mikutter/core/plugin/notify/notify.rb:6: warning: already initialized constant DEFINED_TIME
/usr/share/mikutter/core/plugin/extract/extract.rb:8: warning: previous definition of DEFINED_TIME was here

```

とりあえずは起動メニューにこれを入れてあげると良さそうです．


<iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00H8X84E6" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe><iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00FOSXTVA" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe><iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00A71AKXE" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>
