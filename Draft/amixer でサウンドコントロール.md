# Awesome wm でボリュームコントロール

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/20906066190/in/dateposted-public/" title="IMGP0223_変更済み"><img src="https://farm1.staticflickr.com/746/20906066190_06c1f3882b.jpg" width="500" height="331" alt="IMGP0223_変更済み"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

Gnome Shell や XFce4 などでは NotePC の音量コントロールボタンで音量のUp/Down 及びミュートが操作出来ましたが， Awesome ではそのままでは動作しないようです．ということで設定してみました．

(以下の時の設定では unmute がうまく行っていなかった & 解りやすく修正．)
- [awesome wm を久々に使おうとしたら設定ファイルが使えなくなっていたので再設定 | matoken's meme](http://matoken.org/blog/blog/2015/08/23/awesome-reconfigure/)

## キーコードを調べる

まずはボタンのキーコードを調べました．`xev` コマンドを実行しながら該当ボタンを押して調べます．手元のマシンでは以下のようになりました．


ThinkPad X201s のボリュームキー情報(xev で確認)
- VolUp
```bash
KeyRelease event, serial 33, synthetic NO, window 0x1800001,
    root 0x98, subw 0x0, time 74266491, (715,471), root:(717,492),
    state 0x0, keycode 123 (keysym 0x1008ff13, XF86AudioRaiseVolume), same_screen YES,
    XLookupString gives 0 bytes:
    XFilterEvent returns: False
```
- VolDown
```bash
KeyRelease event, serial 33, synthetic NO, window 0x1800001,
    root 0x98, subw 0x0, time 74268735, (715,471), root:(717,492),
    state 0x0, keycode 122 (keysym 0x1008ff11, XF86AudioLowerVolume), same_screen YES,
    XLookupString gives 0 bytes:
    XFilterEvent returns: False
```
- ミュート
```bash
KeyPress event, serial 30, synthetic NO, window 0x3e00001,
    root 0x98, subw 0x0, time 107269062, (0,480), root:(212,630),
    state 0x0, keycode 121 (keysym 0x1008ff12, XF86AudioMute), same_screen YES,
    XLookupString gives 0 bytes:
    XmbLookupString gives 0 bytes:
    XFilterEvent returns: False
```

ということでこうなりました．
- ミュートボタン -> XF86AudioMute
- 音量Upボタン -> XF86AudioRaiseVolume
- 音量Downボタン -> XF86AudioLowerVolume

## 音量の操作コマンドを調べる

alsa の操作をコマンドで実行できる `amixer` で操作するサウンドカードなどを確認します．`amixer` でサウンドカードを確認して，`amixer -c 0` などとしてサウンドカードの内訳を確認したり出来ます．`-c 0` は0番目のカードという意味になります．2枚目のカードは，`-c 1` というオプションで指定できます．

今回は sound card 0 の Master を操作します．以下のコマンドで情報の表示や操作ができます．

- sound card 0 の Master の状態確認
```bash
% amixer -c 0 get Master
Simple mixer control 'Master',0
  Capabilities: pvolume pvolume-joined pswitch pswitch-joined
  Playback channels: Mono
  Limits: Playback 0 - 74
  Mono: Playback 58 [78%] [-16.00dB] [off]
```

- sound card 0 の Master を無音に
```bash
% amixer -c 0 set Master mute  
Simple mixer control 'Master',0
  Capabilities: pvolume pvolume-joined pswitch pswitch-joined
  Playback channels: Mono
  Limits: Playback 0 - 74
  Mono: Playback 58 [78%] [-16.00dB] [off]
```

- sound card 0 の Master の音を有効に
```bash
% amixer -c 0 set Master unmute
Simple mixer control 'Master',0
  Capabilities: pvolume pvolume-joined pswitch pswitch-joined
  Playback channels: Mono
  Limits: Playback 0 - 74
  Mono: Playback 58 [78%] [-16.00dB] [on]
```

Master の unmute だけだとスピーカーがミュートになったままなのでスピーカーも unmute します．

- sound card 0 の Speaker の音を有効に
```
% amixer -c 0 set Speaker unmute
Simple mixer control 'Speaker',0
  Capabilities: pvolume pswitch
  Playback channels: Front Left - Front Right
  Limits: Playback 0 - 74
  Mono:
  Front Left: Playback 74 [100%] [0.00dB] [on]
  Front Right: Playback 74 [100%] [0.00dB] [on]
```

- sound card 0 の Master の音量を 2dB 上げる
```bash
% amixer -c 0 set Master 2dB+
Simple mixer control 'Master',0
  Capabilities: pvolume pvolume-joined pswitch pswitch-joined
  Playback channels: Mono
  Limits: Playback 0 - 74
  Mono: Playback 60 [81%] [-14.00dB] [off]
```

- sound card 0 の Master の音量を 2dB 下げる
```bash
% amixer -c 0 set Master 2dB-
Simple mixer control 'Master',0
  Capabilities: pvolume pvolume-joined pswitch pswitch-joined
  Playback channels: Mono
  Limits: Playback 0 - 74
  Mono: Playback 58 [78%] [-16.00dB] [off]
```

## Script にする


`~/script/awesome-audio.bash` という名前のscript にしました．引数にキーコードを渡して実行すると，ミュートボタンを押すとミュート状態ならミュートを解除，そうでないならミュートに，音量ボタンで 2dB ずつUp/Down するようになっています．
それだけだと状態がわかりにくかったので，`notify-send` コマンドでアイコンとメッセージを表示するようにしました．

メッセージの表示は awesome 的には `naughty.notify` で行うのが正しいと思うのですが，使い方がよく解らずorz
まあ `notify-send` なら awesome 以外の環境でも動くのでいいかなと．

```bash
#!/bin/bash

case "$1" in
  "XF86AudioMute" )
    MUTE=`amixer -c 0 get Master|tail -1|cut -d '[' -f 4|sed s/\]//`
    if [ $MUTE = "on" ] ; then
      amixer -q -c 0 set Master mute
      echo -e "🔊☓\nmute!"
      notify-send -u low -t 500 -i '/usr/share/icons/ContrastHigh/scalable/status/audio-volume-muted.svg' mute "☓"
    else
      amixer -q -c 0 set Master unmute
      amixer -q -c 0 set Speaker unmute
      echo -e "🔊\nunmute!"
      amixer -c 0 get Master | tail -1 | cut -d '[' -f 2 | sed s/\]// | xargs notify-send -u low -t 500 -i '/usr/share/icons/ContrastHigh/scalable/status/audio-volume-high.svg' numute
    fi
  ;;
  "XF86AudioRaiseVolume" )
    amixer -c 0 set Master 2dB+ | tail -1 | cut -d '[' -f 2 | sed s/\]// | xargs notify-send -u low -t 500 -i '/usr/share/icons/ContrastHigh/scalable/status/audio-volume-medium.svg' Vol
  ;;
  "XF86AudioLowerVolume" )
    amixer -c 0 set Master 2dB- | tail -1 | cut -d '[' -f 2 | sed s/\]// | xargs notify-send -u low -t 500 -i '/usr/share/icons/ContrastHigh/scalable/status/audio-volume-medium.svg' Vol
  ;;
esac

amixer -c 0 get Master | tail -1 | cut -d '[' -f 2 | sed s/\]//

```

以下のように実行して動作確認しておきます．
```
% ~/script/awesome-audio.bash XF86AudioMute
🔊☓
mute!
70%
% ~/script/awesome-audio.bash XF86AudioMute
🔊
unmute!
70%
% ~/script/awesome-audio.bash XF86AudioRaiseVolume
73%
% ~/script/awesome-audio.bash XF86AudioLowerVolume
70%
```

`notify-send` でデスクトップには以下のような通知が表示されます．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/20469895304/in/dateposted-public/" title="20150903_03:09:19-8320"><img src="https://farm6.staticflickr.com/5793/20469895304_93cac39ea4_o.jpg" width="99" height="58" alt="20150903_03:09:19-8320"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script><a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/20471451563/in/dateposted-public/" title="20150903_03:09:04-9640"><img src="https://farm6.staticflickr.com/5738/20471451563_25c278c2b7_o.jpg" width="115" height="58" alt="20150903_03:09:04-9640"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script><a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/21092530935/in/dateposted-public/" title="20150903_03:09:05-10362"><img src="https://farm6.staticflickr.com/5770/21092530935_921747f6a4_o.jpg" width="91" height="58" alt="20150903_03:09:05-10362"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script><a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/20904441840/in/dateposted-public/" title="20150903_03:09:24-10606"><img src="https://farm1.staticflickr.com/700/20904441840_90d249acd1_o.jpg" width="91" height="58" alt="20150903_03:09:24-10606"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

## awesome の設定を行う

音量コントロールボタンで script を呼ぶように awesome の設定ファイル `~/.config/awesome/rc.lua` に以下の設定を追記します．追記場所は ``-- Standard program` の後ろ辺りでok です．

```lua
    -- Audio Controle
    awful.key({         }, "XF86AudioMute", function () awful.util.spawn("/home/mk/script/awesome-audio.bash XF86AudioMute",false)       end),
    awful.key({         }, "XF86AudioRaiseVolume", function () awful.util.spawn("/home/mk/script/awesome-audio.bash XF86AudioRaiseVolume",false) end),
    awful.key({         }, "XF86AudioLowerVolume", function () awful.util.spawn("/home/mk/script/awesome-audio.bash XF86AudioLowerVolume",false) end),
```

ここまで設定できたら awesome を restart します．

>
**メニュー -> awesome -> restart**

音量コントロールボタンで操作ができるようになったら出来上がりです :)


- [Linux/WindowManager/awesome/3.5 - matoken's wiki.](http://hpv.cc/~maty/pukiwiki1/index.php?Linux%2FWindowManager%2Fawesome%2F3.5)
