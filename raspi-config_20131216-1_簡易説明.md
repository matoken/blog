# raspi-config 20131216-1 簡易説明

B+ も出たしそろそろ新しいバージョンが出そうな気がするけど…


```
┌──────────────────────────────┤ Raspberry Pi Software Configuration Tool (raspi-config) ├──────────────────────────────┐
│ Setup Options                                                                                                         │
│                                                                                                                       │
│    1 Expand Filesystem              Ensures that all of the SD card storage is available to the OS                    │
│    2 Change User Password           Change password for the default user (pi)                                         │
│    3 Enable Boot to Desktop/Scratch Choose whether to boot into a desktop environment, Scratch, or the command-line   │
│    4 Internationalisation Options   Set up language and regional settings to match your location                      │
│    5 Enable Camera                  Enable this Pi to work with the Raspberry Pi Camera                               │
│    6 Add to Rastrack                Add this Pi to the online Raspberry Pi Map (Rastrack)                             │
│    7 Overclock                      Configure overclocking for your Pi                                                │
│    8 Advanced Options               Configure advanced settings                                                       │
│    9 About raspi-config             Information about this configuration tool                                         │
│                                                                                                                       │
│                                                                                                                       │
│                                  <Select>                                  <Finish>                                   │
│                                                                                                                       │
└───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
                                                                                                                         

```

## 1 Expand Filesystem
2GB より大きな容量の SD Card を利用した場合 2GB しか利用できないので前領域が利用できるように領域確保します．
NOOBS を使って Rasbian を導入した場合は NOOBS が自動的に面倒を見てくれるので利用する必要はありません．

## 2 Change User Password
パスワードの変更が出来ます．

## 3 Enable Boot to Desktop/Scratch Choose whether to boot into a desktop environment, Scratch, or the command-line
起動時にデスクトップ，Scratch，コマンドラインのどれかで起動できるように設定します．規定値はコマンドラインです．

### Console Text console, requiring login (default)
テキストコンソールを起動するよう設定します（規定値)

### Desktop Log in as user 'pi' at the graphical desktop
pi ユーザでデスクトップ環境を自動起動するよう設定します．

### Scratch Start the Scratch programming environment upon boot
スクラッチを起動するよう設定します．

## 4 Internationalisation Options
国際化関連の設定メニューです．

### I1 Change Locale
ロケールの設定を行います．日本語は **ja_JP.UTF-8** です．
コンソールをメインに使う場合は標準では日本語が表示できないので **en_US.UTF-8** にしたほうがいいかもしれません．コンソールで日本語を表示するには後述の **fbterm** の使い方を参照してください．

コマンドラインで以下のコマンドでも設定可能です．（Debianでの設定方法）
`$ sudo dpkg-reconfigure locales`

### I2 Change Timezone
タイムゾーンの変更が出来ます．
日本時間にする場合は， **Asia->Tokyo** を選択します．

コマンドラインで以下のコマンドでも設定可能です．（Debianでの設定方法）
`$ sudo dpkg-reconfigure tzdata`

### I3 Change Keyboard Layout
キーボードのキーマップを変更出来ます．
コマンドラインで以下のコマンドでも設定可能です．（Debianでの設定方法）
`$ sudo dpkg-reconfigure keyboard-configuration`

日本語のキーマップがメニューに出てこない場合は，`/etc/default/keyboard` を以下のように変更します．

```
XKBMODEL="jp106"
XKBLAYOUT="jp"
```
その後以下のコマンドで反映します．
`$ sudo /etc/init.d/keyboard-setup restart`

## 5 Enable Camera
RaspberryPi 標準のカメラモジュールを利用できるようにします．USB 経由のカメラなどは Disable のままでOK です．

## 6 Add to Rastrack
Rastrack (http://rastrack.co.uk) への登録が出来ます．

## 7 Overclock
オーバークロックが行えます．オーバークロックを行うと RaspberryPi の寿命が縮む可能性があるのに注意してください．また，不安定になる可能性があります．不安定になったら控えめなオーバークロックを試してみてください．
http://elinux.org/RPi_Overclocking に更に解説があります．

## 8 Advanced Options
### A1 Overscan
画面に黒い帯が表示されている場合に改善されます．**/boot/config.txt** で微調整も可能です．（恐らくコンポジット出力のための設定）

### A2 Hostname
ホスト名を変更できます．ホスト名は大文字込時を区別しないASCII文字の aからz，数字の 0から9 及びハイフンg利用できます．但しハイフンは先頭と最後には利用できません．

### A3 Memory Split
GPU に割り当てるメモリを設定します．16/32/64/128/256MB が指定でき，規定値は128MB です．割り当てたメモリはメインメモリから割り当てられるのでその分メインメモリの割当量が減ります．

### A4 SSH
ssh server の有効，無効の設定が出来ます．規定値では有効です．

### A5 SPI
SPI の自動読み込みの有効，無効の設定が出来ます．

### A6 Audio
オーディオ出力は自動的に切り替わりますが，強制設定が出来ます．

### A7 Update
OS のアップデートを行います．
apt-get や aptitude コマンドを実行してもいいです．

## 9 About raspi-config
このコマンドの説明が表示されます．

