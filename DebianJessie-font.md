ある時からDebianJessieのあちこちのフォントが中華な感じに．

<a href="https://www.flickr.com/photos/119142834@N05/16066993296" title="Screenshot from 2014-12-24 14:37:17 by Kenichiro MATOHARA, on Flickr"><img src="https://farm9.staticflickr.com/8569/16066993296_7d19a0c4b9_n.jpg" width="320" height="205" alt="Screenshot from 2014-12-24 14:37:17"></a><a href="https://www.flickr.com/photos/119142834@N05/15906692799" title="Screenshot from 2014-12-24 14:36:28 by Kenichiro MATOHARA, on Flickr"><img src="https://farm9.staticflickr.com/8581/15906692799_87ee30cb61_o.png" width="46" height="122" alt="Screenshot from 2014-12-24 14:36:28"></a>

確認すると DejaVu というフォントらしい．

- [DejaVu](http://dejavu-fonts.org/wiki/Main_Page "DejaVu")

LibO で設定を変えてみても恐らく和文フォントを設定されていないテンプレートを利用した時などに戻ってしまったり．Chromium で設定を変えみても反映されなかったりでした．
<a href="https://www.flickr.com/photos/119142834@N05/15905501460" title="Screenshot from 2014-12-24 14:49:03 by Kenichiro MATOHARA, on Flickr"><img src="https://farm9.staticflickr.com/8569/15905501460_f5f8e14deb_n.jpg" width="320" height="225" alt="Screenshot from 2014-12-24 14:49:03"></a><a href="https://www.flickr.com/photos/119142834@N05/15907089757" title="Screenshot from 2014-12-24 15:06:24 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7564/15907089757_0f89b3087a_n.jpg" width="304" height="320" alt="Screenshot from 2014-12-24 15:06:24"></a>

とりあえずこのフォントは使わないなってことで消して対処療法…．
```sh
% sudo apt purge fonts-dejavu fonts-dejavu-core fonts-dejavu-extra ttf-dejavu ttf-dejavu-core ttf-dejavu-extra
```

