<iframe src="//www.slideshare.net/slideshow/embed_code/43898360" width="425" height="355" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/matoken/ss-43898360" title="マウスを使わないでキーボードで快適に生活する方法(GNU Screen/tmux/byobu/awesome)" target="_blank">マウスを使わないでキーボードで快適に生活する方法(GNU Screen/tmux/byobu/awesome)</a> </strong> from <strong><a href="//www.slideshare.net/matoken" target="_blank">Kenichiro MATOHARA</a></strong> </div>

鹿児島らぐ 鹿児島Linux勉強会 -第09回- での発表スライド
- [鹿児島Linux勉強会 - connpass](http://kagolug.connpass.com/event/11078/)


コミックマーケットの報告と 
ターミナルマルチプレクサのGNU Screen/tmuxやヘルパーアプリのbyobu 
タイル型ウィンドウマネージャのawesome の紹介です．


<iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00S5TM46Q" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe> <iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=4873114950" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe> <iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00AVA3A38" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>
