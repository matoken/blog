<a href="https://www.flickr.com/photos/119142834@N05/17312706255" title="Screenshot_2015-04-21-20-55-30 by Kenichiro MATOHARA, on Flickr"><img src="https://farm9.staticflickr.com/8778/17312706255_4c93cd9583.jpg" width="281" height="500" alt="Screenshot_2015-04-21-20-55-30"></a>
LGL22 にmineo のSIM を挿して運用していますが，どうもGPS の動作がおかしい．[Ingress](https://play.google.com/store/apps/details?id=com.nianticproject.ingress) や [GPS Status & Toolbox](https://play.google.com/store/apps/details?id=com.eclipsim.gpsstatus2) を起動して1時間とかずっと置いといても測位できなかったりする．
以下のページを見るとキャリアが用意したA-GPS 情報を配布しているサーバは外からアクセスできなくてMVNO だと使えない，Google提供のA-GPS サーバがあってこれは外から使えるらしいということが分かりました．A-PGS 使えなかった時はタイムアウトして欲しい気もしますが…．

- [トークセッション. MVNOとGPSについて (大内)](http://techlog.iij.ad.jp/archives/1293#session3)
> 趣旨:「MVNOではGPSは使えない」という誤解がありますが、正しくありません。しかし、一部のスマホでGPSの位置情報がうまく利用できないように見えるのも事実です。Androidを例に位置情報取得の仕組みとその原因についてご紹介します。

ということで，`/etc/gps.conf` のSUPL 部分を以下のように書き換えてみました．

<script src="https://gist.github.com/matoken/17e5f4e90ec3116892c0.js"></script>

`SUPL_HOST=supl.google.com`　と `SUPL_PORT=7275` or `SUPL_PORT=7276` 部分．書き換えた後再起動しても症状は変わらないよう．

Cinnamon Mod だと元々Google のサーバを向いているとのことなので CM12 をそのまま真似してみる．

<script src="https://gist.github.com/matoken/78c25518560797982467.js"></script>

これで数日使ってみているけど今のところ問題無さそう．
ただ，この方法は /system 領域の編集が必要なので通常は要root
もうすぐキャリア端末のSIM Free がやりやすくなるようですが，この辺りも解決して欲しいですね．APN みたいに設定画面から書き換えできるようにするとか．

以下は書き換え時のメモです．
```bash
$ ./adb shell
$ su
# mount -o rw,remount /system
# cp -p /etc/gps.conf /etc/gps.conf.org
# vi /etc/gps.conf
# cat /etc/gps.conf
XTRA_SERVER_1=http://xtra1.gpsonextra.net/xtra.bin
XTRA_SERVER_2=http://xtra2.gpsonextra.net/xtra.bin
XTRA_SERVER_3=http://xtra3.gpsonextra.net/xtra.bin
SUPL_HOST=supl.google.com
SUPL_PORT=7276
# mount -o ro,remount /system
# exit
$ exit
```

<iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00IHW5MLK" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe><iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=4789845451" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe><iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=4822296148" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>


