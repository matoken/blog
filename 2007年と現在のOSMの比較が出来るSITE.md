
Twitter の #osmjp ハッシュタグで以下の書き込みを見かけました．
<blockquote class="twitter-tweet" lang="en"><p>2007年のOSMデータといまのデータ。ヨコに並べるとまさに隔世の感、あります。 <a href="http://t.co/7OWoJSh1eI">http://t.co/7OWoJSh1eI</a> <a href="https://twitter.com/hashtag/osmjp?src=hash">#osmjp</a></p>&mdash; nyampire (@nyampire) <a href="https://twitter.com/nyampire/statuses/493052373003010048">July 26, 2014</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

九州の辺りをみると真っ白><
<a href="https://www.flickr.com/photos/119142834@N05/14779982412" title="Screenshot from 2014-07-30 06:49:31 by Kenichiro MATOHARA, on Flickr"><img src="https://farm4.staticflickr.com/3926/14779982412_6a83e84cec.jpg" width="500" height="251" alt="Screenshot from 2014-07-30 06:49:31"></a>
"OSM Then And Now" http://mvexel.github.io/thenandnow/#8/32.697/130.897

OSM を知ったのは2006年頃だったのですが，当時はこんな感じでデータのインポートもトレースも出来ず基本GPSロガーを使ってマッピングするしか無い頃でした．
これは無理だ><って感じでなかなか手を出してなかったんですが数年でかなり埋まってすごいなと思ったもんでした．もちろん今の時点でもまだまだ建物の情報やPOIも少ないのですが建物は衛星写真や基板地図情報のトレースも出来ますし，スマホが普及してPOIの入力もお手軽に出来るようになって編集方法もすごくお手軽になってます．

そのうち自分のやっている入力方法もまとめてみたいところです．
