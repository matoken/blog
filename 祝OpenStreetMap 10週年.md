
8/9はOpenStreetMap 10周年ということで世界各地でパーティが開かれました．

[OpenStreetMap 10th Anniversary Birthday party - OpenStreetMap Wiki](http://wiki.openstreetmap.org/wiki/OpenStreetMap_10th_Anniversary_Birthday_party "OpenStreetMap 10th Anniversary Birthday party - OpenStreetMap Wiki")

このページを見て，地図上にピンを立ててみると楽しいかもしれないと思い日本のパーティ会場をプロットしてみました．
<blockquote class="twitter-tweet" lang="en"><p>今度の土曜のOSM 10周年パーティ会場をプロット <a href="http://t.co/NYCqi01S5R">http://t.co/NYCqi01S5R</a> <a href="https://twitter.com/hashtag/osmjp?src=hash">#osmjp</a></p>&mdash; (「ΦωΦ)「 (@matoken) <a href="https://twitter.com/matoken/statuses/497361902813454337">August 7, 2014</a></blockquote>

<iframe width="100%" height="300px" frameBorder="0" src="http://umap.openstreetmap.fr/ja/map/openstreetmap-10th-anniversary-birthday-party-for-_14105?scaleControl=false&miniMap=false&scrollWheelZoom=false&zoomControl=true&allowEdit=false&moreControl=true&datalayersControl=true&onLoadPanel=undefined"></iframe>
<p><a href="http://umap.openstreetmap.fr/ja/map/openstreetmap-10th-anniversary-birthday-party-for-_14105">フルスクリーン表示</a>

これを見た @ikiyaさんが世界版やりませんか?ってことで全世界分もプロットしてみることに．

<blockquote class="twitter-tweet" lang="en"><p><a href="https://twitter.com/matoken">@matoken</a> ありがとうございます！　相談ですがせっかくなのでドンとこのumapで全世界やりませんか？　<a href="http://t.co/lOh0iuvX7F">http://t.co/lOh0iuvX7F</a>　</p>&mdash; ikiya (@ikiya) <a href="https://twitter.com/ikiya/statuses/497382876170579968">August 7, 2014</a></blockquote><blockquote class="twitter-tweet" lang="en"><p><a href="https://twitter.com/ikiya">@ikiya</a> 浜松反映しましたー．そんなに数多くないので全世界も行けそうですね．今出先なので帰ってから試してみますー</p>&mdash; (「ΦωΦ)「 (@matoken) <a href="https://twitter.com/matoken/statuses/497384052228243457">August 7, 2014</a></blockquote><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

で，出来たのがこれです．

<iframe width="100%" height="300px" frameBorder="0" src="http://umap.openstreetmap.fr/ja/map/openstreetmap-10th-anniversary-birthday-party_14139?scaleControl=false&miniMap=false&scrollWheelZoom=false&zoomControl=true&allowEdit=false&moreControl=true&datalayersControl=true&onLoadPanel=undefined"></iframe><p><a href="http://umap.openstreetmap.fr/ja/map/openstreetmap-10th-anniversary-birthday-party_14139">フルスクリーン表示</a>

WikiPage に載せてもらったり，
- [OpenStreetMap 10th Anniversary Birthday party - OpenStreetMap Wiki](http://wiki.openstreetmap.org/wiki/OpenStreetMap_10th_Anniversary_Birthday_party "OpenStreetMap 10th Anniversary Birthday party - OpenStreetMap Wiki")

本家ML に投稿してもらったりしました．
- [[OSM-talk] OSM 10th Anniversary Birthday party World Map](https://lists.openstreetmap.org/pipermail/talk/2014-August/070411.html "[OSM-talk] OSM 10th Anniversary Birthday party World Map")

初め世界中合わせてもそんなに数ないからすぐだなーとか思っていたのですが，よく見るとアメリカは別ページにまとめられていたり，座標がないところは地名から探したりと結構時間かかっちゃいました．
＃実はOSM 使わずにGoogleMap 使っているところも多かった…
uMap はgeojson の読み込みとかもできるのでもし座標データがあれば機械的にマッピングも出来るんですけど今回は手作業でした><



さて，自分はというと台風で外に出られないこともあってサーベイは諦めて桜島の東側から基板地図情報2500をトレースして黒神の辺りまで入力してました．（埋没鳥居で有名かも）．早いとこ桜島トレース終わらせたいです．

<iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=4048867733" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe><iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=4844396218" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe><iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00D0SEGMC" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>
