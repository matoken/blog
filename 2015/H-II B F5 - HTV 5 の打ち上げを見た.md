とはいっても家のすぐ側の射場から 100km ほど離れた場所からです．鹿児島に住んでいるので晴れていれば見晴らしのいい場所でなら大抵見られます．日中打ち上げで福岡で H-II A を見た人も居たはず．

とはいえ家は山に囲まれているので出来れば佐多岬かせめて太平洋沿岸まで出たいと思っていたのですが，日が暮れてから雨が降ってきたので今回は観測できないかなーと思って家でゴロゴロしていました．うと上げ30分位前に外に出てみると星が見えます．これは行けるか?ってことで慌てて家の側の丘になっている場所へ移動．地理院地図を見るとここは標高 210m ほどのようです．
見学場所に付いたの打ち上げ1,2分前．これはカメラの準備は無理か?と思いなが三脚立てたりしてました．三脚にカメラ乗せてこっちかなーと真南あたりに向けていたのですが，思ったよりずっと東の方角が明るく．慌ててカメラの角度を変えて撮影開始．とりあえず 24mm(35mm換算36mm) 残しなのレンズに Pentax K-5 で F2.4 ISO6400 露出5秒で撮影．インターバルはレリーズ接続が間に合わなかったので手動で……．三脚の足場や固定が甘い&手動でシャッター切ってたのでブレブレでしたorz

雨が降った割には雲も少なく肉眼では噴射炎の形や何かが分離して分離したものが暫く光っているのが見えたり(時間的に SRB-A だと思う)しましたがこの撮り方では写らず．24mm は適当に手持ちのを選んだのですがちょうどいい感じでした．少なくとも肉眼で見えなくなるまでの画角は賄えました．
(雲があるとあっという間に見えなくなって悲しいのですよね……)

次回はビデオと一眼レフx2 とかで狙ってみたいところです．

これは比較明合成してみたもの
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/20717829351/in/dateposted/" title="H-IIBF5_比較明"><img src="https://farm6.staticflickr.com/5746/20717829351_4c54bae4d8.jpg" width="500" height="331" alt="H-IIBF5_比較明"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

こちらはアニメーションにしてみたものです．
<iframe width="420" height="315" src="https://www.youtube.com/embed/t-LcWeiG9Bo" frameborder="0" allowfullscreen></iframe>


これは打ち上げ後に10分ほど星景を撮ったもの．飛行機と衛星らしきものが横切っていきます．こういうのだと24mm は物足りないですね．
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/20717954681/in/dateposted/" title="星景比較明合成20150820"><img src="https://farm6.staticflickr.com/5764/20717954681_4674b967c2.jpg" width="500" height="331" alt="星景比較明合成20150820"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<iframe width="420" height="315" src="https://www.youtube.com/embed/yvS63BiY390" frameborder="0" allowfullscreen></iframe>


合成方法はこの辺りを(最近この辺りは調べてないのでもっといいものがあるかもしれません．)
- [インターバル撮影/比較明合成 - matoken's wiki.](http://hpv.cc/~maty/pukiwiki1/index.php?%A5%A4%A5%F3%A5%BF%A1%BC%A5%D0%A5%EB%BB%A3%B1%C6%2F%C8%E6%B3%D3%CC%C0%B9%E7%C0%AE)
- [衛星をインターバル撮影して合成](http://www.slideshare.net/matoken/iss-33569833)


