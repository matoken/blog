ReText 5.1.0 を翻訳してみました．そのとき遠回りしたのでメモしておきます．

このアプリケーションの翻訳ファイルは Qt の .ts を翻訳して .qm で利用する形のようです．この辺りは以下のエントリを参照して下さい．

- [翻訳ファイルの .ts の翻訳方法 | matoken's meme](http://matoken.org/blog/blog/2015/09/17/ts-method-of-the-translation-of-the-translation-file/)

先ずは ReText の source を入手．

```
% git clone https://github.com/retext-project/retext.git
% cd retext/locale
% linguist retext_ja.ts
```

これで一旦[翻訳](https://gist.github.com/matoken/96de208921442b511477)してみましたが， git log 見て Transifex 使ってるぽいのを見つけましたorz

>     locale: Updated French translation from Transifex

Transifex を見に行くとありましたorz

- [ReTextの多国語化](https://www.transifex.com/mitya57/ReText/﻿)

とりあえず未翻訳でわかる部分は翻訳しておきました．結果をダウンロードして反映してみると大体日本語化出来た感じですが，以下の部分は翻訳しないほうがいいと思うのですが ReText の中に見当たりません．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/20905118613/in/dateposted-public/" title="corp"><img src="https://farm6.staticflickr.com/5691/20905118613_5c0c24d201_o.png" width="336" height="339" alt="corp"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

.ts や source 全部から検索
```
% grep ページ ./locale/retext_ja.ts
% grep -i PageDown ./locale/retext_ja.ts
% find . -type f | xargs grep -i PageDown    
./ReText/window.py:                     lambda: self.switchTab(1), shct=Qt.CTRL+Qt.Key_PageDown)
```

ReText パッケージ全部から検索
```
% dpkg -L retext | xargs -I{} sh -c "if test -f {} ; then grep ページ {} ; fi"

```

ベースの Qtの方かなと思って，全 .ts からも探してみます．
```
% find /usr/share -type f -name "*_ja.ts"| xargs grep 'ページ'
find: `/usr/share/doc/google-chrome-stable': 許可がありません
/usr/share/skype/lang/skype_ja.ts:        <translation>ページ%1</translation>
/usr/share/skype/lang/skype_ja.ts:        <translation>ホームページ:</translation>
/usr/share/skype/lang/skype_ja.ts:        <translation>ダウンロードページを開く</translation>
/usr/share/skype/lang/skype_ja.ts:        <translation>アカウントページの閲覧</translation>
```

.ts が用意されていないパッケージにあるのかもしれないと .qm から .ts に変換する方法を探すと `lconvert` コマンドを見つけました．( 簡単な利用方法は次のエントリに追記した  [翻訳ファイルの .ts の翻訳方法 | matoken's meme](http://matoken.org/blog/blog/2015/09/17/ts-method-of-the-translation-of-the-translation-file/) )


`*_ja.qm` を.ts にして検索すると発見しました．

```
% find /usr/share -type f -name "*_ja.qm" | xargs -n1 -I{} lconvert -if qm -i {} -of ts -o - | grep 'ページアップ'
        <translation>ページアップ</translation>
        <translation>ページアップ</translation>
% find /usr/share -type f -name "*_ja.qm" | xargs -n1 -I{} lconvert -if qm -i {} -of ts -o - | grep 'ページダウン'
        <translation>ページダウン</translation>
        <translation>ページダウン</translation>
```

これはどのファイルに含まれているのかを確認すると `/usr/share/qt5/translations/qtbase_ja.qm`
```
% find /usr/share -type f -name "*_ja.qm" | xargs -n1 -I{} sh -c "lconvert -if qm -i {} -of ts -o - | grep 'ページアップ'; echo {}"

/usr/share/gnuplot/gnuplot/4.6/qt/qtgnuplot_ja.qm
/usr/share/virtualbox/nls/VirtualBox_ja.qm
/usr/share/virtualbox/nls/qt_ja.qm
/usr/share/qt5/translations/qtmultimedia_ja.qm
/usr/share/qt5/translations/linguist_ja.qm
/usr/share/qt5/translations/qtdeclarative_ja.qm
/usr/share/qt5/translations/qtquick1_ja.qm
/usr/share/qt5/translations/qmlviewer_ja.qm
/usr/share/qt5/translations/qtconfig_ja.qm
/usr/share/qt5/translations/qtscript_ja.qm
/usr/share/qt5/translations/qt_ja.qm
/usr/share/qt5/translations/qtquickcontrols_ja.qm
/usr/share/qt5/translations/assistant_ja.qm
/usr/share/qt5/translations/designer_ja.qm
/usr/share/qt5/translations/qtxmlpatterns_ja.qm
        <translation>ページアップ</translation>
        <translation>ページアップ</translation>
/usr/share/qt5/translations/qtbase_ja.qm
/usr/share/qt5/translations/qt_help_ja.qm
/usr/share/gnuplot5/gnuplot/5.0/qt/qtgnuplot_ja.qm
/usr/share/libqtxdg/libqtxdg_ja.qm
/usr/share/librazorqt/librazorqt_ja.qm
/usr/share/owncloud/i18n/client_ja.qm
/usr/share/skype/lang/skype_ja.qm

% lconvert -if qm -i /usr/share/qt5/translations/qtbase_ja.qm -of ts -o - | grep 'ページアップ'

        <translation>ページアップ</translation>
        <translation>ページアップ</translation>
% lconvert -if qm -i /usr/share/qt5/translations/qtbase_ja.qm -of ts -o - | grep 'ページダウン'

        <translation>ページダウン</translation>
        <translation>ページダウン</translation>
```

このファイルはどのパッケージに含まれているか確認すると `qttranslations5-l10n` でした．
```
% apt-file search qtbase_ja.qm
qttranslations5-l10n: /usr/share/qt5/translations/qtbase_ja.qm
```

Transifex を探すと qttranslations の qtbase にそれらしいものがありました．

- [qttranslationsの多国語化](https://www.transifex.com/qt-users-jp/qtbase/)

該当部分はここ
- [qttranslations | Transifexエディタ](https://www.transifex.com/qt-users-jp/qtbase/viewstrings/#ja/qtbase/18517188?q=PgUp)
- [qttranslations | Transifexエディタ](https://www.transifex.com/qt-users-jp/qtbase/viewstrings/#ja/qtbase/18517189?q=PgDown)

.ts をダウンロードして該当部分を以下のように書き換えて

```
@@-5569,12 +5571,12 @@
     <message>
         <location line="+1"/>
         <source>PgUp</source>
-        <translation>ページアップ</translation>
+        <translation type="unfinished"></translation>
     </message>
     <message>
         <location line="+1"/>
         <source>PgDown</source>
-        <translation>ページダウン</translation>
+        <translation type="unfinished"></translation>
     </message>
     <message>
         <location line="+1"/>
```

.qm を作って反映してみると，
```
% sudo cp ~/Downloads/for_use_qtbase_qtbase_ja.qm /usr/share/qt5/translations/qtbase_ja.qm
```

以下のように反映されました．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/21515112352/in/dateposted-public/" title="20150919_09:09:58-6829"><img src="https://farm6.staticflickr.com/5748/21515112352_6324fbbda1_o.jpg" width="341" height="350" alt="20150919_09:09:58-6829"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

とりあえず「翻訳のために qttranslations team チームに参加」を押して申請をだしてみました．

