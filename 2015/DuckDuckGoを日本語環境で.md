DuckDuckGo という検索エンジンがあります．検索エンジンといえばGoogle一強という感じですがその分プライバシーがGoogleに筒抜けだったり蓄積されたりするのが嫌かもしれません(その分その人に合う検索結果が帰ってくるはずですが)．それに対してDuckDuckGo はユーザプロファイルを作らず誰に対しても同じキーワードから同じ検索結果を返します．
DuckDuckGo使いたいけど日本語の情報が出てこないから使い物にならないって人が居たので日本語の検索結果を表示する設定が知られて無さそうということで最近使っている設定をメモしておきます．

https://duckduckgo.com/?q=keyword&kp=-1&kl=jp-jp&kc=1&kf=-1&kh=1&k1=-1
この例だと以下のように日本語の検索結果を返すようになっています．

q=検索キーワード
kp=-1	セーフサーチ無効
kl=jp-jp	日本向けの検索結果
kc=1	自動先読み有効
kf=-1	サイトアイコン有効
kh=1	https有効
k1=-1	広告無効

ブックマークツールバーなどに以下を登録しておくといい感じです．
https://duckduckgo.com/?q=keyword&kp=-1&kl=jp-jp&kc=1&kf=-1&kh=1&k1=-1

常用するなら以下のように規定の検索エンジンを差し替えてしまうといいでしょう．
<a href="https://www.flickr.com/photos/119142834@N05/15832522433" title="Screenshot from 2015-02-06 06:54:45 by Kenichiro MATOHARA, on Flickr"><img src="https://farm9.staticflickr.com/8633/15832522433_2d758f4503.jpg" width="500" height="412" alt="Screenshot from 2015-02-06 06:54:45"></a>
<a href="https://www.flickr.com/photos/119142834@N05/16265172390" title="Screenshot from 2015-02-06 06:55:20 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7452/16265172390_0c843f2fe4.jpg" width="399" height="263" alt="Screenshot from 2015-02-06 06:55:20"></a>
<a href="https://www.flickr.com/photos/119142834@N05/16450847011" title="Screenshot from 2015-02-06 06:56:17 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7297/16450847011_fcbed2efda.jpg" width="500" height="205" alt="Screenshot from 2015-02-06 06:56:17"></a>

<iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=4774167533" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe> <iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=4048662023" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe> <iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=4822284611" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>
