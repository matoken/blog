<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/2545627936/in/dateposted/" title="IMGP5154"><img src="https://farm4.staticflickr.com/3071/2545627936_4b70ee9feb.jpg" width="500" height="334" alt="IMGP5154"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

EncFS という恐らく Linux のみで動作する暗号化ファイルシステムがあります．

＜追記＞  
コメントで Windows の移植版を教えていただきました :)  
- [EncFS4Winy ファイル暗号化 « 座間ソフト](http://zamasoft.net/encfs4winy-%e6%9a%97%e5%8f%b7%e5%8c%96%e3%83%95%e3%82%a1%e3%82%a4%e3%83%ab%e3%82%b7%e3%82%b9%e3%83%86%e3%83%a0/)  
＜／追記＞

これは FUSE 経由で通常のファイルシステムの上に暗号化ファイルシステムを作成するもので，設定した暗号化領域のディレクトリを Encfs でマウントすることで透過的に暗号化ファイルシステムが利用できます．ただし，ファイル名暗号化時にはメタデータをファイル名に含めるためファイル名長が短く制限されるなどの制限はあります．このあたりは eCryptFS と同じですね．私はオンラインストレージ領域などに利用しています．（同期がファイル単位の場合 EncFS はお勧め）

オプションを眺めていて EncFS コマンドの `--reverse` というオプションに気づきました．

>   --reverse             reverse encryption

```
Reverse mode

encfs --reverse provides an encrypted view of an unencrypted folder. This enables encrypted remote backups using standard tools like rsync.
```

この EncFS の Reverse mode はその名の通り，通常の非暗号化領域をマウントして EncFS による暗号化領域を提供してくれるようです．例えば rsync のバックアップ時に利用して，バックアップ経路からバックアップ先まで暗号化すると行ったことが出来ます．(タイムスタンプやパーミッションはそのままなので rsync の差分バックアップも問題なく行える)

## 利用例

初回起動時は要設定ですが，2回目からはパスワードの入力だけでOK です．

- マウント対象: ~/
- マウントポイント: /tmp/encfs

```
% mkdir -m 700 /tmp/encfs
% encfs -i 10 --reverse ~/ /tmp/encfs
新しい暗号化ボリュームを作成します。
Please choose from one of the following options:
 enter "x" for expert configuration mode,
 enter "p" for pre-configured paranoia mode,
 anything else, or an empty line will select standard mode.
?> x

手動設定モードが選択されました
以下の暗号アルゴリズムが使用できる :
1. AES : 16 byte block cipher
 -- 鍵長 128 から 256 ビットをサポート
 -- ブロックサイズ 64 から 4096 バイトをサポート
2. Blowfish : 8 byte block cipher
 -- 鍵長 128 から 256 ビットをサポート
 -- ブロックサイズ 64 から 4096 バイトをサポート

使用するアルゴリズムの番号を入力してください: 1

アルゴリズム "AES" が選択されました

鍵サイズをビット単位で指定してください。選択された
暗号アルゴリズムは 128 から 256 ビット (64 ビット間隔) の
鍵サイズをサポートしています。
例えば:
128, 192, 256
使用する鍵サイズ: 256

鍵サイズ 256 ビットを使用します
ブロックサイズをバイト単位で指定してください。選択された
暗号アルゴリズムは 64 から 4096 バイト (16 バイト間隔) の
ブロックサイズをサポートしています。
Enter を押すとデフォルト値 (1024 バイト) を使用します。

ファイルシステムブロックサイズ: 4096

ファイルシステムブロックサイズ 4096 バイトを使用します

以下のファイル名暗号アルゴリズムが使用できます:
1. Block : Block encoding, hides file name size somewhat
2. Block32 : Block encoding with base32 output for case-sensitive systems
3. Null : No encryption of filenames
4. Stream : Stream encoding, keeps filenames as short as possible

使用するアルゴリズムの番号を入力してください: 2

アルゴリズム "Block32" が選択されました"

reverse encryption - chained IV and MAC disabled
Enable per-file initialization vectors?
This adds about 8 bytes per file to the storage requirements.
It should not affect performance except possibly with applications
which rely on block-aligned file io for performance.
y/[n]: y


設定が完了しました。以下のプロパティのファイルシステムが
作成されます:
ファイルシステム暗号アルゴリズム: "ssl/aes", バージョン 3:0:2
Filename encoding: "nameio/block32", version 4:0:2
鍵サイズ: 256 ビット  
ブロックサイズ: 4096 バイト
Each file contains 8 byte header with unique IV data.
File holes passed through to ciphertext.

Now you will need to enter a password for your filesystem.
You will need to remember this password, as there is absolutely
no recovery mechanism.  However, the password can be changed
later using encfsctl.

新しい Encfs パスワード:
Encfs パスワードの確認:
% mount |grep -i encfs
encfs on /tmp/encfs type fuse.encfs (rw,nosuid,nodev,relatime,user_id=1000,group_id=1000)
% ls /tmp/encfs
23GX4BPMMVSWOUUXIQCCX4EGI4BHG
24FEOFMBBK774KOXKO6CD5LVPB2JH
2ANNTH3275PXEFQCTLRIXDUBJFMPG
2B72K7FH3NZDIXX7S6P3FSMNCPIWL
2BX2O7FBGPIMLVQ66GD67O5MRYUJN
2GXZR6YY5C2T66GTHAOTBFDHHURZF
2HRGOCQRQI2FKQU5T4YR7X756KQ37IVWCWDUJIL7U4X5APCL4GH3ITX4QRWIDCLSZRTKWXVUA7WFVWME
2HUXRTPOJQFPEY7G62TCSD6BLXI6J
2JMEPULBHHVQVIOUHT2XEMVLD4BAO
2JTHYUVWCXXNYVAWGY4GIV4HGFMBL5BFRU7SGYGVHIR43ZOLVCEG5BA
2LFQK6EBYEOKU2UQV7F32ATU4XQF2EAOXKL75QXYUG4HKKRIADTIJGD
2NU47TEEVRXUIMWBMKGTPXJUEGCAM
2OI3GILDZS3UAMVQB4TL46DQCREKL
 :
```
アンマウント
```
% fusermount -u /tmp/encfs
```
2回目以降のマウント
```
% encfs -i 10 --reverse ~/ /tmp/encfs
EncFS パスワード:
```

問題なく利用できました．  
ただ，注意しないといけなさそうな点が1つ．マウント時にパスワードだけでマウントするために暗号化情報を格納したデータの含まれたファイルが作成されるのですが，`Reverse mode` の場合このファイルも暗号化されてしまうのでこのファイルは別途バックアップを取っておいたほうが良いです．

~~オプションを覚えていればどうにかなりますがそうでないとオプションを試行錯誤しないといけないと思います．~~

＜追記＞  
man を見るとマスターキーが含まれているのでこのファイルがなくなると復元できなくなるようです．注意しましょう．

>
Warning: If you lose the config file, the encrypted file contents are irrecoverably lost. It contains the master
key encrypted with your password. Without the master key, recovery is impossible, even if you know the password.

＜／追記＞

このファイルは `EncFS 1.8.1` では `.encfs6.xml` です．(EncFS のバージョンにより数字部分は変わるようです)今回の例では `~/.encfs6.xml` を別途バックアップします．


EncFS は他の暗号化ファイルシステムより弱い気がしていますが，この辺の柔軟さはいいですね．  
出来れば他の OS にも対応してくれると利用範囲が広がって嬉しいのですが．

- [Valient Gough | EncFS](http://www.arg0.net/#!encfs/c1awt)
- [EncFS](https://vgough.github.io/encfs/)
- [vgough/encfs](https://github.com/vgough/encfs)
- [FUSE: Filesystem in Userspace](http://fuse.sourceforge.net/)
- [バックアップに一番いいファイルシステムを頼む](http://www.slideshare.net/matoken/backup-fs-4)
- [Lessfs をかじってみた(小江戸らぐオフな集まり第110回)](http://www.slideshare.net/matoken/lessfs-110)
- [偏執的な人の為の? NotePCセキュリティ HDD編 -小江戸らぐ1月のオフな集まり(第102回)-](http://www.slideshare.net/matoken/notepc-hdd-1102)
- [Slax で暗号化fs を持ち運ぶ Koedo20081220 Install Party](http://www.slideshare.net/matoken/koedo20081220-install-party)
