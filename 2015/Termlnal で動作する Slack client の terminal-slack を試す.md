Termlnal で動作する Slack client の `terminal-slack` というものを見かけたので試してみました．
- [evanyeung/terminal-slack](https://github.com/evanyeung/terminal-slack)

```
% git clone https://github.com/evanyeung/terminal-slack.git
% cd terminal-slack
```
"[Slack Web API | Slack](https://api.slack.com/web)" で Token を入手して実行．

```
% SLACK_TOKEN=xxxx-xxxxxxxxxx-xxxxxxxxxx-xxxxxxxxxx-xxxxxxxxxx node main.js
```

![https://www.flickr.com/photos/119142834@N05/22901628116/in/datetaken/](https://farm1.staticflickr.com/652/22901628116_82742e73fc.jpg)

さくっと動作しましたが日本語は文字化けしてしまいます．残念．


追記)

Google+ で教えてもらったのですが，次のパッチで日本語が見られるようになりました．書き込みもOK でした :)
- [unicode and docker support by alu · Pull Request #5 · evanyeung/terminal-slack](https://github.com/evanyeung/terminal-slack/pull/5)

<!-- Place this tag in your head or just before your close body tag. -->
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<!-- Place this tag where you want the widget to render. -->
<div class="g-post" data-href="https://plus.google.com/+KenichiroMATOHARA/posts/HgGGrLUrNp4"></div>
