
S<blockquote class="twitter-tweet" lang="ja"><p lang="en" dir="ltr">Slack for Linux: Open beta! You get Slack for Linux and YOU get Slack for Linux… <a href="http://t.co/fawrRuvJFW">http://t.co/fawrRuvJFW</a>&#x1f384;&#x1f381;&#x1f427;<a href="https://twitter.com/hashtag/changelog?src=hash">#changelog</a> <a href="http://t.co/gOZUf2IXum">pic.twitter.com/gOZUf2IXum</a></p>&mdash; Slack (@SlackHQ) <a href="https://twitter.com/SlackHQ/status/646818761874411520">2015, 9月 23</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

サポートディストリビューションも多そう!ということで試してみました．
Twitter の画像ではたくさんのでぃとリビューションのログがあったのですが，ダウンロードページに行くとこんだけでした><

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/21728495582/in/dateposted-public/" title="20150924_10:09:57-20642"><img src="https://farm1.staticflickr.com/584/21728495582_543d4c0ea7_o.jpg" width="363" height="303" alt="20150924_10:09:57-20642"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

ダウンロードして Debian testing stretch amd64 環境で展開して試してみると特に問題なく動きました．

ダウンロードしたパッケージはこんな感じ  
```
% dpkg --info ~/Downloads/slack-desktop-1.2.2-amd64.deb          
 新形式 debian パッケージ、バージョン 2.0。
 サイズ 45815918 バイト: コントロールアーカイブ = 446 バイト。
     473 バイト、   12 行      control              
 Package: slack-desktop
 Version: 1.2.2
 Depends: gconf2, gconf-service, libgtk2.0-0, libudev0 | libudev1, libgcrypt11 | libgcrypt20, libnotify4, libxtst6, libnss3, python, gvfs-bin, xdg-utils, apt-transport-https
 Suggests: libgnome-keyring0, gir1.2-gnomekeyring-1.0
 Replaces: slack (<< 1.2.1~)
 Breaks: slack (<< 1.2.1~)
 Section: misc
 Priority: optional
 Architecture: amd64
 Installed-Size: 134268
 Maintainer: Slack Technologies <feedback@slack.com>
 Description: Slack Desktop
```

とりあえず実行．（ `dpkg -i` でインストールしてもとりあえず動いています．）
```bash
% ar x ~/Downloads/slack-desktop-1.2.2-amd64.deb
% tar xf ../data.tar.xz
% ./usr/bin/slack
```

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/21552096210/in/dateposted-public/" title="20150924_10:09:08-13152"><img src="https://farm6.staticflickr.com/5656/21552096210_d236bc6daa_o.jpg" width="324" height="121" alt="20150924_10:09:08-13152"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>


日本語のリソースファイルぽいものもあるけど日本語が表示されないなと思ったのですが，中身は他の言語も含め全て空でした．この辺はこれからのようですね．
```
% ls -l ./usr/share/slack/locales/ja.pak 
-rw-r--r-- 1 mk mk 0  9月 23 14:42 ./usr/share/slack/locales/ja.pak
```

起動すると ブラウザと同じような感じでログインできます．  
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/21553242789/in/dateposted-public/" title="20150924_11:09:35-26588"><img src="https://farm1.staticflickr.com/620/21553242789_71272d5b10.jpg" width="500" height="417" alt="20150924_11:09:35-26588"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

タスクトレイのメニューはこんな感じでした．タスクトレイからウィンドウを開いた後タスクトレイへの格納は出来ないようです．  
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/21749520701/in/dateposted-public/" title="20150927_07:09:20-30617"><img src="https://farm1.staticflickr.com/593/21749520701_459647b183_o.jpg" width="183" height="112" alt="20150927_07:09:20-30617"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

複数の team にも対応しています．  
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/21714127126/in/dateposted-public/" title="20150927_07:09:15-1275"><img src="https://farm1.staticflickr.com/690/21714127126_410a5d9dfa_o.jpg" width="328" height="187" alt="20150927_07:09:15-1275"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

日本語の表示や入力も問題なく行えました．
と，ここまではいい感じかなー．と思っていたのですが起動に時間がかかるのはともかくメモリ消費量が大きいです．

```
% ps aux| head -1                                                           
USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
% ps aux| grep -i slack                                                     
mk        2463  0.1  0.5 1331200 40756 ?       Sl   01:48   0:09 /usr/share/slack/slack
mk        2497  0.0  0.1 313440  8020 ?        S    01:48   0:00 /usr/share/slack/slack --type=zygote --no-sandbox
mk        2597  0.0  0.5 581152 44584 ?        Sl   01:48   0:04 /usr/share/slack/slack --type=gpu-process --channel=2463.0.1022154471 --no-sandbox --supports-dual-gpus=false --gpu-driver-bug-workarounds=2,14,43 --disable-accelerated-video-decode --gpu-vendor-id=0x8086 --gpu-device-id=0x0046 --gpu-driver-vendor --gpu-driver-version
mk        2648  0.1 11.4 2024760 909348 ?      Sl   01:48   0:16 /usr/share/slack/slack --type=renderer --force-device-scale-factor=1 --no-sandbox --enable-deferred-image-decoding --lang=ja --node-integration=true --enable-plugins --subpixel-font-scaling=true --enable-pinch-virtual-viewport --enable-delegated-renderer --num-raster-threads=2 --enable-gpu-rasterization --use-image-texture-target=3553 --disable-accelerated-video-decode --channel=2463.1.1087300934
mk        2750  0.3 13.5 2102980 1082316 ?     Sl   01:49   0:37 /usr/share/slack/slack --type=renderer --force-device-scale-factor=1 --no-sandbox --enable-deferred-image-decoding --lang=ja --guest-instance-id=1 --node-integration=false --enable-plugins --preload=/usr/share/slack/resources/app.asar/static/ssb-interop --enable-pinch-virtual-viewport --enable-delegated-renderer --num-raster-threads=2 --enable-gpu-rasterization --use-image-texture-target=3553 --disable-accelerated-video-decode --channel=2463.2.1767621685
mk        3127  0.0  0.0  13700  2224 pts/2    S+   04:25   0:00 grep -i slack
% ps aux | grep -i slack | awk '{print $6}' | xargs echo| sed -e 's/\ /\+/g' | bc
2087260
% pstree 29317
slack─┬─slack─┬─slack─┬─{Chrome_ChildIOT}
      │       │       ├─2*[{CompositorTileW}]
      │       │       ├─{Compositor}
      │       │       ├─{HTMLParserThrea}
      │       │       ├─{handle-watcher-}
      │       │       └─5*[{slack}]
      │       └─slack─┬─{Chrome_ChildIOT}
      │               ├─2*[{CompositorTileW}]
      │               ├─{Compositor}
      │               ├─{HTMLParserThrea}
      │               ├─{ScriptStreamerT}
      │               ├─{handle-watcher-}
      │               └─{slack}
      ├─slack─┬─{Chrome_ChildIOT}
      │       └─{Watchdog}
      ├─{AudioThread}
      ├─3*[{BrowserBlocking}]
      ├─5*[{CachePoolWorker}]
      ├─{Chrome_CacheThr}
      ├─{Chrome_DBThread}
      ├─{Chrome_FileThre}
      ├─{Chrome_FileUser}
      ├─{Chrome_IOThread}
      ├─{Chrome_ProcessL}
      ├─{IndexedDB}
      ├─{NSS SSL ThreadW}
      ├─{NetworkChangeNo}
      ├─{gdbus}
      ├─{gmain}
      ├─{handle-watcher-}
      ├─{inotify_reader}
      ├─{sandbox_ipc_thr}
      ├─5*[{slack}]
      └─{threaded-ml}
% free
              total        used        free      shared  buff/cache   available
Mem:        7971864     6427012      245032      344740     1299820     1111572
% kill 29317
% free
              total        used        free      shared  buff/cache   available
Mem:        7971864     4251376     2457392      306516     1263096     3325500
```

この作りだと Chrome/Chromium を併用している場合 Chrome アプリケーションにしてもらったほうがリソース食わないんじゃないかなと思ったりも．


ちなみに Hipchat には以前から Linux Client は存在していてこんな感じです．Slack に比べると大分軽いです．

```
% ps aux | grep hipchat 
mk        1953  2.4  1.6 2584884 128724 ?      Ssl  08:23   0:01 /usr/bin/hipchat
% pstree 1953
hipchat.bin─┬─{QProcessManager}
            ├─2*[{QQmlThread}]
            ├─{QXcbEventReader}
            ├─{Qt HTTP thread}
            ├─{Qt bearer threa}
            └─5*[{hipchat.bin}]
```

Node.ja で作られた plaidchat というクライアントもあるようです．これも試してみようと思います．  
- [plaidchat/plaidchat](https://github.com/plaidchat/plaidchat)