一部の楽屋ネタ的なものですが，Twitter であひるを焼く人たちがいます．
<a class="twitter-timeline" href="https://twitter.com/search?q=%E3%81%82%E3%81%B2%E3%82%8B%E7%84%BC%E3%81%8D%20OR%20%23%E3%81%82%E3%81%B2%E3%82%8B%E7%84%BC%E3%81%8D%20include%3Aretweets" data-widget-id="630808989089935360">あひる焼き OR #あひる焼き include:retweetsに関するツイート</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

libcaca というライブラリのデモで cacafire というものがあるのですが，これがアスキーアートで炎の燃える様子が動画で表現されるので楽しいです．これであひるを焼くように見せかけないだろうかと思いました．

<blockquote class="twitter-tweet" lang="ja"><p lang="ja" dir="ltr">ふとcaca-utils の img2txt であひるを表示しつつ cacafire で <a href="https://twitter.com/hashtag/%E3%81%82%E3%81%B2%E3%82%8B%E7%84%BC%E3%81%8D?src=hash">#あひる焼き</a> ができないだろうかとか思った <a href="http://t.co/0qt03heN82">pic.twitter.com/0qt03heN82</a></p>&mdash; (「ΦωΦ)「 (@matoken) <a href="https://twitter.com/matoken/status/629854999896440833">2015, 8月 8</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

フレームバッファを使えばきっと出来るなーと．そしてfbterm 1.5 からバックグラウンドイメージのサポートが入っていて，デモ用に fbv というものがあります．これを組み合わせればいけるだろうと試してみました．
- [fbterm - a fast FrameBuffer based TERMinal emulator for linux - Google Project Hosting](https://code.google.com/p/fbterm/)
> The updated FbTerm's man page contains a demo script using fbv. Here is a screenshot:

- [Index of /fbv/](http://s-tech.elsat.net.pl/fbv/)

が，試してみたところ libgif まわりで少し嵌ったのでメモ．

## 環境

- Debian stretch amd64 or Debian jessir amd64

## 関連パッケージを導入

```
$ sudo apt install build-essential libpng-dev libjpeg-dev libgif-dev
```

## source 入手 & build

```
$ wget http://s-tech.elsat.net.pl/fbv/fbv-1.0b.tar.gz
$ tar tvf fbv-1.0b.tar.gz
$ tar xf fbv-1.0b.tar.gz
$ cd fbv-1.0b
$ ./configure --help
    :
$ ./configure --prefix=$HOME/usr/local
$ make
```

ここで問題発生．

```
% ./configure
checking for libungif presence... no
 :
% make
gif.o: In function `fh_gif_load':
gif.c:(.text+0x111): undefined reference to `DGifOpenFileName'
gif.c:(.text+0x12a): undefined reference to `DGifGetRecordType'
gif.c:(.text+0x156): undefined reference to `DGifGetExtension'
gif.c:(.text+0x189): undefined reference to `DGifGetExtensionNext'
gif.c:(.text+0x1b0): undefined reference to `DGifCloseFile'
gif.c:(.text+0x1c4): undefined reference to `DGifGetImageDesc'
gif.c:(.text+0x2dc): undefined reference to `DGifGetLine'
gif.c:(.text+0x409): undefined reference to `DGifCloseFile'
gif.c:(.text+0x45b): undefined reference to `DGifGetLine'
gif.o: In function `fh_gif_getsize':
gif.c:(.text+0x5df): undefined reference to `DGifOpenFileName'
gif.c:(.text+0x5f8): undefined reference to `DGifGetRecordType'
gif.c:(.text+0x61a): undefined reference to `DGifGetExtension'
gif.c:(.text+0x631): undefined reference to `DGifGetExtensionNext'
gif.c:(.text+0x654): undefined reference to `DGifCloseFile'
gif.c:(.text+0x674): undefined reference to `DGifGetImageDesc'
gif.c:(.text+0x68d): undefined reference to `DGifCloseFile'
collect2: error: ld returned 1 exit status
```

**libungif** が見つからずエラーになっています．パッケージ内にはそれらしいものは存在しますが実体が無さそうです．

```
% dpkg -L libgif-dev|grep ungif
/usr/lib/libungif.la
/usr/lib/libungif.so
/usr/lib/libungif.a
% ls -l /usr/lib/libungif.so
lrwxrwxrwx 1 root root 15 12月  8  2013 /usr/lib/libungif.so -> libgif.so.4.1.6
$ ls -l /usr/lib/libgif.so.4.1.6
ls: cannot access /usr/lib/libgif.so.4.1.6: No such file or directory
```

libungif/libgif の違いはなんだろうと調べてみるとUnisys社のLZW特許問題を回避するためのものが libungif だったようです．2004年に特許は切れているので現在は libgif になってる感じ?
- [History of GIFLIB](http://giflib.sourceforge.net/history.html)

## gif ファイルを使わないようにして回避

configure option で gifを使わないようにして回避します．これでもほぼ問題ない気もする．

```
./configure --without-libgif
```

## configure script を書き換えて libgif を使うようにする

ちょっと乱暴ですが， configure script の libungif を libgif に一括で書き換えてしまいます．
```
% sed -e "s/ungif/gif/g" ./configure-org > ./configure
% ./configure
% make
```

## シンボリックリンクを貼って逃げる

現在こんな感じなので，
```
% ls -la /usr/lib/lib*gif*
lrwxrwxrwx 1 root root  8 Dec  8  2013 /usr/lib/libungif.a -> libgif.a
lrwxrwxrwx 1 root root  9 Dec  8  2013 /usr/lib/libungif.la -> libgif.la
lrwxrwxrwx 1 root root 15 Dec  8  2013 /usr/lib/libungif.so -> libgif.so.4.1.6
```

こんな感じでシンボリックリンクを貼ってあげる．
```
% sudo ln -s /usr/lib/x86_64-linux-gnu/libgif.so /usr/lib/libgif.so.4.1.6
```

後はそのままbuild


## あひるの表示

fbv で画像を表示してみます．

```
$ fbv ahiru.png
```

以下のページに fbv で背景を設定して fbterm を起動する script があります．
- [Fbterm - ArchWiki](https://wiki.archlinux.org/index.php/Fbterm#Background_image)

これを少しいじってこんな感じに

```
#!/bin/bash
# fbterm-bi: a wrapper script to enable background image with fbterm
# usage: fbterm-bi /path/to/image fbterm-options
clear
echo -ne "\e[?25l" # hide cursor
fbv -ciuke "$1" << EOF
q
EOF
shift
export FBTERM_BACKGROUND_IMAGE=1
exec fbterm "$@"
```

いじったのはゴミが残る場合があるので初めに `clear` を実行．fbv で縦横比虫をしないように `-r` option を消しました．


## あひる焼き

もう少しで焼けます．
あひるは用意出来たので今度は炎の用意です．cacafire の含まれている `caca-utils` パッケージを導入します．
```
% sudo apt install caca-utils
```

焼きます
```
% cacafire
```

<iframe width="420" height="315" src="https://www.youtube.com/embed/Qkru4henwi8" frameborder="0" allowfullscreen></iframe>

＃terminal をフレームバッファも含めて動画でスクリーンショットとる方法が解らず今回は仮想マシンで実行してキャプチャしました．だれか端末のフレームバッファも含めての動画でのスクリーンショットのとり方を知ってる人が居たら教えて下さい_o_
＃＃文字だけなら ttyrec とか [[asciicast](https://asciinema.org/)](https://asciinema.org/) が，1枚の画像だけであれば [fbgrab](http://hpv.cc/~maty/pukiwiki1/index.php?Linux%2FConsole#id4005aa) があるんですけどね……・


## 実用性?

今回のは完全にネタですが，切替器で1つの KVM で複数端末を使う場合はそれぞれ別の壁紙を設定しておくと間違いが起こりにくくなっていいかもしれません．しかし今回の方法だと login しないといけないのでいまいち．login 以前に壁紙を設置する方法も調べてみたいと思います．

## 関連URL

- [Linux/Console - matoken's wiki.](http://hpv.cc/~maty/pukiwiki1/index.php?Linux%2FConsole#jeb6fc3d)
- [#732272 - Should not ship dangling symlinks for libungif.* - Debian Bug report logs](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=732272)
