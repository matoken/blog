- [asciinema - Record and share your terminal sessions, the right way](https://asciinema.org/)

asciinema ちょっと面白い．端末の操作を gyazo や pastebin みたいにお手軽にwebで公開みたいなイメージ．ttyrec とかもあるけどお手軽にWebで見られるのがいい．ぱっと見動画みたいだけどコピペも出来る．

Web への埋め込みも出来て，画像のリンク形式と
[![asciicast](https://asciinema.org/a/24971.png)](https://asciinema.org/a/24971)
直接再生できるプレイヤー形式が選べる．
<script type="text/javascript" src="https://asciinema.org/a/24971.js" id="asciicast-24971" async></script>

色だけでなく日本語もいけるけれど w3m-img とかの画像は見えない．以下は録画時には w3m-img で画像が出ていた．
<script type="text/javascript" src="https://asciinema.org/a/24972.js" id="asciicast-24972" async></script>

クライアントコマンドを使うのだけどRaspberry Pi とかも狙っているのか armv6/armv7 もあるのが便利．

```
case "$(uname -s).$(uname -m)" in
    Linux.x86_64) platform=linux-amd64;;
    Linux.i?86) platform=linux-386;;
    Linux.armv6l) platform=linux-arm;;
    Linux.armv7l) platform=linux-arm;;
    Darwin.x86_64) platform=darwin-amd64;;
    Darwin.i?86) platform=darwin-386;;
    *) echo "Sorry, there is no asciinema binary available for your platform. Try building from source." >&2; exit 1;;
esac
```

## 導入例
```
$ curl -sL https://asciinema.org/install > install
$ lv install
$ sh install
Downloading asciinema v0.9.9 for linux-arm...
######################################################################## 100.0%
Warning: you may be asked for administrator password to save the file in /usr/local/bin directory
Installing to /usr/local/bin/asciinema...
sudo: unable to resolve host raspberrypi
sudo: unable to resolve host raspberrypi
Success.

Start recording your terminal by running: asciinema rec
```
オプションは少ない
```
$ asciinema 
usage: asciinema [-h] [-v] <command> [command-options]

Record and share your terminal sessions, the right way.

Commands:
   rec            Record terminal session
   auth           Assign local API token to asciinema.org account

Options:
   -h, --help     Display help message
   -v, --version  Display version information

   Run "asciinema <command> -h" to see the options available for the given command.
```

## 認証
認証しなくても使える．未認証の時にUpしたものも認証後認証ユーザのものになった．
```
$ asciinema auth
Open the following URL in your browser to register your API token and assign any recorded asciicasts to your profile:
https://asciinema.org/connect/8025b685-d45e-4631-8dcf-xxxxxxxxxxxx
```

## 録画

rec option で録画できる．exit で録画終了で，その後 Enter でアップロード，Ctrl+c でキャンセル．
```
$ asciinema rec
~ Current terminal size is 178x49.
~ It may be too big to be properly replayed on smaller screens.
~ You can now resize it. Press <Enter> to start recording.

~ Asciicast recording started.
~ Hit Ctrl-D or type "exit" to finish.
pi@raspberrypi~ $ 
pi@raspberrypi~ $ 
pi@raspberrypi~ $ exit
~ Asciicast recording finished.
~ Press <Enter> to upload, <Ctrl-C> to cancel.

https://asciinema.org/a/16372
```

Web にアクセスすると録画されたものが閲覧できる．アップロードにはプライベートになっていて公開するには公開設定が必要．この動きは [Account settings](https://asciinema.org/user/edit) で設定変更が可能．テーマの選択やトークンの管理もここで出来る．


＃このエントリ公開していたつもりが公開できていなかった><