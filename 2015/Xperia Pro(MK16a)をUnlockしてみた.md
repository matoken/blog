Xperia Pro(MK16a)を持っているのですが，OSが古いし標準アプリも多くてストレージもいっぱいで使いたいアプリもなかなか入れることが出来ません．
最近はこの端末にMVNOのSIMを刺してモバイルルータ and GPSロガー的に使っていますがbluetooth tethering も使えずWi-Fi tethering だとバッテリの減りも早いです．ということでカスタムROMやCyanogenMod11か12あたりに入れ替えたいのですがブートローダーにロックがかかっているのでまずはこれを解除しないといけません．Test Point を使ったりするのは面倒だなと思っていたのですが，現在は公式でアンロックコードを手に入れることができるのでこちらで行うことに．
※保証がきかなくなります．SEUSなどが利用できなくなります．公式アップデートも利用できなくなるそうです．(まあこれからOTAとか来ることはないと思いますが)

本体の他に以下のものが必要です．ここでは開発環境の説明はしません．
- Android 開発環境の動作するPC(今回はDebian Jessie)
- Android 開発環境の中のfastboot コマンド(sdk/platform-tools 以下)
- USB A-microB ケーブル

# アンロックコードの入手

アンロックコードは烏賊のページから入手できます．

- [Unlockbootloader - Developer World](http://developer.sonymobile.com/unlockbootloader/)

機種を選択して，メールアドレスを入力すると確認メールが届くので，そのメールのURL をクリックするとIMEIの入力画面になって，そこでIMEI を入力することでCodeが入手できます．


# アンロック

Xperia Pro の電源を切ります．
USBケーブルはPCのみに接続する
電源Off 状態からメニューボタンを押しながらUSBケーブルを接続する
右上側面のHDMIコネクタ横のLEDが青くなるとFastbootモードになっている

Fastboot モードの時のdmesgはこんな感じでした．
```bash
[78677.917985] usb 1-1.1: new high-speed USB device number 30 using ehci-pci
[78678.013776] usb 1-1.1: unable to get BOS descriptor
[78678.025756] usb 1-1.1: New USB device found, idVendor=0fce, idProduct=0dde
[78678.025764] usb 1-1.1: New USB device strings: Mfr=1, Product=2, SerialNumber=3
[78678.025769] usb 1-1.1: Product: S1Boot Fastboot
[78678.025774] usb 1-1.1: Manufacturer: Sony Ericsson Mobile Communications AB
[78678.025778] usb 1-1.1: SerialNumber: CB5A1G45XXXXXX
```

fastboot devices コマンドで接続を確認します．
```bash
% ./fastboot devices
CB5A1G45XXXXXX  fastboot
% ./fastboot -i 0x0fce getvar version
version: 0.3
finished. total time: 0.001s
```

もしここで`no permissions  fastboot` というエラーが出る場合は`adb server`を起動しなおしてみるとうまく行くようです．それでもうまく行かない場合root権限で起動し直す(`sudo command`)とうまく行ったことも．
```bash
% ./fastboot devices
no permissions  fastboot
% ./adb kill-server
% ./adb start-server
* daemon not running. starting it now on port 5037 *
* daemon started successfully *
% ./fastboot devices
CB5A1G45XXXXXX  fastboot
```


問題ないようなら烏賊のコマンドでアンロックコードを指定してアンロックします．
```bash
% ./fastboot -i 0x0fce oem unlock 0xC3BA1080A6XXXXXX
...
(bootloader) Unlock phone requested
(bootloader) Erasing block 0x00001300
(bootloader) Erasing block 0x00001400
(bootloader) Erasing block 0x00001500
(bootloader) Erasing block 0x00001600
(bootloader) Erasing block 0x00001700
(bootloader) Erasing block 0x00001800
(bootloader) Erasing block 0x00001900
(bootloader) Erasing block 0x00001a00
(bootloader) Erasing block 0x00001b00
(bootloader) Erasing block 0x00001c00
(bootloader) Erasing block 0x00001d00
(bootloader) Erasing block 0x00001e00
(bootloader) Erasing block 0x00001f00
OKAY [  4.587s]
finished. total time: 4.587s
```

これでアンロック完了です．
次はOSを入れ替えてみます．


<iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00CPKFXOQ" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe><iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00CTDOEEE" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe><iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00CHHTOII" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>
- [ ] 
