Android の Google Authenticator 色々な認証に使えて便利ですが，以前 Nexus5 を紛失した時や LGL22 でデータが飛んでしまった後復旧が面倒でした．どうにかバックアップが取れないかなと調べてみました．

## Android からデータを取得

`/data/data/com.google.android.apps.authenticator2/databases/databases` がデータが格納されているファイルらしいです．
adb pull で持ってきたいけど権限がないので一旦 `/storage/sdcard0/` に cp する
```
% adb shell
shell@g2:/ $ su
root@g2:/ # cp /data/data/com.google.android.apps.authenticator2/databases/databases /storage/sdcard0/
```

ローカルPC に退避
```
% adb pull /storage/sdcard0/databases .
```

`/storage/sdcard0/` に cp したデータを消す．暗号化領域の下のはずだけど一応上書きしてから削除
```
% adb shell
shell@g2:/ $ su
root@g2:/ # ls -l /storage/sdcard0/databases
-rw-rw---- root     sdcard_r    16384 2015-10-09 22:51 databases
root@g2:/ # head -c 16384 /dev/random > /storage/sdcard0/databases
root@g2:/ # head -c 16384 /dev/random > /storage/sdcard0/databases
root@g2:/ # head -c 16384 /dev/random > /storage/sdcard0/databases
root@g2:/ # head -c 16384 /dev/random > /storage/sdcard0/databases
root@g2:/ # head -c 16384 /dev/random > /storage/sdcard0/databases
root@g2:/ # head -c 16384 /dev/random > /storage/sdcard0/databases
root@g2:/ # rm /storage/sdcard0/databases
root@g2:/ # ^D
shell@g2:/ $ ^D
```

## データ形式を確認してdump

該当ファイルは `file` コマンドによると SQLite3 のようなので dump してみる
```
% file ./databases
./databases: SQLite 3.x database
% sqlite3 ./databases
SQLite version 3.8.11.1 2015-07-29 20:00:57
Enter ".help" for usage hints.
sqlite> .dump
PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE android_metadata (locale TEXT);
INSERT INTO "android_metadata" VALUES('ja_JP');
CREATE TABLE accounts (_id INTEGER PRIMARY KEY, email TEXT NOT NULL, secret TEXT NOT NULL, counter INTEGER DEFAULT 0, type INTEGER, provider INTEGER DEFAULT 0, issuer TEXT DEFAULT NULL, original_name TEXT DEFAULT NULL);
 :
```

ここで取得した `PRIMARY KEY` を HOTP TOKEN を割り出す script の ["google-authenticator.py](https://gist.github.com/matoken/e82e309c4f34e572a80d﻿) に食わせると Android App と同じコードが帰ってくるのを確認しました．勿論普通に認証も可能．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/21868148528/in/dateposted-public/" title="IMG_20151010_012917"><img src="https://farm6.staticflickr.com/5653/21868148528_efbb381b77_n.jpg" width="320" height="240" alt="IMG_20151010_012917"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

ということで要root ですが， `/data/data/com.google.android.apps.authenticator2/databases/databases` を退避して他の端末に持って行っても動作するかも．少なくとも `PRIMARY KEY` は入手できるので手動で入力すれば OK ですね．  
とはいえ端末紛失時には作りなおしたほうが良いでしょうが．

root が取れない場合は登録時に PRIMARY KEY をメモしておくくらいですかね．QR Code だけしか見えない場合は Google Authenticator に食わせる前に別のリーダーを利用したりすれば可能です
例えば以下のような読み取り内容の場合 `6QHI5WW6H3FMJ2ZI` が PRIMARY KEY です．
> otpauth://t│·········· otp/mk@micro?secret=6QHI5WW6H3FMJ2ZI
