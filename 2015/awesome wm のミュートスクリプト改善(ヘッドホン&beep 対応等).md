awesome wm のミュートスクリプト改善(ヘッドホン&beep 対応等)

- [awesome wm を久々に使おうとしたら設定ファイルが使えなくなっていたので再設定 | matoken's meme](http://matoken.org/blog/blog/2015/08/23/awesome-reconfigure/)
- [Awesome wm でボリュームコントロール | matoken's meme](http://matoken.org/blog/blog/2015/09/03/awesome-wm-volume-controle/)

の続きです．このシリーズは興味ある人ほとんど居ない気もしてますが……．

ボリュームコントロール用の script を書いていい感じでボリュームコントロールが使えている気がしていたのですが，いくつか不具合が見つかったので改良しました．

## 問題1．ヘッドホン利用時にアンミュートされない

ミュート時に Master をミュートして，同様にアンミュート時に Master をアンミュートするのですが，ヘッドホン利用時にはヘッドホンがミュートされたままになってしまいます．

`amixer -c 0 get Headphone` とかして確認すると，off のままです．

```
% amixer -c 0 get Headphone
Simple mixer control 'Headphone',0
  Capabilities: pvolume pswitch
  Playback channels: Front Left - Front Right
  Limits: Playback 0 - 74
  Mono:
  Front Left: Playback 74 [100%] [0.00dB] [off]
  Front Right: Playback 74 [100%] [0.00dB] [off]
```

Headphone に対して unmute すると音が出るようになりました．

ということでアンミュート時に Master だけでなく `Headphone` / `Speaker` もアンミュートするようにしました．`Headphone` / `Speaker` は両方 on にしても排他利用になるようで両方 on にしても問題ないようです．

- 旧
```bash
amixer -q -c 0 set Master unmute
```
- 新
```
amixer -q -c 0 set Master unmute
amixer -q -c 0 set Speaker unmute
amixer -q -c 0 set Headphone unmute
```

これでヘッドホンやスピーカーを切り替えてもちゃんと切り替わるようになりました．

## 問題2．BEEP がミュートされない

Master や Beep を mute しても beep音が出てしまいます．amixer でボリュームを 0% にしても音が出てしまいます．

```
% amixer -c 0 get Beep
Simple mixer control 'Beep',0
  Capabilities: pvolume pvolume-joined pswitch pswitch-joined
  Playback channels: Mono
  Limits: Playback 0 - 7
  Mono: Playback 4 [57%] [-12.00dB] [off]
```

ALSA ではなく基本の? xset で有効，無効にしたら効いたのでこれを設定しました．

- beep ミュート
```bash
xset -b
```
- beep アンミュート
```bash
xset b
```

## 問題3．音量ボタンを押した時のメッセージでミュート・アンミュート状態がわからない

ボリュームUp / Down ボタンを押した時のメッセージがミュート状態にかかわらず同じものを表示していたので音量が小さくて聞こえないのかミュート状態だから音が出ていないのかわかりませんでした．

ミュート状態によりアイコンを変更するようにしました．ついでに Up/Down の矢印も表示するようにしました．

- これまでは1パターン
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/20904441840/in/dateposted-public/" title="20150903_03:09:24-10606"><img src="https://farm1.staticflickr.com/700/20904441840_99a97bb2a8_t.jpg" width="91" height="58" alt="20150903_03:09:24-10606"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

- ミュート状態でアイコンの変更&矢印を表示するようにした
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/21269753155/in/dateposted-public/" title="20150909_21:09:58-12937"><img src="https://farm1.staticflickr.com/744/21269753155_2e4278cac2_t.jpg" width="98" height="58" alt="20150909_21:09:58-12937"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/21277809071/in/dateposted-public/" title="20150909_21:09:41-13553"><img src="https://farm1.staticflickr.com/667/21277809071_0db3601595_t.jpg" width="98" height="58" alt="20150909_21:09:41-13553"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/20647055114/in/dateposted-public/" title="20150909_21:09:37-14398"><img src="https://farm6.staticflickr.com/5799/20647055114_df2d6def32_t.jpg" width="98" height="58" alt="20150909_21:09:37-14398"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/21277820201/in/dateposted-public/" title="20150909_21:09:50-14607"><img src="https://farm1.staticflickr.com/683/21277820201_eeb9832e8a_t.jpg" width="98" height="58" alt="20150909_21:09:50-14607"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>


## 出来上がった script

ということでそれぞれを反映してこんな感じになりました．

```bash
#!/bin/bash

MUTE=`amixer -c 0 get Master|tail -1|cut -d '[' -f 4|sed s/\]//`

if [ $MUTE = "on" ] ; then
  ICON="/usr/share/icons/ContrastHigh/scalable/status/audio-volume-medium.svg"
else
  ICON="/usr/share/icons/ContrastHigh/scalable/status/audio-volume-muted.svg"
fi

case "$1" in
  "XF86AudioMute" )
    if [ $MUTE = "on" ] ; then
      amixer -q -c 0 set Master mute
      xset -b
      echo -e "&#x01f50a;☓\nmute!"
      notify-send -u low -t 500 -i '/usr/share/icons/ContrastHigh/scalable/status/audio-volume-muted.svg' mute "☓"
    else
      amixer -q -c 0 set Master unmute
      amixer -q -c 0 set Speaker unmute
      amixer -q -c 0 set Headphone unmute
      xset b
      echo -e "&#x01f50a;\nunmute!"
      amixer -c 0 get Master | tail -1 | cut -d '[' -f 2 | sed s/\]// | xargs notify-send -u low -t 500 -i '/usr/share/icons/ContrastHigh/scalable/status/audio-volume-high.svg' numute
    fi
  ;;
  "XF86AudioRaiseVolume" )
    amixer -c 0 set Master 2dB+ | tail -1 | cut -d '[' -f 2 | sed s/\]// | xargs notify-send -u low -t 500 -i $ICON "Vol ⤴"
  ;;
  "XF86AudioLowerVolume" )
    amixer -c 0 set Master 2dB- | tail -1 | cut -d '[' -f 2 | sed s/\]// | xargs notify-send -u low -t 500 -i $ICON "Vol ⤵"
  ;;
esac

```
- [awesome-audio.bash](https://gist.github.com/matoken/16f6841cdbf79e51b718)

今のところ満足 :-)
