Ingressで複数人で移動することがあったのですが，連絡は基本的にGoogle ハングアウト．motercycle では基本的に確認できません．Android には標準でTTS(Text To Speak) が利用出来ます．ハングアウトメッセージをTTS に繋げることが出来たら大雑把に状況確認が出来るのではと試してみました．

## N2 TTS の導入

TTS にはKDDI研究所のN2 TTS を利用しました．導入手順は以下のページにあります．
- [N2 TTS - Google Play の Android アプリ](https://play.google.com/store/apps/details?id=jp.kddilabs.n2tts)

追加音声も4x4種類あります．
- [N2 TTS用追加声質データ(女声A) - Google Play の Android アプリ](https://play.google.com/store/apps/details?id=jp.kddilabs.n2tts.voice.fa)
- [N2 TTS用追加声質データ(女声B) - Google Play の Android アプリ](https://play.google.com/store/apps/details?id=jp.kddilabs.n2tts.voice.fb)
- [N2 TTS用追加声質データ(男声A) - Google Play の Android アプリ](https://play.google.com/store/apps/details?id=jp.kddilabs.n2tts.voice.ma)
- [N2 TTS用追加声質データ(男声B) - Google Play の Android アプリ](https://play.google.com/store/apps/details?id=jp.kddilabs.n2tts.voice.mb)

今回はfa001 を利用しました．

<a href="https://www.flickr.com/photos/119142834@N05/19040489016" title="Screenshot_2015-06-23-07-19-04 by Kenichiro MATOHARA, on Flickr"><img src="https://c1.staticflickr.com/1/514/19040489016_f7de969c3b_m.jpg" width="135" height="240" alt="Screenshot_2015-06-23-07-19-04"></a><a href="https://www.flickr.com/photos/119142834@N05/19040489406" title="Screenshot_2015-06-23-07-18-50 by Kenichiro MATOHARA, on Flickr"><img src="https://c1.staticflickr.com/1/466/19040489406_a15ce7396a_m.jpg" width="135" height="240" alt="Screenshot_2015-06-23-07-18-50"></a><a href="https://www.flickr.com/photos/119142834@N05/19066721175" title="Screenshot_2015-06-23-07-18-30 by Kenichiro MATOHARA, on Flickr"><img src="https://c1.staticflickr.com/1/432/19066721175_503a7b6cbc_m.jpg" width="135" height="240" alt="Screenshot_2015-06-23-07-18-30"></a>

## Voice Notify の設定

ハングアウトアプリからTTS に流し込めれば良いのですが，出来ないようです．Google Play Store で使えそうなアプリを探したところ通知を読み上げてくれるVoice Notify というアプリを見つけました．
- [Voice Notify - Google Play の Android アプリ](https://play.google.com/store/apps/details?id=com.pilot51.voicenotify)

このアプリを導入し，音声通知をOn に
<a href="https://www.flickr.com/photos/119142834@N05/19040488286" title="Screenshot_2015-06-23-07-19-33 by Kenichiro MATOHARA, on Flickr"><img src="https://c1.staticflickr.com/1/376/19040488286_df6893bae3_m.jpg" width="135" height="240" alt="Screenshot_2015-06-23-07-19-33"></a>

通知対象のアプリにハングアウトだけ登録します．アプリケーションが多い場合はメニューから `Igoner All` で一旦全部解除してから `filter` で検索すると便利です．
<a href="https://www.flickr.com/photos/119142834@N05/18880547249" title="Screenshot_2015-06-23-07-20-29 by Kenichiro MATOHARA, on Flickr"><img src="https://c1.staticflickr.com/1/406/18880547249_859662fbe4_m.jpg" width="135" height="240" alt="Screenshot_2015-06-23-07-20-29"></a><a href="https://www.flickr.com/photos/119142834@N05/19066718755" title="Screenshot_2015-06-23-07-20-16 by Kenichiro MATOHARA, on Flickr"><img src="https://c1.staticflickr.com/1/257/19066718755_d787c0e0ff_m.jpg" width="135" height="240" alt="Screenshot_2015-06-23-07-20-16"></a>

ここで通知対象のアプリを増やすとそのアプリの通知も読み上げられます．メッセージの内容が読み上げられるかはそのアプリのちいち内容次第になります．例えばGoogle+ では内容により省略されたりします．


## ハングアウトの設定

通知をしたいハングアウトの通知をOn にします．これでハングアウトから通知があればその内容が読み上げられます．

<a href="https://www.flickr.com/photos/119142834@N05/18879245810" title="Screenshot from 2015-06-23 07-35-06 by Kenichiro MATOHARA, on Flickr"><img src="https://c1.staticflickr.com/1/278/18879245810_15a32ca6b4_o.png" width="371" height="180" alt="Screenshot from 2015-06-23 07-35-06"></a>

----

これで実際に試してみましたが，思っていたより内容は把握出来ました．長文だと辛いですが短文なら十分．ボリュームは6割位でイヤホンマイク片耳で聞いていましたが低速なら問題なし，50km/h以上だと聞き取りづらくなってきました．恐らく高速道路だとダメでしょうね．この辺はスピーカーやイヤホン次第で変わると思います．

