
レンタルサーバのheteml にownCloud 7.0.4 を導入する簡単なお仕事．ssh も使えるし〜と思ってさくっと導入してWeb でのテストまでして引渡したのですがユーザからownCloud のクライアントで接続できないとのこと．

ログファイルを出力しつつ試してみると確かに繋がりません．
> 
% owncloud --confdir ./ --logfile ./log --logflush --logwindow

- **--confdir** 設定ファイルのディレクトリ指定
- **--logfile** ログファイル指定
- **--logflush** ログのリアルタイム出力
- **--logwindow** ログ出力ウィンドウ表示

WebDav でもうまく行かない．

怪しそうな以下のエラーメッセージで検索するとそれらしいものを発見．
> 
No basic authentication headers were found


- [Cannot sync with Jolla after update to 7.0.0 No basic authentication headers were found · Issue #9880 · owncloud/core](https://github.com/owncloud/core/issues/9880#issuecomment-64125972)

コメントを参考に以下の2ファイルを修正．
- .htaccess
- lib/base.php

```php
$ diff -u .htaccess.org .htaccess
--- .htaccess.org       2015-02-19 18:41:36.000000000 +0900
+++ .htaccess   2015-03-03 18:09:27.000000000 +0900
@@-17,6 +17,9 @@
 </IfModule>
 <IfModule mod_rewrite.c>
 RewriteEngine on
+RewriteCond %{HTTP:Authorization} ^Basic.*
+RewriteRule ^(.*) $1?Authorization=%{HTTP:Authorization} [QSA,C]
+RequestHeader unset Authorization
 RewriteRule .* - [env=HTTP_AUTHORIZATION:%{HTTP:Authorization}]
 RewriteRule ^\.well-known/host-meta /public.php?service=host-meta [QSA,L]
 RewriteRule ^\.well-known/host-meta\.json /public.php?service=host-meta-json [QSA,L]
@@-40,4 +43,4 @@
 </IfModule>
```

```php
$ diff -u lib/base.php.org lib/base.php
--- lib/base.php.org    2014-12-09 03:34:15.000000000 +0900
+++ lib/base.php        2015-03-02 20:58:04.000000000 +0900
@@-805,6 +805,12 @@
 
        protected static function handleAuthHeaders() {
                //copy http auth headers for apache+php-fcgid work around
+               if(isset($_GET['Authorization']) && preg_match('/Basic\s+(.*)$/i', $_GET['Authorization'], $matches))
+               {
+                   list($name, $password) = explode(':', base64_decode($matches[1]));
+                   $_SERVER['PHP_AUTH_USER'] = strip_tags($name);
+                   $_SERVER['PHP_AUTH_PW'] = strip_tags($password);
+               }
                if (isset($_SERVER['HTTP_XAUTHORIZATION']) && !isset($_SERVER['HTTP_AUTHORIZATION'])) {
                        $_SERVER['HTTP_AUTHORIZATION'] = $_SERVER['HTTP_XAUTHORIZATION'];
                }
```language
```

これでどうにか繋がるようになりました．
同じような環境はそこそこありそうですけどね…．

<iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00944PQMK" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe> <iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=4873116686" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe> <iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00E5TJ120" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>

