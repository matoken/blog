cowsay では `-f` オプションで色々な COW を選択できます，

```
% cowsay -f suse The openSUSE is not OpenSUSE
 ______________________________
< The openSUSE is not OpenSUSE >
 ------------------------------
  \
   \____
  /@    ~-.
  \/ __ .- |
   // //  @
% cowsay -f duck ga~~~
 _______
< ga~~~ >
 -------
 \
  \
   \ >()_
      (__)__ _
```

この COW のリストは `-l` オプションで確認できます．
```
% cowsay -l
Cow files in /usr/share/cowsay/cows:
apt beavis.zen bong bud-frogs bunny calvin cheese cock cower daemon default
dragon dragon-and-cow duck elephant elephant-in-snake eyes flaming-sheep
ghostbusters gnu head-in hellokitty kiss kitty koala kosh luke-koala
mech-and-cow meow milk moofasa moose mutilated pony pony-smaller ren sheep
skeleton snowman sodomized-sheep stegosaurus stimpy suse three-eyes turkey
turtle tux unipony unipony-smaller vader vader-koala www
```

これをランダムに選択できないかと思ったのですがオプションに見当たりません．
ということでこんな感じで script を用意して，

```cowsay-random.bash
#!/bin/bash

COWS=( `cowsay -l | grep -v ^Cow | cut -f1-` )
cowsay -f ${COWS[$((RANDOM%${#COWS[@]}))]} $@
```

実行
```bash
% cowsay-random.bash hello!
 ________
< hello! >
 --------
  \
   \          .
       ___   //
     {~._.~}//
      ( Y )K/
     ()~*~()
     (_)-(_)
     Luke
     Skywalker
     koala
% cowsay-random.bash hello!
 ________
< hello! >
 --------
         \
          \
            ^__^
    _______/(oo)
/\/(       /(__)
   | W----|| |~|
   ||     || |~|  ~~
             |~|  ~
             |_| o
             |#|/
            _+#+_
```

fortune と繋いでみたり

```
% fortune /usr/share/games/fortunes | cowsay-random.bash -n
 _______________________________________________________________
/ Putt's Law:                                                   \
|         Technology is dominated by two types of people:       |
|                 Those who understand what they do not manage. |
\                 Those who manage what they do not understand. /
 ---------------------------------------------------------------
                       \                    ^    /^
                        \                  / \  // \
                         \   |\___/|      /   \//  .\
                          \  /O  O  \__  /    //  | \ \           *----*
                            /     /  \/_/    //   |  \  \          \   |
                            @___@`    \/_   //    |   \   \         \/\ \
                           0/0/|       \/_ //     |    \    \         \  \
                       0/0/0/0/|        \///      |     \     \       |  |
                    0/0/0/0/0/_|_ /   (  //       |      \     _\     |  /
                 0/0/0/0/0/0/`/,_ _ _/  ) ; -.    |    _ _\.-~       /   /
                             ,-}        _      *-.|.-~-.           .~    ~
            \     \__/        `/\      /                 ~-. _ .-~      /
             \____(oo)           *.   }            {                   /
             (    (--)          .----~-.\        \-`                 .~
             //__\\  \__ Ack!   ///.----..<        \             _ -~
            //    \\               ///-._ _ _ _ _ _ _{^ - - - - ~
```

login 時などに表示すると楽しいかもしれません．


- [cowsay 一覧 | matoken's meme](http://matoken.org/blog/blog/2015/09/10/cowsay-list/)
