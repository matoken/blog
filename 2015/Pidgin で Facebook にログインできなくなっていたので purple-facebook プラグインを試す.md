最近 Pidgin での Facebook 接続で以下のようなエラーが発生して接続できなくなっていた．  
<a data-flickr-embed="true" href="https://www.flickr.com/photos/119142834@N05/20252827410/in/dateposted-public/" title="Screenshot from 2015-08-10 07-13-39"><img src="https://farm1.staticflickr.com/550/20252827410_e1995b9dd2.jpg" width="278" height="60" alt="Screenshot from 2015-08-10 07-13-39"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

調べてみるとPidgin で利用している XMPP Chat API は2014-04-30 から非推奨になって，2015-04-30 から使えなくなっていたらしい．
- [Chat API (Deprecated)](https://developers.facebook.com/docs/chat)

何か方法はないかなと探してみると以下のようなプロジェクトを発見．

- [Home · jgeboski/purple-facebook Wiki](https://github.com/jgeboski/purple-facebook/wiki)
  - [Installing on *NIX · jgeboski/purple-facebook Wiki](https://github.com/jgeboski/purple-facebook/wiki/Installing-on-*NIX)
Debian/Ubuntu の i386/amd64 には pkg がある．(Todo list には Fedora/RHEL/CentOS/OpenSUSE/SLES があるのできっとそのうち)

これを使えばfacebook chat が Pidgin でまた利用出来るようになる?ということで試してみた．

## 導入環境

- Debian stretch amd64
- Pidgin 2.10.11-1

## リポジトリの登録

```
% sudo sh -c 'echo "deb http://download.opensuse.org/repositories/home:/jgeboski/Debian_8.0 ./" > /etc/apt/sources.list.d/jgeboski.list' 
```
この URL の `Debian_8.0` 部分は host のディストリビューションによって変わる．以下のページを参照して設定する．
- [Installing on *NIX · jgeboski/purple-facebook Wiki](https://github.com/jgeboski/purple-facebook/wiki/Installing-on-*NIX)

## 鍵登録

```
% wget wget http://download.opensuse.org/repositories/home:/jgeboski/Debian_8.0/Release.key
% gpg  ./Release.key
pub  2048R/1C85BB5E 2015-07-23 home:jgeboski OBS Project <home:jgeboski@build.opensuse.org>
% sudo apt-key add ./Release.key
OK
% rm ./Release.key
```
この URL の `Debian_8.0` 部分は host のディストリビューションによって変わる．以下のページを参照して設定する．
- [Installing on *NIX · jgeboski/purple-facebook Wiki](https://github.com/jgeboski/purple-facebook/wiki/Installing-on-*NIX)

## パッケージを最新にして `purple-facebook pkg` を導入

```
% sudo apt update && sudo apt upgrade && sudo apt install purple-facebook
```

Pidgin が起動していたら終了して起動し直す．
以下のようにこれまであった `facebook (XMPP)` の上に `Facebook` の項目が増えた．
<a data-flickr-embed="true" href="https://www.flickr.com/photos/119142834@N05/20252907088/in/dateposted-public/" title="Screenshot from 2015-08-10 07-40-23"><img src="https://farm4.staticflickr.com/3804/20252907088_8427ca00fe.jpg" width="363" height="448" alt="Screenshot from 2015-08-10 07-40-23"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

既存の設定を書き換えるか，新規に `Facebook` プロトコルでの設定を行い，接続を試みる．

## ログイン失敗

以下のエラーとなりログイン出来ない．

```
Login approvals are on. Expect an SMS shortly with a code to use for log in (406)
```

これはおそらく2段階認証が有効になっているため．パスワードの代わりに**コードジェネレータ** のコードを入力することでログインできた．  
<a data-flickr-embed="true" href="https://www.flickr.com/photos/119142834@N05/20253361730/in/dateposted-public/" title="Screenshot_2015-08-10-08-27-02"><img src="https://farm1.staticflickr.com/525/20253361730_dd31abd195_n.jpg" width="180" height="320" alt="Screenshot_2015-08-10-08-27-02"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>


＃Google Hungout もどうにかならないかな…………