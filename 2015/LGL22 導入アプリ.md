## LGL22 導入アプリ

LGL22 ですが，先日起動してもすぐに再起動してしまう様になってしまいました．起動してパスコードを入力してデスクトップが表示されるくらいの辺りで再起動してしまいます．アイコンが減っていたりするのでストレージが死んだかファイルシステムが壊れているかかな?と初期化しました．

とりあえず復活したようなので導入アプリのメモを書いておきます．

- Squera レジ
関東では使うことあったけど鹿児島では一度も使ったことがない……
- PayPal
- Debian News
- Go雨!探知機
XRAIN をオーバーレイしてくれる
- Gmail
- K-9 Mail
Gmail 以外のメールに利用
- APG
主に K-9 Mail での GPG に
- 音泉
- 響
- Tuneln Radio
- Raziko
- Radiko.jp
- らじるらじる
- Google Playミュージック
- DeviantArt
- Pixiv
- リトルノア
ゲーム．何気に続けてる
- Ingress
緑です
- Ingress Helper
- Intelgrated Timer
- Google+
利用率が一番高い
- Mustard
Twitter や GNU Social 対応クライアント
- SobaCha
軽くてユーザーストリームに対応した Twitter クライアント
- Twitter
- Plag
- Instagram
- Snapchat
- Swarm
- tsu
- Tumblrunning
- WharsApp
- Slack
- SlideShare
- Ustream
- WiFLE WIFI
War Driving に(携帯局も拾う)
- mineo スイッチ
SIMの利用料確認に
- Debian
chroot で Debian が動く．暇つぶしに良い
- Programmer Keyboard
- Google Goggles
翻訳(日本語非対応)したりバーコード読んだり
- WiFi QR Share
端末で設定してある Wi-Fi 設定を QR Code で表示したり※root
- プレゼンタイマ
- Bluetooth Auto Connect
Bluetooth 機器に自動接続する
- OsmAnd+
OSM の確認，POI 登録など
- MAPS ME
OSM 地図確認．見やすい
- OSMTraker
GPS ログ取りに
- Strava
散歩時のログ取りなどに
- GPS Status
- Google Camera
- Jota+
- JotterPad
- LibreOffice Viewer
- N2 TTS
- Voice Notify
N2 TTS とあわせて喋らせる
- ownCloud
- Photo Editor
- Rapica Reader
フェリー乗船前の残高確認に
- SMARTalk
- SuperSU
- 認証システム
Google Authenticator．バックアップコード引っぱり出したり再設定が面倒だった．バックアップ方法を調べたい．
- Titanium Backup
