先日リリースされたGNU Screen 4.3.0 の新機能が気になったのでbuild してみた。

- [GNU Screen v.4.3.0](http://lists.gnu.org/archive/html/screen-users/2015-06/msg00004.html)

> 
* New commands:
  - 'sort' command sorting windows by title
  - 'bumpleft', 'bumpright' - manually move windows on window list
  - 'collapse' removing numbering 'gaps' between windows, by
    renumbering 

Build とか

```bash
% sudo apt-get install build-essential
% sudo apt-get build-dep screen
% wget http://ftp.gnu.org/gnu/screen/screen-4.3.0.tar.gz http://ftp.gnu.org/gnu/screen/screen-4.3.0.tar.gz.sig
% gpg --verify screen-4.3.0.tar.gz.sig
gpg: assuming signed data in `screen-4.3.0.tar.gz'
gpg: Signature made 2015年06月14日 02時01分43秒 JST using RSA key ID C53EF3A4
gpg: Good signature from "Amadeusz Sławiński <amade@asmblr.net>"
gpg: WARNING: This key is not certified with a trusted signature!
gpg:          There is no indication that the signature belongs to the owner.
Primary key fingerprint: 2EE5 9A5D 0C50 167B 5535  BBF1 B708 A383 C53E F3A4
% tar tvzf screen-4.3.0.tar.gz
% tar xzf screen-4.3.0.tar.gz
% cd screen-4.3.0
% ./configure --prefix=$HOME/usr/local --enable-colors256 --enable-rxvt_osc --enable-use-locale --enable-pam
% make
% make install
% where screen
/home/mk/usr/local/bin/screen
/usr/bin/screen
% screen --version
Screen version 4.03.00 (GNU) 27-May-15
```


そして気になっていた新機能を試してみました。

> 
* New commands:
  - 'sort' command sorting windows by title

想像ではawesome みたいにウィンドウの並びが縦横分割されていい感じに並ぶのかなーと思っていたのだけど、大分違った。
`Ctrl-a :` して `sort` と打ってみると、

> 
Sorting inside a window is not allowed. Push CTRL-a " and try again

と言われる。`Ctrl + a "` してscreen のウィンドウリストを出した状態で再度叩くとこんな感じでウィンドウタイトルでソートされた。

<a href="https://www.flickr.com/photos/119142834@N05/18737167608" title="Screenshot from 2015-06-17 19:21:38 by Kenichiro MATOHARA, on Flickr"><img src="https://c1.staticflickr.com/1/500/18737167608_a401c900cd.jpg" width="500" height="75" alt="Screenshot from 2015-06-17 19:21:38"></a>

ちなみに、

> 
  - 'bumpleft', 'bumpright' - manually move windows on window list

も試したけどコアダンプしたorz
多分指定ウィンドウを左右に移動出来るのだと思う

> 
[screen caught signal 11. (core dumped)]

