<!--
デジモノステーション2016年2月号の付録の0SIMをRaspberry Piに繋ぐ
-->

デジモノステーション 2016年2月号に０SIM by So-net というものが付録で付いてきて一部で流行っています．
- [雑誌「デジモノステーション」最新号 – DIGIMONO！（デジモノ！）](http://www.digimonostation.jp/magazine/)

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/23404814754/in/dateposted-public/" title="IMG_20151229_022627"><img src="https://farm2.staticflickr.com/1680/23404814754_33f1ca95c2_m.jpg" width="240" height="180" alt="IMG_20151229_022627"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script><a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/24007043056/in/dateposted-public/" title="IMG_20151229_022636"><img src="https://farm6.staticflickr.com/5757/24007043056_73f6a40538_m.jpg" width="180" height="240" alt="IMG_20151229_022636"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

これはこの雑誌を買って付いてきた付録のSIMを使うと雑誌代(620円)だけで開通手続き手数料無料で月あたり500MB迄無料で利用できるというもの．勿論他にSIMを刺す端末は必要です．

> 
【０SIM by So-net】 詳細
形状：nanoSIM 規格：０SIM by So-net 種類：データ通信専用
データ量と料金（２段階定額）
0MB～499MB：0円
500MB ～ 2047MB：100円～1500円
2048MB ～：1600円
速度制限：なし（ただし5GBまで）/ NTTドコモ 4G LTE（下り最大225Mbps）
開通期限：2016年2月24日

上限もあるのでうっかり使いすぎても安心感があります．ちなみに上限をよく超えるようだと別のプランにしたほうがお得です．
一人で複数契約は出来ないのでたくさん買って500MBごとに差し替えて使うと言ったことは出来ない，3ヶ月利用しないと自動解約されるようなので寝かせておくことも出来ないよう．
普通に使うとあっという間に500MB超えちゃうでしょうけど使いようによっては便利に使えそう(気象センサのデータを送るとかなら余裕)．ということで試してみたかったのですがここは鹿児島．都内から最低でも2日遅れなので試せないでいました．昨日やっとコンビニで入手出来たので試してみました．

ちなみにAmazonでは売り切れでマーケットプレイス扱いの物しか無いようです．その中で安いものはSIMなしと書かれているので注意しましょう．
<iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B018UJLJU2" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>

今回はUSBモデムとして以前500円ほどで買った Docomo L-02C を Rasbian jessie を導入した Raspberry Pi 2B に接続して wvdial で接続しました．このモデムは結構電気を食うらしく電源がある程度大容量でないととても不安定になります．今回は河野総統謹製の RaspberryPot という GPIO 経由での電源を利用しました．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/24006934536/in/dateposted-public/" title="IMG_20151229_071104"><img src="https://farm6.staticflickr.com/5761/24006934536_61a4e1abf3.jpg" width="500" height="375" alt="IMG_20151229_071104"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

# 開通手続き

次のページから開通手続きを行います．開通手続きの期限は2016年02月24日．
- _[http://lte.so-net.ne.jp/r/0sim/a/](http://lte.so-net.ne.jp/r/0sim/a/)_

ログイン情報はSIMの台紙に書かれています．
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/23950599091/in/dateposted-public/" title="IMG_20151229_022855"><img src="https://farm6.staticflickr.com/5648/23950599091_367b660a12_n.jpg" width="320" height="240" alt="IMG_20151229_022855"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

# 必要なソフトウェアの導入

```
$ sudo apt update && sudo apt upgrade
$ sudo apt install cu wvdial
```

# 利用ユーザ `pi` の `dialout` グループへの登録

この手続きをしないと，ダイヤル時など毎回 sudo しないといけません．
```
$ sudo addgroup pi dialout
```
※反映には要ログインしなおし

# Docomo L-02C への APN登録

##モデムへの接続と動作確認
`/dev/ttyUSB2` 部分は `dmesg|tail`などとして確認して下さい．モデムの他に何も接続していなければ ttyUSB0~ttyUSB3 の4つのデバイスが確認できるはずです．
```
% cu -l /dev/ttyUSB2 -s 115200
Connected.
atz
OK
```

## 現在のAPN確認

```
AT+CGDCONT?
+CGDCONT: 1,"IP","mopera.net",,0,0,0
+CGDCONT: 11,"IP","mopera.net",,0,0,0

OK
```

## APN(`so-net.jp`)を設定
```
AT+CGDCONT=1,"IP","so-net.jp"
OK
```

※次のようにして複数のAPNを設定することも可能

```
AT+CGDCONT=2,"IP","lte.nttplala.com"
OK
AT+CGDCONT=3,"IP","mineo-d.jp"
OK
```

## APNが登録できたか確認
```
AT+CGDCONT?
+CGDCONT: 1,"IP","so-net.jp",,0,0,0
+CGDCONT: 11,"IP","mopera.net",,0,0,0
OK
```

## 設定の書き込み
```
ATZ0
OK
```

## モデムから切断
```
~.

Disconnected.
```

# ダイヤルアップのために `wvdial` の設定

`/etc/wvdial.conf` に以下を追記．
```
[Dialer 0sim]
Modem Type = Analog Modem
Phone = *99***1#
Carrier Check = no
Auto Reconnect = yes
Stupid Mode = yes

ISDN = 0
Init1 = ATZ
Init2 = ATH
Init3 = AT+CGDCONT?
Init4 = ATQ0 V1 E1 S0=0 &C1 &D2 +FCLASS=0
Dial Command = ATD
Modem = /dev/ttyUSB2
Baud = 115200

Username = nuro
Password = nuro

```

## 接続

```
$ wvdial 0sim
```

## 接続確認

```
$ w3m -dump http://ifconfig.me/all
ip_addr: 118.241.XXX.XXX
remote_host: XXXXXXXXXX.ap.nuro.jp
user_agent: w3m/0.5.3+debian-19
port: 44286
lang: ja;q=1.0, en;q=0.5
connection: 
keep_alive: 
encoding: gzip, compress, bzip, bzip2, deflate
mime: text/html, text/*;q=0.5, image/*, application/*, audio/*, video/*, x-scheme-handler/*, x-content/*, inode/*
charset: 
via: 
forwarded: 
```

# 利用状況確認

対のページからログインして利用状況が確認できます．また，400MB を超えるとメールでお知らせも来るらしいです．
- [ユーザWebメインメニュー](https://www.so-net.ne.jp/retail/w)

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/23737366240/in/dateposted-public/" title="20151229_07:12:50-10076"><img src="https://farm6.staticflickr.com/5785/23737366240_1469e51923_n.jpg" width="320" height="241" alt="20151229_07:12:50-10076"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

ということで暫く試してみたいと思います．

<iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B018UJLJU2" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe><iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00J2VIPC8" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe><iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00TBKFAI2" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>
※Amazonで雑誌を買う場合はSIMなしのものもあるのでよく確認して購入しましょう．


# 関連URL
- [Linux/Device/Docomo_L-02C - matoken's wiki.](http://hpv.cc/~maty/pukiwiki1/index.php?Linux%2FDevice%2FDocomo_L-02C)


