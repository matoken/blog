- [Yandex.disk service offers a webdav supported platform with 10 Gb free space +…](https://plus.google.com/u/0/+ZekiBildirici/posts/aNSaRPtm22b)

Yandex.Disk は Linux と BSD に ODF 閲覧機能まで!ってことでロシアでシェアの大きいらしい Yandex のアカウントを取ってみました．  
＃ matoken 空いてた:)  
＃＃ここからアカウントを作ると　+512MB  
[https://disk.yandex.com/invite/?hash=URJFTFG9](https://disk.yandex.com/invite/?hash=URJFTFG9)

早速 Yandex.Disk に日本語の .odt をUp してみると，

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/21306984480/in/dateposted-public/" title="20150918_05:09:39-19415"><img src="https://farm6.staticflickr.com/5836/21306984480_85a3a17295_n.jpg" width="201" height="320" alt="20150918_05:09:39-19415"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/21308127529/in/dateposted-public/" title="20150918_05:09:25-20153"><img src="https://farm1.staticflickr.com/778/21308127529_501135a1f0_n.jpg" width="320" height="147" alt="20150918_05:09:25-20153"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

- 日本語ファイル名ok
- ファイル一覧画面でのプレビューは日本語豆腐
- ファイル閲覧ページはは日本語表示ok

という感じでした．

ファイル共有サービスの BSD サポートは実は WebDAV が使えるので Nautilus やDolphin で繋いでねって感じでした．これはちょっとがっかり．WebDAV で OK だと Otixo や ownCloud 経由でかなりのサービスで対応出来てますね．

- [WebDAV in Linux and FreeBSD — Disk — Yandex.Support](https://yandex.com/support/disk/webdav/webdav-linux.xml)

Linux のクライアントは .deb/.rpm のパッケージが提供されていて，.deb はリポジトリも用意されているので手間が省けていいです．但し Console client なので GUI がいい人は WebDAV でいいこともあるかもしれません．(サードパーティ製の GUI はあるみたい)

- [Console client for Linux — Disk — Yandex.Support](https://yandex.com/support/disk/cli-clients.xml)

ちなみに Yandex は OSM にこういう貢献もしてるので好印象だったりします．

- [New Tile Server in Moscow | OpenStreetMap Blog](https://blog.openstreetmap.org/2012/10/24/new-tile-server-in-moscow/)



今のところ ODF の編集は ownCloud で簡易編集が可能，Microsoft OfficeOnline でも編集可能らしい(未検証)．案外選択肢無いですね……．  
Android/iOS 辺りのサポートもされると出先でのプレゼンテーションなどで便利になりそう．(でもこっちは無線プロジェクターの普及が進まないと利用者が増えないかな?)

- [LibreOffice Viewer - Google Play の Android アプリ](https://play.google.com/store/apps/details?id=org.documentfoundation.libreoffice)


