- [Terminal Markdown Viewer - ターミナル上でMarkdownファイルを整形&表示 MOONGIFT](http://www.moongift.jp/2015/10/terminal-markdown-viewer-%e3%82%bf%e3%83%bc%e3%83%9f%e3%83%8a%e3%83%ab%e4%b8%8a%e3%81%a7markdown%e3%83%95%e3%82%a1%e3%82%a4%e3%83%ab%e3%82%92%e6%95%b4%e5%bd%a2%e8%a1%a8%e7%a4%ba/)
- [axiros/terminal_markdown_viewer](https://github.com/axiros/terminal_markdown_viewer)

これは便利そう．ということで試してみました．

```
% git clone https://github.com/axiros/terminal_markdown_viewer.git
% terminal_markdown_viewer/mdv.py -h
Usage:
    mdv [-t THEME] [-T C_THEME] [-x] [-l] [-L] [-c COLS] [-f FROM] [-m] [-M DIR] [-H] [-A] [MDFILE]

Options:
    MDFILE    : Path to markdown file
    -t THEME  : Key within the color ansi_table.json. 'random' accepted.
    -T C_THEME: Theme for code highlight. If not set: Use THEME.
    -l        : Light background (not yet supported)
    -L        : Display links
    -x        : Do not try guess code lexer (guessing is a bit slow)
    -f FROM   : Display FROM given substring of the file.
    -m        : Monitor file for changes and redisplay FROM given substring
    -M DIR    : Monitor directory for markdown file changes
    -c COLS   : Fix columns to this (default: your terminal width)
    -A        : Strip all ansi (no colors then)
    -H        : Print html version

Notes:

    We use stty tool to derive terminal size.

    To use mdv.py as lib:
        Call the main function with markdown string at hand to get a
        formatted one back.

    FROM:
        FROM may contain max lines to display, seperated by colon.
        Example:
        -f 'Some Head:10' -> displays 10 lines after 'Some Head'
        If the substring is not found we set it to the *first* charactor of the
        file - resulting in output from the top (if you terminal height can be
        derived correctly through the stty cmd).

    File Monitor:
        If FROM is not found we display the whole file.

    Directory Monitor:
        We check only text file changes, monitoring their size.

        By default .md, .mdown, .markdown files are checked but you can change
        like -M 'mydir:py,c,md,' where the last empty substrings makes mdv also
        monitor any file w/o extension (like 'README').

        Running actions on changes:
        If you append to -M a '::<cmd>' we run the command on any change
        detected (sync, in foreground).
        The command can contain placeholders:
            _fp_    : Will be replaced with filepath
            _raw_   : Will be replaced with the base64 encoded raw content
                      of the file
            _pretty_: Will be replaced with the base64 encoded prettyfied output

        Like: mdv -M './mydocs:py,md::open "_fp_"'  which calls the open
        command with argument the path to the changed file.


    Theme rollers:
        mdv -T all:  All available code styles on the given file.
        mdv -t all:  All available md   styles on the given file.
                    If file is not given we use a short sample file.

        So to see all code hilite variations with a given theme:
            Say C_THEME = all and fix THEME
        Setting both to all will probably spin your beach ball, at least on OSX.

    Lastly: Using docopt, so this docstring is building the options checker.
    -> That's why this app can't currently use itself for showing the docu.
    Have to find a way to trick docopt to parse md ;-)
```

簡単に導入実行できていい感じです．
色付きのまま使えるページャーが欲しいのですがなにかいいものはないですかね……．
とりあえずは `-A` option で色無しで lv コマンドあたりに食わせています．

```
% terminal_markdown_viewer/mdv.py -A ~/Documents/pelican/content/1st_post.md |lv

```

[![asciicast](https://asciinema.org/a/29763.png)](https://asciinema.org/a/29763)
