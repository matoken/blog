Dropbox に対不特定多数向けファイルアップロード機能が付いたということで試してみました。
(&lt;br&gt;タグが……)

- [Easily collect photos, docs, and more with file requests | Dropbox Blog](https://blogs.dropbox.com/dropbox/2015/06/introducing-file-requests/)
<a href="https://www.flickr.com/photos/119142834@N05/18304179443" title="Screenshot from 2015-06-18 09:28:26 by Kenichiro MATOHARA, on Flickr"><img src="https://c1.staticflickr.com/1/260/18304179443_4169f9e2ff.jpg" width="500" height="248" alt="Screenshot from 2015-06-18 09:28:26"></a>

ファイル非公開anonymous FTP の変りに使えるのかな?
てことで試しにURL 公開
<a href="https://www.flickr.com/photos/119142834@N05/18737168078" title="Screenshot from 2015-06-18 09:30:57 by Kenichiro MATOHARA, on Flickr"><img src="https://c1.staticflickr.com/1/443/18737168078_90eb764e02.jpg" width="500" height="386" alt="Screenshot from 2015-06-18 09:30:57"></a>
<a href="https://www.flickr.com/photos/119142834@N05/18738649969" title="Screenshot from 2015-06-18 09:31:15 by Kenichiro MATOHARA, on Flickr"><img src="https://c1.staticflickr.com/1/463/18738649969_a6287a5cd6.jpg" width="500" height="363" alt="Screenshot from 2015-06-18 09:31:15"></a>
<a href="https://www.flickr.com/photos/119142834@N05/18898558096" title="Screenshot from 2015-06-18 09:31:26 by Kenichiro MATOHARA, on Flickr"><img src="https://c1.staticflickr.com/1/446/18898558096_7dc121b95b.jpg" width="500" height="200" alt="Screenshot from 2015-06-18 09:31:26"></a>
<a href="https://www.flickr.com/photos/119142834@N05/18919562052" title="Screenshot from 2015-06-18 09:40:35 by Kenichiro MATOHARA, on Flickr"><img src="https://c1.staticflickr.com/1/380/18919562052_846d70a6fd.jpg" width="500" height="314" alt="Screenshot from 2015-06-18 09:40:35"></a>

https://www.dropbox.com/request/4mt4eOii9dJb3N5f0LsP﻿

アップロードには氏名とメールアドレスが必要
適当でもよさそうだけど
<a href="https://www.flickr.com/photos/119142834@N05/18919562092" title="Screenshot from 2015-06-18 09:43:28 by Kenichiro MATOHARA, on Flickr"><img src="https://c2.staticflickr.com/4/3942/18919562092_f3a58c2dd1.jpg" width="308" height="500" alt="Screenshot from 2015-06-18 09:43:28"></a><a href="https://www.flickr.com/photos/119142834@N05/18898559006" title="Screenshot from 2015-06-18 09:43:42 by Kenichiro MATOHARA, on Flickr"><img src="https://c1.staticflickr.com/1/298/18898559006_7e2989129c.jpg" width="271" height="500" alt="Screenshot from 2015-06-18 09:43:42"></a>
<a href="https://www.flickr.com/photos/119142834@N05/18919562302" title="Screenshot from 2015-06-18 09:44:24 by Kenichiro MATOHARA, on Flickr"><img src="https://c2.staticflickr.com/4/3686/18919562302_32a0e1c04c.jpg" width="300" height="335" alt="Screenshot from 2015-06-18 09:44:24"></a>
<a href="https://www.flickr.com/photos/119142834@N05/18919562552" title="Screenshot from 2015-06-18 09:46:17 by Kenichiro MATOHARA, on Flickr"><img src="https://c1.staticflickr.com/1/510/18919562552_f7606cf078.jpg" width="500" height="127" alt="Screenshot from 2015-06-18 09:46:17"></a>

まだこなれてないようでWeb画面英語になったり日本語になったり
ファイルUp者へのメールは日本語で、共有元には英語だったりしました。

ファイル非公開anonymous FTP の様にファイル提出を不特定多数から受け取るのに良さそうです。容量制限などが設定出来るといいかもですね。（1ファイル1MB 迄とか.txt のみとか）

