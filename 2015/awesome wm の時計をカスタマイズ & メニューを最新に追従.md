<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/21137641799/in/dateposted-public/" title="20150912_00:09:11-13117"><img src="https://farm1.staticflickr.com/717/21137641799_766efe4748_o.jpg" width="433" height="129" alt="20150912_00:09:11-13117"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

## 時計をカスタマイズ

デスクトップの右上に時計が表示されますが，描画表示されません．ここに秒も表示したいです．
`awful.widget.textclock() で実現しているようなのでここのパラメーターを変更することで実現できました．

- [awesome API documentation](http://awesome.naquadah.org/doc/api/modules/awful.widget.textclock.html)

```txt
Parameters:

format The time format. Default is " %a %b %d, %H:%M ".
timeout How often update the time. Default is 60.
```

以下のように書き換えました．

```
-- {{{ Wibox
-- Create a textclock widget
-mytextclock = awful.widget.textclock()
+mytextclock = awful.widget.textclock("%a %b %d, %H:%M:%S", 1)
```
規定値は `%a %b %d, %H:%M", 60` で前半の " で囲ってある部分は date の FORMAT のようなので `man date` を参照して `:%S` を末尾に追加，後半の `60` は更新時間(秒)のようなのでこのままでは60秒に1回しか更新されないので1秒単位で更新されるように `1` を指定しました．

## メニューを最新に追従するようにする

左上の awesome のメニューの `Debian` ツリーに導入されたアプリケーションメニューがありますが，awesome 初回実行時にコピーされたものが参照されます．そのためそれ以降に導入，削除したものは反映されません．大元の設定は随時反映されるのでそちらを参照するようにします．

- 参照しているメニューファイル: `~/.config/awesome/debian/menu.lua`
- 元メニューファイル: `/etc/xdg/awesome/debian/menu.lua`

自分のホーム以下のメニューは削除してしまってシンボリックリンクを貼ることにします．

```
% rm -rf ~/.config/awesome/debian
% ln -s /etc/xdg/awesome/debian ~/.config/awesome/debian
```

メニューのカスタマイズは以下のような感じで `rc.lua` 内で行うようにしました．数が増えるようならファイルを分けるといいかもしれません．

```
+fav_menu = {
+   { "Nautilus", "nautilus" },
+   { "Atom", "atom" },
+   { "Chromium privacy", "/home/mk/script/chromium-privacy.bash" },
+   { "mikutter", "mikutter" },
+   { "LibreOffice", "libreoffice" },
+}
+
 mymainmenu = awful.menu({ items = { { "awesome", myawesomemenu, beautiful.awesome_icon },
                                     { "Debian", debian.menu.Debian_menu.Debian },
+                                    { "fav", fav_menu },
                                     { "open terminal", terminal }
                                   }
                         })
```

全体の設定は以下の Gist を参照して下さい．

- [awesome configurarion file](https://gist.github.com/matoken/cdd636cc8d16dc824c05)

これまでの awesome 関連のエントリは以下を参照してください．

- [awesome | matoken's meme](http://matoken.org/blog/blog/category/oss/awesome/)
