# 端末をリアルタイムに Web で共有できる GoTTY


- [yudai/gotty](https://github.com/yudai/gotty)
以下のアニメーションでだいたい解ると思いますが，端末を Webブラウザで公開することのできるアプリケーションです．Go で書かれているようです．
![](https://raw.githubusercontent.com/yudai/gotty/master/screenshot.gif)

端末の内容を公開するだけでなく， `-w` オプションを付けると入力でもできるので普通に端末操作が Webブラウザから可能です．

## 導入例

`~/usr/local/go` 以下に導入している．

```
% GOPATH=~/usr/local/go go get github.com/yudai/gotty
% ~/usr/local/go/bin/gotty top
```

この状態で http://localhost:8080/ にアクセスしてみると `top` コマンドが表示されます．
色々試してみましたが， Nyancat は遅延なく動きましたが，cacademo は表示されませんでした．

`gotty bash` とかすればみんなでひとつの端末を触れるのかな?と思ったのですが，ブラウザでアクセスするごとにセッションが作られるようでうまくいきませんでした．

```
2015/08/23 14:47:42 Server is starting with command: bash
2015/08/23 14:47:46 New client connected: 127.0.0.1:36194
2015/08/23 14:47:46 Command is running for client 127.0.0.1:36194 with PID 28469
2015/08/23 14:48:56 New client connected: 127.0.0.1:36208
2015/08/23 14:48:56 Command is running for client 127.0.0.1:36208 with PID 29315
```

README.md に以下のような記述が，

```
Sharing with Multiple Clients

Gotty starts a new process when a new client connects to the server. This means users cannot share a single terminal with others by default. However, you can use terminal multiplexers for sharing a single process with multiple clients.

For example, you can start a new tmux session named gotty with top command by the command below.

$ gotty tmux new -A -s gotty top
This command doesn't allow clients to send keystrokes, however, you can attach the session from your local terminal and run operations like switching the mode of the top command. To connect to the tmux session from your terminal, you can use following command.

$ tmux new -A -s gotty
By using terminal multiplexers, you can have the control of your terminal and allow clients to just see your screen.
```

ということで `gotty tmux new -A -s gotty` で実現できました．GNU Screen でも出来ないかとセッション名を同じように `gotty` にして `multiuser on` にしてみましたがこれだけではうまく行きませんでした．GoTTY 本体に手を入れる必要があるかもしれません．

ただ，何も考えずに使うとプロトコルは http なので盗み見もできるでしょうし誰でも操作できるしで危険です．一応ランダムURL や Bashic AUTH のオプションはありますが十分ではないでしょう．

ドキュメントでは，Docker を利用する例が載っています．

```
Playing with Docker

When you want to create a jailed environment for each client, you can use Docker containers like following:

$ gotty -w docker run -it --rm busybox
```

Fw の内側で社内や，勉強会などの内輪で一時的に公開して公開後はイメージを破棄するくらいが良いのかもしれません．


そういえば勉強会サーバも作っていたのでした．GoTTY も入れてみようと思います．
<iframe src="//www.slideshare.net/slideshow/embed_code/key/vPivD9Zq1Befvh" width="425" height="355" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/matoken/ss-51975843/#12" title="オープンソースカンファレンス大分参加報告 &amp; 勉強会向けサーバを作ってみる" target="_blank">オープンソースカンファレンス大分参加報告 &amp; 勉強会向けサーバを作ってみる</a> </strong> from <strong><a href="//www.slideshare.net/matoken" target="_blank">Kenichiro MATOHARA</a></strong> </div>