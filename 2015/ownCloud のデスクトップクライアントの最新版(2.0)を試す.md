# ownCloud のデスクトップクライアントの最新版(2.0)を試す

<!-- Place this tag in your head or just before your close body tag. -->
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<!-- Place this tag where you want the widget to render. -->
<div class="g-post" data-href="https://plus.google.com/+OwncloudOfficial/posts/cgSH7SeeH1P"></div>

- [download.owncloud.com/download/changelog-client](http://download.owncloud.com/download/changelog-client)
```
Release 2.0.0
Release August 25th 2015
  * Add support for multiple accounts (#3084)
  * Do not sync down new big folders from server without users consent (#3148)
  * Integrate Selective Sync into the default UI
  * OS X: Support native finder integration for 10.10 Yosemite (#2340)
  * Fix situation where client would not reconnect after timeout (#2321)
  * Use SI units for the file sizes
  * Improve progress reporting during sync (better estimations, show all files, show all bandwidth)
  * Windows: Support paths >255 characters (#57) by using Windows API instead of POSIX API
  * Windows, OS X: Allow to not sync hidden files (#2086)
  * OS X: Show file name in UI if file has invalid UTF-8 in file name
  * Sharing: Make use of Capability API (#3439)
  * Sharing: Do not allow sharing the root folder (#3495)
  * Sharing: Show thumbnail
  * Client Updater: Check for updates periodically, not only once per run (#3044)
  * Windows: Remove misleading option to remove sync data (#3461)
  * Windows: Do not provoke AD account locking if password changes (#2186)
  * Windows: Fix installer when installing unprivileged (#2616, #2568)
  * Quota: Only refresh from server when UI is shown
  * SSL Button: Show more information
  * owncloudcmd: Fix --httpproxy (#3465)
  * System proxy: Ask user for credentials if needed
  * Several fixes and performance improvements in the sync engine
  * Network: Try to use SSL session tickets/identifiers. Check the SSL button to see if they are used.
  * Bandwidth Throttling: Provide automatic limit setting for downloads (#3084)
  * Systray: Workaround for issue with Qt 5.5.0 #3656
```

ということでマルチアカウント対応とか便利そう!ってことで試してみました．
導入は ownCloud 自体が公開している OBS のリポジトリから apt で導入しました．

- [software.opensuse.org: パッケージのインストール: isv:ownCloud:community / owncloud](https://software.opensuse.org/download/package?project=isv:ownCloud:community&package=owncloud)

クライアントの `一般` タブの `アカウントを追加` ボタンで普通に追加できます．
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/20963727882/in/dateposted-public/" title="20150828_18:08:02-17719"><img src="https://farm1.staticflickr.com/677/20963727882_8de247605a.jpg" width="500" height="356" alt="20150828_18:08:02-17719"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

同期複数アカウントの場合は明示的にローカルフォルダを指定しないといけません．
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/20785400690/in/dateposted-public/" title="20150829_19:08:11-22354"><img src="https://farm1.staticflickr.com/686/20785400690_db173c3615.jpg" width="500" height="442" alt="20150829_19:08:11-22354"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

同期フォルダが選択できるようになってるのも便利ですね．これまでは除外フォルダとして指定していました．
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/20973466965/in/dateposted-public/" title="20150829_19:08:55-22932"><img src="https://farm1.staticflickr.com/759/20973466965_14832af4cd.jpg" width="500" height="356" alt="20150829_19:08:55-22932"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

自宅サーバで大容量．VPS で出先でアクセスするサーバとか，プライベートや仕事用アカウントやサーバを分けて使うといったことがお手軽にできるようになって便利になりました :-)

ownCLoud どんどん便利になっていきますねー．例えば Raspberry Pi の Rasbian でもパッケージで提供されているので，apt 一発で導入でき省電力ファイルサーバが簡単に作れたりもします．お勧めです．
