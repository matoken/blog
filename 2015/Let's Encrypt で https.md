
# Let's Encrypt を使った https 設定

[![](https://letsencrypt.org/images/letsencrypt-logo-horizontal.svg)](https://letsencrypt.org/)  
[Let's Encrypt](ttps://letsencrypt.org/)

Let's Encrypt に以前メールアドレスを登録していたのですが，

> Let's Encrypt Closed Beta Invite

とうことでメールが届いていたので試してみました．

ちなみに Let's Encrypt は DV(Domain Validation)証明書が無料で取得できるサービスで，経路の暗号化はされるけど組織の実在確認まではしないレベルの物．

早速試してみます．
実行環境は Debian jessie amd64 + Apache httpd 2.4(2.4.10-10+deb8u3)

### Let's Encrypt のツールを入手して実行する．
```
$ git clone https://github.com/letsencrypt/letsencrypt.git
$ cd letsencrypt
$ ./letsencrypt-auto --agree-dev-preview --server https://acme-v01.api.letsencrypt.org/directory auth
Bootstrapping dependencies for Debian-based OSes...
[sudo] password for user:
```

とすると，必要なパッケージを自動的に導入し始めます．
質問がいくつか来るけど，メールアドレス(Let's Encrypt 登録時のもの)とコモンネームにApache とそれ以外の選択くらい．これで自動的に Let's Encrypt のサーバと通信を行い証明書の取得ホト損までしてくれます．
そして，最後にこんな注意書きが表示されました．

```
IMPORTANT NOTES:
 - If you lose your account credentials, you can recover through
   e-mails sent to user@example.org.
 - Congratulations! Your certificate and chain have been saved at
   /etc/letsencrypt/live/example.org/fullchain.pem. Your cert will
   expire on 2016-02-02. To obtain a new version of the certificate in
   the future, simply run Let's Encrypt again.
 - Your account credentials have been saved in your Let's Encrypt
   configuration directory at /etc/letsencrypt. You should make a
   secure backup of this folder now. This configuration directory will
   also contain certificates and private keys obtained by Let's
   Encrypt so making regular backups of this folder is ideal.
```

/etc 以下はデイリーでバックアップ取ってるので大丈夫なはず．24時間以上経ったけどメールは未だ届いていないよう．
そして，自動的に `/etc/apache2/sites-available/009-kagolinux.conf` を元に `/etc/apache2/sites-available/009-kagolinux-le-ssl.conf` が作られていました．
※このファイルは `letsencrypt-auto` を再実行すると消えて実行完了後再生成(以下の修正も必要)されたのでちょっと嫌．多分オプションとかで回避できると思うけど未確認．

以下の2行だけ修正して，

```
SSLCertificateFile /etc/letsencrypt/live/kagolug.org/fullchain.pem
SSLCertificateKeyFile /etc/letsencrypt/live/kagolug.org/privkey.pem
```

a2ensite して restart で
```
$ sudo a2ensite 009-kagolinux-le-ssl
$ sudo service apache2 reload
```

とりあえず動いた

```
% openssl s_client -connect kagolug.org:443
CONNECTED(00000003)
depth=2 O = Digital Signature Trust Co., CN = DST Root CA X3
verify return:1
depth=1 C = US, O = Let's Encrypt, CN = Let's Encrypt Authority X1
verify return:1
depth=0 CN = kagolug.org
verify return:1
---
Certificate chain
 0 s:/CN=kagolug.org
   i:/C=US/O=Let's Encrypt/CN=Let's Encrypt Authority X1
 1 s:/C=US/O=Let's Encrypt/CN=Let's Encrypt Authority X1
   i:/O=Digital Signature Trust Co./CN=DST Root CA X3
---
Server certificate
-----BEGIN CERTIFICATE-----
MIIE/TCCA+WgAwIBAgISAbbh6Bp+aXaatBj/TJ7lkyyZMA0GCSqGSIb3DQEBCwUA
MEoxCzAJBgNVBAYTAlVTMRYwFAYDVQQKEw1MZXQncyBFbmNyeXB0MSMwIQYDVQQD
ExpMZXQncyBFbmNyeXB0IEF1dGhvcml0eSBYMTAeFw0xNTExMDQxNjQ4MDBaFw0x
NjAyMDIxNjQ4MDBaMBYxFDASBgNVBAMTC2thZ29sdWcub3JnMIIBIjANBgkqhkiG
9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzb79Wux4LWzC+ZKTRRXN53+/IRraso2AZRrt
/wesf4EBEIl8i6Iu4Dl0FjLoomxZUCN0T7C5iJ4aPy629UkWDZrawFWGkXYT00ed
UziOKTXpYYTM9BBp9Qx1aw/CT8XY6TjOtaJ21AjcIXZBZ8EPnf6fWcHEFCsNYLKk
7U/e59WJ1B1ciXowS7nMwDy1c3rvu7tlzGRuO/xSx/hu0R5DYL8zyPlLwGZyfVv/
UYYtY6Wf8ItzgthpzltqtbMv4Kuohwu2mPwKQJ73MJoOghUD4p6oxiJ3nsgLY8DO
mIlW6ScXihlZ/pWfzjWaohKsvWM+qgnQpWNUQoaXNj0ES34m4QIDAQABo4ICDzCC
AgswDgYDVR0PAQH/BAQDAgWgMB0GA1UdJQQWMBQGCCsGAQUFBwMBBggrBgEFBQcD
AjAMBgNVHRMBAf8EAjAAMB0GA1UdDgQWBBQH4Pb+Brg5Q0PWhEZ7CEdAkxWKxzAf
BgNVHSMEGDAWgBSoSmpjBH3duubRObemRWXv86jsoTBwBggrBgEFBQcBAQRkMGIw
LwYIKwYBBQUHMAGGI2h0dHA6Ly9vY3NwLmludC14MS5sZXRzZW5jcnlwdC5vcmcv
MC8GCCsGAQUFBzAChiNodHRwOi8vY2VydC5pbnQteDEubGV0c2VuY3J5cHQub3Jn
LzAWBgNVHREEDzANggtrYWdvbHVnLm9yZzCCAQAGA1UdIASB+DCB9TAKBgZngQwB
AgEwADCB5gYLKwYBBAGC3xMBAQEwgdYwJgYIKwYBBQUHAgEWGmh0dHA6Ly9jcHMu
bGV0c2VuY3J5cHQub3JnMIGrBggrBgEFBQcCAjCBngyBm1RoaXMgQ2VydGlmaWNh
dGUgbWF5IG9ubHkgYmUgcmVsaWVkIHVwb24gYnkgUmVseWluZyBQYXJ0aWVzIGFu
ZCBvbmx5IGluIGFjY29yZGFuY2Ugd2l0aCB0aGUgQ2VydGlmaWNhdGUgUG9saWN5
IGZvdW5kIGF0IGh0dHBzOi8vbGV0c2VuY3J5cHQub3JnL3JlcG9zaXRvcnkvMA0G
CSqGSIb3DQEBCwUAA4IBAQBa5DjWSE/d6alvGUDNW4guiJauqvxB3B+YULzRTseb
0kXGyu46u16F4av+Ate0Jxq3NnZdOpy8OTiL/wGQeWOWs33zdlxii5o8R12pMMTS
/NWFxawiCkJnzpWkhdLQGv3RNUUQn0w5yXDSY/4wK8nZYJiHXJyNQen2V6vkRPUA
U+u24R4iytsrCXW08bGa+B3F9VIadBa8Br3bbJxV5hxCC2nCE6J8C9jRERc3GKTG
YBuSlM/gaLFopgFjRIDHY5IY5tCB3P8YFbbahqNHCXkh3Ilnlbmn3WW3sOXGOJDT
2s4AbSyzJHdAk3OqtMUoVl/7fk2a70mFiQi0JWotcsoa
-----END CERTIFICATE-----
subject=/CN=kagolug.org
issuer=/C=US/O=Let's Encrypt/CN=Let's Encrypt Authority X1
---
No client certificate CA names sent
Peer signing digest: SHA512
Server Temp Key: ECDH, P-256, 256 bits
---
SSL handshake has read 3171 bytes and written 441 bytes
---
New, TLSv1/SSLv3, Cipher is ECDHE-RSA-AES128-GCM-SHA256
Server public key is 2048 bit
Secure Renegotiation IS supported
Compression: NONE
Expansion: NONE
No ALPN negotiated
SSL-Session:
    Protocol  : TLSv1.2
    Cipher    : ECDHE-RSA-AES128-GCM-SHA256
    Session-ID: 21463ABE9EDCAF2B93E782CC2C4252E8CAA9A98B6B0036F957218C42A81419CE
    Session-ID-ctx:
    Master-Key: 0B652E199D83894F04BEAB5E268EEA8806F0DAB300AA4F5AA26C3B6361D57766FE5ACF08353DAD07781960A95BDFB7BB
    Key-Arg   : None
    PSK identity: None
    PSK identity hint: None
    SRP username: None
    TLS session ticket lifetime hint: 300 (seconds)
    TLS session ticket:
    0000 - be 38 de da a3 27 cf 1e-be 39 ee df 1c f0 2e e3   .8...'...9......
    0010 - 47 4e a1 ad 15 8f 43 3c-89 2b 1b 1f ea ef 46 a1   GN....C<.+....F.
    0020 - 09 d3 1b 0d c6 09 9a 99-e1 c2 d7 22 fd e0 b7 6f   ..........."...o
    0030 - 08 cb ba 73 d8 cb 3a 82-55 59 ee 5f 05 56 9c d6   ...s..:.UY._.V..
    0040 - bc 80 1a b3 b0 8c 87 16-2f fc 69 e2 03 0c a2 7f   ......../.i.....
    0050 - 9d e2 1f 2b d3 14 fb b7-78 28 22 48 3b ff 28 52   ...+....x("H;.(R
    0060 - 5e 89 bd cd 9f 3d 4f 26-aa 1d 2d bb af 4a 84 cf   ^....=O&..-..J..
    0070 - ce 3c 20 ac 55 84 33 56-10 6c 19 1a d3 15 ce 30   .< .U.3V.l.....0
    0080 - 7e e7 0b 6f f9 31 ef 92-c0 11 7f 95 de a6 fa 80   ~..o.1..........
    0090 - c1 5d 46 92 d6 b5 0c 5a-78 75 92 ad 1f bb 6f c0   .]F....Zxu....o.
    00a0 - 7f 35 ac 07 41 07 0a c7-a5 f5 5b 3f 16 ca b7 4e   .5..A.....[?...N
    00b0 - d7 7f c1 68 dc 28 e8 15-f9 95 d9 e1 a7 bf d0 c4   ...h.(..........

    Start Time: 1446664204
    Timeout   : 300 (sec)
    Verify return code: 0 (ok)
---

% cat cer
-----BEGIN CERTIFICATE-----
MIIE/TCCA+WgAwIBAgISAbbh6Bp+aXaatBj/TJ7lkyyZMA0GCSqGSIb3DQEBCwUA
MEoxCzAJBgNVBAYTAlVTMRYwFAYDVQQKEw1MZXQncyBFbmNyeXB0MSMwIQYDVQQD
ExpMZXQncyBFbmNyeXB0IEF1dGhvcml0eSBYMTAeFw0xNTExMDQxNjQ4MDBaFw0x
NjAyMDIxNjQ4MDBaMBYxFDASBgNVBAMTC2thZ29sdWcub3JnMIIBIjANBgkqhkiG
9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzb79Wux4LWzC+ZKTRRXN53+/IRraso2AZRrt
/wesf4EBEIl8i6Iu4Dl0FjLoomxZUCN0T7C5iJ4aPy629UkWDZrawFWGkXYT00ed
UziOKTXpYYTM9BBp9Qx1aw/CT8XY6TjOtaJ21AjcIXZBZ8EPnf6fWcHEFCsNYLKk
7U/e59WJ1B1ciXowS7nMwDy1c3rvu7tlzGRuO/xSx/hu0R5DYL8zyPlLwGZyfVv/
UYYtY6Wf8ItzgthpzltqtbMv4Kuohwu2mPwKQJ73MJoOghUD4p6oxiJ3nsgLY8DO
mIlW6ScXihlZ/pWfzjWaohKsvWM+qgnQpWNUQoaXNj0ES34m4QIDAQABo4ICDzCC
AgswDgYDVR0PAQH/BAQDAgWgMB0GA1UdJQQWMBQGCCsGAQUFBwMBBggrBgEFBQcD
AjAMBgNVHRMBAf8EAjAAMB0GA1UdDgQWBBQH4Pb+Brg5Q0PWhEZ7CEdAkxWKxzAf
BgNVHSMEGDAWgBSoSmpjBH3duubRObemRWXv86jsoTBwBggrBgEFBQcBAQRkMGIw
LwYIKwYBBQUHMAGGI2h0dHA6Ly9vY3NwLmludC14MS5sZXRzZW5jcnlwdC5vcmcv
MC8GCCsGAQUFBzAChiNodHRwOi8vY2VydC5pbnQteDEubGV0c2VuY3J5cHQub3Jn
LzAWBgNVHREEDzANggtrYWdvbHVnLm9yZzCCAQAGA1UdIASB+DCB9TAKBgZngQwB
AgEwADCB5gYLKwYBBAGC3xMBAQEwgdYwJgYIKwYBBQUHAgEWGmh0dHA6Ly9jcHMu
bGV0c2VuY3J5cHQub3JnMIGrBggrBgEFBQcCAjCBngyBm1RoaXMgQ2VydGlmaWNh
dGUgbWF5IG9ubHkgYmUgcmVsaWVkIHVwb24gYnkgUmVseWluZyBQYXJ0aWVzIGFu
ZCBvbmx5IGluIGFjY29yZGFuY2Ugd2l0aCB0aGUgQ2VydGlmaWNhdGUgUG9saWN5
IGZvdW5kIGF0IGh0dHBzOi8vbGV0c2VuY3J5cHQub3JnL3JlcG9zaXRvcnkvMA0G
CSqGSIb3DQEBCwUAA4IBAQBa5DjWSE/d6alvGUDNW4guiJauqvxB3B+YULzRTseb
0kXGyu46u16F4av+Ate0Jxq3NnZdOpy8OTiL/wGQeWOWs33zdlxii5o8R12pMMTS
/NWFxawiCkJnzpWkhdLQGv3RNUUQn0w5yXDSY/4wK8nZYJiHXJyNQen2V6vkRPUA
U+u24R4iytsrCXW08bGa+B3F9VIadBa8Br3bbJxV5hxCC2nCE6J8C9jRERc3GKTG
YBuSlM/gaLFopgFjRIDHY5IY5tCB3P8YFbbahqNHCXkh3Ilnlbmn3WW3sOXGOJDT
2s4AbSyzJHdAk3OqtMUoVl/7fk2a70mFiQi0JWotcsoa
-----END CERTIFICATE-----
% openssl x509 -in cer -text
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            01:b6:e1:e8:1a:7e:69:76:9a:b4:18:ff:4c:9e:e5:93:2c:99
    Signature Algorithm: sha256WithRSAEncryption
        Issuer: C=US, O=Let's Encrypt, CN=Let's Encrypt Authority X1
        Validity
            Not Before: Nov  4 16:48:00 2015 GMT
            Not After : Feb  2 16:48:00 2016 GMT
        Subject: CN=kagolug.org
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                Public-Key: (2048 bit)
                Modulus:
                    00:cd:be:fd:5a:ec:78:2d:6c:c2:f9:92:93:45:15:
                    cd:e7:7f:bf:21:1a:da:b2:8d:80:65:1a:ed:ff:07:
                    ac:7f:81:01:10:89:7c:8b:a2:2e:e0:39:74:16:32:
                    e8:a2:6c:59:50:23:74:4f:b0:b9:88:9e:1a:3f:2e:
                    b6:f5:49:16:0d:9a:da:c0:55:86:91:76:13:d3:47:
                    9d:53:38:8e:29:35:e9:61:84:cc:f4:10:69:f5:0c:
                    75:6b:0f:c2:4f:c5:d8:e9:38:ce:b5:a2:76:d4:08:
                    dc:21:76:41:67:c1:0f:9d:fe:9f:59:c1:c4:14:2b:
                    0d:60:b2:a4:ed:4f:de:e7:d5:89:d4:1d:5c:89:7a:
                    30:4b:b9:cc:c0:3c:b5:73:7a:ef:bb:bb:65:cc:64:
                    6e:3b:fc:52:c7:f8:6e:d1:1e:43:60:bf:33:c8:f9:
                    4b:c0:66:72:7d:5b:ff:51:86:2d:63:a5:9f:f0:8b:
                    73:82:d8:69:ce:5b:6a:b5:b3:2f:e0:ab:a8:87:0b:
                    b6:98:fc:0a:40:9e:f7:30:9a:0e:82:15:03:e2:9e:
                    a8:c6:22:77:9e:c8:0b:63:c0:ce:98:89:56:e9:27:
                    17:8a:19:59:fe:95:9f:ce:35:9a:a2:12:ac:bd:63:
                    3e:aa:09:d0:a5:63:54:42:86:97:36:3d:04:4b:7e:
                    26:e1
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Key Usage: critical
                Digital Signature, Key Encipherment
            X509v3 Extended Key Usage:
                TLS Web Server Authentication, TLS Web Client Authentication
            X509v3 Basic Constraints: critical
                CA:FALSE
            X509v3 Subject Key Identifier:
                07:E0:F6:FE:06:B8:39:43:43:D6:84:46:7B:08:47:40:93:15:8A:C7
            X509v3 Authority Key Identifier:
                keyid:A8:4A:6A:63:04:7D:DD:BA:E6:D1:39:B7:A6:45:65:EF:F3:A8:EC:A1

            Authority Information Access:
                OCSP - URI:http://ocsp.int-x1.letsencrypt.org/
                CA Issuers - URI:http://cert.int-x1.letsencrypt.org/

            X509v3 Subject Alternative Name:
                DNS:kagolug.org
            X509v3 Certificate Policies:
                Policy: 2.23.140.1.2.1
                Policy: 1.3.6.1.4.1.44947.1.1.1
                  CPS: http://cps.letsencrypt.org
                  User Notice:
                    Explicit Text: This Certificate may only be relied upon by Relying Parties and only in accordance with the Certificate Policy found at https://letsencrypt.org/repository/

    Signature Algorithm: sha256WithRSAEncryption
         5a:e4:38:d6:48:4f:dd:e9:a9:6f:19:40:cd:5b:88:2e:88:96:
         ae:aa:fc:41:dc:1f:98:50:bc:d1:4e:c7:9b:d2:45:c6:ca:ee:
         3a:bb:5e:85:e1:ab:fe:02:d7:b4:27:1a:b7:36:76:5d:3a:9c:
         bc:39:38:8b:ff:01:90:79:63:96:b3:7d:f3:76:5c:62:8b:9a:
         3c:47:5d:a9:30:c4:d2:fc:d5:85:c5:ac:22:0a:42:67:ce:95:
         a4:85:d2:d0:1a:fd:d1:35:45:10:9f:4c:39:c9:70:d2:63:fe:
         30:2b:c9:d9:60:98:87:5c:9c:8d:41:e9:f6:57:ab:e4:44:f5:
         00:53:eb:b6:e1:1e:22:ca:db:2b:09:75:b4:f1:b1:9a:f8:1d:
         c5:f5:52:1a:74:16:bc:06:bd:db:6c:9c:55:e6:1c:42:0b:69:
         c2:13:a2:7c:0b:d8:d1:11:17:37:18:a4:c6:60:1b:92:94:cf:
         e0:68:b1:68:a6:01:63:44:80:c7:63:92:18:e6:d0:81:dc:ff:
         18:15:b6:da:86:a3:47:09:79:21:dc:89:67:95:b9:a7:dd:65:
         b7:b0:e5:c6:38:90:d3:da:ce:00:6d:2c:b3:24:77:40:93:73:
         aa:b4:c5:28:56:5f:fb:7e:4d:9a:ef:49:85:89:08:b4:25:6a:
         2d:72:ca:1a
-----BEGIN CERTIFICATE-----
MIIE/TCCA+WgAwIBAgISAbbh6Bp+aXaatBj/TJ7lkyyZMA0GCSqGSIb3DQEBCwUA
MEoxCzAJBgNVBAYTAlVTMRYwFAYDVQQKEw1MZXQncyBFbmNyeXB0MSMwIQYDVQQD
ExpMZXQncyBFbmNyeXB0IEF1dGhvcml0eSBYMTAeFw0xNTExMDQxNjQ4MDBaFw0x
NjAyMDIxNjQ4MDBaMBYxFDASBgNVBAMTC2thZ29sdWcub3JnMIIBIjANBgkqhkiG
9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzb79Wux4LWzC+ZKTRRXN53+/IRraso2AZRrt
/wesf4EBEIl8i6Iu4Dl0FjLoomxZUCN0T7C5iJ4aPy629UkWDZrawFWGkXYT00ed
UziOKTXpYYTM9BBp9Qx1aw/CT8XY6TjOtaJ21AjcIXZBZ8EPnf6fWcHEFCsNYLKk
7U/e59WJ1B1ciXowS7nMwDy1c3rvu7tlzGRuO/xSx/hu0R5DYL8zyPlLwGZyfVv/
UYYtY6Wf8ItzgthpzltqtbMv4Kuohwu2mPwKQJ73MJoOghUD4p6oxiJ3nsgLY8DO
mIlW6ScXihlZ/pWfzjWaohKsvWM+qgnQpWNUQoaXNj0ES34m4QIDAQABo4ICDzCC
AgswDgYDVR0PAQH/BAQDAgWgMB0GA1UdJQQWMBQGCCsGAQUFBwMBBggrBgEFBQcD
AjAMBgNVHRMBAf8EAjAAMB0GA1UdDgQWBBQH4Pb+Brg5Q0PWhEZ7CEdAkxWKxzAf
BgNVHSMEGDAWgBSoSmpjBH3duubRObemRWXv86jsoTBwBggrBgEFBQcBAQRkMGIw
LwYIKwYBBQUHMAGGI2h0dHA6Ly9vY3NwLmludC14MS5sZXRzZW5jcnlwdC5vcmcv
MC8GCCsGAQUFBzAChiNodHRwOi8vY2VydC5pbnQteDEubGV0c2VuY3J5cHQub3Jn
LzAWBgNVHREEDzANggtrYWdvbHVnLm9yZzCCAQAGA1UdIASB+DCB9TAKBgZngQwB
AgEwADCB5gYLKwYBBAGC3xMBAQEwgdYwJgYIKwYBBQUHAgEWGmh0dHA6Ly9jcHMu
bGV0c2VuY3J5cHQub3JnMIGrBggrBgEFBQcCAjCBngyBm1RoaXMgQ2VydGlmaWNh
dGUgbWF5IG9ubHkgYmUgcmVsaWVkIHVwb24gYnkgUmVseWluZyBQYXJ0aWVzIGFu
ZCBvbmx5IGluIGFjY29yZGFuY2Ugd2l0aCB0aGUgQ2VydGlmaWNhdGUgUG9saWN5
IGZvdW5kIGF0IGh0dHBzOi8vbGV0c2VuY3J5cHQub3JnL3JlcG9zaXRvcnkvMA0G
CSqGSIb3DQEBCwUAA4IBAQBa5DjWSE/d6alvGUDNW4guiJauqvxB3B+YULzRTseb
0kXGyu46u16F4av+Ate0Jxq3NnZdOpy8OTiL/wGQeWOWs33zdlxii5o8R12pMMTS
/NWFxawiCkJnzpWkhdLQGv3RNUUQn0w5yXDSY/4wK8nZYJiHXJyNQen2V6vkRPUA
U+u24R4iytsrCXW08bGa+B3F9VIadBa8Br3bbJxV5hxCC2nCE6J8C9jRERc3GKTG
YBuSlM/gaLFopgFjRIDHY5IY5tCB3P8YFbbahqNHCXkh3Ilnlbmn3WW3sOXGOJDT
2s4AbSyzJHdAk3OqtMUoVl/7fk2a70mFiQi0JWotcsoa
-----END CERTIFICATE-----
```

後はコンテンツを全部 https に設定して http から転送するようにしないといけないですね．

とりあえず無料で使える StartSSL，安めの Rapid SSL，キャンペーンや乗り換えで一定期間無料とかありますが，そのくらいのレベルであれば代替になるかなと思います．それ以上は大抵 EV になるでしょうし．ただ期間が短いのが気になりますね．



### 画像のURL 変更

プロトコル付きの画像ファイルがあったので http://kagolug.org/*.png から /*.png のように変更した

### uMap の ssl

インラインフレームで読み込んでいる uMap を http から https に変更した．

### PathInfo を有効に

Top などは問題ないが，ログインページ( https://kagolug.org/index.php/login/ )などが 404 になってしまう．PathInfo が効いていないよう．http の設定と見比べてみたが同じなのに動作が違う．

とりあえず，`/etc/apache2/sites-enabled/009-kagolinux-le-ssl.conf` の `Directory` ディレクティブに以下を追加
```
AcceptPathInfo On
```

### http を https に転送

mod_rewrite で http から https に転送するようにした．

- `/etc/apache2/sites-enabled/009-kagolinux.conf`
```
@@ -4,23 +4,14 @@

+         <IfModule mod_rewrite.c>
+                RewriteEngine On
+                RewriteRule ^/(.*)?$ https://%{HTTP_HOST}/$1 [L,R]
+        </IfModule>
-        DocumentRoot /var/www-kagolug
-        <Directory /var/www-kagolug>
-                Options All
-                AllowOverride All
-                Order allow,deny
-                allow from all
-              Require all granted
-        </Directory>
```
