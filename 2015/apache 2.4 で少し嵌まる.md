apahce httpd 2.2.*(wheezy)から2.4.*(jessie)への移行で適当なサーバをえいやで上げて動かなくなったのを復旧したメモ．

### virtual host が有効にならない

`/etc/apache2/sites-enabled/` 以下には設定ファイルがあるのに読み込まれず default しか表示されない．

`sites-available` からリンクを貼り直そうとするとエラーとなる
```
$ sudo a2dissite example.com
ERROR: Site example.com does not exist!
$ sudo rm /etc/apache2/sites-enabled/example.com
$ sudo a2ensite example.com
ERROR: Site example.com does not exist!
```

エラーメッセージで検索するとファイル名を *.conf にすれば良いよう．
- [Apache a2ensite – Error! Site Does Not Exist | Epic FooBar](http://epicfoobar.com/2013/08/apache-a2ensite-error-site-does-not-exist/)

この部分の該当場所は `/etc/apache2/apache2.conf` のここ
```
IncludeOptional sites-enabled/*.conf
```

2.2からの変更を確認するとこのように変わっていた．
```
 # Include the virtual host configurations:
-Include sites-enabled/
+IncludeOptional sites-enabled/*.conf
```

ということでこんな感じで
```
$ sudo rm /etc/apache2/sites-enabled/example.com
$ sudo mv /etc/apache2/sites-available/example.com /etc/apache2/sites-available/example.com.conf
$ sudo  a2ensite example.com.conf
```

設定ファイルの数が多かったら `/etc/apache2/apache2.conf` の設定を * にしても良いかも(未検証)
```
-IncludeOptional sites-enabled/*.conf
+IncludeOptional sites-enabled/*
```

### 403 が出る

エラーログは以下のような感じ．見慣れないエラー．
> [authz_core:error] [pid 20566] [client nnn.nnn.nnn.nnn:49821] AH01630: client denied by server configuration: /var/www

以下のガイドを見るとどうもacl 周りのコマンド変更のようで

- [Upgrading to 2.4 from 2.2 - Apache HTTP Server Version 2.4](https://httpd.apache.org/docs/2.4/ja/upgrading.html)

```
Order deny,allow
Deny from all
```
の代わりに
```
Require all granted
```
で動くようになった．


----
