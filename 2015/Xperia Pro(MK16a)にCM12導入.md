[Xperia Pro(MK16a)をBootloader Unlock してみた | matoken's meme](http://matoken.org/blog/blog/2015/03/24/xperia-pro_mk16a_bootloader-unlock/)
でXperia Pro のブートローダーのアンロックをしたのでROMを入れ替えてみます．
入れ替えるROMはAndroidベースのCyanogenModを利用します．
- [CyanogenMod | Android Community Operating System](http://www.cyanogenmod.org/)

CyanogenModのページを見るとXperiaProのイメージはメンテされていないようです．
- [CyanogenMod Downloads](http://download.cyanogenmod.org/?device=iyokan)

古いXperiaのプロジェクトのLegacyXperia Projectを見に行くとアクティブなようなのでこちらのイメージを利用することにします．
- [LegacyXperia Project by LegacyXperia](https://legacyxperia.github.io/)

せっかくなのでLolipop ベースのCyanogenMod12 を試してみます．
- [LegacyXperia's Files :: BasketBuild](https://basketbuild.com/devs/LegacyXperia/iyokan/cm-12.0)

BasketBuild から`cm-12-20150314-UNOFFICIAL-LegacyXperia-iyokan.zip`を入手しました．`cm-12-20150314-UNOFFICIAL-LegacyXperia-iyokan.zip.md5` は0byte でした…．BasketBuild には`8caec5afb32aacd9e529ca31f6df6595` と書かれていたのでこれと突き合わせました．

```bash
% echo '8caec5afb32aacd9e529ca31f6df6595  cm-12-20150314-UNOFFICIAL-LegacyXperia-iyokan.zip' > cm-12-20150314-UNOFFICIAL-LegacyXperia-iyokan.zip.md5
% md5sum -c cm-12-20150314-UNOFFICIAL-LegacyXperia-iyokan.zip.md5
cm-12-20150314-UNOFFICIAL-LegacyXperia-iyokan.zip: 完了
% sha1sum cm-12-20150314-UNOFFICIAL-LegacyXperia-iyokan.zip
7d9b2c06c34bb540f5ed74635e7ddee65d27c2fa  cm-12-20150314-UNOFFICIAL-LegacyXperia-iyokan.zip
% sha256sum cm-12-20150314-UNOFFICIAL-LegacyXperia-iyokan.zip 
b82909fffea8ddf9ceb4f69b84970e7148a4de1b6f745a7a17f1c9cf9b8b2436  cm-12-20150314-UNOFFICIAL-LegacyXperia-iyokan.zip
% sha512sum cm-12-20150314-UNOFFICIAL-LegacyXperia-iyokan.zip 
5a7c6b5c0314ee4df6d9e381669518468c265cdeec0c831c20278c852aa2c4c32146ede11a5650b57e07ecddccb02950b58509d3a53310512be704975e0cbae9  cm-12-20150314-UNOFFICIAL-LegacyXperia-iyokan.zip
```
CyanogenModにはGoogleの不自由なソフトウェアやプロプライエタリなドライバが同梱されていません．別パッケージのGoogle Apps(GApps)として配布されています．
- [Google Apps - CyanogenMod](http://wiki.cyanogenmod.org/w/Google_Apps)

GAppsは巨大なので機能を削ってスリム化されたパッケージも配布されています．
- [FINAL Post From TKruzze Regarding PA GApps | Paranoid Android | XDA Forums](http://forum.xda-developers.com/paranoid-android/general/final-post-tkruzze-regarding-pa-gapps-t3064539)
- [Dev-Host - The Ultimate Free File Hosting / File Sharing Service](http://d-h.st/users/TKruzze/?fld_id=44392#files)

今回はこの `pa_gapps-modular-pico(uni)-5.0.1-20150315-signed.zip` を利用しました．hashは見当たりませんでした．手元では以下のような感＃じです．

```bash
% md5sum pa_gapps-modular-pico\(uni\)-5.0.1-20150315-signed.zip
3ee06867bb52465a83fd7625f7750f75  pa_gapps-modular-pico(uni)-5.0.1-20150315-signed.zip
% sha1sum pa_gapps-modular-pico\(uni\)-5.0.1-20150315-signed.zip 
639411d58817e38212b892a34832bbe1db094d36  pa_gapps-modular-pico(uni)-5.0.1-20150315-signed.zip
% sha256sum pa_gapps-modular-pico\(uni\)-5.0.1-20150315-signed.zip
f4a88039ff7870eaf989ab63c5d01ac315aecef3e66bff24e723cb8de66681fe  pa_gapps-modular-pico(uni)-5.0.1-20150315-signed.zip
% sha512sum pa_gapps-modular-pico\(uni\)-5.0.1-20150315-signed.zip 
daec26a87eb5acb6a492d6a691c2d602adc9fb201a9d9e0604f199654c16c59dcc99f636f21684a38931930951cbf68692a4c4ac717a09a60802fc0d2ce854b2  pa_gapps-modular-pico(uni)-5.0.1-20150315-signed.zip
```

＃このPA_GAppsは記事を書くために今確認したら開発をやめちゃうようです><
＃＃次の辺りが使えるかもしれません．(未確認)
- [[GAPPS][5.0/4.4] Official Slim GApps & Custo… | SlimRoms | XDA Forums](http://forum.xda-developers.com/slimroms/general/gapps-official-slim-gapps-trds-slimkat-t2792842 "[GAPPS][5.0/4.4] Official Slim GApps &amp; Custo… | SlimRoms | XDA Forums")

`cm-12-20150314-UNOFFICIAL-LegacyXperia-iyokan.zip`と`pa_gapps-modular-pico(uni)-5.0.1-20150315-signed.zip` の2ファイルをmicroSD Card にコピーして，microSD をXperiaProに挿します．


`cm-12-20150314-UNOFFICIAL-LegacyXperia-iyokan.zip`内から`boot.img`ファイルを取り出します．
```bash
% unzip cm-12-20150314-UNOFFICIAL-LegacyXperia-iyokan.zip boot.img
Archive:  cm-12-20150314-UNOFFICIAL-LegacyXperia-iyokan.zip
signed by SignApk
  inflating: boot.img
```

Xperia Pro をfastboot モードで起動します．
- 電源off 状態からメニューキーを押しながらUSB Cable 接続
- 右上側面のHDMIコネクタ上のLEDが青になる

fastboot コマンドで`boot.img` を送り込み起動させます．

```bash
% ./fastboot flash boot boot.img
[sudo] password for mk:
sending 'boot' (8704 KB)...
(bootloader) USB download speed was 139264kB/s
OKAY [  0.999s]
writing 'boot'...
(bootloader) Download buffer format: boot IMG
(bootloader) Flash of partition 'boot' requested
(bootloader) S1 partID 0x00000003, block 0x00000280-0x000002e3
(bootloader) Erase operation complete, 0 bad blocks encountered
(bootloader) Flashing...
(bootloader) Flash operation complete
OKAY [  1.707s]
 inished. total time: 2.706s
```

この後，USB Cableを抜いて電源を入れます．**LegacyXperia**のロゴが出ている間に下ボリュームキーを連打します．うまく行くと**CYANOGEN Recovery**の画面になります．

<a href="https://www.flickr.com/photos/119142834@N05/16734921007" title="IMG_20150327_130657 by Kenichiro MATOHARA, on Flickr"><img src="https://farm9.staticflickr.com/8739/16734921007_126c19b38b_n.jpg" width="240" height="320" alt="IMG_20150327_130657"></a>

先ずはデータ消去を行います．
- ボリュームの上下でメニューを移動し，**Wipe cache partition** で電源キーを押し決定
- ボリュームの上下でメニューを移動し，**Wipe data/factory reset**で電源キーを押し決定

ROMを焼きます．
- ボリュームの上下でメニューを移動し，**Apply update** で電源キーを押し決定
- **Choose from sdcard0**を選択し，SD Card内の`cm-12-20150314-UNOFFICIAL-LegacyXperia-iyokan.zip`を選択して焼く
- 同様にGAppsの`pa_gapps-modular-pico(uni)-5.0.1-20150315-signed.zip`を焼きます．
- **Reboot system now**で再起動します．

後はAndroidの初期設定です．しかし，初期設定時はすごく重い(ホームボタンを押してもタイムアウトして何も出ないうちに画面が消灯したりする)ので気長にやりましょう．初期設定が終わればそこそこ実用的な速度になります．

<a href="https://www.flickr.com/photos/119142834@N05/16940952272" title="IMG_20150322_101440 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7643/16940952272_ff5d0d2dd7.jpg" width="500" height="375" alt="IMG_20150322_101440"></a>
<a href="https://www.flickr.com/photos/119142834@N05/16320027854" title="Screenshot_2015-03-27-13-36-34 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7634/16320027854_ab02c7c25a_n.jpg" width="180" height="320" alt="Screenshot_2015-03-27-13-36-34"></a><a href="https://www.flickr.com/photos/119142834@N05/16942424245" title="Screenshot_2015-03-27-13-36-41 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7603/16942424245_89ab1d9381_n.jpg" width="180" height="320" alt="Screenshot_2015-03-27-13-36-41"></a>

重いけどIngressも動きます．
<a href="https://www.flickr.com/photos/119142834@N05/16942269025" title="Screenshot_2015-03-25-12-45-44 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7641/16942269025_7f957b5e1e_n.jpg" width="180" height="320" alt="Screenshot_2015-03-25-12-45-44"></a><a href="https://www.flickr.com/photos/119142834@N05/16756057719" title="Screenshot_2015-03-25-12-59-16 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7628/16756057719_eb6a3659a6_n.jpg" width="180" height="320" alt="Screenshot_2015-03-25-12-59-16"></a>



- [Xperia Pro(MK16a)をBootloader Unlock してみた | matoken's meme](http://matoken.org/blog/blog/2015/03/24/xperia-pro_mk16a_bootloader-unlock/)