<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/20939102371/in/dateposted-public/" title="20150828_04:08:45-13916"><img src="https://farm6.staticflickr.com/5689/20939102371_29a2e82911.jpg" width="500" height="307" alt="20150828_04:08:45-13916"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

Debian stretch amd64 で haroopad が起動しなくなった．`libudev.so.0` から `libudev.so.1` にリンクを張って動くようになった．

```
% haroopad 
/usr/share/haroopad/haroopad: error while loading shared libraries: libudev.so.0: cannot open shared object file: No such file or directory
% sudo ln -s /lib/x86_64-linux-gnu/libudev.so.1 /lib/x86_64-linux-gnu/libudev.so.0
```

Debian wheesy あたりから dist-upgrade を繰り返してるのでその時作られていたリンクが昨日の記事の `/lib` が飛んでしまって作りなおした時に消えて動かなくなったのかな?
(若しくは覚えてないけど手動でリンク貼っていたのが消えたか)

- [Haroopad - The Next Document processor based on Markdown](http://pad.haroopress.com/)

ところで Haroopad は結構バグがあるのですが最近開発停滞しているのかバージョン上がらないですね．他に良い Markdown Editor がないかな……．

今のところウェブベースの wri.pe がいい感じなのですがオフラインでは使えない．
ReText も最近は Webkitレンダラーを有効にすると大分 Markdown 対応が増えた感じですが未だ足りない感じ．
Atom Editor はちょっと重そうで敬遠しているのですが試してみようかな．
何気に適当なエディタで Markdown を書いて Pandoc でリアルタイム変換&ブラウザでプレビューとか設定するとかのほうが良いのかもしれない……．

- [wri.pe - simple and smart notepad](https://wri.pe/)
- [ReText download | SourceForge.net](http://sourceforge.net/projects/retext/)
- [Atom](https://atom.io/)
