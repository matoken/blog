# Debian stretch で /lib を飛ばしてしまい困ったけどどうにか復旧

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/19994565204/in/dateposted/" title="IMGP0010.DNG_DI4Y3X.ufraw"><img src="https://farm1.staticflickr.com/762/19994565204_41e81e657c.jpg" width="500" height="332" alt="IMGP0010.DNG_DI4Y3X.ufraw"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

lxc-creates して失敗してから bash も他のアプリも起動しなくなってしまった．どうにか復旧できたのでメモ．

bash も ls も全部コマンドがないと言われる．すでに起動していた zsh では tab 補完で見つからなかったコマンドは出てくる．この状態で再起動すると recovary mode でも kernel panick に．SSD が死んだ?と戦々恐々としながら BIOS で USB 起動を有効にして USBメモリの Linux から起動して対象Disk を mount( LUKS + LVM + ext4 ) chroot しようとすると bash がないと言われる．ls すると矢張りちゃんとある．`ldd ./bin/bash` して ライブラリを確認しようとすると chroot 先の `/lib` が空!
lxc-create が host の /lib 消しちゃったぽい?

とりあえずこの状態で差分バックアップを取っておく． /home, /etc, /var, /opt あたり．バックアップログを見た感じこの辺は特に消えたりとかはしていないようだった．

多分 /lib が復活すれば治ると思う．ということで，前もって取ってあった `dpkg --get-selections "*"` のパッケージリストから `/lib` を含むパッケージを抜き出して `apt-get download` して，`dpkg -x` して出てきた `./lib` を `/lib` にcp してどうにか OS が起動するように．
その後 `dpkg --configure -a` とか `apt-get install -f ` してパッケージ状態を正常な状態に．

そんなこんなでどうにか復旧しました．
思ったより時間かかってしまった．Debian Installer で起動して上書きインストールして `dpkg --set-selections` とかしたほうが速かったかもしれない．


多分 `lxc-create` でおかしくなった気がするのでこの辺ちゃんと調べたいところ．
```
% sudo lxc-create -t /usr/share/lxc/templates/lxc-busybox -n busybox01
lxc_container: No such file or directory - Failed to make / rslave to run template
lxc_container: Continuing...
warning : busybox is not statically linked.
warning : The template script may not correctly
warning : setup the container environment.
chmod: `/usr/local/var/lib/lxc/busybox01/rootfs/bin/passwd' 
setting root password to "root"
Failed to change root password
lxc_container: unknown key lxc.haltsignal
lxc_container: Failed to parse config: lxc.haltsignal = SIGUSR1
lxc_container: _recursive_rmdir_onedev: failed to delete /usr/local/var/lib/lxc/busybox01/rootfs/lib
lxc_container: _recursive_rmdir_onedev: failed to delete /usr/local/var/lib/lxc/busybox01/rootfs
lxc_container: Error destroying rootfs for busybox01
lxc_container: Error creating container busybox01
precmd:2: command not found: basename
```
