<!-- Place this tag in your head or just before your close body tag. -->
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<!-- Place this tag where you want the widget to render. -->
<div class="g-post" data-href="https://plus.google.com/+KenichiroMATOHARA/posts/FsquopwjpPF"></div>

てことで暫く運用して問題なさそう&思ってたより対応遅そうなのでこちらにもメモ．
所謂Stagefright 問題．

- [Androidの95%に遠隔乗っ取りの脆弱性。MMSメール着信だけで盗聴や情報漏洩も - Engadget Japanese](http://japanese.engadget.com/2015/07/27/android-95-mms/)
- [Experts Found a Unicorn in the Heart of Android › Zimperium Mobile Security Blog](http://blog.zimperium.com/experts-found-a-unicorn-in-the-heart-of-android/)
- [Androidに最悪の脆弱性発見―ビデオメッセージを受信するだけでデバイスが乗っ取られる | TechCrunch Japan](http://jp.techcrunch.com/2015/07/28/20150727nasty-bug-lets-hackers-into-nearly-any-android-phone-using-nothing-but-a-message/)

メディアプレイヤーフレームワーク Stagefright に欠陥があり，メディアファイルにより任意のコードが実行可能．対象はAndroid 2.2(Froyo) 以降全てと幅広い．(なお，95%のAndroid が対象らしい．逆に2.1以前が 5% もあるのに驚いたり)
MMS 経由で攻略メディアファイルを送信してユーザの操作なしに乗っ取り可能．日本ではMMS が利用できる環境は少ないけれどこのパターンは気づかないうちに乗っ取られるのがとても怖い．

脆弱性の確認は以下のアプリでも可能．
- [Stagefright Detector - Google Play の Android アプリ](https://play.google.com/store/apps/details?id=com.lookout.stagefrightdetector)
<a data-flickr-embed="true" href="https://www.flickr.com/photos/119142834@N05/20168393378/in/dateposted-public/" title="Screenshot_2015-08-07-06-57-53"><img src="https://farm1.staticflickr.com/390/20168393378_ba12f24b85.jpg" width="281" height="500" alt="Screenshot_2015-08-07-06-57-53"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

対策はAndroid 5.1.1_r5 にする，Google が各端末メーカに提供したパッチを配布したのでそれがリリースされるのを待って適用する．
しかし，Nexus でさえやっと8/5 にリリースなので，日本の端末は何時になるだろう……という感じ．
- [Official Android Blog: An Update to Nexus Devices](http://officialandroid.blogspot.jp/2015/08/an-update-to-nexus-devices.html)

ちなみにCynamonMod は即日対応完了している．
<!-- Place this tag in your head or just before your close body tag. -->
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<!-- Place this tag where you want the widget to render. -->
<div class="g-post" data-href="https://plus.google.com/+CyanogenMod/posts/7iuX21Tz7n8"></div>

先ずはMMS で勝手にメディアファイルが展開されるのを防げないかと幾つかのアプリを確認してみたがメディアを自動読み込みしないというような設定のあるものは見当たらなかった．

検索したりしていると以下のフォーラムを発見．build.drop でStagefright を無効にしてしまうというもの．
- [About Android MMS Stagefright exploit | Android Development and Hacking | XDA Forums](http://forum.xda-developers.com/android/help/android-mms-stagefright-exploit-t3166457)
手持ちの端末(root 取得済 LGL22)で build.drop の関係の有りそうな以下を false にしてみました．多分影響を受けなくなった?

```
media.stagefright.enable-aac=false
media.stagefright.enable-fma2dp=false
media.stagefright.enable-http=false
media.stagefright.enable-player=false
media.stagefright.enable-qcp=false
media.stagefright.enable-scan=false
```

build.drop の書き換えは権限さえあれば以下のアプリでお手軽に出来る．
- [Build Prop Editor - Google Play の Android アプリ](https://play.google.com/store/apps/details?id=com.jrummy.apps.build.prop.editor)

root が無くても書き換えが可能な場合もあるらしいが，AU で契約している端末(KYOCERA URBANO PROGRESSO)はダメだった．
```
% ./adb pull /system/build.prop
% vim build.prop
% ./adb push ./build.prop /system/build.prop 
failed to copy './build.prop' to '/system/build.prop': Read-only file system
% ./adb shell
shell@android:/$ ls -l /system/build.prop
rw-r--r- root     root         4977 2014-07-29 21:20 build.prop
/dev/block/mmcblk0p12 /system ext4 ro,relatime,user_xattr,barrier=1,nodelalloc,data=journal 0 0﻿
```

手持ちのAU と契約している端末は2012年5月のものらしい．セキュリティ修正は何時やってくるのか，そもそもやってくるのか……．
- [KYOCERA Introduces URBANO PROGRESSO, Worlds First Smartphone to Transmit Clear Voice Reception via Vibration of Display Screen ; Kyoceras proprietary Smart Sonic Receiver technology implemented in sophisticated handset design for the Japan market | News Releases | KYOCERA](http://global.kyocera.com/news/2012/0501_akgu.html)

可能なら CynamonMod にしてしまいたいところですが，国内端末とかはなかなかですね……．
