Android などでよく使う USB の mtp プロトコルですが，GVfs で自動マウントされます．ファイルマネージャの Nautilus などでアクセスできますが，途中で失敗することがあります．そこで端末上から rsync コマンドを使って失敗しても失敗した部分だけ再試行できるようにしていました．今回 Debian stretch amd64 + Gnome-shell 環境だと以前のマウントポイントの `~/.gvfs` にはマウントされなくなっていました．

```
% ls -lA ~/.gvfs 
合計 0
```

Nautilus のロケーションは次のようになっていますがこれも端末からはアクセスでき無さそう． `mtp://[usb:001,009]/`

Nautilus で umount して，fuser mount して rsync してみようと思ったのですが， mtpfs も jmtpfs 失敗してしまいました．


```
% mtpfs fuse/mtp
Listing raw device(s)
Device 0 (VID=1004 and PID=631c) is a LG Electronics Inc. LG-E610/E612/E617G/E970/P700.
   Found 1 device(s):
   LG Electronics Inc.: LG-E610/E612/E617G/E970/P700 (1004:631c) @ bus 1, dev 6
Attempting to connect device
Android device detected, assigning default bug flags
Error 1: Get Storage information failed.
Error 2: PTP Layer error 02fe: get_handles_recursively(): could not get object handles.
Error 2: Error 02fe: PTP: Protocol error, data expected
Listing File Information on Device with name: LGL22
LIBMTP_Get_Storage() failed:-1
% ls -la /home/mk/fuse/mtp
合計 8
drwxr-xr-x 2 mk mk 4096  8月 17 17:19 .
drwxr-xr-x 8 mk mk 4096  8月 17 17:19 ..
```
```
% jmtpfs fuse/mtp
Device 0 (VID=1004 and PID=631c) is a LG Electronics Inc. LG-E610/E612/E617G/E970/P700.
Android device detected, assigning default bug flags
% ls -la /home/mk/fuse/mtp
ls: /home/mk/fuse/mtp にアクセスできません: 入力/出力エラーです
% fusermount -u /home/mk/fuse/mtp
```

Nautilus で右クリックして `端末で開く` から確認することが出来ました．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/20463353649/in/dateposted-public/" title="Screenshot from 2015-08-17 18-19-43"><img src="https://farm6.staticflickr.com/5801/20463353649_a0b1e662ff.jpg" width="500" height="366" alt="Screenshot from 2015-08-17 18-19-43"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/20650063075/in/dateposted-public/" title="Screenshot from 2015-08-17 18-26-02"><img src="https://farm1.staticflickr.com/631/20650063075_1af84ff304.jpg" width="500" height="384" alt="Screenshot from 2015-08-17 18-26-02"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

どうも GVfs のマウントポイントは `~/.gvfs` から `/run/user/1000/gvfs` に変わったようです． `1000` は ユーザの UID でしょうね．

ということで，rsync でデータコピーが出来ました．
```
% rsync -av /run/user/1000/gvfs/mtp:host=%5Busb%3A001%2C009%5D/内部ストレージ/DCIM /export/Photo
```



その後 `man gvfsd-fuse` に発見．
 
       gvfsd-fuse is normally started by gvfsd(1). In this case, the mount point is $XDG_RUNTIME_DIR/gvfs or $HOME/.gvfs.

確認すると一緒ですね．
```
% echo $XDG_RUNTIME_DIR/gvfs
/run/user/1000/gvfs
```

しかし，`gvfs-*` 系のツールでも確認できそうなんですがうまく行かないですね……．

```
% gvfs-mount -h
用法:
  gvfs-mount [オプション...] [LOCATION...]

ロケーションをマウントします。

ヘルプのオプション:
  -h, --help                      ヘルプのオプションを表示する

アプリケーションのオプション:
  -m, --mountable                 マウント可能としてマウントする
  -d, --device=DEVICE             デバイスファイルでボリュームをマウントする
  -u, --unmount                   アンマウントする
  -e, --eject                     取り出す
  -s, --unmount-scheme=SCHEME     指定されたスキームですべてのマウントを解除する
  -f, --force                     Ignore outstanding file operations when unmounting or ejecting
  -a, --anonymous                 Use an anonymous user when authenticating
  -l, --list                      リスト表示する
  -o, --monitor                   イベントを監視する
  -i, --detail                    その他の情報を表示する
  --version                       Show program version
% gvfs-mount -l
Drive(0): INTEL SSDSA2CW600G3
  Type: GProxyDrive (GProxyVolumeMonitorUDisks2)
Drive(1): ST932032 0AS
  Type: GProxyDrive (GProxyVolumeMonitorUDisks2)
  Volume(0): 320 GB ボリューム
    Type: GProxyVolume (GProxyVolumeMonitorUDisks2)
    Mount(0): 320 GB ボリューム -> file:///media/mk/555bf630-0ca8-4282-9a79-7acfd6355a0a
      Type: GProxyMount (GProxyVolumeMonitorUDisks2)
Volume(0): LGE Android Phone
  Type: GProxyVolume (GProxyVolumeMonitorMTP)
  Mount(0): LGE Android Phone -> mtp://[usb:001,009]/
    Type: GProxyShadowMount (GProxyVolumeMonitorMTP)
Mount(1): mtp -> mtp://[usb:001,009]/
  Type: GDaemonMount
% gvfs-info -f 'mtp://[usb:001,009]/'
属性:
  standard::type: 2
  standard::name: [usb:001,009]
  standard::display-name: LGL22
  standard::icon: multimedia-player
  standard::content-type: inode/directory
  standard::size: 0
  standard::symbolic-icon: multimedia-player-symbolic
  access::can-read: TRUE
  access::can-write: FALSE
  access::can-execute: TRUE
  access::can-delete: FALSE
  access::can-trash: FALSE
  access::can-rename: FALSE
  filesystem::size: 38238453760
  filesystem::free: 12884361216
  filesystem::type: mtpfs
  gvfs::backend: mtp
```


- [mtp - Debian Wiki](https://wiki.debian.org/mtp)
- [Projects/gvfs - GNOME Wiki!](https://wiki.gnome.org/action/show/Projects/gvfs?action=show&redirect=gvfs)
