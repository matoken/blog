> [TeamViewer Linux版 あるのか．しらなかった
> 
"Linux版TeamViewerの無料ダウンロード" https://www.teamviewer.com/ja/download/linux.aspx

https://plus.google.com/u/0/+KenichiroMATOHARA/posts/MtcA41kdpD9
(元投稿消えている)

<!-- Place this tag in your head or just before your close body tag. -->
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<!-- Place this tag where you want the widget to render. -->
<div class="g-post" data-href="https://plus.google.com/+KenichiroMATOHARA/posts/BD2Sm5kiNSB"></div>

<!-- Place this tag in your head or just before your close body tag. -->
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<!-- Place this tag where you want the widget to render. -->
<div class="g-post" data-href="https://plus.google.com/+KenichiroMATOHARA/posts/HHp12Wshg3p"></div>

ということでTeamViewer にLinux版があるのを知り試してみました．

- [Linux版TeamViewerの無料ダウンロード](https://www.teamviewer.com/ja/download/linux.aspx)
  - [TeamViewerサポート – ライセンスおよび技術的な問題に関するヘルプ](https://www.teamviewer.com/ja/help/363-How-do-I-install-TeamViewer-on-my-Linux-distribution.aspx)  

導入自体はパッケージがあるのでこんな感じで(hash は見つからなかった……)
amd64版もあるようですがteamviewerd.service が起動しなかったりしたのでMultiarch なi386 を利用しています．

```
% wget http://download.teamviewer.com/download/teamviewer_i386.deb
% sha1sum teamviewer_10.0.41499_i386.deb 
438a1b27531faf8a89148b011e5214aa3f95ab1e  teamviewer_10.0.41499_i386.deb
% sha256sum teamviewer_10.0.41499_i386.deb 
0694c756b8cb5f2b09c8d6db960ced3e899d4ce43060e77ef4f1c36254dc31c4  teamviewer_10.0.41499_i386.deb
% sudo dpkg -i teamviewer_10.0.41499_i386.deb
```

このとき日本語Linux環境で利用した場合所々文字化けが起こります．
<a data-flickr-embed="true" href="https://www.flickr.com/photos/119142834@N05/20249515885/in/dateposted-public/" title="Screenshot from 2015-08-02 08-03-25"><img src="https://farm4.staticflickr.com/3726/20249515885_afa4277ef9.jpg" width="500" height="314" alt="Screenshot from 2015-08-02 08-03-25"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

TeamViewer はWine経由で動作しているよう&Wineでよく見かける感じの文字化けだったのでWineの設定を探し設定変更したところ文字化けしなくなりました．

- 修正内容
`/opt/teamviewer/tv_bin/wine/share/wine/wine.inf` の `[Fonts]` セクションに以下の設定を追加

```
HKLM,%FontSubStr%,"MS Gothic",,"Ume Gothic"
HKLM,%FontSubStr%,"MS PGothic",,"Ume Gothic"
HKLM,%FontSubStr%,"MS UI Gothic",,"Ume Gothic"
HKLM,%FontSubStr%,"MS Mincho",,"Ume Gothic"
HKLM,%FontSubStr%,"MS PMincho",,"Ume Gothic"
```

デーモンの再起動

```
% teamviewer daemon stop
% teamviewer daemon start
```

次バージョン以降で修正されることを期待してサポートにも投げてみました．(窓口合ってたかな?)


いつもはssh over (X/RDP/VNC)な感じで個人用途以外は有償なのであまり興味はなかったのですが，今回使ってみてAndorid のの画面も引っ張ってこれるのはいいなと思いました．丁度ちょっと前に幾つかのAndroid版VNC Server software を試してうまく動かなかったので特に．
