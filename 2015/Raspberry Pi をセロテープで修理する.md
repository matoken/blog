Raspberry Pi 2 model B の microSD card slot のロックが壊れてしまい microSD を挿しても出てきてしまう状態になっていました．
新しいの欲しいなぁと思っていたのですが，値段を確認するとRS は \3,966(2015/02/02の値段) -> \4,900(今) と約1,000円値上がりしています．ちょっとつらい．
![https://www.flickr.com/photos/119142834@N05/22624238729/in/dateposted-public/](https://farm1.staticflickr.com/675/22624238729_dd9220c064.jpg)

microSD 刺さることは刺さるのでどうにか microSD を固定すればいけるのではと試しにセロテープ貼って固定したら動くようになりました．

![https://www.flickr.com/photos/119142834@N05/23045475125/in/dateposted-public/](https://farm6.staticflickr.com/5732/23045475125_6db1eefdbb.jpg)
![https://www.flickr.com/photos/119142834@N05/23056665281/in/dateposted-public/](https://farm1.staticflickr.com/673/23056665281_f7553f9e68.jpg)


暫くこれで様子見てみようかと思います．
+より前の旧型なら SD ソケット付け替えや増設出来そうなんですけど + 以降はmicroSD だし BGA ぽいしソケット入手出来ても辛そうですね．

ちなみに Androidの SIMやmicroSD ソケットを交換してくれる業者もあるようなのですが，手数料と新品がそう変わらなくなりそうな感じです……．

- [MOUMANTAI オンラインショップ｜海外スマートフォングッズ販売](http://moumantai.biz/)

<iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00T356SFO" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>
<iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00PI9KPLC" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>
<iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B0155WN7CK" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>
