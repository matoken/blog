Thinkpad x201s にBluetooth module 増設

最近のメインマシンはThinkpad x201s です．これは2台めで中古で買って元のものとニコイチで使っています．良い部品はだいたい移行していたのですが，Bluetooth module は液晶パネルしたとめんどくさいので後回しになっていたのでした．でもXperia Pro でBluetooth テザリングが可能になったので重い腰を上げることに．実際やってみるとあっという間でした．

Thinkpad は保守マニュアルが日本語で公開されているのでとても便利です :)
- [ThinkPad X200、X200s、X200si、X201、X201i、および X201s 保守マニュアル]( http://download.lenovo.com/jp/mobiles_pdf/43y6632_05_j.pdf)

昔から公開していて，Webでpdfが公開される前はコピーサービスで入手していました．これのおかげで試行錯誤する必要がなくとても助かっています．これもThinkpadを使っている理由の一つです :)

マニュアルを見ると以下の辺りを参考にLCD全面ベゼルを取り外せば良いようですが，ネジキャップを剥がすのとかが面倒です．試しに赤く印をつけた3本だけを外して試すと旧マシンからモジュールの取り出しが出来ました．同様に新マシンも3本だけネジを外して取り付けも出来ました．
<a href="https://www.flickr.com/photos/119142834@N05/16957118941" title="Screenshot from 2015-03-28 10:13:07 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7652/16957118941_bbdac2a72a_n.jpg" width="302" height="320" alt="Screenshot from 2015-03-28 10:13:07"></a>
<a href="https://www.flickr.com/photos/119142834@N05/16338155123" title="IMG_20150328_091845 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7653/16338155123_c39d1ee0b7_n.jpg" width="320" height="240" alt="IMG_20150328_091845"></a><a href="https://www.flickr.com/photos/119142834@N05/16335877794" title="IMG_20150328_093620 by Kenichiro MATOHARA, on Flickr"><img src="https://farm9.staticflickr.com/8698/16335877794_c6a1e0182e_n.jpg" width="320" height="240" alt="IMG_20150328_093620"></a>

組み付けた後起動するとさくっと認識．Bluetooth PAN 接続も問題なく行えました．
<a href="https://www.flickr.com/photos/119142834@N05/16337978613" title="Screenshot from 2015-03-28 10:11:29 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7623/16337978613_67b8f15296.jpg" width="500" height="313" alt="Screenshot from 2015-03-28 10:11:29"></a>

たまにUSBドングルで利用していましたが内臓はスッキリしてていいですね．
