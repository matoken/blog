最近Debian Jessie でAdobe Flash が古いと怒られてるけどpkgでなかなか降りて来ないので`update-flashplugin-nonfree` を手動で叩いて最新にしたメモです．
＃え?GNU Gnashですか?ちょっとつらいですねARM とかでも動くのは良いんですが…．

最近Iceweasel やFirefox でAdobe Flash のあるページを見ようとするとバージョンが古いのでAdobe Flash がブロックされます．とりあえず許可すると動作しますが面倒だし意図しないものも実行してしまいそうです．Jessie はtesting とはいえもう2週間位この状態が続いている気がするのでもしかしてFirefox がLinux以外のバージョンを元に古いと行ってる可能性があるのでは?と思って以下のページで確認すると最新は`11.2.202.451` なのに対して`11.2.202.425` と実際古かったです．
- "Adobe - Flash Player" http://www.adobe.com/jp/software/flash/about/

＃しかしLinux版大分バージョン番号が離れてきましたね…．今のところ見当たらないけどそろそろ対応できないsiteとか出てきたりして．

Debian Jessie ではAdobe Flash は`flashplugin-nonfree` パッケージを利用して導入しています．このパッケージはAdobe Flash Player のバージョンの確認を行い，最新版をダウンロードして導入してくれるものです．パッケージの中を見るとこんな感じでした．
```bash
% dpkg -L flashplugin-nonfree
/.
/var
/var/lib
/var/lib/flashplugin-nonfree
/var/cache
/var/cache/flashplugin-nonfree
/usr
/usr/sbin
/usr/sbin/update-flashplugin-nonfree
/usr/share
/usr/share/applications
/usr/share/man
/usr/share/man/man8
/usr/share/man/man8/update-flashplugin-nonfree.8.gz
/usr/share/icons
/usr/share/icons/hicolor
/usr/share/icons/hicolor/22x22
/usr/share/icons/hicolor/22x22/apps
/usr/share/icons/hicolor/48x48
/usr/share/icons/hicolor/48x48/apps
/usr/share/icons/hicolor/32x32
/usr/share/icons/hicolor/32x32/apps
/usr/share/icons/hicolor/24x24
/usr/share/icons/hicolor/24x24/apps
/usr/share/icons/hicolor/16x16
/usr/share/icons/hicolor/16x16/apps
/usr/share/pixmaps
/usr/share/bug
/usr/share/bug/flashplugin-nonfree
/usr/share/bug/flashplugin-nonfree/script
/usr/share/doc
/usr/share/doc/flashplugin-nonfree
/usr/share/doc/flashplugin-nonfree/copyright
/usr/share/doc/flashplugin-nonfree/changelog.gz
/usr/share/doc/flashplugin-nonfree/README
/usr/share/lintian
/usr/share/lintian/overrides
/usr/share/lintian/overrides/flashplugin-nonfree
/usr/lib
/usr/lib/flashplugin-nonfree
/usr/lib/flashplugin-nonfree/pubkey.asc
/usr/lib/mozilla
/usr/lib/mozilla/plugins
/usr/bin
```
README を確認するとWiki page 読んでねとのこと．
```bash
% cat /usr/share/doc/flashplugin-nonfree/README
Please read the information at:
http://wiki.debian.org/FlashPlayer
```
- [FlashPlayer - Debian Wiki](https://wiki.debian.org/FlashPlayer)
`/usr/sbin/update-flashplugin-nonfree`

てことで`--status` で確認を行い，`--install`で最新に出来るようです．実際に叩いてみます．
```bash
% sudo /usr/sbin/update-flashplugin-nonfree
Usage:
  update-flashplugin-nonfree --install
  update-flashplugin-nonfree --uninstall
  update-flashplugin-nonfree --status
Additional options:
  --verbose
  --quiet
% sudo /usr/sbin/update-flashplugin-nonfree --status
Flash Player version installed on this system  : 11.2.202.425
Flash Player version available on upstream site: 11.2.202.451
flash-mozilla.so - auto mode
  link currently points to /usr/lib/flashplugin-nonfree/libflashplayer.so
/usr/lib/flashplugin-nonfree/libflashplayer.so - priority 50
/usr/lib/gnash/libgnashplugin.so - priority 10
Current 'best' version is '/usr/lib/flashplugin-nonfree/libflashplayer.so'.
% sudo /usr/sbin/update-flashplugin-nonfree --install
--2015-03-29 06:35:12--  https://fpdownload.macromedia.com/get/flashplayer/pdc/11.2.202.451/install_flash_player_11_linux.x86_64.tar.gz
 :
% sudo /usr/sbin/update-flashplugin-nonfree --status
Flash Player version installed on this system  : 11.2.202.451
Flash Player version available on upstream site: 11.2.202.451
flash-mozilla.so - auto mode
  link currently points to /usr/lib/flashplugin-nonfree/libflashplayer.so
/usr/lib/flashplugin-nonfree/libflashplayer.so - priority 50
/usr/lib/gnash/libgnashplugin.so - priority 10
Current 'best' version is '/usr/lib/flashplugin-nonfree/libflashplayer.so'.
```

ということで最新の`11.2.202.451`になりました．
なんで最近apt コマンドで最新になっていないのかは未確認です…．


<iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00UR986N2" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe><iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00L10NDNY" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe><iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00FJRS6QY" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>
