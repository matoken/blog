以前のエントリの 「[暗号化ファイルシステム EncFS の Reverse mode を試す | matoken's meme](http://matoken.org/blog/blog/2015/09/12/try-reverse-mode-of-encryption-file-system-encfs/)」のコメントで EncFS の Windows移植版の EncFS4Winy というものを教えてもらいました．

- [EncFS4Winy ファイル暗号化 « 座間ソフト](http://zamasoft.net/encfs4winy-%E6%9A%97%E5%8F%B7%E5%8C%96%E3%83%95%E3%82%A1%E3%82%A4%E3%83%AB%E3%82%B7%E3%82%B9%E3%83%86%E3%83%A0/)
- [Release EncFS4Winy latest · miyabe/encfs4winy](https://github.com/miyabe/encfs4winy/releases/latest)

EncFS の移植 & GUI で簡単にマウントしたりできるもののようです．

今回は VirtualBox 内の Windows 8.1 Professional 64bit 版に検証時の最新版の `EncFS4WinyInstall_1.7.5_2.exe` を試しました．手元で計算した HASH は以下の通り．

```zsh
% sha256sum EncFS4WinyInstall_1.7.5_2.exe
fd8c5109876730eb1a0395e9d85a81007655fc0ae135f63adc30e3b34da84315  EncFS4WinyInstall_1.7.5_2.exe
% sha512sum EncFS4WinyInstall_1.7.5_2.exe
c722c41d99944ccc247ac8b3859a2a3f3cfc2e467c814b865adda57d767837ec0561c23725b5d3f79a03babcce8137b3553291aa498a20eedcd46fb00504cc15  EncFS4WinyInstall_1.7.5_2.exe
```

実行するとインストーラが起動してよきに計らってくれます．
早速起動してみると，`「コンピューターに LIBEAY32.dll がないため、プログラムを開始できません。この問題を解決するには、プログラムを再インストールしてみて下さい。」` と言われて起動出来ませんでした．

[![Gyazo](https://i.gyazo.com/b1c4bdd474b293a651d0405cc6e83ee7.png)](https://gyazo.com/b1c4bdd474b293a651d0405cc6e83ee7)

`LIBEAY32.dll` は何者だろうと検索すると OpenSSL のもののようです．OpenSSL 本家ではバイナリを配布してないので Vector/窓の杜などで探しますが見当たりません．`ReadMe_Windows.txt` にも書かれている以下があちこちから参照されているし最新のバージョンに追従しているようですが，個人の人の運用しているページのようで配布物のハッシュや署名なども見当たらず．Announcement ML も登録できずでちょっと不安．皆さん自分でビルドされてるんですかね?(それとも他にいい配布元がある?)

- [Shining Light Productions - Win32 OpenSSL](http://slproweb.com/products/Win32OpenSSL.html)

仮想マシン環境なのでスナップショットを取ってあるので後で巻き戻すことにしてひとまずここから OpenSSL を導入しました．
すると EncFS は動作するようになりました．

```
C:\Program Files (x86)\EncFS\EncFS4Winy>.\encfs.exe --version
encfs version 1.7.4

C:\Program Files (x86)\EncFS\EncFS4Winy>.\encfs.exe -h
.\encfs.exe: invalid option -- h
05:57:06 (..\encfs\main.cpp:354) Missing one or more arguments, aborting.
Build: encfs version 1.7.4

Usage: .\encfs.exe [options] rootDir mountPoint [-- [FUSE Mount Options]]

Common Options:
  -H                    show optional FUSE Mount Options
  -s                    disable multithreaded operation
  -f                    run in foreground (don't spawn daemon).
                        Error messages will be sent to stderr
                        instead of syslog.
  -v, --verbose         verbose: output encfs debug messages
  -i, --idle=MINUTES    Auto unmount after period of inactivity
  --anykey              Do not verify correct key is being used
  --forcedecode         decode data even if an error is detected
                        (for filesystems using MAC block headers)
  --public              act as a typical multi-user filesystem
                        (encfs must be run as root)
  --reverse             reverse encryption

Example, to mount at c:\plain with raw storage in c:\crypt :
    encfs c:\crypt c:\plain

For more information, see the man page encfs(1)

```

早速 Linux で作った暗号化ファイルシステムをマウントしてみると以下のようにエラーとなってしまいました．

```
C:\Program Files (x86)\EncFS\EncFS4Winy>.\encfs.exe F:\tmp\encfs_c I: EncFS Password:
06:53:02 (..\encfs\FileUtils.cpp:1694) Unable to find nameio interface nameio/block, version 4:0:0
The requested filename coding interface is not available

```

ちょっと調べると，EncFS 領域作成時のファイル名エンコードのオプションで `Blick32` を指定しているとこうなるようです．EncFS4Winy で利用している EncFS 1.7.4 にはこのオプションがありません．

```
The following filename encoding algorithms are available:
1. Block : Block encoding, hides file name size somewhat
2. Null : No encryption of filenames
3. Stream : Stream encoding, keeps filenames as short as possible

Enter the number corresponding to your choice:
```

Linux 側で暗号化ファイルシステム作成時に `Block` を指定してみたところ EncFS4Winy でマウントできました．Windows 側で作成した領域の Linux でのマウントもOK でした．EncFS4Winy の EncFS バージョンが追従するまでは Linux 側で注意する必要があるようです．
ファイル名については UTF-8 であれば相互問題ありませんでした．

ということで，今度は GUI を試してみます．マウント元の暗号化フォルダと，マウント先のドライブレター，パスワードと入力していくとマウントできました．(コマンドプロンプトからの実行時にはマウント先はドライブレターだけでなくパスも指定可能)

[![Gyazo](https://i.gyazo.com/af89ceba0c46efe5c7e4ddadc29b51af.png)](https://gyazo.com/af89ceba0c46efe5c7e4ddadc29b51af)[![Gyazo](https://i.gyazo.com/2b35dbc7343963ad95f3baefcfe071a9.png)](https://gyazo.com/2b35dbc7343963ad95f3baefcfe071a9)[![Gyazo](https://i.gyazo.com/07fb98a377c9bd5867d321f80c1f5447.png)](https://gyazo.com/07fb98a377c9bd5867d321f80c1f5447)

マウント元の暗号化フォルダに空のものを指定すると新しい EncFS の暗号化領域が作成できます．オプションで paranoia mode が指定できます．

[![Gyazo](https://i.gyazo.com/4e58490e088af401687911160ca4deee.png)](https://gyazo.com/4e58490e088af401687911160ca4deee)

動作は Linux の Cryptkeeper とほぼ同じですね．違いは EncFS4Winy では暗号化FS 作成時のオプションに paranoia mode があって，Cryptkeeper のマウントオプションに自動アンマウントオプションがあるくらいでしょうか．

- [Tom Morton's beloved Website!](http://tom.noflag.org.uk/cryptkeeper.html)


GitHUB などで検索してみると Mac版の EncFS も複数出てきました．何年も更新されていないものとかもありますが GUI のあるものもあるようです．今手元に環境がないので試していませんがうまく動くようならマルチプラットホームで利用できるかもしれませんね．
