ReText がなんか寂しい．よく見るとツールバーのアイコンが無いです．見えないけどボタンは動作します．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/20926830192/in/dateposted-public/" title="20150828_08:08:11-8636"><img src="https://farm6.staticflickr.com/5777/20926830192_b99853c932.jpg" width="500" height="305" alt="20150828_08:08:11-8636"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

検索してみると以下のようなエントリを発見．

- [Fix Missing Toolbar Icon in ReText - svenBit](http://www.svenbit.com/2012/05/fix-missing-toolbar-icon-in-retext/)

真似をして以下のようにして修正しました．

## icon-theme を調べる

`gconftool-2 –get /desktop/gnome/interface/icon_theme` か `gsettings get org.gnome.desktop.interface icon-theme` で icon_theme を調べる．上のエントリでは前者で表示されていましたが，手元の環境では後者でしか表示されませんでした．

```
% gconftool-2 –get /desktop/gnome/interface/icon_theme
% gsettings get org.gnome.desktop.interface icon-theme
'gnome'
```

## ReText の設定修正

`~/.config/ReText\ project/ReText.conf` の `[General]` セクションに `iconTheme=上で調べたテーマ名` を指定します．今回は `iconTheme=gnome`

```
% cat ~/.config/ReText\ project/ReText.conf
[General]
autoSave=true
highlightCurrentLine=true
useWebKit=true
iconTheme=gnome
```

この状態で ReText を起動するとアイコンが表示されるようになりました :)

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/20748524620/in/dateposted-public/" title="20150828_08:08:39-8956"><img src="https://farm1.staticflickr.com/707/20748524620_1be4543c0b.jpg" width="500" height="305" alt="20150828_08:08:39-8956"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
