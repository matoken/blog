<a href="https://www.flickr.com/photos/119142834@N05/18447314240" title="logo-gnupg-light-purple-bg by Kenichiro MATOHARA, on Flickr"><img src="https://c1.staticflickr.com/9/8853/18447314240_c0dced5e40_o.png" width="356" height="120" alt="logo-gnupg-light-purple-bg"></a>

- [Facebook、ユーザー宛メールのPGP暗号化をサポート | TechCrunch Japan](http://jp.techcrunch.com/2015/06/02/20150601facebook-now-supports-pgp-to-send-you-encrypted-emails/)

<!-- Place this tag in your head or just before your close body tag. -->
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<!-- Place this tag where you want the widget to render. -->
<div class="g-post" data-href="https://plus.google.com/+KenichiroMATOHARA/posts/dP3LeNo8sSF"></div>

ということでFacebook 自体は殆ど利用していないのですが、PGP GNU 実装のGnuPG の鍵を登録してみました。

先ずは自分の鍵を確認して、
```
% gpg --fingerprint 572E532C         
pub   4096R/572E532C 2010-03-17
                 指紋 = 9C3E C527 2FFD AF80 3289  ADE4 398C 09CC 572E 532C
uid                  K.I.Matohara <matoken@gmail.com>
uid                  [jpeg image of size 2077]
uid                  Kenichiro MATOHARA <matohara@gdnewhat-jp.org>
uid                  Kenichiro MATOHARA (KagoshimaLinuxUserGroup.) <matoken@kagolug.org>
sub   2048R/74FBCF68 2015-01-15
```

公開鍵をASCIIで書き出しして、
```
% gpg -a --export 572E532C|xclip
```
facebook に登録します。登録ページは次からアクセス出来ます。
https://www.facebook.com/me/about?section=contact-info

<a href="https://www.flickr.com/photos/119142834@N05/18013148363" title="Screenshot from 2015-06-02 11:16:18 by Kenichiro MATOHARA, on Flickr"><img src="https://c1.staticflickr.com/9/8870/18013148363_c5b52bc6c4_o.png" width="142" height="42" alt="Screenshot from 2015-06-02 11:16:18"></a>
<a href="https://www.flickr.com/photos/119142834@N05/18629149102" title="Screenshot from 2015-06-02 11:16:38 by Kenichiro MATOHARA, on Flickr"><img src="https://c1.staticflickr.com/1/430/18629149102_3b2a96623a_o.png" width="531" height="310" alt="Screenshot from 2015-06-02 11:16:38"></a>

~~ここで `Use this public key to encrypt notification emails that Facebook sends you?` にチェックを付けるとFacebook からの通知も暗号化されるのですが、以下のエラーになってしまいました。(チェックをしない場合はエラーにならない)~~
利用した鍵の問題だったようです。下記の追記を参照して下さい。

<a href="https://www.flickr.com/photos/119142834@N05/18011119624" title="Screenshot from 2015-06-02 11:17:19 by Kenichiro MATOHARA, on Flickr"><img src="https://c1.staticflickr.com/1/500/18011119624_39fdf8bb78.jpg" width="500" height="137" alt="Screenshot from 2015-06-02 11:17:19"></a>
```
PGP public key will be unusable too soon
This PGP Public Key will be unusable for encryption within 30 days. This most likely means that its expiry date is within 30 days, it is already revoked, or the key does not have encryption enabled. Please extend the expiry date or add an encryption subkey to your existing key, or generate a new public key to receive encrypted notifications.
```

鍵は全部期限内で無期限なんだけどなぜだろう?1024R/2048R/4096Rの鍵をそれぞれ新しく作って試すとこれはいけました。
~~副鍵が多かったりimage がいけないのかな?公開鍵も大きいし~~
```
$ gpg -a --export 572E532C|wc -l #怒られる4096R
208
$ gpg -a --export BD929CA7|wc -l #1024R
13
$ gpg -a --export DB7157B1|wc -l #2048R
30
$ gpg -a --export D9A959F4|wc -l #4096R
52
```

鍵の登録がうまくいくと以下のような暗号化メールが届きます。これを復号して確認URL にアクセスすると次のメールから暗号化されます。

```
% head /home/mk/encrypted.asc
-----BEGIN PGP MESSAGE-----

hQIMAxvR4gJcick7AQ/9GA1pbc/bU7p3o3PcxxsTT3HdPeOA5KdD5/GBcxODM1SS
ousJS+9g/IRgykaHcUHcYTJCbW/vyhQnm33Q+HjmOGI6t3n6UfTpxiNFH7WVSNVW
cd5nVSYj/9nlvq1lBk200+NjYdbxSPK40RrLMGwjMHkqRNilK85xFrOW1+B7FRXq
mYdSZkQ2n1XQsqywL+onPGkuM1RA0NARqNX9Iq4PJLrSx04h+8DgCAk8gul7z/iU
l3YnVjDz+I45EMQyzABckFbQQDoFC1T75tV+HDKJetSGQALoYGltA5COZusQdarM
Qmk+ukg+4xWAinV/Vci9jqANdeUDryADKh8nnuJoxVFyz9V4fIJjfp75+D5zPNdQ
uwrTQzBDRQIF9Qttk+5Id8Pi2/vuDzrRFO0ELrh/Zah78Wd3iMUykk2GMeXdnGve
7sq+LT0xkOt1i6vUzYpvlWwxlQ8gdUCXoIIlzEsMD0+TvEhx5xu9/OWt7CCKpYUg
% gpg -d /home/mk/encrypted.asc

次のユーザーの秘密鍵のロックを解除するには
パスフレーズがいります:“Kenichiro MATOHARA (4096R key) <matoken@gmail.com>”
4096ビットRSA鍵, ID 5C89C93B作成日付は2015-06-08 (主鍵ID D9A959F4)

gpg: 4096-ビットRSA鍵, ID 5C89C93B, 日付2015-06-08に暗号化されました
	  “Kenichiro MATOHARA (4096R key) <matoken@gmail.com>”
Content-Type: multipart/alternative;
		boundary="b1_391ab87f1b42dbae36913baf98ac3c0c"


--b1_391ab87f1b42dbae36913baf98ac3c0c
Content-Type: text/plain; charset="UTF-8"
Content-Transfer-Encoding: 7bit

こんにちは、Kenichiroさん

This is an email to help you enable encrypted notification emails for your Facebook account.

If you prefer to not enable encrypted notification emails from Facebook, you may simply ignore this message.

If you enable encrypted notification emails, Facebook will begin encrypting notification emails to you with your public key. These may include account recovery notification emails.

BEWARE: If at some time in the future you cannot decrypt your account recovery notification emails and if you also become locked out of Facebook, you may be unable to recover your Facebook account.

To proceed with enabling encrypted notification emails, please click this link:

Yes, encrypt notification emails sent to me from Facebook.[https://www.facebook.com/XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX]

よろしくお願いいたします。
Facebookチーム
```

Facebook はなんでこの機能を付けたのかよくわかりませんが，以下の辺りも関係しているのかもですね．
- [オープンソースの暗号化ソフト「GNU Privacy Guard」、Facebookなどから寄付を獲得 - ZDNet Japan](http://japan.zdnet.com/article/35060156/)


ところで，この機能はクレジットカード会社や銀行なども取り入れて欲しいところです．自分の契約しているところだと法人向けだとやっている所はあるようですが個人向けは無さそうで残念です……．


おまけ?
`Key fingerprint = 9C3E C527 2FFD AF80 3289  ADE4 398C 09CC 572E 532C`
https://gist.github.com/200fce0896b735ccbc13

----

追記)
Google+ で[+Niibe Yutaka](https://plus.google.com/109927329313008001365)さんにコメントを貰って暗号化用の副鍵が無いのが原因だったようですorz
- [Facebook でGnupPG の公開鍵登録が出来るようになったということで登録してみた 鍵を確認して、 % gpg --fingerprint…](https://plus.google.com/u/0/+KenichiroMATOHARA/posts/dP3LeNo8sSF)

>572E532Cを--edit-keyで見てみると暗号のための副鍵はないので(署名Sと認証Aだけ)、主鍵は署名Sと鍵への署名Cで、この鍵では暗号化メールは送れません。暗号化の副鍵を追加して、ついでにrevokeした副鍵は削除して登録してみるといいんじゃないでしょうか。

```bash
% gpg --edit-key 572E532C
gpg (GnuPG) 1.4.18; Copyright (C) 2014 Free Software Foundation, Inc.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

秘密鍵が使用できます。

pub  4096R/572E532C  作成: 2010-03-17  満了: 無期限      利用法: SC
 信用: 絶対的     有効性: 絶対的
This key was revoked on 2014-02-11 by RSA key 572E532C K.I.Matohara <matoken@gmail.com>
sub  2048R/E0FE9E3E  作成: 2011-02-23  失効: 2014-02-11  利用法: S
sub  2048R/74FBCF68  作成: 2015-01-15  満了: 無期限      利用法: A
[ultimate] (1). K.I.Matohara <matoken@gmail.com>
[ultimate] (2)  [jpeg image of size 2077]
[ultimate] (3)  Kenichiro MATOHARA <matohara@gdnewhat-jp.org>
[ultimate] (4)  Kenichiro MATOHARA (KagoshimaLinuxUserGroup.) <matoken@kagolug.org>

gpg> uid 1

pub  4096R/572E532C  作成: 2010-03-17  満了: 無期限      利用法: SC
 信用: 絶対的     有効性: 絶対的
This key was revoked on 2014-02-11 by RSA key 572E532C K.I.Matohara <matoken@gmail.com>
sub  2048R/E0FE9E3E  作成: 2011-02-23  失効: 2014-02-11  利用法: S
sub  2048R/74FBCF68  作成: 2015-01-15  満了: 無期限      利用法: A
[ultimate] (1)* K.I.Matohara <matoken@gmail.com>
[ultimate] (2)  [jpeg image of size 2077]
[ultimate] (3)  Kenichiro MATOHARA <matohara@gdnewhat-jp.org>
[ultimate] (4)  Kenichiro MATOHARA (KagoshimaLinuxUserGroup.) <matoken@kagolug.org>

gpg> key 1

pub  4096R/572E532C  作成: 2010-03-17  満了: 無期限      利用法: SC
 信用: 絶対的     有効性: 絶対的
This key was revoked on 2014-02-11 by RSA key 572E532C K.I.Matohara <matoken@gmail.com>
sub* 2048R/E0FE9E3E  作成: 2011-02-23  失効: 2014-02-11  利用法: S
sub  2048R/74FBCF68  作成: 2015-01-15  満了: 無期限      利用法: A
[ultimate] (1)* K.I.Matohara <matoken@gmail.com>
[ultimate] (2)  [jpeg image of size 2077]
[ultimate] (3)  Kenichiro MATOHARA <matohara@gdnewhat-jp.org>
[ultimate] (4)  Kenichiro MATOHARA (KagoshimaLinuxUserGroup.) <matoken@kagolug.org>

gpg> delkey
この鍵を本当に削除しますか? (y/N) y

pub  4096R/572E532C  作成: 2010-03-17  満了: 無期限      利用法: SC
 信用: 絶対的     有効性: 絶対的
sub  2048R/74FBCF68  作成: 2015-01-15  満了: 無期限      利用法: A
[ultimate] (1)* K.I.Matohara <matoken@gmail.com>
[ultimate] (2)  [jpeg image of size 2077]
[ultimate] (3)  Kenichiro MATOHARA <matohara@gdnewhat-jp.org>
[ultimate] (4)  Kenichiro MATOHARA (KagoshimaLinuxUserGroup.) <matoken@kagolug.org>

gpg> addkey
鍵は保護されています。

次のユーザーの秘密鍵のロックを解除するには
パスフレーズがいります:“K.I.Matohara <matoken@gmail.com>”
4096ビットRSA鍵, ID 572E532C作成日付は2010-03-17

ご希望の鍵の種類を選択してください:
   (3) DSA (署名のみ)
   (4) RSA (署名のみ)
   (5) Elgamal (暗号化のみ)
   (6) RSA (暗号化のみ)
選択は? 6
RSA keys may be between 1024 and 4096 bits long.
What keysize do you want? (2048) 4096
要求された鍵長は4096ビット
鍵の有効期限を指定してください。
0 = 鍵は無期限
 <n>  = 鍵は n 日間で満了
 <n>w = 鍵は n 週間で満了
 <n>m = 鍵は n か月間で満了
 <n>y = 鍵は n 年間で満了
鍵の有効期間は? (0)
Key does not expire at all
これで正しいですか? (y/N) y
本当に作成しますか? (y/N) y
今から長い乱数を生成します。キーボードを打つとか、マウスを動かす
とか、ディスクにアクセスするとかの他のことをすると、乱数生成子で
乱雑さの大きないい乱数を生成しやすくなるので、お勧めいたします。

十分な長さの乱数が得られません。OSがもっと乱雑さを収集
できるよう、何かしてください! (あと175バイトいります)
...............+++++

十分な長さの乱数が得られません。OSがもっと乱雑さを収集
できるよう、何かしてください! (あと198バイトいります)
...................+++++

pub  4096R/572E532C  作成: 2010-03-17  満了: 無期限      利用法: SC
信用: 絶対的     有効性: 絶対的
sub  2048R/74FBCF68  作成: 2015-01-15  満了: 無期限      利用法: A
sub  4096R/2D937827  作成: 2015-06-11  満了: 無期限      利用法: E
[ultimate] (1). K.I.Matohara <matoken@gmail.com>
[ultimate] (2)  [jpeg image of size 2077]
[ultimate] (3)  Kenichiro MATOHARA <matohara@gdnewhat-jp.org>
[ultimate] (4)  Kenichiro MATOHARA (KagoshimaLinuxUserGroup.) <matoken@kagolug.org>

gpg> save

% gpg --send-keys 572E532C
gpg: 鍵572E532Cをhkpサーバーkeys.gnupg.netへ送信
```
この状態で公開鍵をFacebook に登録するとまくいきました。