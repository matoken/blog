Raspberry Pi B+ / A+ が出たばかりですが，Raspberry Pi 2 model B というものが発表されました．もう出荷も始まっていて注文も可能です．

[Raspberry Pi 2 on sale now at $35 | Raspberry Pi](http://www.raspberrypi.org/raspberry-pi-2-on-sale/ "Raspberry Pi 2 on sale now at $35 | Raspberry Pi")
<a href="https://www.flickr.com/photos/119142834@N05/16257764968" title="Pi2ModB1GB_-comp by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7311/16257764968_60d30eb9f3_n.jpg" width="320" height="181" alt="Pi2ModB1GB_-comp"></a>

ぱっと見の見た目はRaspberry Pi model B+ と一緒な感じですね．RAMは基板裏に言ったらしいので裏返すと違い分かりそう．
CPUがARMv6からQuadCoreのARMv7，RAM 1GBになって6倍に高速化とか．値段は据え置き$35で旧モデルも併売．Raspberry Pi 2 model A+ は少なくとも2015年には出さないとのこと．個人的にはあまり惹かれないのですがWindows10も無償で提供されるようです．WindowsRT のような感じになるのでしょうか．とはいえこれで注目度はまた上がるでしょうね．
- [Windows 10 Coming to Raspberry Pi 2](http://blogs.windows.com/buildingapps/2015/02/02/windows-10-coming-to-raspberry-pi-2/)

ARMv7 QuadCore RAM1GB になったのでARMv6を捨てたディストリビューションも動くようになりそうだし，Xのアプリも大分さくさく動くようになりそうな気がします．
これから買うなら RaspberryPi 2 model B か Raspberry Pi Model A+ という感じになりそうですね．
今回は前回と違ってリークもなくSoCに更新はしばらくないと思っていたのでいきなりの発表でびっくりしました．しかしRaspberry Pi model B+ を買う理由が殆ど無いのでなんか可愛そうです…．

|        | A|B|A+|B+|2 B|
|--------|--------|------------|----|----|
|RAM|256MB|512MB|256MB|512MB|**1GB**|
|ネットワーク|✘|○|✘|○|○|
|USB|   1    |  2     |  1     |    **4**   |    **4**   |
|GPIOピン数|26|**40**|26|**40**|**40**|
|ストレージ|SD|SD|**microSD**|**microSD**|**microSD**|
|消費電力|300mA(1.5W)|700mA(3.5W)|300mA(1.5W)より低い|600mA(3.0W)|?
|サイズ|85mm×56mm|85mm×56mm|**65mmx56mm* ***|85mm×56mm|85mm×56mm|
|価格|$20|$35|$20|$35|$35|


私はとりあえず2/2の夜にRS-Components から注文しました．**1～5営業日でお届け。** になっていてま台本への移動中のようなので恐らく英国在庫ですね．(土曜あたりになりそうだけど土曜OKのオプションを付け忘れたので月曜かな?)値段はこんな感じでした．

|RS品番	|数量	|単価	|合計	|商品の概要|
|--------|--------|--------|--------|--------|
|832-6274|	1	|￥3,966	|￥3,966	|Raspberry Pi 2 Model B|
|||商品本体の合計|	￥3,966	||
|||配送料	|￥450||
|||消費税	|￥353||
|||合計	|￥4,769||

\8,000-以上で送料無料だったと思うので3枚買うと割安になりますね．

しかし，今から注文すると2月末の到着になるようです．割高になりますが在庫のあるケースやさんとかで購入したほうが早く手に入りそうです．

そういえばコミケット87で頒布した Raspberry Pi な同人誌以下で頒布中です．
- [RaspberryPiで遊ぼう! - matoken's meme | 同人誌通販のアリスブックス](http://alice-books.com/item/show/3584-1)

<iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00OHLMSD0" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe> <iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=4822224953" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe> <iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00PI9KPLC" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe> <iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B008UR8TS0" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>

