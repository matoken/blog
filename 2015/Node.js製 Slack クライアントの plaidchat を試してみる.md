- [Slack の Linux版公式クライアントを試してみる | matoken's meme](http://matoken.org/blog/blog/2015/09/27/try-out-a-linux-version-of-the-official-client-of-slack/)
_["Node.ja で作られた plaidchat というクライアントもあるようです．これも試してみようと思います．"](http://matoken.org/blog/blog/2015/09/27/try-out-a-linux-version-of-the-official-client-of-slack/)_

ということで `plaidchat` も少し試してみました．

- [plaidchat/plaidchat](https://github.com/plaidchat/plaidchat)

導入は特に苦労はなくページに書いてあるとおり `npm install` で一発でした．

Slack 製の物に比べると機能は少ないです．多分1プロジェクトのみの対応です．でもリソースの省比量は大分少なくメインウィンドウを隠すことも出来ます．
About が後ろに行って複数出せてしまったり，窓の中が真っ白になったり未だこなれてない感じです．これからに期待です．

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/21757745492/in/dateposted-public/" title="20150928_08:09:00-13716"><img src="https://farm1.staticflickr.com/688/21757745492_fc1911d65d_o.jpg" width="400" height="200" alt="20150928_08:09:00-13716"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

```
% ps aux | grep -i plaidchat | awk '{print $6}' | xargs echo| sed -e 's/\ /\+/g' | bc
326480﻿
```

RAM 消費量が少ないとはいってもそこそこ消費するので今度は Slack IRC GW なども試してみたいです．

