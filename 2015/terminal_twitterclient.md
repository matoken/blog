確かOSSJで見かけて知ったのだと思うのですが，
- [OSS Japan - OSS開発者がつくるOSSユーザーのためのOSS総合情報サイト](https://www.ossj.jp/)

端末上で動作するTwitterのRainbowstreamというソフトウェアを知りました．
- [Rainbowstream](http://www.rainbowstream.org/)
- [DTVD/rainbowstream](https://github.com/DTVD/rainbowstream)

ユーザーストリーム対応で端末上にユーザストリームや検索結果リストなどが表示できます．画像をアスキーアートで表示する機能もあります．

# 導入

pipで入る
```sh
$ sudo apt-get install python-pip python-dev
$ sudo pip install rainbowstream PySocks Pillow
```
初回起動時に認証のためブラウザが起動する．認証ご表示されたpinを端末に貼り付ける．認証情報は `~/.rainbow_oauth` に保存されるので要らなくなったら消す．
無くても動くけど無いと設定変更が出来ないので設定ファイルの用意をする．
```sh
% wget https://raw.githubusercontent.com/DTVD/rainbowstream/master/rainbowstream/colorset/config -O /tmp/config
% mv /tmp/config ~/.rainbow_config.json
```

GNU Screen のウィンドウ分割モードだと崩れてしまうので，小さめのウィンドウに表示するといい感じです．以下の画像はawesome で端末を2つ表示してRainbowstreamを実行した端末を小さくしているところです．  
<a href="https://www.flickr.com/photos/119142834@N05/16264193938" title="20150202_07:02:05-608 by Kenichiro MATOHARA, on Flickr"><img src="https://farm9.staticflickr.com/8573/16264193938_5796be59cd.jpg" width="500" height="313" alt="20150202_07:02:05-608"></a>

画像のアスキーアート表示は楽しいのですが，一気にログが流れてしまうのでoffにしちゃいました．
自作のPerl Script で昔作ったものがあるのですが，こっちのほうがカラフルで見た目がいい感じです．

<iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=4822224961" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe> <iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00S5TM46Q" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe> <iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00BWCSUYS" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>
