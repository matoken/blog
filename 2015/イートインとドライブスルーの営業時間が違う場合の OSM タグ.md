<!-- Place this tag in your head or just before your close body tag. -->
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<!-- Place this tag where you want the widget to render. -->
<div class="g-post" data-href="https://plus.google.com/+KenichiroMATOHARA/posts/jE1vJTku1X5"></div>

<!-- Place this tag in your head or just before your close body tag. -->
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<!-- Place this tag where you want the widget to render. -->
<div class="g-post" data-href="https://plus.google.com/+KenichiroMATOHARA/posts/G22aDjhvuJH"></div>

最寄りのマクドナルドが新装開店しました．敷地が広くなり建物も新しくなって駐車場も広くなって利用しやすくなるなと思っていたのですが行ってみるとこれまでの24時間営業からイートインは06:00-23:00 になっていました．でもドライブスルーは24時間と変則的に．
OSM に反映しないとなーと思いつつタグが解らなくて放置していました．OpenStreetMap Wiki を見てみるとそのものズバリの項目が(時間まで一緒!)

- [JA:Key:opening_hours - OpenStreetMap Wiki](http://wiki.openstreetmap.org/wiki/JA:Key:opening_hours)
- [Key:opening_hours - OpenStreetMap Wiki](http://wiki.openstreetmap.org/wiki/Key:opening_hours)

> 
opening_hours=06:00-23:00 open "Dining in" || 00:00-24:00 open "Drive-through"

こういう書き方もあるみたいだけどアプリケーション側での対応がなかなかかも．両方書いとくのが理想でしょうね．

> 
opening_hours:url=http://www.mcdonalds.co.jp/shop/map/map.php?strcode=46006

こちらに入れて試してみるとよく解らず．2種類の時間帯があるけど両方合わせて計算してる感じ?

[opening_hours evaluation tool](http://openingh.openstreetmap.de/evaluation_tool/)

> 
open 00:00 to 06:00, open 06:00 to 23:00, open 23:00 to 24:00

ということでイートインとドライブスルーの時間が違い場合の opening_hours の入力方法が分かったので編集しておきました．後は敷地も広がったので今度側に行った時にはサーベイして反映したいところです．
