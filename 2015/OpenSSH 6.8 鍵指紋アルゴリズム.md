<a href="https://www.flickr.com/photos/119142834@N05/16771103418" title="openssh by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7612/16771103418_d0789f1b03_o.gif" width="578" height="191" alt="openssh"></a>

OpenSSH 6.8/6.8p1 のリリースノートを眺めていて気になる点が．
http://www.openssh.com/txt/release-6.8

```
* Add FingerprintHash option to ssh(1) and sshd(8), and equivalent
   command-line flags to the other tools to control algorithm used
   for key fingerprints. The default changes from MD5 to SHA256 and
   format from hex to base64.
 Fingerprints now have the hash algorithm prepended. An example of
   the new format: SHA256:mVPwvezndPv/ARoIadVY98vAC0g+P/5633yTC4d/wXE
   Please note that visual host keys will also be different.
```
鍵指紋の規定アルゴリズムがMD5 からSHA256 に変わり，表示形式もhex からbase64 になった，visual host key も変わる．ということで確認してみました．

従来のコマンドでの鍵指紋表示．MD5/hex が使われる
```sh
% ssh-keygen -l -v -f /etc/ssh/ssh_host_ecdsa_key
256 e8:d0:53:e7:34:59:e9:77:3a:e7:8d:8a:a9:f6:91:84 /etc/ssh/ssh_host_ecdsa_key.pub (ECDSA)
+---[ECDSA 256]---+
|            ..   |
|           o.    |
|        . =.     |
|     . o = .. . .|
|    . + E o  . o |
|     o . . .  o .|
|      .   o    =.|
|        .  +  . o|
|       ..o+ ..   |
+-----------------+
```

OpenSSH 6.8/6.8p1 のコマンドでの鍵指紋表示．SHA256/base64が使われて鍵指紋の頭にSHA256が付いたりvisual host key  の見た目も変わる．
```sh
% /home/mk/usr/local/openssh-6.8p1/bin/ssh-keygen -l -v -f /etc/ssh/ssh_host_ecdsa_key
256 SHA256:pDZReijOXeDXAE0IgYb5E+DHgbyvClEllKCs499RI54 root@x220(ECDSA)
+---[ECDSA 256]---+
|+*+++oo=+        |
|*o*o...=.o       |
|.=o+. = = .      |
|.o+o o B         |
|+ ..o.=oS        |
|.o ...+..        |
|...  E           |
|... . .          |
|o  . .           |
+----[SHA256]-----+
```


しかし，`-E` option が提供されるようになってアルゴリズムを指定できるのでこれでMD5/hex で鍵指紋の確認が可能．
※ssh-keygen --help より
 
       ssh-keygen -l [-v] [-E fingerprint_hash] [-f input_keyfile]

※man より

     -E fingerprint_hash
             Specifies the hash algorithm used when displaying key fingerprints.  Valid options are: “md5” and “sha256”.  The default is “sha256”.


OpenSSH 6.8/6.8p1 のコマンドでMD5 を指定．MD5/hex で鍵指紋が表示される．ただ，アルゴリズムの`MD5` が鍵指紋の頭につくし後ろにcomment も付くのでdiff とかを使うと差異が出る．visual host key も下に`[MD5]`がつくので同様．
```
% /home/mk/usr/local/openssh-6.8p1/bin/ssh-keygen -l -v -E md5 -f /etc/ssh/ssh_host_ecdsa_key
256 MD5:e8:d0:53:e7:34:59:e9:77:3a:e7:8d:8a:a9:f6:91:84 root@x220(ECDSA)
+---[ECDSA 256]---+
|            ..   |
|           o.    |
|        . =.     |
|     . o = .. . .|
|    . + E o  . o |
|     o . . .  o .|
|      .   o    =.|
|        .  +  . o|
|       ..o+ ..   |
+------[MD5]------+
% ssh-keygen -l -v -f /etc/ssh/ssh_host_ecdsa_key                                     ```

てことで暫くはMD5/SHA256の2種類の鍵指紋を提供したほうが良さそうですね．
