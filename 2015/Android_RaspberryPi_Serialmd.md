ふと，手元のUSB-TTLアダプタ(Prolific PL2303)とmicroUSBのホストケーブルでAndroidに繋げば普通に使えるのではと思って試してみました．

Raspberry Pi 側の接続．
赤の5Vは接続しなくても動きますが，接続すると少し給電されます．  
<a href="https://www.flickr.com/photos/119142834@N05/15889161283" title="IMG_20150212_163124 by Kenichiro MATOHARA, on Flickr"><img src="https://farm9.staticflickr.com/8603/15889161283_f46e4b27cb.jpg" width="500" height="375" alt="IMG_20150212_163124"></a>

こんな感じに．
<a href="https://www.flickr.com/photos/119142834@N05/16323243869" title="IMG_20150212_204416.jpg by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7456/16323243869_c6830ceb88.jpg" width="375" height="500" alt="IMG_20150212_204416.jpg"></a>

Android側のアプリは10種類ほど試しましたが今回利用したProlific PL2303では以下の3つでデバイスを認識しました．Playストアを見た感じではFTDI対応のものが一番多そうでした．
- [USB Serial Terminal Lite - Google Play の Android アプリ](https://play.google.com/store/apps/details?id=com.oneman.freeusbtools)
コマンドを発行した後そのままテキストが残るので次のコマンドを発行するには文字列を消さないといけないとかエスケープシーケンスを認識しないとかちょっと使いづらいです．
- [Prolific PL2303 USB-UART - Google Play の Android アプリ](https://play.google.com/store/apps/details?id=tw.com.prolific.app.pl2303terminal)
このアプリはChipメーカー純正のアプリ．しかし送信はできるが受信がうまく行っていない．
- [Slick USB 2 Serial Demo - Google Play の Android アプリ](https://play.google.com/apps)
デバイスは認識するが，送受信ウィンドウがスマホでは表示されなくて利用できない．恐らくタブレットならOK

という感じで実際利用できているのはUSB Serial Terminal Liteだけでした．

設定はBitrateの変更と文字コードの変更を行いました．
<a href="https://www.flickr.com/photos/119142834@N05/16483326536" title="Screenshot_2015-02-12-20-39-28 by Kenichiro MATOHARA, on Flickr"><img src="https://farm9.staticflickr.com/8674/16483326536_0cebcbd11e.jpg" width="281" height="500" alt="Screenshot_2015-02-12-20-39-28"></a>

デバイスの認識状態はこんな感じ
<a href="https://www.flickr.com/photos/119142834@N05/16507597421" title="Screenshot_2015-02-12-20-24-12 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7378/16507597421_00e4452288.jpg" width="281" height="500" alt="Screenshot_2015-02-12-20-24-12"></a><a href="https://www.flickr.com/photos/119142834@N05/15886757474" title="Screenshot_2015-02-12-20-24-19 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7434/15886757474_468894136b.jpg" width="281" height="500" alt="Screenshot_2015-02-12-20-24-19"></a>

そしてこんな感じで使えています
<a href="https://www.flickr.com/photos/119142834@N05/16507596951" title="Screenshot_2015-02-12-20-22-35 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7332/16507596951_670678dd7e.jpg" width="281" height="500" alt="Screenshot_2015-02-12-20-22-35"></a>

使い勝手はあまり良くないですが，出先とかでちょっとコマンドを打ちたいときに良さそうです．


<iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00T356SFO" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe><iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00OQYIB1G" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe><iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=4798137316" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>
