Raspberry Pi 2 model B を入手しました．
注文したのはRSオンライン( http://jp.rs-online.com/web/ )で 2015/02/02, 月曜日, 19:48
受け取りは 2/6 でした．
丁度大分に向かうので受け取れるか微妙だったので営業所止にしてもらい受け取ってから大分に向かいました．
<a href="https://www.flickr.com/photos/119142834@N05/16283106250" title="IMG_20150206_194411 by Kenichiro MATOHARA, on Flickr"><img src="https://farm9.staticflickr.com/8592/16283106250_554f4d79b8_n.jpg" width="320" height="240" alt="IMG_20150206_194411"></a><a href="https://www.flickr.com/photos/119142834@N05/16470448945" title="IMG_20150206_194419 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7391/16470448945_151554c437_n.jpg" width="320" height="240" alt="IMG_20150206_194419"></a>
途中のバスの待ち時間に写真を撮ってみました．
<a href="https://www.flickr.com/photos/119142834@N05/16466632802" title="IMGP0250 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7384/16466632802_ca545c9608_n.jpg" width="212" height="320" alt="IMGP0250"></a>
箱がカラフルに!
<a href="https://www.flickr.com/photos/119142834@N05/16279913168" title="IMGP0251 by Kenichiro MATOHARA, on Flickr"><img src="https://farm9.staticflickr.com/8666/16279913168_ce463056d8.jpg" width="500" height="331" alt="IMGP0251"></a>
<a href="https://www.flickr.com/photos/119142834@N05/16284152859" title="IMGP0257 by Kenichiro MATOHARA, on Flickr"><img src="https://farm9.staticflickr.com/8592/16284152859_6757bf9850_n.jpg" width="320" height="212" alt="IMGP0257"></a><a href="https://www.flickr.com/photos/119142834@N05/15845130134" title="IMGP0252 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7392/15845130134_8a855fae19_n.jpg" width="320" height="212" alt="IMGP0252"></a>
中身はぱっと見 Raspberry Pi model B+ と見分けが付かない感じ．
<a href="https://www.flickr.com/photos/119142834@N05/15845163264" title="IMGP0253 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7339/15845163264_07c3c97329_n.jpg" width="320" height="212" alt="IMGP0253"></a>
RAM のChip が別れてウラ面に張り付いてました．
<a href="https://www.flickr.com/photos/119142834@N05/16467700775" title="IMGP0254 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7316/16467700775_0b8048756a.jpg" width="500" height="331" alt="IMGP0254"></a>
<a href="https://www.flickr.com/photos/119142834@N05/16280034818" title="IMGP0255 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7452/16280034818_ee95f27399_n.jpg" width="212" height="320" alt="IMGP0255"></a><a href="https://www.flickr.com/photos/119142834@N05/16281519669" title="IMGP0256 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7361/16281519669_592d6de1af_n.jpg" width="212" height="320" alt="IMGP0256"></a>
大分で起動してみると４Coreになったので Raspberry が4個!
<a href="https://www.flickr.com/photos/119142834@N05/16282812768" title="IMG_20150207_092908 by Kenichiro MATOHARA, on Flickr"><img src="https://farm9.staticflickr.com/8563/16282812768_b71561a5c8.jpg" width="500" height="375" alt="IMG_20150207_092908"></a>
未だ出先なので家に帰ったら色々触ってみるつもりです :)

追記)
Element14 扱いだと箱も説明書も違うみたい．特に説明書．
- [Raspberry Pi 2をゲットしたぞー！ - あっきぃ日誌](http://akkiesoft.hatenablog.jp/entry/20150208/1423376494)


<iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00T356SFO" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe> <iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=4822224953" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe> <iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=4798137316" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>


