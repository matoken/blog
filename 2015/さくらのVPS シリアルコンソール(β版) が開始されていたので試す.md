さくらのVPS ののサーバ設定を変えてtiarra や lingrircgw とかのの自動起動確認のために再起動しようと思い，再起動を見守るためにコンソールを立ち上げようとしたところ新しい項目が増えていました．

<a data-flickr-embed="true" href="https://www.flickr.com/photos/119142834@N05/20002016299/in/dateposted-public/" title="Screenshot from 2015-08-01 02-25-08"><img src="https://farm1.staticflickr.com/310/20002016299_7b0bbf9a84_o.png" width="266" height="132" alt="Screenshot from 2015-08-01 02-25-08"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

**シリアルコンソール(β版)** がそれです．
確認すると 7/23 ににリリースされたようです．

- [さくらのVPS 「シリアルコンソール」機能のベータ版提供開始のお知らせ | さくらインターネット](http://www.sakura.ad.jp/news/sakurainfo/newsentry.php?id=1086)
- [シリアルコンソール｜さくらインターネット公式サポートサイト](https://help.sakura.ad.jp/app/answers/detail/a_id/2498)

早速設定してみます．設定内容は自分メモを見ながら以下のような感じでで設定しましたちなみに，少なくともDebian Squeese ~ Jessie まで同じ設定でいけてます．

`/etc/inittab` の
```
#T0:23:respawn:/sbin/getty -L ttyS0 9600 vt100
```

の部分を以下の様に変更.

```
T0:23:respawn:/sbin/getty -L ttyS0 115200 vt100
```

`/etc/default/grub` の以下の辺りを設定
```
GRUB_CMDLINE_LINUX_DEFAULT="console=tty0 console=ttyS0,115200n8"
GRUB_TERMINAL=serial
GRUB_SERIAL_COMMAND="serial --speed=115200 --unit=0"
```

GRUB に反映
```
$ sudo update-grub
```

再起動して動作確認﻿．
これでGRUB の画面からシリアルコンソールに表示されるようになりました．

<a data-flickr-embed="true" href="https://www.flickr.com/photos/119142834@N05/20000555148/in/dateposted-public/" title="Screenshot from 2015-08-01 03-05-28"><img src="https://farm1.staticflickr.com/401/20000555148_6c4db15cab.jpg" width="500" height="342" alt="Screenshot from 2015-08-01 03-05-28"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<a data-flickr-embed="true" href="https://www.flickr.com/photos/119142834@N05/20180479842/in/dateposted-public/" title="Screenshot from 2015-08-01 02-50-34"><img src="https://farm1.staticflickr.com/272/20180479842_0a584a4588.jpg" width="500" height="392" alt="Screenshot from 2015-08-01 02-50-34"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<a data-flickr-embed="true" href="https://www.flickr.com/photos/119142834@N05/20000934008/in/dateposted-public/" title="Screenshot from 2015-08-01 13-33-25"><img src="https://farm1.staticflickr.com/528/20000934008_8677ffe670.jpg" width="500" height="389" alt="Screenshot from 2015-08-01 13-33-25"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<a data-flickr-embed="true" href="https://www.flickr.com/photos/119142834@N05/20194661891/in/dateposted-public/" title="Screenshot from 2015-08-01 13-32-17"><img src="https://farm4.staticflickr.com/3830/20194661891_00bae4c872.jpg" width="500" height="390" alt="Screenshot from 2015-08-01 13-32-17"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

日本語入力は下のテキスト送信からとサポートサイトには書かれていますが，普通に uim-fep とかでも入力できました．
この辺りは以下のページを参考にしてみてください．
- [Linux/Console - matoken's wiki.](http://hpv.cc/~maty/pukiwiki1/index.php?Linux%2FConsole)

```
$ sudo apt install uim-fep uim-mozc
$ cat << __EOF__ > ~/.uim
(define-key generic-on-key? '("<Control>\\" "<Shift> " "<Control> "))
(define-key generic-off-key? '("<Control>\\" "<Shift> " "<Control> "))
$ uim-mozc -u mozc
```

<a data-flickr-embed="true" href="https://www.flickr.com/photos/119142834@N05/20189018795/in/dateposted-public/" title="Screenshot from 2015-08-01 13-34-11"><img src="https://farm1.staticflickr.com/259/20189018795_8c43b99b28.jpg" width="500" height="371" alt="Screenshot from 2015-08-01 13-34-11"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>
<a data-flickr-embed="true" href="https://www.flickr.com/photos/119142834@N05/20002395289/in/dateposted-public/" title="Screenshot from 2015-08-01 13-37-51"><img src="https://farm1.staticflickr.com/434/20002395289_cecde0895d.jpg" width="500" height="341" alt="Screenshot from 2015-08-01 13-37-51"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

これまでは VNCコンソール(HTML5版)を利用していましたがこれからはシリアルでの利用が多くなりそうです．コピペが出来るのが大きい!後はこのシリアルが ssh 経由で利用できたりしたらまた色々使いやすくなりそうな気もします．(セキュリティは注意が必要ですが)

そうそう，シリアルを切断しただけじゃログアウトされないので最後にちゃんとログアウトしておきましょう．
```
user@hostname:~$ exit
logout

Debian GNU/Linux 8 hostname ttyS0

hostname login:  
```

- [さくらのVPS ののコンソールにシリアルが! -  "さくらのVPS 「シリアルコンソール」機能のベータ版提供開始のお知らせ | さくらインターネット"…](https://plus.google.com/u/0/+KenichiroMATOHARA/posts/Zv9eUpVE3GN)
- [さくらのVPS の Debian jessie をSerial に対応させた．Grub から見える．この辺りの設定は変わりませんね．少なくともSqueeze…](https://plus.google.com/u/0/+KenichiroMATOHARA/posts/89vEQsH9r4M)
- [old stable な squeeze-lts のサーバも Serial 設定した これは今月中に解約したい﻿](https://plus.google.com/u/0/+KenichiroMATOHARA/posts/Q8j58eP4Cp9)
