<a data-flickr-embed="true"  href="https://www.flickr.com/photos/matoken/20676912066/in/dateposted/" title="IMGP0036"><img src="https://farm1.staticflickr.com/698/20676912066_13c58c0826.jpg" width="500" height="331" alt="IMGP0036"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

スイス生まれのセキュアで Linux Client もあるオンラインストレージサービスの Wuala というものが会ったのですが，そちらからメールが．なんだろうと確認するとサービス終了とのこと．

- [WUALA SHUTDOWN NOTICE – Wuala Support](https://support.wuala.com/2015/08/wuala-shutdown-notice/)

```
17 August 2015	No further renewals or purchase of storage
30 September 2015	Wuala service will transition to read-only
15 November 2015	Wuala service terminates and all data stored in the Wuala cloud will be deleted
```

- 08/17 有料アカウントの購入更新停止
- 09/30 読み取り専用に
- 11/15 サービス終了


残念．
似たようなものだと AeroFS(ローカルP2PでAeroFS自体には保存されない) / SpiderOrk(暗号化後転送) くらいですかね．自分のサーバや回線が用意できるなら ownCloud もいい感じです．私は最近 ownCloud メインになっています．

- [Corporate File Sharing and Collaboration | AeroFS](https://www.aerofs.com/)
- [Home Page(SpiderOrk)](https://spideroak.com/)
- [ownCloud.org](https://owncloud.org/)
- [ownCloud + andoroid + picasa + flickr連携](http://www.slideshare.net/matoken/owncloud-andoroid-picasa-flickr)
