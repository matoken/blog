Xperia Pro(MK16a)にCM12を入れました．ちょこちょこ設定をしてみます．

- [Xperia Pro(MK16a)をBootloader Unlock してみた | matoken's meme](http://matoken.org/blog/blog/2015/03/24/xperia-pro_mk16a_bootloader-unlock/)
- [XperiaPro(MK16a)にLolipopベースのCyanogenMod12を導入してみる | matoken's meme](http://matoken.org/blog/blog/2015/03/27/xperiapro_mk16a_cyanogenmod12/)

##Bluetoothテザリングを試す
先ずはやりたかったBluetoothテザリングの設定などをしてみます．
といってもメニューから辿ってOnにするだけです．この設定は再起動してもそのままのようです．
<a href="https://www.flickr.com/photos/119142834@N05/16755164750" title="Screenshot_2015-03-27-13-54-20 by Kenichiro MATOHARA, on Flickr"><img src="https://farm9.staticflickr.com/8700/16755164750_64691f5d9a_n.jpg" width="180" height="320" alt="Screenshot_2015-03-27-13-54-20"></a><a href="https://www.flickr.com/photos/119142834@N05/16320208334" title="Screenshot_2015-03-27-13-54-36 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7596/16320208334_f56c325fe7_n.jpg" width="180" height="320" alt="Screenshot_2015-03-27-13-54-36"></a><a href="https://www.flickr.com/photos/119142834@N05/16941642791" title="Screenshot_2015-03-27-13-54-52 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7641/16941642791_6f22ceda0f_n.jpg" width="180" height="320" alt="Screenshot_2015-03-27-13-54-52"></a>

後は適当な機器とペアリングをすれば接続できます．同時接続も可能で3台までは確認しました．同時にRazikoをBluetoothヘッドホンで聞いても大丈夫．
<a href="https://www.flickr.com/photos/119142834@N05/16941283072" title="Screenshot_2015-03-27-13-59-43 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7603/16941283072_7566567a9f_n.jpg" width="180" height="320" alt="Screenshot_2015-03-27-13-59-43"></a>

このテザリング機能はなかなか良く出来ていて，XperiaProが3Gに接続されている場合はもちろん，Wi-Fiに接続されている場合はそちらにルーティングを行います．これはなかなか便利．
公衆Wi-Fiサービスなどで1台しか接続できない場合もありますがそういう時も大丈夫ですね．

暫く使ってみていますが，途中で切れてしまうこともなく安定しています．
＃手持ちのAUの国内端末すぐ通信できなくなるので見習って欲しい…．

##root権を使えるようにする

adb shellからsuでrootになれず．SuperSUも使えなくてあれ?と思ったのですがCM12ではちょっと方法が変わっているようです．開発者向けオプションの中に以下のような設定ができていました．とりあえずADB shellで使いたいので「ADBのみ」の設定に
<a href="https://www.flickr.com/photos/119142834@N05/16941303801" title="Screenshot_2015-03-23-02-15-40 (2) by Kenichiro MATOHARA, on Flickr"><img src="https://farm9.staticflickr.com/8738/16941303801_82a4acbb4c_n.jpg" width="180" height="320" alt="Screenshot_2015-03-23-02-15-40 (2)"></a>

##カメラのシャッター音を消してみる

ADBでrootが使えるように設定した後，シャッター音を消すためにadb shellでXperiaProの中に入ります．

```bash
% ./adb shell
shell@MK16i:/ $ 
```
suコマンドでrootになります．
```bash
shell@MK16i:/ $ su
root@MK16i:/ # 
```
シャッター音らしきファイルをfindコマンドで探します．それらしいものがありました．
```bash
root@MK16i:/ # find /system -name "*.ogg"|grep -i camera
/system/media/audio/ui/camera_focus.ogg
/system/media/audio/ui/camera_click.ogg
```
該当のファイルシステムを書き込みできるようにremountします．
```bash
root@MK16i:/ # df
Filesystem               Size     Used     Free   Blksize
/dev                   219.4M    36.0K   219.4M   4096
/sys/fs/cgroup         219.4M     0.0K   219.4M   4096
/mnt/asec              219.4M     0.0K   219.4M   4096
/mnt/obb               219.4M     0.0K   219.4M   4096
/system                919.5M   568.7M   350.8M   4096
/cache                   4.0M   804.0K     3.2M   4096
/data                    2.0G   505.2M     1.5G   4096
/mnt/media_rw/sdcard0     5.4G   302.5M     5.1G   32768
/mnt/secure/asec         5.4G   302.5M     5.1G   32768
/storage/sdcard0         5.4G   302.5M     5.1G   32768
root@MK16i:/ # mount -o remount,rw /system
root@MK16i:/ # mount |grep /system
/dev/block/mtd/by-name/system /system yaffs2 rw,seclabel,relatime 0 0
```
該当ファイルをリネームします．
```bash
root@MK16i:/ # cd /system/media/audio/ui/
root@MK16i:/system/media/audio/ui # ls
Dock.ogg
Effect_Tick.ogg
KeypressDelete.ogg
KeypressInvalid.ogg
KeypressReturn.ogg
KeypressSpacebar.ogg
KeypressStandard.ogg
Lock.ogg
LowBattery.ogg
Trusted.ogg
Undock.ogg
Unlock.ogg
VideoRecord.ogg
WirelessChargingStarted.ogg
camera_click.ogg
camera_focus.ogg
root@MK16i:/system/media/audio/ui # mv camera_click.ogg camera_click.ogg-
root@MK16i:/system/media/audio/ui # mv camera_focus.ogg camera_focus.ogg-
root@MK16i:/system/media/audio/ui # mv VideoRecord.ogg VideoRecord.ogg-
```
元のようにファイルシステムを読み込み専用に戻します．
```bash
root@MK16i:/ # mount -o remount,ro /system
root@MK16i:/ # mount |grep /system
/dev/block/mtd/by-name/system /system yaffs2 ro,seclabel,relatime 0 0
```
カメラアプリを試すとシャッター音がしなくなっていました．成功のようです．

##アプリを導入してみる

CM12にしてストレージは結構空きが出来たしSDへの導入も出来るようになりましたが，重いアプリは辛いのであまり入れていません．とりあえずキーボードを活かすためにエディタのJota+，日本語入力のためにFlickWnn，GPSログを取るためにOSMTracker，データやり取りのためにownCloud/Picasa Tool，サイマルラジオのRaziko(radiko.jp,らじる)，念の為?Ingressといったところです．
<a href="https://www.flickr.com/photos/119142834@N05/16942608165" title="Screenshot_2015-03-27-14-08-40 by Kenichiro MATOHARA, on Flickr"><img src="https://farm9.staticflickr.com/8728/16942608165_0b35706fcc_n.jpg" width="180" height="320" alt="Screenshot_2015-03-27-14-08-40"></a>

FlickWnn利用時はShift+Spaceで入力切替，右端の地球マークのキーで大文字小文字切り替えです．起動しは遅いけど起動してしまえば普通に入力できます．

最近メインで使っているSNSのGoogle+も入れてみたのですが，重くて使い物にならないので諦めました．投稿機能だけでいいので軽いものとかがあると良いのですけど…．標準ブラウザでの閲覧も重いです．モバイル版だとそこそこ軽いですが，投稿時に写真添付するのに画面遷移が必要だったりと結構ストレスです．この端末では諦めたほうが良さそう．


しかし，重いとはいえOSが最新になったのは嬉しいです．重いと言っても2.3の頃と同じくらいの重さな感じなので戻す気はしません．今はいろいろと穴が多くてAndroid 4.3以前は使いたくないですしね…．
国内の大抵の端末は2年縛りがあってもそれ以前にサポートやめちゃって穴の開いたまま使うか新しいのに乗り換えるしか無いので辛いです．ずっとサポートされるのが理想ですが，サポートしないならこの端末のように公式でロック解除方法を提供してくれるのが普通になるといいのにと思います(Nexusシリーズなどは公式で初めからロック解除可能)．そうしたら今回のようにOSを入れ替えて使い続けることが出来るかもしれません．そうならないとCMの開発対象になる国際端末やその国内版じゃないとなかなか手を出す気にならないです…．
＃でも保証が無くなるのを理解しないでサポートに文句言うような人が要ると難しいのかもですね．


<iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00CPKFXOQ" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe><iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00F7HGNDY" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe><iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B00O1LEYZG" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>
