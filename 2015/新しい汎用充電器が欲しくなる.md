汎用バッテリーチャージャーはデジモノを沢山持ち歩く場合にそれぞれの充電器を持ち歩かずに済むようになるので便利です．今以下のものを利用しています．
これはAC-DCアダプタ(12V1A)経由で給電するのでACアダプタと本体を持ち運ぶ必要があります．
<iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B004MW50T8" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>

アスキーストアのメールで以下のものを知りました．
[万能充電器 MyCharger Multi U ｜アスキーストア](http://ascii-store.jp/p/4520008241428)  
<a href="https://www.flickr.com/photos/119142834@N05/16444158382" title="4520008241428_02 by Kenichiro MATOHARA, on Flickr"><img src="https://farm8.staticflickr.com/7385/16444158382_2b57b5d2b3_n.jpg" width="320" height="285" alt="4520008241428_02"></a>

大体仕様は同じですが，電源はUSBからとるのでACアダプタの分荷物が減りそうです．USB電源についてはNotePCやAC-USB電源アダプタを持ち歩くのでそちらから．
しかし5pのAC-USB電源アダプタも埋まることがあるのですが…
<!-- Place this tag in your head or just before your close body tag. -->
<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>

<!-- Place this tag where you want the widget to render. -->
<div class="g-post" data-href="https://plus.google.com/103792214056489833385/posts/exud7Cekcde"></div>
とはいえ金欠だし壊れる気配もないし暫くはこれまでのを使い続けるつもりです．

ちなみにスマホの電池だけで良ければよすくてコンパクトなこんなものが．不安定&裏返しに指すと壊れるので取り扱いには少し注意．
<iframe src="http://rcm-fe.amazon-adsystem.com/e/cm?lt1=_blank&bc1=000000&IS2=1&bg1=FFFFFF&fc1=000000&lc1=0000FF&t=matokensmeme-22&o=9&p=8&l=as4&m=amazon&f=ifr&ref=ss_til&asins=B0067DZCVQ" style="width:120px;height:240px;" scrolling="no" marginwidth="0" marginheight="0" frameborder="0"></iframe>

