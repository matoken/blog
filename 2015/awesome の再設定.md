awesome を久々に使おうとしたら設定ファイルが使えなくなっていたので再設定

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/119142834@N05/20153422644/in/dateposted-public/" title="screenFetch-2015-08-22_12-22-58"><img src="https://farm1.staticflickr.com/741/20153422644_9677d80956.jpg" width="500" height="313" alt="screenFetch-2015-08-22_12-22-58"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

Debian stretch 環境の awesome を久々に起動すると awesome のバージョンが新しくなっていて設定ファイルの互換性が失われていました．jessie で使っていた頃入れたので恐らく以前のバージョンは `3.4.15` で，今は `3.5.6` です．
- [Awesome 3.4 to 3.5 - awesome](http://awesome.naquadah.org/wiki/Awesome_3.4_to_3.5)

とりあえず規定値の設定で起動してくるのですが使いにくいので最低限ですが設定をやりなおしました．


## 設定ファイルとテーマファイルの差し替え

既存の古い設定ファイルとテーマファイルを差し替えます．

```
$ cp /etc/xdg/awesome/rc.lua ~/.config/awesome/
$ cp /usr/share/awesome/themes/default/theme.lua ~/.config/awesome/
```

これで規定値と同じになりました．テーマは `/usr/share/awesome/themes` 以下に3種類あるのでお好みで．私は今は明るい `sky( /usr/share/awesome/themes/sky/theme.lua )` を利用しています．

## 設定変更

- 設定ファイル修正( `~/.config/awesome/rc.lua` )

  - テーマファイル変更(3.4と同じ)
```
-beautiful.init("/usr/share/awesome/themes/default/theme.lua")
+beautiful.init("~/.config/awesome/theme.lua")
```

  - 端末アプリ変更(3.4と同じ)
```
-terminal = "x-terminal-emulator"
+terminal = "mate-terminal"
```

  - スクリーンショット設定(3.5)
以前行っていた以下の設定ではうまく行かなかった．
[Linux/WindowManager/awesome - matoken's wiki.](http://hpv.cc/~maty/pukiwiki1/index.php?Linux%2FWindowManager%2Fawesome#kb590d8a)
以下のファイルを用意して，
```
% cat ~/script/ss-root.sh
#!/bin/sh
import -window root ~/Pictures/`date +%Y%m%d_%H:%m:%S-$$.jpg`
% cat ~/script/ss-window.sh
#!/bin/sh
xwininfo |grep '^xwininfo: Window id:' | awk '{print $4}' | xargs -I{} import -window {} ~/Pictures/`date +%Y%m%d_%H:%m:%S-$$.jpg`
% cat ~/script/ss-area.sh
#!/bin/sh
import ~/Pictures/`date +%Y%m%d_%H:%m:%S-$$.jpg`
```
`-- Standard program` の前か後ろ辺りに以下を追加．
```
    -- bind PrintScrn to capture a screen
    awful.key({                   }, "Print", function () awful.util.spawn("/home/mk/script/ss-root.sh",false)   end),
    awful.key({ "Mod1"            }, "Print", function () awful.util.spawn("/home/mk/script/ss-window.sh",false) end),
    awful.key({ "Shift"           }, "Print", function () awful.util.spawn("/home/mk/script/ss-area.sh",false)   end),
```
この script はパスを通してあって，3.4 では script名だけで動いていたのですが 3.5 ではフルパスでないと動かないようでした．

  - 音量ボタン設定(3.5)  
以下のようなミュートとアンミュートを行うscript `audiomute.sh` を用意して，

```
#!/bin/sh

MUTE=`amixer -c 0 get Master|tail -1|cut -d '[' -f 4|sed s/\]//`
if [ $MUTE = "on" ] ; then
  amixer -q -c 0 set Master mute
  echo "mute!"
else
  amixer -q -c 0 set Master unmute
  echo "unmute!"
fi

amixer -c 0 get Master|tail -1|cut -d '[' -f 4|sed s/\]//
```

`-- Standard program` の前か後ろ辺りに以下を追加．
```
    -- Audio Controle
    awful.key({                   }, "XF86AudioMute", function () awful.util.spawn("/home/mk/script/audiomute.sh",false) end),
    awful.key({                   }, "XF86AudioRaiseVolume", function () awful.util.spawn("amixer -c 0 set Master 2dB+",false) end),
    awful.key({                   }, "XF86AudioLowerVolume", function () awful.util.spawn("amixer -c 0 set Master 2dB-",false) end),
```
これもやはりフルパスで script を書かないと動かない．amixer は動いているのでパスがうまく渡っていないのかも．awesome 起動後にパスが設定されるのでうまく動いていないのだと思う．

  - 自動起動アプリ指定(3.4と同じ)
```
-- {{{ Autostart application
-- "Autostart - awesome"
-- http://awesome.naquadah.org/wiki/Autostart
awful.util.spawn_with_shell("~/script/run_once.bash nm-applet")
awful.util.spawn_with_shell("~/script/run_once.bash synergy")
awful.util.spawn_with_shell("~/script/run_once.bash clipit")
awful.util.spawn_with_shell("~/script/run_once.bash owncloud")
awful.util.spawn_with_shell("~/script/run_once.bash xchat")
awful.util.spawn_with_shell("~/script/run_once.bash pidgin")
  :
-- }}}
```
この項目は設定内にないので末尾に追加しました．
`~/script/run_once.bash` は以下のようなもので，アプリケーションの2重起動を回避するために利用しています．
```
#!/bin/bash
pgrep $@ > /dev/null || ($@ &)
```
> [Autostart - awesome](http://awesome.naquadah.org/wiki/Autostart)


- テーマファイル修正( `~/.config/awesome/theme.lua` )

  - 壁紙変更(3.5)
```
-theme.wallpaper             = "/usr/share/awesome/themes/sky/sky-background.png"
+theme.wallpaper             = "~/Pictures/wp/futur_by_takaju-d60mg7y.png"
```


awesome は軽くてサクサクで結構好きです．ただこのように設定は自分で書かないといけないのでとっつきにくいかもしれません．キーバインドも覚えないといけませんし．でも2,3日使っていると主要な物は覚えられます．
ちなみに，3.4 でもあって 3.5 でも治っていない問題として，ブラウザ等でファイルを開くとかなりの確率でアプリが落ちてしまいます．ブラウザでファイルを添付するときにすごく困ります．最近のGoogle+ や Flickr などドラッグ&ドロップで添付できるような場所はいいのですが……．

